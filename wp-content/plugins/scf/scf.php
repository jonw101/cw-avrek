<?php
/**
 * Plugin Name: Avrek Simple Contact Form
 * Description: A Plugin to generate simple contact form for Avrek Bicycle Accident Reporting
 * Version: 100.0
 * License: GPL2
 */

function scf_html() { ?>

	<div id="b_contact-form" class="b_contact-form form-style simple-contact-form">
	<span class="req">* Required</span>

		<h3>Report Your <strong>Bike Crash or Accident</strong></h3>
		<div class="formmessage"><p class="msg_contain"></p></div>
		<form id="scuf" class="noauto">
		<input type="text" placeholder="Full Name *" required="true" name="b-name" class="required input-field" />
		<input type="text" placeholder="Phone or Email *" required="true" name="phone_or_email" class="required PhoneEmailField input-field" />
		<label id="severity">Incident Details </label>
		<input type="text" placeholder="Address or Intersection *" required="true" name="address" style="margin-bottom:10px;" class="input-field" />
		<input type="text" placeholder="Incident Date" name="date" id="datepicker" style="margin-bottom:15px;" class="input-field">
		<div id="severity-buttons-wrapper">
			<label id="severity-button" class="fatal">
				<input type="radio" name="type-of-crash" value="Fatal" />
				<span>Fatal</span>
			</label>

			<label id="severity-button" class="personal-injury">
				<input type="radio" name="type-of-crash" value="Personal Injury" />
					<span>Injury</span>
			</label>

			<label id="severity-button" class="property-damage">
				<input type="radio" name="type-of-crash" value="Property Damage" />
					<span>Damage</span>
			</label>

			<label id="severity-button" class="no-damage">
				<input type="radio" name="type-of-crash" value="No Damage or Injury" />
				<span>None</span>
			</label>
		</div>
		<div id="weather-buttons-wrapper">
			<label id="weather-button" class="sun">
				<input type="radio" name="weather" value="Sunny" />
				<span>Sun</span>
			</label>

			<label id="weather-button" class="cloud">
					<input type="radio" name="weather" value="Clouds"/>
						<span title="Includes fog and pollution">Clouds</span>
			</label>

			<label id="weather-button" class="rain">
					<input type="radio" name="weather" value="Rain" />
						<span>Rain</span>
			</label>

			<label id="weather-button" class="wind">
					<input type="radio" name="weather" value="Wind" />
					<span>Wind</span>
			</label>

			<label id="weather-button" class="snow">
				<input type="radio" name="weather" value="Snow" />
				<span>Snow</span>
			</label>

		</div>
		<div id="lighting-buttons-wrapper">
			<label id="lighting-button" class="day">
				<input type="radio" name="lighting" value="Day Light" />
				<span>Day</span>
			</label>

			<label id="lighting-button" class="night">
					<input type="radio" name="lighting" value="Night Time"/>
						<span>Night</span>
			</label>

			<label id="lighting-button" class="dim">
					<input type="radio" name="lighting" value="Dusk / Dawn" />
						<span>Dusk / Dawn</span>
			</label>

			<label id="lighting-button" class="unknown">
					<input type="radio" name="lighting" value="unknown" />
					<span>Unknown</span>
			</label>
			</div>
			<textarea name="comments" class="textarea-field" placeholder="Additional Details and/or Comments"></textarea>
			<input name="action" type="hidden" value="simple_contact_form" />
			<input id="scfs" class="button" type="submit" name="scfs" value="Submit Your Bike Crash"/>
			 <?php wp_nonce_field( 'scf_html', 'scf_nonce' ); ?>
			<img class="scf-ajax" src="http://i61.tinypic.com/11kguf4.gif" alt="Sending Message">
		</form>

	</div>

<!-- Form Filtering -->

  <div id="map-filtering">
  <h3>View Options</h3>
    <ul id="map-filtering-options">
      <li> Select a Year: 
        <select>
          <option value="all">View All</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
        </select>
       </li>
      <li>
        <input type="checkbox" name="severity" value="no-injury"> No Injury
      </li>
      <li>
        <input type="checkbox" name="severity" value="damage"> Damage
      </li>
      <li>
        <input type="checkbox" name="severity" value="injury"> Injury
      </li>
      <li>
        <input type="checkbox" name="severity" value="fatal" id="check-fatal"> Fatal
      </li>
    </ul>
  </div>

<!-- -->
<?php }


/**
 * scf_enqueue()
 *
 */
function scf_enqueue() {

	//Enqueue jQuery if not already loaded
	wp_enqueue_script('jquery');
	wp_enqueue_script('scfjs', plugins_url( 'scf.js' , __FILE__ ), array('jquery'));

	wp_register_script('google-api', 'http://maps.googleapis.com/maps/api/js?sensor=true', array('jquery'));

	wp_register_script('jquery-ui', 'http://code.jquery.com/ui/1.11.1/jquery-ui.js', array('jquery'));

	wp_register_script('map-params', get_template_directory_uri() . '/js/map_params.js', array('jquery'));

	wp_enqueue_script('google-api');
	wp_enqueue_script('jquery-ui');
	wp_enqueue_script('map-params');

	$localize = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	);
	wp_localize_script('scfjs', 'SCF', $localize);
}

add_action( 'wp_enqueue_scripts', 'scf_enqueue' );

/**
 * scf_ajax_simple_contact_form()
 *
 */
function scf_ajax_simple_contact_form() {
	//	Nonce Security and Spam Check
	if ( isset( $_POST['scf_nonce'] ) && wp_verify_nonce( $_POST['scf_nonce'], 'scf_html' ) ) {

		//----POST Vars
		$name = sanitize_text_field($_POST['b-name']);
		$phone_or_email = sanitize_text_field($_POST['phone_or_email']);
		$address = $_POST['address'];
		$date = sanitize_text_field($_POST['date']);
		$type_of_crash = sanitize_text_field($_POST['type-of-crash']);
		$weather = sanitize_text_field($_POST['weather']);
		$lighting = sanitize_text_field($_POST['lighting']);
		$comments = wp_kses_data($_POST['comments']);
		$urlencode = 'urlencode';

		$details_url = "https://maps.googleapis.com/maps/api/geocode/json?address={$urlencode($address)}&key=AIzaSyAcZQFV70znAglgPed7sdQaQe6qv49n2Xs";

		# Call the REST API
		$data = wp_remote_get( $details_url, array('timeout' => 10 ) );

		if ( is_wp_error( $data ) ) {
			return new WP_Error( 'error', __( $data->get_error_message() ) );
		}

		# we have to get the body from the REST GET API
		$response = json_decode(wp_remote_retrieve_body( $data ));

		// check if we do have this index
		if(isset($response->results[0]->geometry)) {
			$geometry = $response->results[0]->geometry;
		}

		// check if we do have this index
		if(isset($geometry->location->lat) && isset($geometry->location->lng)) {
			$longitude = $geometry->location->lng;
			$latitude = $geometry->location->lat;
		}

		//----Save to Custom Post Bicycle Accident
		$b_post = array(
			'post_name'      => $address,     // The name (slug) for your post
			'post_title'     => sanitize_title($address),     // The title of your post.
			'post_status'    => 'publish',  // Default 'draft'.
			'post_type'      => 'bike_accidents',    // Default 'post'.
			//		  'post_author'    => $user_ID,   // The user ID number of the author. Default is the current user ID.
			'ping_status'    => 'closed',   // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'.
			'comment_status' => 'closed'   // Default is the option 'default_comment_status', or 'closed'.
		);
		$insert_post_id = wp_insert_post( $b_post, $wp_error );

		//add meta
		if ($insert_post_id) {
			update_post_meta($insert_post_id, "b_fullname", $name);
			update_post_meta($insert_post_id, "b_phone_or_email", $phone_or_email);

			update_post_meta($insert_post_id, 'b_date', $date);
			update_post_meta($insert_post_id, 'b_address', $address);
			update_post_meta($insert_post_id, 'b_coordinates_lat', $latitude);
			update_post_meta($insert_post_id, 'b_coordinates_lon', $longitude);
			update_post_meta($insert_post_id, 'b_severity', $type_of_crash);
			update_post_meta($insert_post_id, 'b_weather', $weather);
			update_post_meta($insert_post_id, 'b_lighting', $lighting);
			update_post_meta($insert_post_id, 'b_comments', $comments);

		} //end if

		echo json_encode(array(array(
			$date,
			$latitude,
			$longitude,
			$address,
			$type_of_crash,
			$weather,
			$lighting,
			$comments
		)));

		//----MAIL
		//	  EMAIL SETTINGS
		$headers[] = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
		$headers[] = 'Content-type: text/html' . "\r\n"; //Enables HTML ContentType. Remove it for Plain Text Messages
		$headers[] = 'From: Avrek Law - Bicycle Accident Reporting' . "\r\n";


		$to = get_option( 'admin_email' );
		//$to = 'nickeleus@gmail.com';
		$subject = 'Avrek Law - New Bicycle Accident Report';
		$message = 'Avrek Manager,<br>
			<br>
			New Bicycle Accident Report:<br>
			<br>
			NAME: ' .$name .'<br>
			EMAIL OR PHONE: '.$phone_or_email .'<br>
			ADDRESS OF ACCIDENT: '.$address .'<br>
			DATE: '.$date .'<br>
			CRASH TYPE:'.$type_of_crash .'<br>
			CRASH WEATHER: '.$weather .'<br>
			CRASH LIGHTING: '.$lighting .'<br>
			EXTRA COMMENTS: '.$comments;

		wp_mail( $to, $subject, $message, $headers );
	}
	die(); // Important
}

add_action( 'wp_ajax_simple_contact_form', 'scf_ajax_simple_contact_form' );
add_action( 'wp_ajax_nopriv_simple_contact_form', 'scf_ajax_simple_contact_form' );
?>
