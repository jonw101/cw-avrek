(function($) {
	$( document ).ready(function() {
		$( ".scf-ajax").hide();
		$(".formmessage p").hide();
	});

	$( "#scuf" ).live('submit', function(event) {
		$( ".scf-ajax").show();
		$.post( SCF.ajaxurl, $("#scuf :input").serialize(), function(data) {
			window.updateMap(data);
		}, 'json')
		.done(function() {
			$( ".scf-ajax").hide();
			$("#scuf").slideUp();
			$(".formmessage p").show().html('<span class="f-success">Thank you for reporting this accident.</span><br /><br /><a href="'+ homeUrl +'/bicycle-accident-reporting/"><span class="f-success">[ Click here to report another accident ]</span></a>');
			$("#scuf  input, #scuf textarea").val('');
			$("input[name=type-of-crash], input[name=weather], input[name=lighting]").prop('checked', false);
			$("#scuf  input[name=scfs]").val('Submit Your Bike Crash');
		})

		.fail(function() {
			$( ".scf-ajax").hide();
			$(".formmessage p").html('<span class="f-error">Oops, something went wrong.</span><br /><br /><a href="'+ homeUrl +'/bicycle-accident-reporting/">[ Click here to Try Again. ]</a>');
		});
		event.preventDefault();
	});
})(jQuery);
