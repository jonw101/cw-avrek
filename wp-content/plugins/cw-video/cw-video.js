
	/*
		--- Document Ready ---
	*/
	
	jQuery(document).ready(function() {
		//jQuery('.cw_video_open_popup').live('click', cwVideoOpenVideo);
		jQuery('.cw_video_open_popup').live('click', cwVideoOpenVideoCustom);
		jQuery('.cw_video_open_popup_customized').live('click', cwVideoOpenVideoCustom);
	});
	
	/*
		--- Video Popup ---
	*/
	function cwVideoOpenVideoCustom() {
		var viideo_type = jQuery(this).attr("video_type");
		var video_id = jQuery(this).attr("video_id");
		var video_height = jQuery(this).attr("video_height");
		var video_width = jQuery(this).attr("video_width");
		video_showinfo = jQuery(this).attr("video_showinfo");
		video_rel = jQuery(this).attr("video_rel");
		video_autoplay = jQuery(this).attr("video_autoplay");
		var is_wistia = false;
		if(viideo_type == "youtube") {
		var video_html = '<iframe class="cw_video_single" src="http://www.youtube.com/embed/' + video_id + '?rel=' + video_rel + '&showinfo=' + video_showinfo + '&autoplay=' + video_autoplay + '" frameborder="0" allowfullscreen="allowfullscreen" width="' + video_width + '" height="' + video_height + '"></iframe>';
		} else if(viideo_type == "vimeo") {
			var video_html = '<iframe src="http://player.vimeo.com/video/' + video_id + '?title=' + video_showinfo + '&byline=' + video_showinfo + '&autoplay=' + video_autoplay + '&portrait=0&color=333" width="' + video_width + '" height="' + video_height + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		} else {
			is_wistia = true;
			var video_html = '<iframe id="my_iframe" src="http://fast.wistia.net/embed/iframe/' + video_id + '" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="' + video_width + '" height="' + video_height + '"></iframe>';
		}
		jQuery.fancybox(
			{
				'padding':			 0,
				'autoScale':		false,
				'transitionIn':		'elastic',
				'transitionOut':	'elastic',
				'titleShow':		false,
				'overlayOpacity':	0.7,
				'overlayColor':		'#000',
				'scrolling'     : 'no',
				'content': video_html
			}
		);
		return false;
	}
	function cwVideoOpenVideo() {
		jQuery.fancybox(
			{
				'padding':			 0,
				'autoScale':		false,
				'transitionIn':		'elastic',
				'transitionOut':	'elastic',
				'titleShow':		false,
				'overlayOpacity':	0.7,
				'overlayColor':		'#000',
				'width':			640,
				'height':			480,
				'href':				this.href = this.href.replace(new RegExp("watch\\?v=", "i"), 'v/') + '?autoplay=1&rel=0&showinfo=0&modestbranding=1&autohide=1',
				'type':				'swf',
				'swf':				{
										'allowfullscreen':	'true',
										'wmode': 			'transparent'
									}
			}
		);
		return false;
	}