		<aside class="sidebar right">
		
			<div class="padding-box">
			
				<div>
					<h3>Who will receive this information?</h3>
					<?php if(is_page(256)) { ?>
					<img src="/wp-content/themes/avrek/images/who-receives-info-calculator.jpg" alt="Who receives this information?" />
					<? } else { ?>
					<img src="/wp-content/themes/avrek/images/who-receives-info-contact.jpg" alt="Who receives this information?" />
					<?php } ?>
					<p>Your information is reviewed by Avrek's dedicated legal team who have represented injured accident victims for more than 30 years. </p>
				</div>
				
				<div>
					<h3>What to Expect</h3>
					<p>A phone call from our staff within 1 hour to set up your initial consultation where we will discuss how we can help you.</p>
				</div>
				
				<div>
					<h3>Your Privacy Matters</h3>
					<p>All of the information you provide will be kept strictly confidential.</p>
				</div>	
			
			</div>
						
			<!-- <div class="side-award">
				<a href="http://www.avvo.com/" target="_blank"><img src="/wp-content/themes/avrek/images/logo-avvo.jpg" alt="AVVO" /></a>
			</div>-->
		
		</aside>