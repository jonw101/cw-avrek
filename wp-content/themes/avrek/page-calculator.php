<?php 
/*
* Template Name: Personal Injury Calculator
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
			<?php the_field('intro'); ?>
			
			<?php get_template_part('includes/calculator-form-pi'); ?>
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>		
			<?php endwhile; endif; ?>			
		
		</div>
		
		<?php get_sidebar('contact'); ?>
		
	</div>
</section>

<script type="text/javascript">

	function getHashValue(key) {
		var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
		return ( matches !== null && matches.length >= 2 ? jQuery.trim(unescape(matches[1])).replace(/\"|\<|\>|\/|\'/g, '') : '' );
	}
	
	jQuery(document).ready(function() {
		var name = getHashValue('name'),
		    phone = getHashValue('phone'),
		    email = getHashValue('email');
	    
		setTimeout(function() {
			if( name != '' ) jQuery('div.calculator form input[name=name]').val(name).valid();
			if( phone != '' ) jQuery('div.calculator form input[name=phone]').val(phone).valid();
			if( email != '' ) jQuery('div.calculator form input[name=email]').val(email).valid();
			jQuery('div.calculator form input[name=phone]').focus();
		}, 500);
	});

	jQuery('#calc-estimate').click(function() {
		var damages = ( parseFloat(jQuery('div.calculator form input[name=income_lost]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('div.calculator form input[name=auto_repair_costs]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('div.calculator form input[name=auto_rental_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
		var special_damages = ( parseFloat(jQuery('div.calculator form input[name=medical_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
		var low_settlement = (2.5 * special_damages) + damages;
		var high_settlement = (3.5 * special_damages) + damages;
			
		jQuery('#calc-value').val('$' + Math.round(low_settlement) + ' - $' + Math.round(high_settlement));
			
		jQuery('#calc-estimate').hide();
		jQuery('#calc-results,#calc-submit').fadeIn();
	});

</script>

<?php get_footer(); ?>