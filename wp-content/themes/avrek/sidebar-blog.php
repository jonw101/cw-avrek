		<aside class="sidebar right">
		
			<div class="visible-desktop">
				<?php get_search_form(); ?>
			</div>
			
			<div class="padding-box">
			
				<div class="archives visible-desktop">
					<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						<option value=""><?php echo esc_attr( __( 'Archives' ) ); ?></option> 
						<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 0 ) ); ?>
					</select>
				</div>
				
				<div class="other-info">
					<h3>Categories</h3>
					<ul>
						<?php wp_list_categories('title_li='); ?>
					</ul>
				</div>
				
				<div class="other-info popular">
					<h3>Popular Articles</h3>
					<ul>
						<?php 
						$popularpost = new WP_Query( array( 'posts_per_page=3&meta_key=wpb_post_views_count&orderby=meta_value_num&order=DESC'  ) );
						while ( $popularpost->have_posts() ) : $popularpost->the_post(); 
						?>						
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endwhile; wp_reset_postdata(); ?>
					</ul>
				</div>				
			
			</div>
			
			<?php get_template_part('includes/contact-form'); ?>
			
			<!--<div class="padding-box">
				<?php get_template_part('includes/attorney-thumbs'); ?>
			</div>-->
		
		</aside>