<?php 
/*
* Template Name: Our Staff
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
			
			<section id="staff">
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/04/Maryam-Parman3.jpg" alt="Maryam Parman" />
						<span class="name">Maryam Parman</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/melody-parman.jpg" alt="Melody Parman" />
						<span class="name">Melody Parman</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/michael-smith.jpg" alt="Michael Smith" />
						<span class="name">Michael Smith</span>
					</div>	
								
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/04/Fernanda-Ortega.jpg" alt="Fernanda Ortega" />
						<span class="name">Fernanda Ortega</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/04/Christina-Jimenez.jpg" alt="Christina Jimenez" />
						<span class="name">Christina Jimenez</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/04/Taylor-Smith.jpg" alt="Taylor Smith" />
						<span class="name">Taylor Smith</span>
					</div>
					
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/esli-frias.jpg" alt="Esli Frias" />
						<span class="name">Esli Frias</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/nicole-loyola.jpg" alt="Nicole Loyola" />
						<span class="name">Nicole Loyola</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/alexander-sanchez.jpg" alt="Alexander Sanchez" />
						<span class="name">Alexander Sanchez</span>
					</div>
										
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/roya-parman.jpg" alt="Roya Parman" />
						<span class="name">Roya Parman</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/hector-romo.jpg" alt="Hector Romo" />
						<span class="name">Hector Romo</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/marty-etcheverry.jpg" alt="Marty Etcheverry" />
						<span class="name">Marty Etcheverry</span>
					</div>
					
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/jay-rios.jpg" alt="Jay Rios" />
						<span class="name">Jay Rios</span>
					</div>
					<div class="column staff-img">
						<img src="/wp-content/uploads/2014/02/lisa-unger.jpg" alt="Lisa Unger" />
						<span class="name">Lisa Unger</span>
					</div>					
															
			</section>		
		
		</div>
		
		<?php get_sidebar('firm'); ?>
		
	</div>
</section>

<?php get_footer(); ?>