		<aside class="sidebar right">
		
			<div class="padding-box">
							
				<?php if(is_single()) {
					$id = $post->ID;
				} elseif(is_page(353)) {
					$id = 230;
				} else {
					$id = 213;
				} ?>
				
				<div class="office-info">
				
					<?php
			          if( function_exists( 'wpseo_local_show_map' ) ) {
			               $params = array(
			                    'echo' => true,
			                    'id' => $id,
			                    'width'     => 320,
			                    'height' => 230,
			                    'zoom' => 11,
			                    'show_route' => false
			               );
			               wpseo_local_show_map( $params );
			          }
					?>	
					
					<?php  
			          if( function_exists( 'wpseo_local_show_address' ) ) {
			               $params = array(
			                    'echo' => true,
			                    'id' => $id,
			                    'show_state' => true,
			                    'show_country' => false,
			                    'show_phone' => true,
			                    'show_fax' => true,
			                    'show_phone_2' => false,
			                    'oneline' => false,
			                    'show_opening_hours' => false
			               );
			               wpseo_local_show_address( $params );
			          }
					?>  
					<a href="https://goo.gl/maps/ZYQer" target="_blank" class="btn-small">Directions to Office</a>	
					
					<div class="office-hours">
						<h3>Office Hours</h3>
						<?php
						  if ( function_exists( 'wpseo_local_show_opening_hours' ) ) {
							  $params = array(
							          'id'          => $id,
							          'hide_closed' => true,
							          'echo'        => true,
							          'comment'     => ''
							     );
							     wpseo_local_show_opening_hours( $params );
						  }
						?> 	
					</div>
					
					<?php get_template_part('includes/btns-social-share'); ?>
					
				</div>												
				
				<?php get_template_part('includes/testimonial'); ?>
			
			</div>
		
		</aside>