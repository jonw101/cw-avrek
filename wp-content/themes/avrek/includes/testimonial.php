						<div class="testimonial">
							<?php if(!is_front_page() && !is_page_template('page-landing.php')) { ?><h3>Great Service With <br /> Avrek Law Firm</h3><?php } ?>
			                <?php
							global $post;
				            $slug = get_post($post)->post_name;
				            $term = term_exists($slug, 'testimonials-category');
				            if ($term !== 0 && $term !== null) {
			                	$the_query = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => 1, 'orderby' => 'rand', 'tax_query' => array(
			                    					array(
			                                             'taxonomy' => 'testimonials-category',
			                                             'field' => 'slug',
			                                             'terms' => $slug
			                                        )
			                ) ) );
			                } else {
				                $the_query = new WP_Query( array( 'post_type' => 'testimonials', 'posts_per_page' => 1, 'orderby' => 'rand' ) );
			                }
			                while ( $the_query->have_posts() ) : $the_query->the_post();
			                ?>												
							<img src="/wp-content/themes/avrek/images/stars-dark-large.png" alt="stars" class="visible-desktop" />
							<img src="/wp-content/themes/avrek/images/stars-dark-small.png" alt="stars" class="visible-phone" />
							<p>"<?php echo excerpt(20); ?>"</p>
							<p class="name">- <strong><?php the_title(); ?></strong></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<?php if(!is_front_page() && !is_page_template('page-landing.php')) { ?><a href="/testimonials/" class="btn-small-wt right">More Client Reviews</a><?php } ?>
						</div>