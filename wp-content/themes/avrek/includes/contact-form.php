<?php
if( is_parent(42) && !is_page(42) ){
	$form_id = 'sb-cmform';
}

?>


			<div id="<?php echo $form_id; ?>" class="contact-form clearB">
				<span class="req">* Required</span>
				<?php if(!is_page(9)) { ?>
				<h3>Free Case Review <strong>Do You Have a Case?</strong></h3>
				<?php } ?>
				<form action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
					<input type="text" value="Full Name *" name="name" class="required" />
					<input type="text" value="Phone or Email *" name="phone_or_email" class="required PhoneEmailField" />
					<textarea name="comments">Comments and Questions</textarea>
					<div style="margin-bottom: 10px;" class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" checked value="I have read the disclaimer">
						<label for="disclaimer">I have read the disclaimer</label>
					</div> 
					<input type="submit" value="Send For a Free Consultation" />
				</form>
			</div>			