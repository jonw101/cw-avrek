<?php
if( is_parent(3655) && !is_page(3655) ){
	$form_id = 'sb-cmform';
}
?>


			<div id="<?php echo $form_id; ?>" class="accident-form contact-form clearB">
				<span class="req">* Required</span>

				<h3>Report Your <strong>Bike Crash or Accident</strong></h3>

				<form id="contact_form" action="#" method="post">
					<input type="text" value="" name="name" class="required" />
					<input type="text" value="Phone or Email *" name="phone_or_email" class="required PhoneEmailField" />
					<label id="severity">Incident Details </label>
					<input type="text" value="Address or Intersection *" name="address" style="margin-bottom:10px;" class="required"/>
					<input type="text" value="Incident Date"id="datepicker" style="margin-bottom:15px;">
				<div id="severity-buttons-wrapper">
					<label id="severity-button" class="fatal">
						<input type="radio" name="type-of-crash" value="Fatal" />
						<span>Fatal</span>
					</label>

						<label id="severity-button" class="personal-injury">
							<input type="radio" name="type-of-crash" value="Personal Injury" />
								<span>Injury</span>
							</label>

						<label id="severity-button" class="property-damage">
							<input type="radio" name="type-of-crash" value="Property Damage" />
								<span>Damage</span>
						</label>

						<label id="severity-button" class="no-damage">
							<input type="radio" name="type-of-crash" value="No Damage or Injury" />
							<span>None</span>
					</label>
				</div>
			<div id="weather-buttons-wrapper">
				<label id="weather-button" class="sun">
					<input type="radio" name="weather" value="Sunny" />
					<span>Sun</span>
				</label>

					<label id="weather-button" class="cloud">
						<input type="radio" name="weather" value="Clouds"/>
							<span title="Includes fog and pollution">Clouds</span>
						</label>

					<label id="weather-button" class="rain">
						<input type="radio" name="weather" value="Rain" />
							<span>Rain</span>
					</label>

					<label id="weather-button" class="wind">
						<input type="radio" name="weather" value="Wind" />
						<span>Wind</span>
				</label>

				<label id="weather-button" class="snow">
					<input type="radio" name="weather" value="Snow" />
					<span>Snow</span>
			</label>

			</div>
			<div id="lighting-buttons-wrapper">
				<label id="lighting-button" class="day">
					<input type="radio" name="lighting" value="Day Light" />
					<span>Day</span>
				</label>

					<label id="lighting-button" class="night">
						<input type="radio" name="lighting" value="Night Time"/>
							<span>Night</span>
						</label>

					<label id="lighting-button" class="dim">
						<input type="radio" name="lighting" value="Dusk / Dawn" />
							<span>Dusk / Dawn</span>
					</label>

					<label id="lighting-button" class="unknown">
						<input type="radio" name="lighting" value="unknown" />
						<span>Unknown</span>
				</label>
			</div>
					<textarea name="comments">Additional Details and/or Comments</textarea>
					<!--<div style="margin-bottom: 10px;" class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" checked value="I have read the disclaimer">
						<label for="disclaimer">I have read the disclaimer</label>
					</div> -->
					<input type="submit" id="submit_btn" value="Submit Your Bike Crash" />
				</form>
			</div>
