			<div class="contact-form calculator clearB">
				<span class="req">* Required</span>
				<h3>Find out if you have a case and what it is worth.</h3>
				<form action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
					<input type="text" value="Full Name *" name="name" class="required" />
					<input type="text" value="Email *" name="email" class="required" />
					<input type="text" value="Phone *" name="phone" class="required PhoneField" />
					<input type="text" value="Medical Costs *" name="medical_costs" class="required" />
					<input type="text" value="Auto Repair Costs *" name="auto_repair_costs" class="required" />
					<input type="text" value="Auto Rental Costs *" name="auto_rental_costs" class="required" />
					<input type="text" value="Income Lost *" name="income_lost" class="required" />
					<div style="position: absolute; bottom: 27px;" class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" checked value="I have read the disclaimer">
						<label for="disclaimer" style="color:#fff;">I have read the disclaimer</label>
					</div>
					<input type="button" id="calc-estimate" value="Estimate My Settlement" />
					<div id="calc-results">
						<p>Your personal injury settlement is valued between</p>
						<input type="text" readonly="readonly" id="calc-value" />
						<p>Due to various other factors that may in affect each individual case and the expertise the attorney, your settlement may be more or less. We encourage you to consult a few law professional to determine the best representation for your case. For a comprehensive evaluation with Avrek, call 1.888.333.5009 or click below for a FREE consultation.</p>
					</div>
					
					<input type="submit" id="calc-submit" value="Send For a Free Consultation" />
				</form>
			</div>			