	        <div id="comments" class="comment-box">
				<div class="comment-form-holder">
					<h3><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></h3>
					<div class="avatar-thumb left">
						<?php echo get_avatar( $comment, 61 ); ?>
					</div>
					<div id="respond-form" class="comment-form left">
						<?php 
						$comments_args = array(
						  'id_form'           => 'commentform',
						  'id_submit'         => '',
						  'title_reply'       => '',
						  'title_reply_to'    => __( 'Leave a Reply to %s' ),
						  'cancel_reply_link' => __( 'Cancel Reply' ),
						  'label_submit'      => __( 'Submit' ),
						
						  'comment_field' =>  '<textarea id="comment" name="comment">Leave a Comment</textarea>',
						
						  'comment_notes_before' => '',
						
						  'comment_notes_after' => '',
						
						  'fields' => apply_filters( 'comment_form_default_fields', array(
						
						    'author' =>
						      '<input type="text" value="Full Name*" id="author" name="author">',
						
						    'email' =>
						      '<input type="text" value="Email*" id="email" name="email">'
						      
						    )
						  ),
						);
						comment_form($comments_args); ?>
					</div>	
				</div>			
				<?php if(have_comments()) { ?>
				<div class="comment-list">
					<ul class="comment-list-cont">
						<?php wp_list_comments('type=comment&avatar_size=61'); ?> 
					</ul>
				</div>
				<?php } ?>
	          </div>