<?php get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1>Error 404: Page not found</h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
			<p>Sorry, the page you are looking for cannot be found.  Try a search below:</p>
			<?php get_search_form(); ?>
		</div>
		
		<?php get_sidebar(); ?>
		
	</div>
</section>

<?php get_footer(); ?>