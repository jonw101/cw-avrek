<?php 
/*
* Template Name: Our Firm
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		<div class="spanfull award-logos" style="margin-top: -30px;">
		
			<img style="width:110px;" src="/wp-content/uploads/2014/03/avvo1.png" alt="logo 1" />
			<img style="width:110px;" src="/wp-content/uploads/2014/03/avvo2.png" alt="logo 2" />
			<img style="width:110px;" src="/wp-content/uploads/2014/03/avvo3.png" alt="logo 3" />
			<img style="width:110px;" src="/wp-content/uploads/2014/03/avvo4.png" alt="logo 4" />
			
			</div>
		
		<?php the_field('intro'); ?>
		
		<!--section id="attorneys" class="our-firm">
				<!--div class="column">
					<h2>Meet Our Attorneys</h2>
					<p>Avrek Law attorneys are seasoned veterans in personal injury law.</p>
					<p>Avrek has successfully litigated personal injury cases involving all types of accidents and liability.</p>
					<img src="/wp-content/themes/avrek/images/logo-caala.jpg" alt="Consumer Attorneys Association of Los Angeles" class="award" />
				</div-->
<!--				<div class="column atty-img">
					<a href="/about-us/maryam-parman/"><img src="/wp-content/uploads/2014/02/maryam-parman.jpg" alt="Maryam Parman" /></a>
					<a href="/about-us/maryam-parman/" class="name">Maryam Parman</a>
				</div>
				<div class="column atty-img">
					<a href="/about-us/melody-parman/"><img src="/wp-content/uploads/2014/02/melody-parman.jpg" alt="Melody Parman" /></a>
					<a href="/about-us/melody-parman/" class="name">Melody Parman</a>
				</div>
				<div class="column atty-img">
					<a href="/about-us/michael-smith/"><img src="/wp-content/uploads/2014/02/michael-smith.jpg" alt="Michael Smith" /></a>
					<a href="/about-us/michael-smith/" class="name">Michael Smith</a>
				</div>
-->
		</section-->		
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<?php the_content(); ?>		
		<?php endwhile; endif; ?>
		
		</div>
		
		<?php get_sidebar('firm'); ?>
		
	</div>
</section>

<?php get_footer(); ?>