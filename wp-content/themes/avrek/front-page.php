<?php get_header(); ?>

<section id="banner">
	<div class="wrapper">
		<div class="slide-holder visible-desktop">
			<div class="slide slide1">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Integrity</strong> <span>A commitment to professionalism and client service</span></h2>
						<p>We help our clients through their difficult time while building a case that will deliver a full and fair settlement.</p>
					</div>
				</div>
			</div>
			<div class="slide slide2">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Passion</strong> <span>We care about our clients and their cases</span></h2>
						<p>Our legal professionals are driven by the desire to succeed for our injury clients. We’ll fight aggressively and stand up for your rights.</p>
					</div>
				</div>
			</div>
			<div class="slide slide3">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Accountability</strong> <span>We devote personal attention to every person we help</span></h2>
						<p>In these difficult times we will keep you informed every step of the way.  You’ll be treated with the respect and compassion you deserve.</p>
					</div>
				</div>
			</div>

	<!-- Additional slide -->
			<div class="slide slide4">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Results</strong> <span>A record of<br>success<br>for our clients</span></h2>
						<p>Since 1998 Avrek Law has obtained more than $100 million in settlements and courtroom verdicts for our accident and injury clients.</p>
					</div>
				</div>
			</div>

		</div>
		<div class="banner-nav visible-desktop"></div>
		<div class="visible-phone">
			<img src="/wp-content/themes/avrek/images/bg-banner-solutions-mobile.jpg" alt="Modern Solutions" />
			<div class="slide-text">
				<h2><span>Utilizing</span> <strong>Modern Solutions</strong> <span>to get you results</span></h2>
				<?php get_template_part('includes/testimonial'); ?>
			</div>
		</div>		
	</div>
</section>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="pa-cols">
	<div class="wrapper">
		<h2>How Were You Injured?</h2>
		<!-- additional -->
		<div class="column">
			<a href="/legal-services/accident-lawyer/car-accident/"><img src="http://www.avrek.com/wp-content/uploads/2014/03/ico-car-accidents.jpg" alt="Car Accidents" />
			Car Accidents</a>
		</div>
		<!-- end -->
		<div class="column">
			<a href="/legal-services/personal-injury-lawyer/burn-injury/"><img src="/wp-content/themes/avrek/images/ico-burn-injuries.jpg" alt="Burn Injuries" />
			Burn Injuries</a>
		</div>
		<div class="column">
			<a href="/legal-services/accident-lawyer/bicycle-accident/"><img src="/wp-content/themes/avrek/images/ico-bike-injuries.jpg" alt="Bike Injuries" />
			Bike Injuries</a>	
		</div>	
		<div class="column">
			<a href="/legal-services/personal-injury-lawyer/dog-bite-injury/"><img src="/wp-content/themes/avrek/images/ico-animal-attacks.jpg" alt="Animal Attacks" />
			Animal Attacks</a>	
		</div>
		<div class="column">
			<a href="/legal-services/medical-malpractice-lawyer/birth-injury/"><img src="/wp-content/themes/avrek/images/ico-birth-injuries.jpg" alt="Birth Injuries" />
			Birth Injuries</a>	
		</div>
		<div class="column">
			<a href="/legal-services/personal-injury-lawyer/brain-injury/"><img src="/wp-content/themes/avrek/images/ico-brain-injuries.jpg" alt="Brain Injuries" />
			Brain Injuries</a>	
		</div>
		<div class="column col-last">
			<a href="/legal-services/personal-injury-lawyer/spinal-injury/"><img src="/wp-content/themes/avrek/images/ico-back-spine-injuries.jpg" alt="Back/Spine Injuries" />
			Back/Spine Injuries</a>	
		</div>
		<a href="/legal-services/" class="btn-services">See Our Legal Services</a>	
	</div>			
</section>

<section id="case-form-mobile" class="visible-phone">
	<div class="wrapper">
		<h3>Personal Injury <span>Calculator</span></h3>
		<p>Just a few small steps to find out if you have a case and what it is worth</p>
		<a href="/legal-services/personal-injury-lawyer/personal-injury-calculator/">Estimate My Settlement</a>
	</div>
</section>

<section id="attorneys">
	<div class="wrapper">
		<div class="column-span2">
			<h1><?php h1_title(); ?></h1>

			<?php the_content()?>

			<a href="/about-us/" class="btn-small right">About Our Firm</a>
		</div>
		
		<div class="column">
			<?php get_template_part('includes/contact-form'); ?>
		</div>


		<div class="spanfull award-logos">
			<img alt="The National Trial Lawyers" src="http://www.thenationaltriallawyers.org/images/NTL-top-40-40-member.png" width="75" height="75">
			<?php
			for($i = 1; $i <= 6; $i++){
				echo '<img src="/wp-content/uploads/2014/03/logo'.$i.'.jpg" alt="logo '.$i.'" />';
			}
			?>
			
		</div>
	</div>
</section>

<section id="case-form" class="visible-desktop">
	<div class="wrapper">
		<div class="step-form">
			
			<h3>Just a few small steps to find out if you have a case and what it is worth</h3>

			<span class="req">* Required</span>
			
			<div class="step-nav"></div>
			
			<form class="step-form-cycle" action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
			
				<div id="form-slide-1" class="step-slide">
					<div class="left">
						<h4>What were your medical costs?</h4>
						<p>Medical expenses are the cost of your medical treatment and how much more cost is incurred for following treatment.</p>
					</div>
					<h4 class="dollar">$</h4>
					<input type="text" value="Amount *" name="medical_costs" class="required" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Next Step" class="next-btn" />
				</div>
				
				<div id="form-slide-2" class="step-slide">
					<h4>What were your auto repair / auto rental costs?</h4>
					<div class="clearfix">
						<div class="left">
							<h4 class="dollar">$</h4>
							<input type="text" value="Auto Repair Costs *" name="auto_repair_costs" class="required" />
						</div>
						<div class="right">
							<h4 class="dollar">$</h4>
							<input type="text" value="Auto Rental Costs *" name="auto_rental_costs" class="required" />
						</div>
					</div>
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Next Step" class="next-btn" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Back" class="prev-btn" />
				</div>
								
				<div id="form-slide-3" class="step-slide">
					<h4>What was your total income lost?</h4>
					<h4 class="dollar">$</h4>
					<input type="text" value="Income Lost *" name="income_lost" class="required" />
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Next Step" class="next-btn" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Back" class="prev-btn" />
					
				</div>
				
				<div id="form-slide-4" class="step-slide">
					<div id="calc-info">
						<h4>Provide us with your name and contact information.</h4>
						<input type="text" value="Name *" name="name" class="name required" />
						<input type="text" value="Email" name="user_email" />
						<input type="text" value="Phone *" name="phone" class="required PhoneField" />
						<div class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" value="I have read the disclaimer" checked />
						<label for="disclaimer">I have read the disclaimer</label>
						</div> 
					</div>
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'estimate');" type="button" value="Estimate My Settlement" id="calc-estimate" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Back" class="estimate-btn prev-btn" />
					<div id="calc-results">
						<div class="clearfix">
							<h4 class="left">Your personal injury settlement is valued between:</h4>
							<input class="right" type="text" readonly="readonly" id="calc-value" />
						</div>
						<div class="clearfix">
							<p class="left">Due to various other factors that may in affect each individual case and the expertise the attorney, your settlement may be more or less. We encourage you to consult a few law professional to determine the best representation for your case. For a comprehensive evaluation with Avrek, call 1.888.333.5009 or click to the right for a FREE consultation.</p>
							<input onclick="ga('send', 'event', 'settlement form', 'click', 'free consult');" class="right" type="submit" value="Send For a Free Consultation" />
						</div>
					</div>					
				</div>
					
			</form>
			
			<script type="text/javascript">
			
				function getHashValue(key) {
					var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
					return ( matches !== null && matches.length >= 2 ? jQuery.trim(unescape(matches[1])).replace(/\"|\<|\>|\/|\'/g, '') : '' );
				}
				
				jQuery(document).ready(function() {
					var name = getHashValue('name'),
					    phone = getHashValue('phone'),
					    email = getHashValue('email');
				    
					setTimeout(function() {
						if( name != '' ) jQuery('.step-form-cycle input[name=name]').val(name).valid();
						if( phone != '' ) jQuery('.step-form-cycle input[name=phone]').val(phone).valid();
						if( email != '' ) jQuery('.step-form-cycle input[name=email]').val(email).valid();
						jQuery('.step-form-cycle input[name=phone]').focus();
					}, 500);
				});
			
				jQuery('#calc-estimate').click(function() {
					var damages = ( parseFloat(jQuery('.step-form-cycle input[name=income_lost]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('.step-form-cycle input[name=auto_repair_costs]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('.step-form-cycle input[name=auto_rental_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
					var special_damages = ( parseFloat(jQuery('.step-form-cycle input[name=medical_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
					var low_settlement = (2.5 * special_damages) + damages;
					var high_settlement = (3.5 * special_damages) + damages;
						
					jQuery('#calc-value').val('$' + Math.round(low_settlement) + ' - $' + Math.round(high_settlement));
						
					jQuery('#calc-info,#calc-estimate,#form-slide-4 .prev-btn').hide();
					jQuery('#calc-results').fadeIn();
				});
			
			</script>			
	
		</div>
	</div>
</section>

<section id="results-blog">
	<div class="wrapper">
			<div class="spanfull award-logos" style="margin-top: -30px;">
						<?php
			for($i = 1; $i <= 5; $i++){
				echo '<img src="/wp-content/uploads/2014/03/avvo'.$i.'.png" alt="logo '.$i.'" />';
			}
			?>

			</div>
	
		<?php get_template_part('includes/case-results'); ?>
		
		<div class="blog-post">
			<h3>From Our Blog</h3>
			<?php
			$the_query = new WP_Query( 'post_type=post&posts_per_page=1' );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<span class="metadata">Posted on <?php the_time('F j, Y'); ?> by <?php the_author(); ?></span>
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="btn-small">Read More</a>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
		
	</div>
</section>

<?php get_footer(); ?>