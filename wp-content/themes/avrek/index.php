<?php get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline" class="blog">
	<div class="wrapper">
		<?php if ( is_day() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'l, F j, Y' ); ?></span></h1>                                  
		<?php elseif ( is_month() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'F Y' ); ?></span></h1>    
		<?php elseif ( is_year() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'Y' ); ?></span></h1>
		<?php elseif ( is_category() ) : ?>
		<h1>Archive: <span><?php single_term_title(); ?></span></h1>    
		<?php elseif ( is_search() ) : ?>
		<h1>Search Results for: <span><?php the_search_query(); ?></span></h1>    
		<?php else : ?>
		<h1>Our Blog</h1>    
		<?php endif; ?> 
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left blog">
		
			<div class="search-archives visible-phone">
				<?php get_search_form(); ?>
				<div class="archives">
					<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						<option value=""><?php echo esc_attr( __( 'Archives' ) ); ?></option> 
						<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 0 ) ); ?>
					</select>
				</div>
			</div>
					
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="blog-post">
				
				<div class="post-header">
					<div class="date left">
						<span><?php the_time('M'); ?></span><span class="blue"><?php the_time('j'); ?></span>
					</div>
					<h2><a href="<?php the_permalink(); ?>"><?php h1_title(); ?></a></h2>
					<span class="metadata">by <span><?php the_author(); ?></span></span>
				</div>
				
				<div class="blog-img left">
					<?php
					$post_object = get_field('video');
					if( $post_object ) {
						// override $post
						$post = $post_object;
						setup_postdata( $post );
						$vid = $post->ID;
						$video_id = get_post_meta($vid, 'cw_video_id', true);
						$video_type = get_post_meta($vid, 'cw_video_type', true);
						$video_image = get_post_meta($vid, 'cw_video_thumbnail', true);
					?>
						<div class="inner-video">
							<a class="cw_video_open_popup_customized" href="javascript:void(0);" video_id="<?php esc_attr_e($video_id); ?>" video_type="<?php echo $video_type; ?>" video_rel="<?php esc_attr_e($rel); ?>" video_showinfo="<?php esc_attr_e($showinfo); ?>" video_autoplay="1">
								<img src="/wp-content/themes/avrek/images/btn-play-video.png" alt="Play Video" class="cw_video_video_btn" />
								<img src="<?php echo $video_image; ?>" alt="Play Video" class="cw_video_img">
							</a>
						</div>
					<?php wp_reset_postdata();
					} elseif ( '' != get_the_post_thumbnail() ) {
						the_post_thumbnail('inner_photo');
					} 
					?> 
				</div>
				
				<?php the_excerpt(); ?>
			
				<a href="<?php the_permalink(); ?>" class="continue">Continue Reading</a>
			
			</div>	
			<?php endwhile; endif; ?>
		
			<div class="blog-pag">
				<?php
				global $wp_query;
				$big = 999999999; // need an unlikely integer
				echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '/page/%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages, 
				'type' => 'list'
				) );
				?>
			</div>		
		
		</div>
		
		<?php get_sidebar('blog'); ?>
		
	</div>
</section>

<?php get_footer(); ?>