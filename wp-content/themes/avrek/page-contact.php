<?php 
/*
* Template Name: Contact
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
			<?php get_template_part('includes/contact-form'); ?>
			
			<div class="address-hours">
				<div class="column ctm-no-swap">
					<?php  
			          if( function_exists( 'wpseo_local_show_address' ) ) {
			               $params = array(
			                    'echo' => true,
			                    'id' => 213,
			                    'show_state' => true,
			                    'show_country' => false,
			                    'show_phone' => true,
			                    'show_fax' => true,
			                    'show_phone_2' => false,
			                    'oneline' => false,
			                    'show_opening_hours' => false
			               );
			               wpseo_local_show_address( $params );
			          }
					?>  
					<a href="https://goo.gl/maps/ZYQer" target="_blank" class="btn-small">Directions to Office</a>
				</div>
				<div class="column">
					<h3>Office Hours</h3>
					<?php
					  if ( function_exists( 'wpseo_local_show_opening_hours' ) ) {
						  $params = array(
						          'id'          => 213,
						          'hide_closed' => true,
						          'echo'        => true,
						          'comment'     => ''
						     );
						     wpseo_local_show_opening_hours( $params );
					  }
					?> 	
				</div>	
			</div>
		
			<?php
	          if( function_exists( 'wpseo_local_show_map' ) ) {
	               $params = array(
	                    'echo' => true,
	                    'id' => 213,
	                    'width'     => 597,
	                    'height' => 230,
	                    'zoom' => 11,
	                    'show_route' => false
	               );
	               wpseo_local_show_map( $params );
	          }
			?>				
		
		</div>
		
		<?php get_sidebar('contact'); ?>
		
	</div>
</section>

<?php get_footer(); ?>