<?php 
/*
* Template Name: Attorney Bio
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
			<div class="attorney-image">
				<?php
				the_post_thumbnail('attorney_photo');
				$ptitle = get_the_title($post->ID);
				$ptitle = explode(" ", $ptitle);
				get_template_part('includes/attorney-form');
				?>			
				<a href="#contact-me" class="popup">Contact <?php echo $ptitle[0]; ?></a>
			</div>
			      
			<?php the_content(); ?>
		
		<?php endwhile; endif; ?>
		
		</div>
		
		<?php get_sidebar('attorney'); ?>
		
	</div>
</section>

<?php get_footer(); ?>