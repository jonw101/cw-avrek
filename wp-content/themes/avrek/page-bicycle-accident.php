<?php 
/*
Template Name: Bicycle Accident Page Template
*/

get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
  <div class="wrapper">
    <h1><?php h1_title(); ?></h1>
  </div>
</section>

<section id="body">
  <div id="map-wrapper">
  <div id="map-canvas"></div>
  </div>
  <?php get_sidebar('bicycle'); ?>
  
  <div class="wrapper">
  
    <div class="content full-width">
    
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              
      <?php the_content(); ?>   
    
    <?php endwhile; endif; ?>
    
    </div>
        
  </div>
</section>

<?php get_footer(); ?>