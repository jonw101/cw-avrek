<?php 
/*
* Template Name: Case Results
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="intro">
			<strong>Avrek attorneys work aggressively for our clients and have obtained over $100,000,000 in settlements and judgments since 1998.</strong>
		</div>
		
		<div class="grid">
			<?php
			$counter = 1;
			$display_count = -1;
			$page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			//$offset = ( $page - 1 ) * $display_count;
			$query_args = array(
			  'post_type'  =>  'case-results',
			  'orderby'    =>  'menu_order',
			  'order'      =>  'ASC',
			  'posts_per_page'     =>  -1,
			  'post_status' => 'publish',

			  'meta_key' => 'amount_in_numbers',
			  'orderby' => 'meta_value_num',
			  'order' => 'DESC',
			);
			$the_query = new WP_Query ( $query_args );
			if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : 
				$third_box = $counter == 3 ? TRUE : FALSE;
			?>
			<?php if( $third_box && $page == 1) { ?>
			<div class="box review">
				<div class="head">
					<h2>Leave a Review</h2>
					<img src="/wp-content/themes/avrek/images/stars-dark-small.png" alt="stars" />
				</div>
				<div class="logos">
					<a href="#"><img src="/wp-content/themes/avrek/images/logo-google.jpg" alt="Google" /></a>
					<a href="http://www.yelp.ca/biz/avrek-law-firm-irvine"><img src="/wp-content/themes/avrek/images/logo-yelp.jpg" alt="Yelp" /></a>
				</div>
				<div class="review-body">
					<?php
					$testimonial_query = new WP_Query( 'post_type=testimonials&posts_per_page=1' );
					while ( $testimonial_query->have_posts() ) : $testimonial_query->the_post();
					?>
					<p>"<?php echo excerpt(80); ?>"</p>
					<p>- <strong><?php the_title(); ?></strong></p>
					<?php endwhile; ?>
				</div>
			</div>
			<?php } ?>
			<?php $the_query->the_post(); ?>
			<div class="box">
				<h3><?php the_title(); ?></h3>
				<?php the_content(); ?>
				<strong>Settlement 
				<?php if(get_field('settlement_date')) { ?> on <?php the_field('settlement_date'); } ?> 
				Amount: <span><?php the_field('settlement_amount'); ?></span>
				<?php if(get_field('confidential_settlement')) { ?> -- Confidential Settlement.<?php } ?></strong>
			</div>
			<?php 
				$counter++;
				endwhile; 
			?>														
		</div>
		<!-- 
		<div class="navigation load-more">
			<?php next_posts_link( 'Next Posts >>', $the_query->max_num_pages ); ?>
		</div>
		-->
		<?php endif; wp_reset_postdata(); ?>		
		<!--<a href="#" class="load-more">Load More Results</a>-->
		
	</div>
</section>

<?php get_footer(); ?>