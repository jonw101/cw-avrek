jQuery(document).ready(function(){

	// The number of the next page to load (/page/x/).
	var pageNum = parseInt(pbd_alp.startPage) + 1;
	
	// The maximum number of pages the current query can return.
	var max = parseInt(pbd_alp.maxPages);
	
	// The link of the next page of posts.
	var nextLink = pbd_alp.nextLink;
	
	/**
	 * Replace the traditional navigation with our own,
	 * but only if there is at least one page of new posts to load.
	 */
	console.log(pageNum);
	console.log(max);
	if(pageNum <= max) {
		// Insert the "More Posts" link.
		jQuery('#grid-holder')
			.append('<div class="pbd-alp-placeholder-'+ pageNum +'"></div>')
			.append('<div id="pbd-alp-load-posts"><a href="#" class="load-more">Load More Posts</a></div>');
			
		// Remove the traditional navigation.
		jQuery('.navigation').remove();
	}
	
	
	/**
	 * Load new posts when the link is clicked.
	 */
	jQuery('#pbd-alp-load-posts a').click(function() {
	
		// Are there more posts to load?
		if(pageNum <= max) {
		
			// Show that we're working.
			jQuery(this).text('Loading posts...');
			
			jQuery('.pbd-alp-placeholder-'+ pageNum).load(nextLink + ' .grid',
				function() {
					
					//remove review box
					jQuery('.box.review:not(:first)').remove();
					
					jQuery('.grid').masonry({
					  itemSelector: '.box'
					});
					
					jQuery("a.popup").fancybox({
						'padding' : 20
					});	
					
					// Update page number and nextLink.
					pageNum++;
					nextLink = nextLink.replace(/\/page\/[0-9]*/, '/page/'+ pageNum);
					
					
					// Add a new placeholder, for when user clicks again.
					jQuery('#pbd-alp-load-posts')
						.before('<div class="pbd-alp-placeholder-'+ pageNum +'"></div>')
					
					// Update the button message.
					if(pageNum <= max) {
						jQuery('#pbd-alp-load-posts a').text('Load More Posts');
					} else {
						jQuery('#pbd-alp-load-posts a').text('No more posts to load.');
					}
				}
			);
		} else {
			jQuery('#pbd-alp-load-posts a').append('.');
		}	
		
		return false;
	});
});