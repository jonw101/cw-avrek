jQuery(document).ready(function($){

	/* mobile menu dropdown toggle */
	jQuery('.menu-drop a').bind('click touch',function(){
		jQuery('.menu-second').slideToggle('slow'); 
		return false;
	});
	
	/* sticky nav */
	jQuery('nav').sticky({topSpacing:0});
	
	/* Sticky sidebar form */
	if( ('#sb-cmform').length != 0){
		jQuery('#sb-cmform').sticky({topSpacing:90, className: 'divfloat'});
	}		
	
	/* clear form fields */
	var el = jQuery('input[type=text], textarea');
	    el.focus(function(e) {
	        if (e.target.value == e.target.defaultValue)
	            e.target.value = '';
	    });
	    el.blur(function(e) {
	        if (e.target.value == '')
	            e.target.value = e.target.defaultValue;
	    });	
	
	jQuery('#commentform').addClass('noauto');
	jQuery('#searchform').addClass('noauto');
	
	/* home banner cycle */
	jQuery('.slide-holder').cycle({
		fx:			'scrollLeft',
		timeout:	14000,
		pager:		'.banner-nav'
	});
	jQuery('.banner-nav a:first-child').text('Integrity');
	jQuery('.banner-nav a:nth-child(2)').text('Passion');
	jQuery('.banner-nav a:nth-child(3)').text('Accountability');
	jQuery('.banner-nav a:last-child').text('Results');

	/* case results cycle */
	/*
	jQuery('.results-holder').cycle({
		fx:			'scrollHorz',
		speed:		500,
		timeout:	0,
		pager:		'.results-nav'
	});*/

	jQuery('.results-nav').append( '<a href="/case-results/" class="btn-small">See All</a>' );
	
	/* step form cycle */
	jQuery('.step-form-cycle').cycle({
		fx:			'scrollHorz',
		speed:		500,
		timeout:	0,
		pager:		'.step-nav', 
		prev:   	'.prev-btn', 
		next:		'.next-btn'
	});
	
	/* dynamic sidebar height */
	if (jQuery(window).width() > 767) {
		jQuery('.sidebar').height(Math.max(jQuery('#body .content').height(),jQuery('.sidebar').height()));
	}

	/* attorney form & testimonial/results popup */
	if ( jQuery('a.popup').length > 0 ) {  // don't run this on pages that don't have a link with the class "popup"
		jQuery("a.popup").fancybox({
			'padding' : 20
		});	  
	}	
	
	/* masonry grid for case results & testimonials */
	jQuery('.grid').masonry({
	  itemSelector: '.box'
	});

});