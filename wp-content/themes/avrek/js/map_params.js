window.gMap = new google.maps.Map(document.getElementById("map-canvas"), {
	center: new google.maps.LatLng(33.985037, -117.971878),
	zoom: 11,
	scrollwheel: false
});


function initialize() {
	window.updateMap(accidents);
}

window.updateMap = function(data) {
	var styles = [
		{
			stylers: [
			{ hue: "#00ffe6" },
			{ saturation: -70 }
			]
		},{
			featureType: "road",
				elementType: "geometry",
				stylers: [
				{ lightness: 100 }
			]
		},{
			featureType: "road",
				elementType: "labels",
				stylers: [
				{ visibility: "on" }
			]
		}
	];

	window.gMap.setOptions({styles: styles});
	setMarkers(window.gMap, data);
}

function setMarkers(map, locations) {
	var image_black = {
		url: templateUrl + '/images/map/black-marker.png',
		size: new google.maps.Size(30, 40),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(0, 40),
	}
	var image_blue = {
		url: templateUrl + '/images/map/blue-marker.png',
		size: new google.maps.Size(30, 40),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(0, 40),
	}
	var image_red = {
			url: templateUrl + '/images/map/red-marker.png',
			size: new google.maps.Size(30, 40),
			origin: new google.maps.Point(0,0),
			anchor: new google.maps.Point(0, 40),
		}
	var image_green = {
		url: templateUrl + '/images/map/green-marker.png',
		size: new google.maps.Size(30, 40),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(0, 40),
	}
	var infowindow = new google.maps.InfoWindow();

	for (var i = 0; i < locations.length; i++) {
		var report = locations[i];
		var myLatLng = new google.maps.LatLng(report[1], report[2]);
		var imageMarker;
		switch(true){
			case (report[4] == 'Fatal'):imageMarker = image_black; break;
			case (report[4] == 'Personal Injury'): imageMarker = image_red; break;
			case (report[4] == 'Injury'): imageMarker = image_red; break;
			case (report[4] == 'Property Damage'): imageMarker = image_blue; break;
			case (report[4] == 'No Damage or Injury'): imageMarker = image_green; break;
			default: imageMarker = image_green;
		}
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: imageMarker,
			title: report[3]
		});

		contentString = '<div class="crash-data"><b>' + report[0] + '</b><br/><b>Address:</b> ' + report[3] + '<br> <b>Severity:</b> ' + report[4] + '<br> <b>Weather:</b> ' + report[5] + '<br> <b>Lighting:</b> ' + report[6] + '<br> <b>Comments:</b> ' + report[7] + '</div>';

		makeInfoWindowEvent(map, infowindow, contentString, marker);
	}
}

function makeInfoWindowEvent(map, infowindow, contentString, marker) {
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(contentString);
		infowindow.open(map, marker);
	});
}

function clearOverlays(markersArray) {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
  markersArray.length = 0;
}

document.getElementById("check-fatal").addEventListener("click", function(){
var showMarker;
	if (document.getElementById("check-fatal").checked) {
		 marker.setMap(window.gMap);
    	} else {
    		marker.setMap(null);
    	}

});

google.maps.event.addDomListener(window, 'load', initialize);


