<?php 
/*
* Template Name: Thank You
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>			
			<?php the_content(); ?>		
		<?php endwhile; endif; ?>
		
		<div class="thankyou-results clearfix">
			<h2>Our Case Results</h2>
			<?php 
			$the_query = new WP_Query( array( 'post_type' => 'case-results', 'posts_per_page' => 4, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<div class="result">
				<h3><?php the_title(); ?></h3>
				<?php the_content(); ?>
				<strong>Settlement 
				<?php if(get_field('settlement_date')) { ?> on <?php the_field('settlement_date'); } ?> 
				Amount: <span><?php the_field('settlement_amount'); ?></span>
				<?php if(get_field('confidential_settlement')) { ?> -- Confidential Settlement.<?php } ?></strong>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<a class="btn-small right" href="/case-results/">See All Results</a>
		</div>
		
		</div>
		
		<?php get_sidebar('thankyou'); ?>
		
	</div>
</section>

<?php get_footer(); ?>