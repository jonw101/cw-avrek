		<aside class="sidebar right">
		
			<div class="padding-box">
			
				<?php get_template_part('includes/attorney-thumbs'); ?>
				
				<?php get_template_part('includes/testimonial'); ?>
				
			</div>
			
			<?php get_template_part('includes/contact-form'); ?>
			
			<div class="padding-box">
				<?php get_template_part('includes/case-results'); ?>
			</div>
		
			<div class="side-award">
				<a href="http://www.calbar.ca.gov" target="_blank"><img src="/wp-content/themes/avrek/images/logo-ca-state-bar.jpg" alt="The State Bar of California" /></a>
			</div>
					
		</aside>