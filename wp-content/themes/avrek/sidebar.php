		<aside class="sidebar right">

			<?php get_template_part('includes/contact-form'); ?>
		
			<div class="padding-box">
			
				<?php get_template_part('includes/testimonial'); ?>

				<?php get_template_part('includes/other-info'); ?>
				
				<?php get_template_part('includes/case-results'); ?>
				
				<?php get_template_part('includes/faqs-list'); ?>	
			
			</div>
			
			
		
		</aside>