<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="shortcut icon" href="/wp-content/themes/avrek/favicon.ico" />
<link rel="apple-touch-icon" type="image/png" href="/wp-content/themes/avrek/images/apple-touch-icon.png">
<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/wp-content/themes/avrek/images/apple-touch-icon-72.png">
<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/wp-content/themes/avrek/images/apple-touch-icon-114.png">

<link href="<?php echo get_template_directory_uri(); ?>/css/map.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<link href="/wp-content/themes/avrek/css/ie.css" rel="stylesheet" />
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>

<?php if(is_front_page()) { ?>
<meta name="geo.region" content="US-CA" />
<meta name="geo.placename" content="Irvine" />
<meta name="geo.position" content="33.635249,-117.739764" />
<meta name="ICBM" content="33.635249,-117.739764" />
<meta name="copyright" content=" Avrek Law Firm" />
<link rel="publisher" href="https://plus.google.com/+Avrek/posts" title="Avrek Law Firm" />
<?php } ?>
<!-- <link rel="author" href="" title="" /> -->

<script async src="//16440.tctm.co/t.js"></script>

</head>

<body <?php body_class(); ?>>

<div id="skip"><a href="#body">Skip to Main Content</a></div> 

<header>
	<div class="wrapper">

		<nav class="mobile-menu visible-phone">
			<ul>
				<li class="home"><a href="/">Home</a></li>
				<li><a href="/about-us/">Our Firm</a></li>
				<li><a href="/contact-us/">Contact Us</a></li>
				<li class="menu-drop"><a href="javascript:void(0)">Menu</a></li>
			</ul>
			<?php wp_nav_menu( array( 'theme_location' => 'mobile_drop', 'container' => '', 'menu_class' => 'menu-second' ) ); ?>		
		</nav>

		<div itemscope itemtype="http://schema.org/Organization" class="logo left">
			<a itemprop="url" href="/">Avrek Law Firm</a>
		</div>
		
		<?php get_template_part('includes/call-chat'); ?>
	
	</div>
</header>

<nav class="visible-desktop">
	<div class="wrapper">

		<?php wp_nav_menu( array( 'theme_location' => 'main_nav', 'container' => '' ) ); ?>

	</div>	
</nav>