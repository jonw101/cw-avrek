<?php
add_theme_support( 'post-thumbnails' );
add_image_size('inner_photo', 289, 9999); // featured image for inner page content areas, fixed width, variable height
add_image_size('attorney_photo', 300, 9999); // featured image for attorney bio pages, fixed width, variable height
add_image_size('attorney_thumb', 119, 126, true); // attorney thumbnail for sidebars, fixed height and width, hard crop


add_theme_support( 'menus' );
register_nav_menus( array(
	'main_nav' => 'Main Navigation',
	'mobile_drop' => 'Mobile Drop Menu'
));


/* remove [...] from default excerpt */
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/* Custom excerpt lengths */
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	return $excerpt;
}
function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}
	$content = preg_replace('/[.+]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}


/* post view count */
function wpb_set_post_views($postID) {
	$count_key = 'wpb_post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


/* Enqueue scripts and styles */

function avrek_theme_scripts() {

	wp_enqueue_style( 'avrek-theme-style', get_stylesheet_uri() );

	if(is_page_template('page-attorney.php')) {
		wp_enqueue_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('jquery'), '1.10.3', true );
	}

	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'avrek_theme_scripts' );


add_action( 'wp_enqueue_scripts', 'de_script', 100 );

function de_script() {
/*
	if ( is_plugin_active('optimizePressPlugin/optimizepress.php') ) {

	wp_dequeue_script( 'fancybox' );
	wp_deregister_script( 'fancybox' );
	wp_dequeue_style( 'fancybox' );

	}
 */
}

/* Remove url field from comments form */
function alter_comment_form_fields($fields){
	$fields['url'] = '';  //removes website field
	return $fields;
}
add_filter('comment_form_default_fields','alter_comment_form_fields');


/* H1 title */
function h1_title($id=false, $echo=true){
	global $post;
	$title = "";
	if ($id)
		$title = get_field('h1_title', $id)? get_field('h1_title', $id) : get_the_title($id);
	else
		$title = get_field('h1_title', $post->ID)? get_field('h1_title', $post->ID) : get_the_title($post->ID);
	if ($title != ""){
		if ($echo)
			echo $title;
		else
			return $title;
	}else{
		return false;
	}
}


/* Fix for WP SEO Local Plugin */
function locations_cpt($args){
	if (count($args)){
		$args['rewrite'] = array( 'slug' => 'locations' , 'with_front' => false);
	}
	return $args;
}
add_filter('wpseo_local_cpt_args', 'locations_cpt' );


/**
 * Ajax Load Posts
 */
function pbd_alp_init() {
	global $wp_query;

	//if( isset( $_REQUEST['debug'] )  ) {
	$page_id = get_the_ID();
	if( in_array( $page_id, array(264, 262) ) ) {
		// Queue JS and CSS

		$s_args = array(
			264 => array(
				'post_type'  =>  'testimonials',
				'orderby'    =>  'menu_order',
				'order'      =>  'ASC'
			),
			262 => array(
				'post_type'  =>  'case-results',
				'orderby'    =>  'menu_order',
				'order'      =>  'ASC'
			)
		);

		wp_enqueue_script(
			'pbd-alp-load-posts',
			get_template_directory_uri() . '/js/ajax-load-posts.js',
			array('jquery'),
			'1.0',
			true
		);
		$query_args = $s_args[$page_id];
		$the_query = new WP_Query ( $query_args );
		$max = $the_query->max_num_pages;
		//}
		$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

		// Add some parameters for the JS.
		wp_localize_script(
			'pbd-alp-load-posts',
			'pbd_alp',
			array(
				'startPage' => $paged,
				'maxPages' => $max,
				'nextLink' => next_posts($max, false)
			)
		);
	}

	//} else {
	/*
	// Add code to testimonial page.
	if( is_page(264) ) {
		// Queue JS and CSS
		wp_enqueue_script(
			'pbd-alp-load-posts',
			get_template_directory_uri() . '/js/ajax-load-posts.js',
			array('jquery'),
			'1.0',
			true
		);

		// What page are we on? And what is the pages limit?
		//$max = $wp_query->max_num_pages;
		//if( isset( $_REQUEST['debug'] ) ) {
		$query_args = array(
			  'post_type'  =>  'testimonials',
			  'orderby'    =>  'menu_order',
			  'order'      =>  'ASC'
			);
		$the_query = new WP_Query ( $query_args );
		$max = $the_query->max_num_pages;
		//}
		$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

		// Add some parameters for the JS.
		wp_localize_script(
			'pbd-alp-load-posts',
			'pbd_alp',
			array(
				'startPage' => $paged,
				'maxPages' => $max,
				'nextLink' => next_posts($max, false)
			)
		);
	}


	}*/
}
add_action('template_redirect', 'pbd_alp_init');

function is_parent($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if(is_page()&&($post->post_parent==$pid||is_page($pid)))
		return true;   // we're at the page or at a sub page
	else
		return false;  // we're elsewhere
};


//-------  BICYCLE ACCIDENT REPORTING  ----------------------------//

//---ADMIN  ----------------------------//
//Add Custom Post List
add_action('init', 'bicycle_reporting');

function bicycle_reporting() {

	$labels = array(
		'name' => _x('Bicycle Accident Reporting', 'post type general name'),
		'singular_name' => _x('Bicycle Accident Report', 'post type singular name'),
		'add_new' => _x('Add New', 'Bicycle Accident Report'),
		'add_new_item' => __('Add New Bicycle Accident Report'),
		'edit_item' => __('Edit Bicycle Accident Report'),
		'new_item' => __('New Bicycle Accident Report'),
		'view_item' => __('View Bicycle Accident Report'),
		'search_items' => __('Search Bicycle Accident Reports'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/bike-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor', 'author', 'thumbnail')
	);

	register_post_type( 'bike_accidents' , $args );
}

register_taxonomy("Categories", array("bike_accidents"), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => true));

add_action("admin_init", "admin_init");

function admin_init(){
	add_meta_box("credits_meta", "Bicycle Accident Personal Info", "credits_meta", "bike_accidents", "normal", "low");
	add_meta_box("credits_meta_accidents", "Bicycle Accident Crash Info", "credits_meta_accidents", "bike_accidents", "normal", "low");
}

function credits_meta() {
	global $post;

	$custom = get_post_custom($post->ID);
	$b_fullname = $custom["b_fullname"][0];
	$b_phone_or_email = $custom["b_phone_or_email"][0];

?>
  <p><label>Full Name:</label>
  <input name="b_fullname" class="large-text" type="text" value="<?php echo $b_fullname; ?>" /></p>
  <p><label>Phone or Email:</label>
  <input name="b_phone_or_email" class="large-text" type="text" value="<?php echo $b_phone_or_email; ?>"></p>


<?php
}

function credits_meta_accidents() {
	global $post;

	$custom = get_post_custom($post->ID);
	$b_date = $custom["b_date"][0];
	$b_address = $custom["b_address"][0];
	$b_coordinates_lat = $custom["b_coordinates_lat"][0];
	$b_coordinates_lon = $custom["b_coordinates_lon"][0];
	$b_severity = $custom["b_severity"][0];
	$b_weather = $custom["b_weather"][0];
	$b_lighting = $custom["b_lighting"][0];
	$b_comments = $custom["b_comments"][0];

?>
  <p><label>Accident Date:</label>
  <input name="b_date" class="large-text" type="text" value="<?php echo $b_date; ?>"></p>
  <p><label>Address or Intersection:</label>
  <input name="b_address" class="large-text" type="text" value="<?php echo $b_address; ?>"></p>
  <p><span>Geo Coordinates: </span></p>
  <p><label>Lat:</label>
  <input name="b_coordinates_lat" class="large-text" type="text" value="<?php echo $b_coordinates_lat; ?>"></p>
  <p><label>Lng:</label>
  <input name="b_coordinates_lon" class="large-text" type="text" value="<?php echo $b_coordinates_lon; ?>"></p>
  <!--<p><span>Geo Coordinates: </span><span><?php echo $b_coordinates_lat .', ' .$b_coordinates_lon; ?></span></p>-->
  <p><label>Accident Severity:</label>
  <select name="b_severity" class="large-text" type="text">
		<option value="<?php echo $b_severity; ?>" selected><?php echo $b_severity; ?></option>
		<option class="select-dash" disabled="disabled">----</option>
	   <option value="Fatal">Fatal</option>
	   <option value="Personal Injury">Personal Injury</option>
	   <option value="Property Damage">Property Damage</option>
	   <option value="No Damage or Injury">No Damage or Injury</option>
  </select></p>
  <p><label>Accident Weather Conditions:</label>
  <select name="b_weather" class="large-text" type="text">
		<option value="<?php echo $b_weather; ?>" selected><?php echo $b_weather; ?></option>
		<option class="select-dash" disabled="disabled">----</option>
		<option value="Sunny">Sunny</option>
		<option value="Cloudy">Cloudy</option>
		<option value="Rain">Rain</option>
		<option value="Wind">Wind</option>
		<option value="Snow">Snow</option>
  </select></p>
  <p><label>Accident Lighting Conditions:</label>
  <select name="b_lighting" class="large-text" type="text">
		<option value="<?php echo $b_lighting; ?>" selected><?php echo $b_lighting; ?></option>
		<option class="select-dash" disabled="disabled">----</option>
		<option value="Day">Day</option>
		<option value="Night">Night</option>
		<option value="Dawn / Dusk">Dawn / Dusk</option>
		<option value="Unknown">Unknown</option>
  </select></p>
  <p><label>Comments:</label><br />
  <textarea cols="50" rows="5" name="b_comments"><?php echo $b_comments; ?></textarea></p>
<?php
}

function save_details(){
	global $post;

	//person info
	update_post_meta($post->ID, "b_fullname", $_POST["b_fullname"]);
	update_post_meta($post->ID, "b_phone_or_email", $_POST["b_phone_or_email"]);

	//accident info
	update_post_meta($post->ID, "b_date", $_POST["b_date"]);
	update_post_meta($post->ID, "b_address", $_POST["b_address"]);
	update_post_meta($post->ID, "b_coordinates_lat", $_POST["b_coordinates_lat"]);
	update_post_meta($post->ID, "b_coordinates_lon", $_POST["b_coordinates_lon"]);
	update_post_meta($post->ID, "b_severity", $_POST["b_severity"]);
	update_post_meta($post->ID, "b_weather", $_POST["b_weather"]);
	update_post_meta($post->ID, "b_lighting", $_POST["b_lighting"]);
	update_post_meta($post->ID, "b_comments", $_POST["b_comments"]);
}
add_action('save_post', 'save_details', 999);



//------END ADMIN  ----------------------------//

//------ACCIDENT DATA FOR MAP  ----------------------------//

//GET ALL ACCIDENT DATA
function get_all_accident_post_data() {
echo 'test';
	//get all Accident Posts with meta info
	$args = array( 'posts_per_page' => -1, 'post_status' => 'any', 'post_type' => 'bike_accidents');
	$query = get_posts( $args );
	//return $query;

	$accidents = array();

	foreach ( $query as $accident ) {
		$accidents[] = array(
			get_post_meta( $accident->ID, 'b_date', true ),
			get_post_meta( $accident->ID, 'b_coordinates_lat', true ),
			get_post_meta( $accident->ID, 'b_coordinates_lon', true ),
			get_post_meta( $accident->ID, 'b_address', true ),
			get_post_meta( $accident->ID, 'b_severity', true ),
			get_post_meta( $accident->ID, 'b_weather', true ),
			get_post_meta( $accident->ID, 'b_lighting', true ),
			get_post_meta( $accident->ID, 'b_comments', true ),
		);
	}


	//CONNECT TO DATABASE
	$username = "dataserv_devfars";
	$password = "w+0VXof=Jp0t";
	$database = "dataserv_FARS";
	$hostname = "104.131.8.157";

	//connection to the database
	$db_handle = mysqli_connect($hostname, $username, $password, $database);
	//or die("Unable to connect to MySQL");

	if (!$db_handle) {
		die('test');
		$results = array();
	} else {

		//Get Persons_Type Bicycleist from FARS person table
//		$per_SQL = "SELECT * FROM FARS_2012_CA_person WHERE `STATE,N,16,0`='6' AND `PER_TYP,N,16,0`='6'";
//		$per_result = $db_handle->query($per_SQL);
//
//		while ( $db_per_field = $per_result->fetch_assoc() ) {
//			$per_array[] = $db_per_field;
//			$per_inj[] = $db_per_field['INJ_SEV,N,16,0'];
//			$case_no[] = $db_per_field['ST_CASE,N,16,0'];
//		}
//
//		var_dump($case_no);
//		var_dump('nickeleus');
//
//		$acc_SQL = "SELECT * FROM FARS_2012_CA_accident WHERE `STATE,N,16,0`='6' AND `ST_CASE,N,16,0` IN ('" . join(',',$case_no) . "')";
//		$acc_result = $db_handle->query($acc_SQL);
//
//		while ($row = $acc_result->fetch_assoc()) {
//			echo "<pre>";
//			var_dump($row);
//			echo "</pre>";
//		}
//
//		die();

		foreach ($per_array as $key => $value){
			$per_inj = $value['INJ_SEV,N,16,0'];
			$case_no = $value['ST_CASE,N,16,0'];

			$acc_SQL = "SELECT * FROM FARS_2012_CA_accident WHERE `STATE,N,16,0`='6' AND `ST_CASE,N,16,0`='$case_no'";
			$acc_result = $db_handle->query($acc_SQL);
			$acc = $acc_result->fetch_assoc();


			$result[] = $acc['MONTH,N,16,0'] .'/' .$acc['DAY,N,16,0'] .'/' .$acc['YEAR,N,16,0'];
			$result[] = $acc['LATITUDE,N,16,8'];
			$result[] = $acc['LONGITUD,N,16,8'];
			$result[] = sort_address($acc['TWAY_ID,C,30'], $acc['TWAY_ID2,C,30']);
			$result[] = get_inj_result($per_inj);
			$result[] = get_weather_result($acc['WEATHER,N,16,0']);
			$result[] = get_lighting_result($acc['LGT_COND,N,16,0']);
			$rusult[] = '';



			$results[] = $result;
			unset($result);
		}
		unset($per_result);
		unset($per_array);
		//Get Persons_Type Bicycleist from FARS person table
		$per_SQL = "SELECT * FROM FARS_2011_CA_person WHERE `STATE,N,16,0`='6' AND `PER_TYP,N,16,0`='6'";
		$per_result = $db_handle->query($per_SQL);

		while ( $db_per_field = $per_result->fetch_assoc() ) {
			$per_array[] = $db_per_field;
		}

		foreach ($per_array as $key => $value){
			$per_inj = $value['INJ_SEV,N,16,0'];
			$case_no = $value['ST_CASE,N,16,0'];

			$acc_SQL = "SELECT * FROM FARS_2011_CA_accident WHERE `STATE,N,16,0`='6' AND `ST_CASE,N,16,0`='$case_no'";
			$acc_result = $db_handle->query($acc_SQL);
			$acc = $acc_result->fetch_assoc();


			$result[] = $acc['MONTH,N,16,0'] .'/' .$acc['DAY,N,16,0'] .'/' .$acc['YEAR,N,16,0'];
			$result[] = $acc['LATITUDE,N,16,8'];
			$result[] = $acc['LONGITUD,N,16,8'];
			$result[] = sort_address($acc['TWAY_ID,C,30'], $acc['TWAY_ID2,C,30']);
			$result[] = get_inj_result($per_inj);
			$result[] = get_weather_result($acc['WEATHER,N,16,0']);
			$result[] = get_lighting_result($acc['LGT_COND,N,16,0']);
			$rusult[] = '';



			$results[] = $result;
			unset($result);
		}

		unset($per_result);
		unset($per_array);
		//Get Persons_Type Bicycleist from FARS person table
		$per_SQL = "SELECT * FROM FARS_2010_CA_person WHERE `STATE,N,16,0`='6' AND `PER_TYP,N,16,0`='6'";
		$per_result = $db_handle->query($per_SQL);

		while ( $db_per_field = $per_result->fetch_assoc() ) {
			$per_array[] = $db_per_field;
		}

		foreach ($per_array as $key => $value){

			$per_inj = $value['INJ_SEV,N,16,0'];
			$case_no = $value['ST_CASE,N,16,0'];

			$acc_SQL = "SELECT * FROM FARS_2010_CA_accident WHERE `STATE,N,16,0`='6' AND `ST_CASE,N,16,0`='$case_no'";
			$acc_result = $db_handle->query($acc_SQL);
			$acc = $acc_result->fetch_assoc();


			$result[] = $acc['MONTH,N,16,0'] .'/' .$acc['DAY,N,16,0'] .'/' .$acc['YEAR,N,16,0'];
			$result[] = $acc['LATITUDE,N,16,8'];
			$result[] = $acc['LONGITUD,N,16,8'];
			$result[] = sort_address($acc['TWAY_ID,C,30'], $acc['TWAY_ID2,C,30']);
			$result[] = get_inj_result($per_inj);
			$result[] = get_weather_result($acc['WEATHER,N,16,0']);
			$result[] = get_lighting_result($acc['LGT_COND,N,16,0']);
			$rusult[] = '';



			$results[] = $result;
			unset($result);
		}

	}

	return array_merge($accidents, $results);
}//end function

function print_out_accident_data() {?>
<script>
	var accidents = <?php echo json_encode(get_all_accident_post_data()); ?>
	</script>
<?php
}

//------ACCIDENT DATA FOR FARS  ----------------------------//



//HELPER FUNCTIONS:
//ADDRESS STREETS:
function sort_address($a, $b) {
	if (!empty($b)) {
		return $a .' and ' .$b;
	} else {
		return $a;
	}
}

//GET WEATHER RESULTS
function get_inj_result($i) {
	if ($i == 0 || $i == 00) {
		return "None";
	} elseif ($i == 1 || $i == 01) {
		return "Injury";
	} elseif ($i == 2 || $i == 02) {
		return "Injury";
	} elseif ($i == 3 || $i == 03) {
		return "Injury";
	} elseif ($i == 4 || $i == 04) {
		return "Fatal";
	} elseif ($i == 5 || $i == 05) {
		return "Injury";
	} elseif ($i == 6 || $i == 06) {
		return "Fatal";
	} else {
		return "Unknown";
	}
}//function

//GET WEATHER RESULTS
function get_weather_result($i) {
	if ($i == 0 || $i == 00) {
		return "";
	} elseif ($i == 1 || $i == 01) {
		return "Sunny or Clear";
	} elseif ($i == 2 || $i == 02) {
		return "Rain";
	} elseif ($i == 3 || $i == 03) {
		return "Snow";
	} elseif ($i == 4 || $i == 04) {
		return "Snow";
	} elseif ($i == 10) {
		return "Clouds";
	} elseif ($i == 11) {
		return "Snow";
	} elseif ($i == 5 || $i == 05) {
		return "Clouds";
	} elseif ($i == 6 || $i == 06) {
		return "Wind";
	} elseif ($i == 7 || $i == 07) {
		return "Wind";
	} elseif ($i == 8 || $i == 08) {
		return "";
	} elseif ($i == 98) {
		return "";
	} elseif ($i == 99) {
		return "";
	} else {
		return "";
	}
}//function

//GET WEATHER RESULTS
function get_lighting_result($i) {
	if ($i == 1 || $i == 01) {
		return "Day";
	} elseif ($i == 2 || $i == 02) {
		return "Night";
	} elseif ($i == 3 || $i == 03) {
		return "Night";
	} elseif ($i == 4 || $i == 04) {
		return "Dusk/Dawn";
	} elseif ($i == 5 || $i == 05) {
		return "Dusk/Dawn";
	} elseif ($i == 6 || $i == 06) {
		return "Night";
	} else {
		return "Unknown";
	}
}//function

//------END ACCIDENT DATA  ----------------------------//
//------FORM SUBMIT WITH AJAX  ----------------------------//

//------END FORM SUBMIT  ----------------------------//


//-------  END BICYCLE ACCIDENT REPORTING   ----------------------------//

?>
