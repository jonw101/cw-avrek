<?php 
		error_reporting(0);
		header('Content-Type: application/json');
		require_once('../../../wp-load.php');
		require_once('webinar-db-interaction/webinar-db-interaction.php');
		require_once('license.php');
		require_once('access-validation.php');
	
		switch($_POST['request']){
			
			case 'webinar_insights' :
								$ob_webinar_db_interaction	=	new webinar_db_interaction();
								$all_webinars								= $ob_webinar_db_interaction->get_all_webinars_with_insights($_POST['offset'],$_POST['limit'],$_POST['search'],$_POST['storing_mode']);
		                         if($_POST['storing_mode'] != 'draft'){
								if(count($all_webinars)>0) {
									foreach($all_webinars as $webinars_key => $webinars_val){
										$all_webinars[$webinars_key]->activity_stats = $ob_webinar_db_interaction->get_webinar_activity_stats($webinars_val->webinar_id_pk);
										$all_webinars[$webinars_key]->attendees 		 = $ob_webinar_db_interaction->get_webinar_registrant_insights($webinars_val->webinar_id_pk,$_POST['registrant_offset'],$_POST['registrant_limit']);
										$all_webinars[$webinars_key]->links			     = $ob_webinar_db_interaction->get_webinar_all_pages_links($webinars_val->webinar_id_pk);
										$all_webinars[$webinars_key]->total_visitors = $ob_webinar_db_interaction->get_webinar_total_visitor($webinars_val->webinar_id_pk);
										$all_webinars[$webinars_key]->actions 			 = $ob_webinar_db_interaction->get_webinar_total_actions_taken($webinars_val->webinar_id_pk);
									}
										$all_webinars[0]->total_webinars						 = $ob_webinar_db_interaction->get_webinars_count()->total;
								}
		                         }
								echo json_encode($all_webinars);exit;
								break;
		 	
			case 'single_webinar_insights' :
								$ob_webinar_db_interaction	=	new webinar_db_interaction();
								$webinar_details						= $ob_webinar_db_interaction->get_single_webinar_with_insight($_POST['webinar_id']);
			
								if(count($webinar_details)>0) {
										$webinar_details[0]->activity_stats= $ob_webinar_db_interaction->get_webinar_activity_stats($webinar_details[0]->webinar_id_pk);
										$webinar_details[0]->total_webinars= $ob_webinar_db_interaction->get_webinars_count()->total;
										$webinar_details[0]->total_visitors= $ob_webinar_db_interaction->get_webinar_total_visitor($webinar_details[0]->webinar_id_pk);
										$webinar_details[0]->actions 			 = $ob_webinar_db_interaction->get_webinar_total_actions_taken($webinar_details[0]->webinar_id_pk);
										$webinar_details[0]->attendees  	 = $ob_webinar_db_interaction->get_webinar_registrant_insights($webinar_details[0]->webinar_id_pk,$_POST['registrant_offset'],$_POST['registrant_limit'],$_POST['action']);
										$webinar_details[0]->links				 = $ob_webinar_db_interaction->get_webinar_all_pages_links($webinar_details[0]->webinar_id_pk);
								}
								echo json_encode($webinar_details);exit;
								break;	
								
			case 'regsitrant_insights' :		
								$ob_webinar_db_interaction	=	new webinar_db_interaction();
								$all_data = $ob_webinar_db_interaction->get_webinar_registrant_insights($_POST['webinar_id'],$_POST['registrant_offset'],$_POST['registrant_limit']);
								echo json_encode($all_data);
								break;
			case 'filtered_regsitrant_insights' :
								$ob_webinar_db_interaction	=	new webinar_db_interaction();
								$attendees_data = $ob_webinar_db_interaction->get_filtered_webinar_registrant_insights($_POST['webinar_id'],$_POST['event_actions'],$_POST['replay_actions'],$_POST['took_action'],$_POST['attended'],$_POST['attended_last'],$_POST['registerd'],$_POST['registerd_last'],$_POST['watched'],$_POST['watching_opt'],$_POST['query_opt'],$_POST['namemail'],$_POST['registrant_offset'],$_POST['registrant_limit']);
								echo json_encode($attendees_data);exit;
								break;
								
			case 'regsitrant_stats' : 
								$ob_webinar_db_interaction	=	new webinar_db_interaction();
								$regsitrant_stats 					=	$ob_webinar_db_interaction->get_registrant_stats($_POST['registrant_id']);
								echo json_encode($regsitrant_stats);
								break;
			
			case 'generate_report':
                $ob_webinar_db_interaction	=	new webinar_db_interaction();
								$webinar_details						= $ob_webinar_db_interaction->get_single_webinar_with_insight($_POST['webinar_id']);
			
								if(count($webinar_details)>0) {
										$webinar_details[0]->attendees  =  $ob_webinar_db_interaction->get_webinar_registrant_insights($webinar_details[0]->webinar_id_pk,$_POST['registrant_offset'],$_POST['registrant_limit'],"");
								}
								echo json_encode($webinar_details);exit;
								break;
								
		 	case 'calender_events' :
							$ob_webinar_db_interaction		 =	new webinar_db_interaction();
							$webinar_details_for_slots		 =  $ob_webinar_db_interaction->get_webinars_for_a_date($_POST['date'],$_POST['limit']);
							echo json_encode($webinar_details_for_slots);
							break;
							
			case	'webinars_count' :
							$ob_webinar_db_interaction		 =	new webinar_db_interaction();
							$webinars_count		 						 =  $ob_webinar_db_interaction->get_webinars_count();
							echo json_encode($webinars_count);exit;
							break;
			case  'plugin_stats' :
							$ob_webinar_db_interaction		=	new webinar_db_interaction();
							$upload_max_filesize					=	ini_get('upload_max_filesize');
							$post_max_size								=	ini_get('post_max_size');
							$max_execution_time 					=	(int)ini_get('max_execution_time');
							$delayed_events_paths					=	$ob_webinar_db_interaction->get_delayed_events_paths();
							$webinars_count		 						= $ob_webinar_db_interaction->get_webinars_count('')->total;
							$drafts_count		 						= $ob_webinar_db_interaction->get_webinars_count('draft')->total;
							$plugins_stats	 							= array('delayed_events_paths'=>$delayed_events_paths,'upload_max_filesize'=>$upload_max_filesize,'post_max_size'=>$post_max_size,'max_execution_time'=>$max_execution_time,'webinars_count'=>$webinars_count,'drafts_count' =>$drafts_count);
							echo json_encode($plugins_stats);exit;
							break;
			case 'delete_specific_attendees':
							$ob_webinar_db_interaction	=	new webinar_db_interaction();
							$ids_array 									= json_decode($_POST['attendees']);
							$result 										=	$ob_webinar_db_interaction->delete_specific_attendees($ids_array);
							echo json_encode(array('result'=>$result));exit;
							break;
			case 'generate_specific_report':
							$ob_webinar_db_interaction	=	new webinar_db_interaction();
							$webinar_details						= $ob_webinar_db_interaction->get_single_webinar_with_insight($_POST['webinar_id']);
							$ids_array 									= json_decode($_POST['attendees']);
							
							if(count($webinar_details)>0) {
										$webinar_details[0]->attendees  =  $ob_webinar_db_interaction->get_webinar_insights_for_specific_registrants($webinar_details[0]->webinar_id_pk,$ids_array,$_POST['registrant_offset'],$_POST['registrant_limit']);
							}
							echo json_encode($webinar_details);exit;
							break;
			case 'delete_webinar':
						 $webinar_id							  =	(int)$_POST['webinar_id'];				
						 $ob_webinar_db_interaction	=	new webinar_db_interaction();
						 $is_webinar_deleted				=	$ob_webinar_db_interaction->delete_webinar($webinar_id);
						
							if($is_webinar_deleted==1){
								$ob_webinar_db_interaction->delete_webinar_default_attendees($webinar_id);	
								$ob_webinar_db_interaction->delete_webinar_delayed_events($webinar_id);	
								$ob_webinar_db_interaction->delete_notification_detail($webinar_id);	
								$ob_webinar_db_interaction->delete_webinar_schedules($webinar_id);	
								$ob_webinar_db_interaction->delete_webinar_registered_users($webinar_id);
								$ob_webinar_db_interaction->delete_wbeinar_notification_dettails($webinar_id);
								$ob_webinar_db_interaction->delete_webinar_pages_meta($webinar_id);				
								$ob_webinar_db_interaction->delete_webinar_pages($webinar_id);	
								$ob_webinar_db_interaction->delete_webinar_scarcity($webinar_id);
								$ob_webinar_db_interaction->delete_sharing($webinar_id);
								$ob_webinar_db_interaction->delete_webinar_questions($webinar_id);
								$ob_webinar_db_interaction->delete_webinar_events_tracking($webinar_id);
								$ob_webinar_db_interaction->delete_webinar_visitor_tracking($webinar_id);
								$ob_webinar_db_interaction->delete_webinar_template($webinar_id);
								delete_option('ewp_emarketing_settings_'.$webinar_id);
							}
						echo json_encode(array('result'=>$is_webinar_deleted));exit;
						break;
			case 'set_homepage':
						$ob_webinar_db_interaction	=	new webinar_db_interaction();	
						$webinar_id	=	(int)$_POST['webinar_id'];
						$pages 			= $ob_webinar_db_interaction->get_webinar_all_pages_ids($webinar_id);
						$ob_webinar_db_interaction->set_homepage($pages[0]->ID);
						echo json_encode(array('result'=>1));exit;
						break;
		}
		exit;
?>