<?php
define('WEBINAR_PLUGIN_NAME','webinar_plugin');
define('WEBINAR_PLUGIN_PATH',ABSPATH . 'wp-content/plugins/'.WEBINAR_PLUGIN_NAME.'/');
define('WEBINAR_PLUGIN_URL',plugins_url().'/'.WEBINAR_PLUGIN_NAME.'/');
define('EASYWEBINAR_WEBSITE_LINK',"http://www.easywebinarplugin.com");
define('EWP_UPDATE_FILE_PATH',EASYWEBINAR_WEBSITE_LINK."/ewp-update/update.php");
define('EASYWEBINAR_UPDATE_FILE',EASYWEBINAR_WEBSITE_LINK.'/wp-content/auto-updater/webinar_plugin.zip');
define('EWP_ATTACHMENTS_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR."webinar-attachment");
define('EWP_TEMP_PATH',ABSPATH.'wp-content/plugins/temp-ewp-attachments');
define('NOT_LOGGED_IN',"Please login to webinar session in order to join webinar room.");
define('VIDEO_ENDS',"This webinar is over");
define('WEBINAR_TITLE',"Webinar Broadcast Event");
define('WEBINAR_EXPIRE',"Webinar is expired");
define('WEBINAR_REGISTRATION_DATES_NOT_AVAILABLE','Webinar registration dates are not available');
define('FAVICON_URL',get_template_directory_uri().'/images/favicon.ico');
define('POWERED_BY_SOFTOBIZ','<a href="'.EASYWEBINAR_WEBSITE_LINK.'" target="_blank">Powered by Easy Webinar</a>');
$webinar_layout_variable = array(
	'0' => array(
	'registration_page_video_width' => '600',
	'registration_page_video_height'=> '338',
	'thankyou_page_video_width' 		=> '600',
	'thankyou_page_video_height' 		=> '338',
	'thankyou_share_video_width'		=> '600',
	'thankyou_share_video_height' 	=> '338',
	'video_page_video_width' 				=> '880',
	'video_page_video_height' 			=> '495'
	),
	'1' => array(
	'registration_page_video_width' => '600',
	'registration_page_video_height'=> '450',
	'thankyou_page_video_width' 		=> '600',
	'thankyou_page_video_height' 		=> '450',
	'thankyou_share_video_width'		=> '600',
	'thankyou_share_video_height' 	=> '450',
	'video_page_video_width' 				=> '880',
	'video_page_video_height' 			=> '660'
	)
);
?>