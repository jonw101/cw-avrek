﻿<?php
/**
 * Template Name: Webinar Registration
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
include('webinar-config.php');
global $post;
global $wpdb;
	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions				=	new webinar_functions();

	$purge_cache								= $ob_webinar_functions->purge_cache();
	
	if(isset($_COOKIE['_ew_'.$post->webinar_id])){
	  $_wVisitor = explode('|',base64_decode(($_COOKIE['_ew_'.$post->webinar_id])));
	  if(($_wVisitor[0]!=$post->webinar_id) or ($_wVisitor[1]!=$_SERVER['REMOTE_ADDR'])){
		  $ob_webinar_db_interaction->set_visitor_info($post->webinar_id,$_SERVER['REMOTE_ADDR'],date("Y-m-d H:i:s",$_SERVER['REQUEST_TIME']));
		  setcookie('_ew_'.$post->webinar_id,base64_encode(implode('|',array($post->webinar_id,$_SERVER['REMOTE_ADDR']))),time()+31536000,"/");
	  }
	}else{
		 setcookie('_ew_'.$post->webinar_id,base64_encode(implode('|',array($post->webinar_id,$_SERVER['REMOTE_ADDR']))),time()+31536000,"/");
		 $ob_webinar_db_interaction->set_visitor_info($post->webinar_id,$_SERVER['REMOTE_ADDR'],date("Y-m-d H:i:s",$_SERVER['REQUEST_TIME']));
	}

	$webinar_detail					=	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);
	
	$attachment_type				=	$webinar_detail[0]->attachment_type;
	$attachment_path				=	trim($webinar_detail[0]->attachment_path);
	$attachment_embed_script				=	stripslashes($webinar_detail[0]->attachment_embed_script);
	
	$attachment_format			=	pathinfo($attachment_path,PATHINFO_EXTENSION);
	
	$alternate_video_url		= trim($webinar_detail[0]->reg_alternate_video);		
	$alternate_video_format	=	$ob_webinar_functions->get_video_type($alternate_video_url);
	
	$sharing_detail					=	$ob_webinar_db_interaction->get_sharing_detail($post->webinar_id);	

	$video_length						=	$webinar_detail[0]->webinar_video_length;

	$webinar_pages					=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);

	$login_link							=	get_permalink($webinar_pages[1]->ID);

	$thank_page_link				=	get_permalink($webinar_pages[2]->ID);
	
	$error_page_id 					= $ob_webinar_db_interaction->get_error_page_id(); 
	
	$webinar_timezone_detail=	$ob_webinar_db_interaction->get_timezone_detail($webinar_detail[0]->webinar_timezone_id_fk);
	
	$webinar_timezone_id    = $webinar_detail[0]->webinar_timezone_id_fk;
	$webinar_timezone				=	$webinar_timezone_detail[0]->name;	
	
	$timezones_detail				=	$ob_webinar_db_interaction->get_timezones_detail();
	
	$webinar_schedule_id		=	$webinar_detail[0]->webinar_schedule_type_id_fk;

	$notification_type			=	$webinar_detail[0]->notification_type;	

	$autoresponder_array		=	$ob_webinar_db_interaction->get_notification_details($post->webinar_id);	

	$webinar_days_to_show		=	$webinar_detail[0]->webinar_days_to_show;
	
	$webinar_time_format	  =	$webinar_detail[0]->webinar_time_format;
	
	$html = $autoresponder_text	=	$ob_webinar_functions->_esc_decode_html($webinar_detail[0]->autoresponder_text);				    
	
	$name_field_tag 	= '';
	$email_field_tag  = '';
	$name_field_name  = '';
	$email_field_name = '';
	$url_field_tag	  = '';
	$url_field_name	  = '';
	$other_input_fields = array();
	$form = array();
	
	$keywords   = array("\r\n", "\n", "\r");
	$html = str_replace($keywords, '', $html);
        $html = stripslashes($html);
       
	$pattern = "/\<form\b[^>]*\>(.*[\s\n\t\v\e\b\/]*)\<\/form\>/";
        //$pattern = "/<input[^>]*name\s*=\s*[\"'](.*?\[.*?\])['\"]/i";
        //$pattern = "/\<form\b[^>]*\>(.*[\s\n\t\v\e\b\/]*)\<\/form\>/"; 
	preg_match($pattern, $html, $matches);
	$form_container = $matches[0];
        
	$pattern = "/\<form\b[^>]*\>/";
	preg_match($pattern, $form_container, $matches);
	$starting_form = $matches[0];
       
    
        //$starting_fform = $matches[0];
	$pattern = '/\<input\b[^>]*[\s\/]*>/';
	preg_match_all($pattern, $form_container, $matches);
	$arr_input_fields = $matches[0];
      
       
        //print_r($arr_input_fields);
        
	$pattern = '/\<input\b[^>]* type=[\'"]{1}(text|email)[\'"]{1}[^>]*[\s\/]*>/i';
	preg_match_all($pattern, $form_container, $matches);
        //print_r($matches);
        $field_type	=	implode("",$matches[0]);
        
        if(strstr($starting_form,'ymlp.com') != FALSE)
        {
        $total_input_fields = sizeof($matches[0]);
        if($total_input_fields >=2){
            for($i=0;$i<2;$i++){
               
                $input_checker = $matches[0][$i];
                
                $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
                preg_match_all($pattern, $input_checker, $matching);
                if($i == 0){
                    $email_field_name = $matching[1][0];
                }
                if($i == 1){
                    $name_field_name = $matching[1][0];
                }
            }
        }
        } else {
	$pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*NAME*[0-9A-Za-z-_])[\'"]{1}[^>]*[\s\/]*>/i';
	preg_match_all($pattern, $field_type, $match);
	if(isset($match[1][0])){
	 $name_field_name = $match[1][0];
         
	}else{
	 $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*CustomFields[\[][0-9]*[\]])[\'"]{1}[^>]*[\s\/]*>/i';
	 preg_match_all($pattern, $field_type, $match);
	 if(isset($match[1][0])){
		$name_field_name = $match[1][0];
	 }else{
		 $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*NAME*[0-9A-Za-z-_ (]*[() ])[\'"]{1}[^>]*[\s\/]*>/i';
		 preg_match_all($pattern, $field_type, $match);
		 if(isset($match[1][0])){
			$name_field_name = $match[1][0];
                 }
                 else {
                    $pattern = '/\<input\b[^>]* name=[\'"]{1}(NAME*[0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
            preg_match_all($pattern, $field_type, $match);
            $name_field_name = $match[1][0];
                }
		}
                
	}
 
	$pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*EMAIL*[0-9A-Za-z-_])[\'"]{1}[^>]*[\s\/]*>/i';
	preg_match_all($pattern, $field_type, $match);
	if(isset($match[1][0])){
	$email_field_name = $match[1][0];
        
        }else{
            $pattern = '/\<input\b[^>]* name=[\'"]{1}(EMAIL*[0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
            //\<input\b[^>]* name=[\'"]{1}(EMAIL*[a-z])[\'"]{1,2}[^>]*[\s\/]*>
            preg_match_all($pattern, $field_type, $match);
            $email_field_name = $match[1][0];
         
        }
	}
	//echo $email_field_name .$name_field_name;
	//print_r(array_values(array_filter($arr_input_fields)));
	//unset($arr_input_fields[12]);
	//unset($arr_input_fields[11]);
	for($i=0;$i<count($arr_input_fields);$i++){
          
		if($name_field_name!="" && (strstr($arr_input_fields[$i],'"' . $name_field_name.'"')!=FALSE || strstr($arr_input_fields[$i],"'" . $name_field_name."'")!=FALSE)){
			$name_field_tag = $arr_input_fields[$i];	
		}elseif($email_field_name!="" && (strstr($arr_input_fields[$i],'"' . $email_field_name.'"')!=FALSE || strstr($arr_input_fields[$i],"'" . $email_field_name."'")!=FALSE)){
			$email_field_tag = $arr_input_fields[$i];	
		}else{
			$other_input_fields[] = $arr_input_fields[$i];
		}
	}
	//echo $name_field_tag;
	//echo $email_field_tag;
	$youtube_pattern 	= '/youtube.com/';
	$rtmp_pattern 	 	= '/rtmp:\/\//';
	$vimeo_pattern	 	= '/vimeo.com/';
	
	$stream = "";
	$service = "";
	if(isset($attachment_path) && trim($attachment_path)!=""){
		if(preg_match($youtube_pattern, $attachment_path)){
			$service = "youtube";
		}elseif(preg_match($rtmp_pattern, $attachment_path)){
			$service =	"stream";
			$attachment_path = explode('/', $attachment_path);
			$stream = $attachment_path[count($attachment_path)-1];
			unset($attachment_path[count($attachment_path)-1]);
			$attachment_path = implode('/',$attachment_path);
		}elseif(preg_match($vimeo_pattern, $attachment_path)){
			$service = "vimeo";
		}else{
			$service = "other";
		}
	}
	
	$device_type 	= 	$ob_webinar_functions->get_device_type();
	$iPod 				= 	$device_type['iPod'];
	$iPhone 			= 	$device_type['iPhone'];
	$iPad 				= 	$device_type['iPad'];
	$Android 			=   $device_type['Android'];
	$AndroidTablet=		$device_type['AndroidTablet'];
	
	$webinar_sessions =	array();	
	
	//**************************************************************************//
		 $today = date('Y-m-d',strtotime("-12 hours"));
		 $flag_check_webinar_end_date = $ob_webinar_db_interaction->check_webinar_validity_for_end_date($today,$webinar_detail[0]->webinar_end_date);

		if($flag_check_webinar_end_date==false){
			$_SESSION['not_found'] = 'Webinar is expired';
			$error_page			   =  get_permalink($error_page_id);
			header("Location: ".$error_page);
			exit;
		}
		
		$dates_array 		    = 	$ob_webinar_db_interaction->get_webinar_slots($webinar_days_to_show,$webinar_detail[0]->webinar_block_days,$post->webinar_id,$webinar_detail[0]->max_number_of_attendees,$webinar_schedule_id);
	  $is_right_now_enable		= 	$ob_webinar_db_interaction->check_webinar_right_now_with_fixed_timezone($webinar_detail[0]->is_right_now_enable,$webinar_detail[0]->webinar_start_date,$webinar_detail[0]->webinar_timezone_id_fk,$webinar_schedule_id,$post->webinar_id,$webinar_detail[0]->event_type);
		
		if(count($dates_array)==0 and $is_right_now_enable==false){
			$_SESSION['not_found'] = 'Webinar registration date is unavailable';
			$error_page    		   	 = get_permalink($error_page_id);
			header("Location: ".$error_page);
			exit;
		}

		$web_date = $dates_array[0];
	
		$day_name = date("N", strtotime($web_date));
		$webinar_id = $post->webinar_id;
		$max_attendee = $webinar_detail[0]->max_number_of_attendees;
		$everyday_session_detail	=	$ob_webinar_db_interaction->get_registered_attendees_for_everyday($post->webinar_id,$web_date);
		if(count($everyday_session_detail)>0){
			$invalid_sessions	=	"";	
			$total_rows				=	count($everyday_session_detail);
			$i=1;
			foreach($everyday_session_detail as $session_detail){
				if($session_detail->counts==$webinar_detail[0]->max_number_of_attendees){
					$invalid_sessions.=	$session_detail->webinar_schedule_id_fk;
					if($i!=$total_rows){
						$invalid_sessions.=	",";	
					}
					$i++;
				}	
				if($session_detail->counts!=0){
					 $scheduled_date= $web_date;
				}
			}
			$last_char = $invalid_sessions[strlen($invalid_sessions)-1];
			if($last_char==","){
				$invalid_sessions	=	substr($invalid_sessions,0,strlen($invalid_sessions)-1);	
			}
			
			if($webinar_schedule_id==4){
				if($max_attendee==0){
					$condition		=	 '';
				}else {
						$condition	=	 'having count<'.$max_attendee;
				}
				$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
			}else {
				$webinar_sessions	=	$ob_webinar_db_interaction->get_unexpired_webinar_sessions($post->webinar_id,$invalid_sessions);
			}
			
			$webinar_date	=	$scheduled_date;			
		}else{
			if($webinar_schedule_id==4){
				if($max_attendee==0){
						$condition	=	 '';
				}else {
						$condition	=	 'having count<'.$max_attendee;
				}
				$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
				
			}else {
			  	$webinar_sessions =	$ob_webinar_db_interaction->get_today_unexpired_session_for_everyday_mod($post->webinar_id,$webinar_detail[0]->max_number_of_attendees);
			    
			}
			
			if(count($webinar_sessions)==0){
				 $webinar_date = $web_date;
				if($webinar_schedule_id==4){
					if($max_attendee==0){
							$condition =	'';
					}else {
							$condition =	'having count<'.$max_attendee;
					}
					$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
				}else {
					$webinar_sessions	=	$ob_webinar_db_interaction->get_webinar_sessions($post->webinar_id);
				}
			}else{
				     $webinar_date = $web_date;
			}
		}
		
	$webinar_reg_template_options	=  $ob_webinar_db_interaction->get_registration_template_data($post->webinar_id);
	include_once(WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/'.$current_template);
?>