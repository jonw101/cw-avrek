<?php
require_once("infusionsoft/isdk.php");
		
class ewpInfusionsoftIntegration extends iSDK{
			
			private $account_id;
			private $account_key;
			public  $is_valid_connection;
			
			
		function __construct($acct_id,$acct_key){
				$this->account_id  			 = $acct_id;
				$this->account_key 			 = $acct_key;
				$this->is_valid_connection = $this->cfgCon($this->account_id,$this->account_key);
		}
			
		public function authenticateCredentials($acct_id,$acct_key){
			 	return $this->cfgCon($acct_id,$acct_key);
		}
			
		public function getTags(){
			
				if(!$this->is_valid_connection) return false;
				
				try{
				return $allTags = $this->dsQueryOrderBy('ContactGroup',1000,0,array('Id'=>'%'),array(
					'GroupCategoryId',
					'GroupDescription',
					'GroupName',
					'Id'
					),'Id',FALSE);	
				}catch(iSDKException $e){
					return false;
				}
		}
		
		public function createTag($tagDetails){
				
				if(!$this->is_valid_connection) return false;
				
				$groupCategoryId = 0;
				try{	
			/* $groupCategoryId = $this->dsAdd('ContactGroupCategory', array(
														'CategoryDescription'     => $tagDetails['desc'],
														'CategoryName'            => $tagDetails['name']
														));
			*/
			
				return $this->dsAdd('ContactGroup', array(
								'GroupCategoryId'         => 0,
								'GroupDescription'        => $tagDetails['desc'],
								'GroupName'               => $tagDetails['name']
								));
			}catch(iSDKException $e){
					return false;
			}
		}
			
		public function getCategories(){
				
				if(!$this->is_valid_connection) return false;
				
				try{
					return $this->dsQuery('ContactGroupCategory',1000,0,array('Id' => '%'),array(
					'CategoryDescription',
					'CategoryName',
					'Id'
					));	
				}catch(iSDKException $e){
					return false;
				}
		}
		
		public function createContactIDbyEmail($userInfo){
			
				if(!$this->is_valid_connection) return false;
				
				try{			
					return $this->addWithDupCheck(array('FirstName'=>$userInfo['FirstName'],'Email'=>$userInfo['Email']),'Email');
				}catch(iSDKException $e){
					return false;
				}
		}
		
		public function applyTagToUser($userID,$tagID){
			
			if(!$this->is_valid_connection) return false;
			try{
				return $this->grpAssign($userID,$tagID);
			}catch(iSDKException $e){
					return false;
			}
		}
		
		public function grpRemoveFromUser($userID,$tagID){
			
			if(!$this->is_valid_connection) return false;
			try{
			return $this->grpRemove($userID,$tagID);
			}catch(iSDKException $e){
					return false;
			}
		}
		
		public function checkIfTagAssignedToUser($userID,$tagID){
				
				if(!$this->is_valid_connection) return false;
				
				try{
					$searchQuery	= array('ContactId'=>$userID,'GroupId'=>$tagID);
					$tagExists = $this->dsQuery("ContactGroupAssign",1,0,$searchQuery,array('GroupId'));
					if(empty($tagExists)) return false;	else return true;
				}catch(iSDKException $e){
					return false;
				}
				
		}
}
?>