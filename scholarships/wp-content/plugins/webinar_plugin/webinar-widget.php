<?php
/**
 *	Plugin Name: Easy Webinar Widget
 *	Plugin URI:	 http://easywebinarplugin.com/
 *	Description: Displays latest dates of a webinar or webinar registration page as a widget.
 *	Author: <a href="http://caseyzemanonline.com/" target="_blank" >CSZ/Casey Zeman</a> and <a href="http://www.softobiz.com/" target="_blank" >Softobiz</a>.
 *	Version: EWP(3.8)
 */
error_reporting(0);
function webinar_load_widgets() {
	register_widget( 'Webinar_Widget');
}
add_action('widgets_init','webinar_load_widgets');

class Webinar_Widget extends WP_Widget {
	function Webinar_Widget() {

		$widget_ops = array( 'classname' => 'webinar', 'description' => __('Displays latest dates of a webinar or webinar registration page as a widget.', 'webinar') );
		$control_ops = array( 'width' => 250, 'height' => 150, 'id_base' => 'webinar-widget' );
		$this->WP_Widget( 'webinar-widget', __('Webinar Widget', 'webinar'), $widget_ops, $control_ops );
	}

	function get_all_webinar(){
	 	global $wpdb;
		$wpdb->all_webinar		=	"webinar";
		$all_webinar			=	$wpdb->get_results("SELECT * FROM $wpdb->all_webinar");
		return $all_webinar;
	 }

	function widget( $args, $instance ) {
		extract( $args );
		global $post;

		$title = apply_filters('widget_title', $instance['title'] );
	 	$width = $instance['width'];
	  $webinar_id = (int)$instance['webinar_id'];
		$show_page_id = (int)$instance['show_where_page_id'];
    $webinar_widget_version = $instance['webinar_widget_version'];
		$widget_color = $instance['widget_color'];

		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		$wpdb->all_webinar			=	"webinar";
		$all_webinar						=	$wpdb->get_results("SELECT * FROM $wpdb->all_webinar WHERE webinar_id_pk=$webinar_id");
		$wpdb->posts						=	$table_name;
		$page_id								=	$wpdb->get_results("SELECT * FROM $wpdb->posts WHERE webinar_id=$webinar_id");
		$selected_template_dir	= $all_webinar[0]->template_style;
		$selected_template_color= $all_webinar[0]->template_color;
		if($show_page_id==$post->ID){
			if($webinar_widget_version=='1'){
				$this->get_registration_widget_html(array('title'=>$title,'webinar_width'=>$width,'webinar_id'=>$webinar_id,'color'=>$widget_color,'webinar_widget_version'=>$webinar_widget_version));
			}elseif($webinar_widget_version=='0'){
				$this->get_widget_html(array('title'=>$title,'webinar_width'=>$width,'webinar_id'=>$webinar_id,'color'=>$widget_color));	
			}
		}
	}
	
	function get_registration_widget_html($widget_options){
		if(!session_id()) session_start();
		global $wpdb;
		
		$ob_webinar_db_interaction	=	new webinar_db_interaction();
		$ob_webinar_functions				=	new webinar_functions();
		
		$webinar_id			= 	(int)$widget_options['webinar_id'];
		$width 					= 	(int)$widget_options['webinar_width'];
		$widget_width 	= 	$width.'px';
		$theme_color 		= 	$widget_options['color'];
			
		if(empty($theme_color)){
		 $theme_color 				= 	'blue';
		}
		if(empty($width)){
		 $widget_width 				= 	'240px';
		}
			
		$webinar_detail				=	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
	
		if($webinar_detail){
				
			$webinar_pages				=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($webinar_id);	
		
			$video_length					=	$webinar_detail[0]->webinar_video_length;
		
			$webinar_pages				=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($webinar_id);
		
			$login_link						=	get_permalink($webinar_pages[1]->ID);
		
			$thank_page_link			=	get_permalink($webinar_pages[2]->ID);	
				
			$webinar_timezone_detail=	$ob_webinar_db_interaction->get_timezone_detail($webinar_detail[0]->webinar_timezone_id_fk);
			$webinar_timezone				=	$webinar_timezone_detail[0]->name;	
			
			$timezones_detail				=	$ob_webinar_db_interaction->get_timezones_detail();
		
			$webinar_schedule_id		=	$webinar_detail[0]->webinar_schedule_type_id_fk;
		
			$notification_type			=	$webinar_detail[0]->notification_type;	
		
			$autoresponder_array		=	$ob_webinar_db_interaction->get_notification_details($webinar_id);	
		
			$webinar_days_to_show		=	$webinar_detail[0]->webinar_days_to_show;
			
			$autoresponder_text			=	stripslashes($webinar_detail[0]->autoresponder_text);
		
			$autoresponder_text			=	html_entity_decode(stripslashes($webinar_detail[0]->autoresponder_text));
			$html										=	$autoresponder_text;
			$name_field_tag = '';
			$email_field_tag = '';
			$name_field_name = '';
			$email_field_name = '';
			$other_input_fields = array();
			$form = array();
			
			$keywords   = array("\r\n", "\n", "\r");
			$html = str_replace($keywords, '', $html);
			htmlentities($html);
			$pattern = "/\<form\b[^>]*\>(.*[\s\n\t\v\e\b\/]*)\<\/form\>/";
			
			preg_match($pattern, $html, $matches);
			$form_container = $matches[0];
		
			$pattern = "/\<form\b[^>]*\>/";
			preg_match($pattern, $form_container, $matches);
			$starting_form = $matches[0];
		
			$pattern = '/\<input\b[^>]*[\s\/]*>/';
			preg_match_all($pattern, $form_container, $matches);
			$arr_input_fields = $matches[0];
		
			$pattern = '/\<input\b[^>]* type=[\'"]{1}(text|email)[\'"]{1}[^>]*[\s\/]*>/i';
			preg_match_all($pattern, $form_container, $matches);
			$field_type	=	implode("",$matches[0]);
			if(strstr($starting_form,'ymlp.com') != FALSE)
        {
        $total_input_fields = sizeof($matches[0]);
        if($total_input_fields >=2){
            for($i=0;$i<2;$i++){
               
                $input_checker = $matches[0][$i];
                
                $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
                preg_match_all($pattern, $input_checker, $matching);
                if($i == 0){
                    $email_field_name = $matching[1][0];
                }
                if($i == 1){
                    $name_field_name = $matching[1][0];
                }
            }
        }
        } else {
			$pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*NAME*[0-9A-Za-z-_])[\'"]{1}[^>]*[\s\/]*>/i';
			preg_match_all($pattern, $field_type, $match);
			if(isset($match[1][0])){
			 $name_field_name = $match[1][0];
			}else{
			 $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*CustomFields[\[][0-9]*[\]])[\'"]{1}[^>]*[\s\/]*>/i';
			 preg_match_all($pattern, $field_type, $match);
			 if(isset($match[1][0])){
				$name_field_name = $match[1][0];
			 }else{
				 $pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*NAME*[0-9A-Za-z-_ (]*[() ])[\'"]{1}[^>]*[\s\/]*>/i';
				 preg_match_all($pattern, $field_type, $match);
				 if(isset($match[1][0])){
					$name_field_name = $match[1][0];
				 }
			 else {
                    $pattern = '/\<input\b[^>]* name=[\'"]{1}(NAME*[0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
            preg_match_all($pattern, $field_type, $match);
            $name_field_name = $match[1][0];
                }
				}
			}
		
			$pattern = '/\<input\b[^>]* name=[\'"]{1}([0-9A-Za-z-_]*EMAIL*[0-9A-Za-z-_])[\'"]{1}[^>]*[\s\/]*>/i';
			preg_match_all($pattern, $field_type, $match);
			//print_r($match);
			if(isset($match[1][0])){
			$email_field_name = $match[1][0];
			}
		else{
            $pattern = '/\<input\b[^>]* name=[\'"]{1}(EMAIL*[0-9A-Za-z-_]+)[\'"][^>]*[\s\/]*>/i';
            //\<input\b[^>]* name=[\'"]{1}(EMAIL*[a-z])[\'"]{1,2}[^>]*[\s\/]*>
            preg_match_all($pattern, $field_type, $match);
            $email_field_name = $match[1][0];
         
           }
		}
			//print_r($arr_input_fields);
			for($i=0;$i<count($arr_input_fields);$i++){
				if($name_field_name!="" && (strstr($arr_input_fields[$i],'"' . $name_field_name.'"')!=FALSE || strstr($arr_input_fields[$i],"'" . $name_field_name."'")!=FALSE)){
					$name_field_tag = $arr_input_fields[$i];	
				}elseif($email_field_name!="" && (strstr($arr_input_fields[$i],'"' . $email_field_name.'"')!=FALSE || strstr($arr_input_fields[$i],"'" . $email_field_name."'")!=FALSE)){
					$email_field_tag = $arr_input_fields[$i];	
				}elseif($url_field_name!="" && strstr($arr_input_fields[$i],$url_field_name)!=FALSE){
					$url_field_tag = $arr_input_fields[$i];
				}
				else{
					$other_input_fields[] = $arr_input_fields[$i];
				}
			}
			
			$form[] = $starting_form;
			$form[] = $name_field_tag;
			$form[] = $email_field_tag;
			$form[] = implode('',$other_input_fields);
			$form[] = '</form>';
			$webinar_sessions =	array();	
			
				$today = date('Y-m-d',strtotime("-12 hours"));
				$flag_check_webinar_end_date = $ob_webinar_db_interaction->check_webinar_validity_for_end_date($today,$webinar_detail[0]->webinar_end_date);
		
				if($flag_check_webinar_end_date==false){
					echo '<p>Webinar is expired</p>';
					exit;
				}
				
				$dates_array = $ob_webinar_db_interaction->get_webinar_slots($webinar_days_to_show,$webinar_detail[0]->webinar_block_days,$webinar_id,$webinar_detail[0]->max_number_of_attendees,$webinar_schedule_id);
				
				$is_right_now_enable		= 	$ob_webinar_db_interaction->check_webinar_right_now_with_fixed_timezone($webinar_detail[0]->is_right_now_enable,$webinar_detail[0]->webinar_start_date,$webinar_detail[0]->webinar_timezone_id_fk,$webinar_schedule_id,$webinar_id,$webinar_detail[0]->event_type);
		
				if(count($dates_array)==0 and $is_right_now_enable==false){
					echo '<p>Webinar registration date is unavailable</p>';
					exit;
				}
		
		 	  $web_date 							 = $dates_array[0];
				$day_name								 = date("N", strtotime($web_date));
				$webinar_id 						 = $webinar_id;
				$max_attendee 					 = $webinar_detail[0]->max_number_of_attendees;
				 $everyday_session_detail= $ob_webinar_db_interaction->get_registered_attendees_for_everyday($webinar_id,$web_date);
				
				if(count($everyday_session_detail)>0){
					$invalid_sessions		=	"";	
					$total_rows					=	count($everyday_session_detail);
					$i=1;
					foreach($everyday_session_detail as $session_detail){
						if($session_detail->counts==$webinar_detail[0]->max_number_of_attendees){
							$invalid_sessions.=	$session_detail->webinar_schedule_id_fk;
							if($i!=$total_rows){
								$invalid_sessions.=	",";	
							}
		
							$i++;
						}	
						if($session_detail->counts!=0){
							 $scheduled_date	= $web_date;
						}
					}
					$last_char = $invalid_sessions[strlen($invalid_sessions)-1];
					if($last_char==","){
						$invalid_sessions	=	substr($invalid_sessions,0,strlen($invalid_sessions)-1);	
					}
					
					if($webinar_schedule_id==4){
						
						if($max_attendee==0){
							$condition			=	 '';
						}else {
								$condition		=	 'having count<'.$max_attendee;
						}
						
						$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
						
					}else {
						$webinar_sessions	=	$ob_webinar_db_interaction->get_unexpired_webinar_sessions($webinar_id,$invalid_sessions);
					}
					$webinar_date				=	$scheduled_date;			
				}else{
					if($webinar_schedule_id==4){
						if($max_attendee==0){
							$condition	=	 '';
						}else {
							$condition	=	 'having count<'.$max_attendee;
						}
						$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date' and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name' group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
					}else {
						$webinar_sessions =	$ob_webinar_db_interaction->get_today_unexpired_session_for_everyday_mod($webinar_id,$webinar_detail[0]->max_number_of_attendees);
					}
					
					if(count($webinar_sessions)==0){
						 $webinar_date = $web_date;
						if($webinar_schedule_id==4){
							if($max_attendee==0){
							$condition				=	'';
							}else {
									$condition		=	'having count<'.$max_attendee;
							}
							
							$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." order by t1.start_time ASC");
										
						}else {
							$webinar_sessions	=	$ob_webinar_db_interaction->get_webinar_sessions($webinar_id);
						}
					}else{
						$webinar_date 			= $web_date;
					}
				}
		
			$widget_options_webinar_id = $webinar_id;
		?>
<?php if(count($webinar_sessions)>0){ ?>
<link type="text/css" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'templates/widget/css/'.$theme_color.'.css'?>"  />
<link type="text/css" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'templates/widget/css/fonts.css'?>"  />
<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]--><!--[if IE 8]><style type="text/css">.drop{display:none;}</style><![endif]-->
<h3 class="widget-title"><?php echo $widget_options['title']; ?></h3>
<div id="easywebinarplugin_widget_form">
  <div class="form_wrapper" style="width:<?php echo $widget_width;?>;">
    <div id="loader_wrapper"><img src="<?php echo WEBINAR_PLUGIN_URL.'templates/widget/images/loading.gif'; ?>" alt="loader" /></div>
    <div class="form_inner_wrapper">
      <div class="header gradient">
        <div class="heading">RESERVE YOUR SPOT!</div>
        <div class="clr"></div>
      </div>
      <div class="max-size">
        <div class="form_content">
          <div class="blueheading"><?php if($webinar_detail[0]->event_type==1) echo 'Webinar Date'; else echo 'Which day do you want to attend?';?></div>
          <?php if($webinar_detail[0]->notification_type==1){ ?>
          <form name="" id="" action="" method="post" >
            <?php }else{ ?>
            <?php echo $starting_form; ?>
            <?php } ?>
            <?php if($webinar_detail[0]->event_type==1){ ?>
              <center class="webinar_dates" style="min-height:28px;padding-top:5px;padding-left:10px;color:#797979;font-family:'Conv_MyriadPro-Regular';"></center>
            <?php } ?>
            <div class="checkbox_wrapper" <?php if($webinar_detail[0]->event_type==1) echo 'style="display:none"'?> ><span class="drop"></span>
              <?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
              <input type="hidden" name="ewp_requested_name" id="ewp_requested_name" value="<?php if(!empty($_REQUEST['ew-name'])) echo trim($_REQUEST['ew-name'])?>" />
              <input type="hidden" name="ewp_requested_mail" id="ewp_requested_mail" value="<?php if(!empty($_REQUEST['ew-email'])) echo trim($_REQUEST['ew-email'])?>" />
              <?php } ?>
              <input type="hidden" name="admin_tz" id="admin_tz" value="<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>" />
              <input type="hidden" name="timezone_gmt" id="timezone_gmt"/>
              <input type="hidden" name="timezone_gmt_operation" id="timezone_gmt_operation"/>
              <input type="hidden" name="timezone_string" id="timezone_string"/>
              <input type="hidden" name="webinar_real_date" id="webinar_real_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>" />
              <input type="hidden" name="TIME" id="webinar_time"  />
              <input type="hidden" name="DATE" id="webinar_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>"  />
              <?php if($webinar_detail[0]->webinar_timezone_id_fk>0) { ?>
              <input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->timezone_gmt_symbol; ?>" value="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->GMT; ?>" />
              <?php }else{ ?>
              <input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
              <?php } ?>
              <select name="webinar_dates" id="webinar_dates" class="checkbox"></select>
            </div>
            <div style="display:none;" id="max_attendee"><?php echo $webinar_detail[0]->max_number_of_attendees;?></div>
            <div class="blueheading">Which time works best for you? </div>
            <div class="error" id="message_div"></div>
            <center>
              <div class="local_time">Your local time <span id="local_time"></span></div>
            </center>
            <div class="checkbox_wrapper setmargin"> <span class="drop"></span>
              <select name="select_webinar_session" id="select_webinar_session" class="checkbox">
                <option value="0">Select desired time</option>
              </select>
            </div>
            <div class="blueheading">Where to send the invitation?</div>
            <?php if($webinar_detail[0]->notification_type==1){ ?>
            <div class="input">
              <input autocomplete="off" name="NAME" id="NAME" class="textbox nobg" type="text" onfocus="if(this.value == 'Enter your name here...'){this.value = '';}" onblur="if(this.value == ''){this.value='Enter your name here...';}" value="Enter your name here..." />
            </div>
            <div class="input">
              <input autocomplete="off" name="EMAIL" id="EMAIL" class="textbox nobg" type="text" onfocus="if(this.value == 'Enter your email here...'){this.value = '';}" onblur="if(this.value == ''){this.value='Enter your email here...';}" value="Enter your email here..." />
            </div>
            <center>
              <div class="button_frame gradient">
                <input class="button gradient" name="register" id="ewp_sumit" type="submit" value="Register Now" style="cursor:pointer;display:block;width:138px;height:40px;line-height:40px;text-align:center;text-transform:uppercase;font-size:14px;
	text-decoration:none;border:none;border-radius:24px;overflow:hidden;" />
              </div>
            </center>
            <?php }else{
            	
							 if($name_field_name!='' && $email_field_name!=''){ ?>
            <div class="input"> <?php echo $name_field_tag; ?> </div>
            <div class="input"> <?php echo $email_field_tag; ?> </div>
            <?php echo '<div id="autoresponder_extra_fields" style="display:none;height:0px;">'.implode('',$other_input_fields).'</div>'; ?>
            <input type="hidden" name="webinar_username" id="webinar_username" value="<?php echo $name_field_name; ?>"  />
            <input type="hidden" name="webinar_email" id="webinar_email" value="<?php echo $email_field_name; ?>"  />
            <center>
              <div class="button_frame gradient">
                <input class="button gradient" name="register" id="ewp_sumit" type="submit" value="Register Now" style="cursor:pointer;display:block;width:138px;height:40px;line-height:40px;text-align:center;text-transform:uppercase;font-size:14px;
        text-decoration:none;border:none;border-radius:24px;overflow:hidden;" />
              </div>
            </center>
            <?php }else{ ?>
            <div>Something went wrong with registration fields.</div>
            <!--Autoresponder Form Error -->
            <?php } 
					 } ?>
          </form>
          <div class="clr"></div>
        </div>
      </div>
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
  </div>
</div>
<!--end of #easywebinarplugin_widget_form -->
<div id="easywebinarplugin_event_box" style="display:none;">
<div class="form_wrapper" style="width:<?php echo $widget_width;?>;">
  <div class="form_inner_wrapper">
    <div class="header gradient">
      <div class="heading">THANK YOU<br/>
        FOR REGISTRATION</div>
      <div class="clr"></div>
    </div>
    <div class="max-size">
      <div class="form_content">
        <div class="blueheading fontsize"><span><img src="<?php echo WEBINAR_PLUGIN_URL.'templates/widget/images/ticket_icon.jpg';?>" alt="ticket icon" /></span>Your Webinar Ticket </div>
        <div class="thankyou_info">
          <table align="center" width="100%" border="0" cellspacing="8" cellpadding="5">
            <tr>
              <td class="name_title"> Webinar: </td>
              <td><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); ?></td>
            </tr>
            <tr>
              <td> Host: </td>
              <td><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></td>
            </tr>
            <tr>
              <td class="name_title">Name:</td>
              <td id="reg_name"></td>
            </tr>
            <tr>
              <td class="name_title"> Email:</td>
              <td id="reg_email"></td>
            </tr>
            <tr>
              <td class="name_title"> Date:</td>
              <td id="reg_date"></td>
            </tr>
            <tr>
              <td class="name_title"> Time:</td>
              <td id="reg_time"></td>
            </tr>
          </table>
        </div>
        <div class="blueheading">Your Custom Link to the Webinar </div>
        <div class="customlink"> <a id="easywebinarplugin_event_link" href="" target="_blank"></a> </div>
        <div class="clr"></div>
      </div>
    </div>
    <div class="clr"></div>
  </div>
  <div class="clr"></div>
</div>
<?php 
		 include_once("templates/js/webinar-registration.php");
		}else{ echo "<p>Webinar is currently disabled.</p>"; exit;}
				
		}else{
			 //If invalid $webinar_id
			echo "<p>Sorry! didn't find the webinar you wanted.</p>";
			exit; 
		}
				
	}	//func get_registration_widget_html

function get_widget_html($widget_options){
		$title 				= $widget_options['title'];
		$width 				= (int)$widget_options['webinar_width'];
		$widget_width = $width.'px';
		$webinar_id 	= (int)$widget_options['webinar_id'];
		$theme_color 	= $widget_options['color'];
		
		global $wpdb;
		$wpdb->all_webinar=	"webinar";
		$all_webinar			=	$wpdb->get_results("SELECT * FROM $wpdb->all_webinar WHERE webinar_id_pk=$webinar_id");
	
		if(count($all_webinar)>0){
		$table_name 						= $wpdb->prefix.'posts';
		$wpdb->posts						=	$table_name;
		$page_id								=	$wpdb->get_results("SELECT * FROM $wpdb->posts WHERE webinar_id=$webinar_id ORDER BY ID");
		$selected_template_dir  = $all_webinar[0]->template_style;
		$selected_template_color= $all_webinar[0]->template_color;

		/* Object */
		$ob_webinar_db_interaction	=	new webinar_db_interaction();
		$ob_webinar_functions				=	new webinar_functions();

		?>
<link type="text/css" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'templates/widget_theme/css/widget.css'  ?>"  />
<link type="text/css" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'templates/widget_theme/css/style.css'  ;?>" />
<link type="text/css" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'templates/widget_theme/'.$theme_color.'/css/style.css'  ;?>" />
<?php
		$webinar_schedule_id= $all_webinar[0]->webinar_schedule_type_id_fk;
		$pageid 						= $page_id[0]->ID;
		$webinar_sessions		=	array();
		echo $before_widget;
		$max_attendee 			= $all_webinar[0]->max_number_of_attendees;
		$dates_array 				= $ob_webinar_db_interaction->get_webinar_slots($all_webinar[0]->webinar_days_to_show,$all_webinar[0]->webinar_block_days,$webinar_id,$max_attendee,$webinar_schedule_id);
?>
<?php if(count($dates_array)>0){ ?>
<h3 class="widget-title"><?php echo  $widget_options['title']; ?></h3>
<div class="widget_container clearfix" style=" <?php if(! empty($width) ){ echo "min-width:$widget_width;";} ?>">
  <form class="widget_form"  name="date" id="opt_in_form" action="<?php echo bloginfo('url')?>?page_id=<?php echo $pageid; ?>" method="GET">
    <div class="widget_header">
      <div class="widget_header_border"></div>
      <h1 style="font-size:14px;" >Choose the best day for you</h1>
    </div>
    <div class="widget_content clearfix">
      <div class="widget_list">
        <ul>
          <?php
						$i = 0;
						foreach($dates_array as $value){
						$date = strtotime($value); ?>
          <li>
            <input type="radio" <?php if($i==0){?> checked="checked" <?php } ?>value="<?php echo $value; ?>" name="webinar_date"  class="widget_radio_button" />
            <label style="font-size:12px;"><?php echo date('l,F,jS',$date); ?></label>
          </li>
          <?php $i++;}?>
        </ul>
        <input type="hidden" name="page_id" value="<?php echo $pageid; ?>">
      </div>
      <div class="clear"></div>
      <div class="center">
        <div class="clear"></div>
        <input name="submit" type="submit" class="widget_submit" value="Register Now" style="font-size:14px;"/>
        <div class="clear"></div>
      </div>
    </div>
  </form>
</div>
<?php  } else { ?>
<div class="msg_container_wrap clearfix">
  <div class="msg_container">
    <div class="expired_msg_icon"></div>
    <div class="expired_msg_devider"></div>
    <h1>Webinar has been expired</h1>
  </div>
</div>
<?php } 	
		echo $after_widget;
		}else {?>
<p>Didn't find the page you wanted.</p>
<?php }
}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] 			 					 = strip_tags( $new_instance['title'] );
		$instance['width'] 			 					 = strip_tags( $new_instance['width'] );
		$instance['widget_color']					 = $new_instance['widget_color'];
		$instance['webinar_id']  					 = $new_instance['webinar_id'];
	  $instance['show_where_page_id'] 	 = $new_instance['show_where_page_id'];
		$instance['webinar_widget_version']= $new_instance['webinar_widget_version'];
		return $instance;
	}

	function form( $instance ) {
			$defaults = array( 'title' => __('Webinar', 'webinar'), 'pageid' => __('', 'webinar'));
			$instance = wp_parse_args( (array) $instance, $defaults );
	
			global $wpdb;
			$wpdb->all_webinar		=	"webinar";
			$all_webinar			=	$wpdb->get_results("SELECT * FROM $wpdb->all_webinar ORDER BY created_date DESC, modified_date DESC");
	
			?>
<p>
  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
    <?php _e('Title:', 'hybrid'); ?>
  </label>
  <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat" />
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'width' ); ?>">
    <?php _e('Width (pixels): >239 (e.g. 240)', 'width'); ?>
  </label>
  <input id="<?php echo $this->get_field_id( 'width' ); ?>" name="<?php echo $this->get_field_name( 'width' ); ?>" value="<?php echo $instance['width']; ?>" type="text" class="widefat" />
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'widget_color' ); ?>">
    <?php _e('Theme:', 'widget_color'); ?>
  </label>
  <select id="<?php echo $this->get_field_id( 'widget_color' ); ?>" name="<?php echo $this->get_field_name( 'widget_color' ); ?>" style="width:100%;">
    <option value="blue" <?php selected( $instance['widget_color'], "blue"); ?>>Blue</option>
    <option value="green" <?php selected( $instance['widget_color'],"green"); ?>>Green</option>
    <option value="orange" <?php selected( $instance['widget_color'],"orange"); ?>>Orange</option>
    <option value="red" <?php selected( $instance['widget_color'],"red"); ?>>Red</option>
    </option>
  </select>
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'show_where_page_id' ); ?>">
    <?php _e('Page ID: (where you want the widget)', 'show_where_page_id'); ?>
  </label>
  <input id="<?php echo $this->get_field_id( 'show_where_page_id' ); ?>" name="<?php echo $this->get_field_name( 'show_where_page_id' ); ?>" value="<?php echo $instance['show_where_page_id']; ?>" type="text" class="widefat"/>
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'webinar_widget_version' ); ?>">
    <?php _e('Version:', 'webinar'); ?>
  </label>
  <select class="widefat" id="<?php echo $this->get_field_id( 'webinar_widget_version' ); ?>" name="<?php echo $this->get_field_name( 'webinar_widget_version' ); ?>" >
    <option value="1" <?php selected( $instance['webinar_widget_version'], "1"); ?>>Registration</option>
    <option value="0" <?php selected( $instance['webinar_widget_version'], "0"); ?>>Default</option>
  </select>
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'webinar_id' ); ?>">
    <?php _e('Event Name:', 'webinar'); ?>
  </label>
  <select id="<?php echo $this->get_field_id( 'webinar_id' ); ?>" name="<?php echo $this->get_field_name( 'webinar_id' ); ?>" class="widefat" style="width:100%;">
    <?php foreach($all_webinar as $value){ ?>
    <option value="<?php echo $value->webinar_id_pk;?>" <?php selected( $instance['webinar_id'], $value->webinar_id_pk ); ?>"><?php echo $value->webinar_event_name;?></option>
    <?php } ?>
  </select>
</p>
<?php
	}

}
add_action('Webinar_Widget', create_function('','return register_widget("Webinar_Widget");'));

include_once(ABSPATH.'wp-admin/includes/plugin.php');
if(!is_plugin_active('webinar_plugin/webinar-plugin.php')){
	$get_plugins = get_option('active_plugins'); 
	$plugin_to_deactivate='webinar_plugin/webinar-widget.php';	
	for($i=1;$i<=count($get_plugins);$i++){
		if($get_plugins[$i]==$plugin_to_deactivate){
			unset($get_plugins[$i]);
		}
		update_option('active_plugins', $get_plugins );
	}
}


