<?php
/**
 * Template Name: Webinar Countdown
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
//ini_set('display_errors',1);
error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
date_default_timezone_set('GMT');
global $post;
  
  $ob_webinar_db_interaction	=	new webinar_db_interaction();
  $ob_webinar_functions				=	new webinar_functions();
  
  $purge_cache										 =	$ob_webinar_functions->purge_cache();
  $attendee_webinar_session_detail =	$ob_webinar_db_interaction->get_registration_details($ob_webinar_functions->_sanitise_string($_GET['attendee']));
  
  if(isset($_GET['attendee']) && !empty($attendee_webinar_session_detail)){
	  $webinar_date_array					=	explode("-",$attendee_webinar_session_detail[0]->webinar_date);
	  $webinar_time_array					=	explode(":",$attendee_webinar_session_detail[0]->webinar_time);	
	  $webinar_scheduled_time			=	$attendee_webinar_session_detail[0]->webinar_date." ".$attendee_webinar_session_detail[0]->webinar_time;	
	  
	  $webinar_milliseconds				=	strtotime($webinar_scheduled_time);
	  $current_timestamp					=	time();
	  $desired_time_in_milliseconds	=	1000*($webinar_milliseconds-$current_timestamp);
	  
	  $webinar_video_page_id				=	$ob_webinar_db_interaction->get_webinar_video_id($attendee_webinar_session_detail[0]->webinar_id_fk);
	  $webinar_detail								=	$ob_webinar_db_interaction->get_webinar_detail($attendee_webinar_session_detail[0]->webinar_id_fk);
	  $selected_template_dir				=	$webinar_detail[0]->template_style;
	  $selected_template_color			=	$webinar_detail[0]->template_color;
	  $affilate_param = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param);
    $affilate_param_val = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param_val);
    if($affilate_param_val != '' && $affilate_param != ''){
    	$affilate_param_query = '&' . $affilate_param  . '=' . $affilate_param_val;
    }
    else {
    	$affilate_param_query = '';
    	$affilate_param_val = '';
    	$affilate_param ='';
    }
	if($affilate_param_val != '' && $affilate_param != ''){
	  $webinar_video_page_link			=	add_query_arg(array('attendee'=>$attendee_webinar_session_detail[0]->key,$affilate_param =>$affilate_param_val),get_permalink($webinar_video_page_id));
	  }
	  else
	  {
	   $webinar_video_page_link			=	add_query_arg(array('attendee'=>$attendee_webinar_session_detail[0]->key),get_permalink($webinar_video_page_id));
	  }
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if(empty($webinar_detail[0]->webinar_page_title)) bloginfo('name'); else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_page_title);?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php if($selected_template_dir == 'new_theme'){ ?>
<link rel="Stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/css/style.css'; ?>" />
<link rel="Stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/css/main.css'; ?>" />
<?php } else { ?>
<link rel="Stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/css/countdown.css'; ?>" />
<?php }?>
<?php require_once(WEBINAR_PLUGIN_PATH.'templates/js/webinar-countdown.php');?>
</head>
<body>
<noscript>
<center>
  <h2>JavaScript must be enabled in order for you to view webinar.</h2>
</center>
</noscript>
<input type="hidden" name="webinar_video_link" id="webinar_video_link"  value="<?php echo $webinar_video_page_link;?>" />
<?php  if($selected_template_dir == 'new_theme') { 
include('new_counter.php');
 } else { ?>
<div class="header">
 <h1 style="padding:2px 0 4px;" class="darkgrey"><?php if($ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_headline) == ''){ echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); } else { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_headline); } ?></h1>
  <h3 style="font-size: 16pt; letter-spacing: 4px; margin: 13px 0 0;" class="darkgrey"><?php if($ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_subheadline) == ''){ echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->topic); } else { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_subheadline);  } ?></h3>
 </div>
<div class="counter"> 
  <!-- Countdown dashboard start -->
  <div id="countdown_dashboard">
    <div class="dash weeks_dash"> <span class="dash_title">weeks</span>
      <div class="digit">0</div>
      <div class="digit">0</div>
    </div>
    <div class="dash days_dash"> <span class="dash_title">days</span>
      <div class="digit">0</div>
      <div class="digit">0</div>
    </div>
    <div class="dash hours_dash"> <span class="dash_title">hours</span>
      <div class="digit">0</div>
      <div class="digit">0</div>
    </div>
    <div class="dash minutes_dash"> <span class="dash_title">minutes</span>
      <div class="digit">0</div>
      <div class="digit">0</div>
    </div>
    <div class="dash seconds_dash"> <span class="dash_title">seconds</span>
      <div class="digit">0</div>
      <div class="digit">0</div>
    </div>
  </div>
  
  <!-- Countdown dashboard end --> 
</div>
<?php }?>
</body>
</html>