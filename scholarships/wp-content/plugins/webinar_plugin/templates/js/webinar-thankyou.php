<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
<?php if($webinar_detail[0]->thankyou_attachment_type==2 || $sharing_detail[0]->is_video_enabled==1) { ?>
<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player-responsive.js'?>"></script>
<?php }else{ ?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player.js'?>"></script>
<?php } ?>
<?php if($service=='vimeo' || $service_sharing == 'vimeo'){?>
<script type="text/javascript" src="//a.vimeocdn.com/js/froogaloop2.min.js"></script>
<?php } ?>
<link rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelementplayer.css'?>"/>
<script type="text/javascript">
$(function() {
          $('video#thankyou').mediaelementplayer({
				  mode: 'auto',
				  autoplay : true,
				   <?php if($service=='vimeo'){?>
				  isClickable : true,
				  <?php }else{ ?>
				  isClickable : false,
				  <?php } ?>
				  clickToPlayPause : false,
				  iPhoneUseNativeControls: true, 
				  iPadUseNativeControls: true,
				  AndroidUseNativeControls: true,
				  enableKeyboard: true,
				  alwaysShowControls : false,
				  enableAutosize: true,
				  enablePseudoStreaming : true,
				   <?php if($service=='vimeo'){?>
				  hideControlsForVimeo:true,
				  <?php } ?>
				  <?php if($service=='youtube' || $service=='stream'){ ?>
				  features: ['playpause','current','progress','duration','tracks','volume'],
					<?php }else{?>
					features: ['playpause','current','progress','duration','tracks','volume','fullscreen'],
					<?php }?>
				  success: function(player, node) {
				  	
					if (player.pluginType == 'flash') {
         				player.addEventListener('canplay', function() {
							player.play();
						}, false);
					}else{
						<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	}else {?>
						player.play();
						<?php } ?>
					}	
					
					<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
					$('.mejs-overlay-button, .mejs-overlay, .ewp-player-overlay').bind("click mouseover",function() {
					   player.play();
					});
					<?php } ?>				
				
					$('.ewp-player-overlay').bind('contextmenu', function(e) {return false;});
				  }
			});
});
</script>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(){
	$("#footer_container a").each(function(){
		$(this).attr("target","_blank");
		$(this).css("text-decoration",'none');
	});

	$(".print-ticket").click(function(event){
		event.preventDefault();
		window.print();
	})
	
});
	
/*fb api */
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=613968581984183";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
	FB._initialized = false;
	FB.init({appId: '613968581984183', status: true, cookie: true, xfbml: true});
	FB.Canvas.setSize({ width: 520, height: 1500 });
	FB.Event.subscribe('edge.create',
		function(response) {
			$("#video-reward").show();
			$("#document-reward").show();
			$("#reward-doc").trigger('click');
			
			<?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
			$('html,body').animate({
				scrollTop: $("#video-reward").offset().top},
				1000);
		<?php } ?>
				
		<?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
				$('html,body').animate({
				scrollTop: $("#document-reward").offset().top},
				1000);
		 <?php } ?>		
		}
	);
};
/*fb api ends */		
					
/*g+ api */
(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
function reward(plusone){
	   $(function(){	
			$("#video-reward").show();
			$("#document-reward").show();
			$("#reward-doc").trigger('click');
			
			<?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
			$('html,body').animate({
				scrollTop: $("#video-reward").offset().top},
				1000);
		<?php } ?>
				
		<?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
				$('html,body').animate({
				scrollTop: $("#document-reward").offset().top},
				1000);
		<?php } ?>		
	
	  });
}
/*g+ api ends */
</script>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script> 
<script type="text/javascript">
/*twitter api */
	 twttr.events.bind('tweet', function(event) {
				$("#video-reward").show();
				$("#document-reward").show();
				$("#reward-doc").trigger('click');
				
		<?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
			$('html,body').animate({
				scrollTop: $("#video-reward").offset().top},
				1000);
		<?php } ?>
				
		<?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
				$('html,body').animate({
				scrollTop: $("#document-reward").offset().top},
				1000);
		<?php } ?>		
	 });
/*twitter api ends*/

</script>
<?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
<script type="text/javascript">
		$(function() {
				  $('video#sharing_video').mediaelementplayer({
						  mode: 'auto',
						  autoplay : false,
						  <?php if($service_sharing=='vimeo'){?>
						  isClickable : true,
						  <?php }else{ ?>
						  isClickable : false,
						  <?php } ?>
						  clickToPlayPause : false,
						  iPhoneUseNativeControls: true, 
						  iPadUseNativeControls: true,
						  AndroidUseNativeControls: true,
						  enableAutosize: true,
						  enablePseudoStreaming : true,
						   <?php if($service_sharing=='vimeo'){?>
						  hideControlsForVimeo:true,
						  <?php } ?>
						  <?php if($service_sharing=='youtube' || $service_sharing=='stream'){ ?>
							features: ['playpause','current','progress','duration','tracks','volume'],
							<?php }else{?>
							features: ['playpause','current','progress','duration','tracks','volume','fullscreen'],
							<?php }?>
						  success: function(player, node) {
							if (player.pluginType == 'flash') {
								player.addEventListener('canplay', function() {
									player.play();
								}, false);
							}else{
								<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	}else {?>
								player.play();
								<?php } ?>
							}	
							
						<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
						$('.mejs-overlay-button, .mejs-overlay, .ewp-player-overlay').bind("click mouseover",function() {
						    player.play();
						});
						<?php }else{ ?>
						$('.ewp-player-overlay').bind("click mouseover",function() {	
						  if(player.ended==true){
						  player.play();
						  }
						});
						<?php } ?>
							
							$('.ewp-player-overlay').bind('contextmenu', function(e) {return false;});
						  }
					});
					
		});
		</script>
<?php } ?>