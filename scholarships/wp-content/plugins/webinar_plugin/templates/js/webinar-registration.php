<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
<?php if($selected_template_dir == 'new_theme'){ ?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir. '/js/jquery-1.10.1.min.js'; ?> "></script>
<?php } else {?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-view-control/js/jquery.reg.min.js'?>"></script>
<?php } if((!isset($widget_options_webinar_id)) && (empty($widget_options_webinar_id))) {?>
<?php if($attachment_type==2){ ?>
<link rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelementplayer.css'?>"/>
<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player-responsive.js'?>"></script>
<?php }else{ ?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player.js'?>"></script>
<?php } ?>
<?php if($service=='vimeo'){?>
<script type="text/javascript" src="//a.vimeocdn.com/js/froogaloop2.min.js"></script>
<?php } ?>
<script type="text/javascript">
$(function() {
	 $('video').mediaelementplayer({
				  mode: 'auto',
				  autoplay : true,
				  <?php if($service=='vimeo'){?>
				  isClickable : true,
				  <?php }else{ ?>
				  isClickable : false,
				  <?php } ?>
				  clickToPlayPause : false,
				  iPhoneUseNativeControls: true, 
				  iPadUseNativeControls: true,
				  AndroidUseNativeControls: true,
				  enableKeyboard: false,
				  alwaysShowControls : false,
				  enableAutosize: true,
				  enablePseudoStreaming : false,
				  <?php if($service=='vimeo'){?>
				  hideControlsForVimeo:true, 
				  <?php } ?>
				  <?php if($service=='youtube' || $service=='stream'){ ?>
				  features: ['playpause','current','progress','duration','tracks','volume'],
					<?php }else{?>
					features: ['playpause','current','progress','duration','tracks','volume','fullscreen'],
					<?php }?>
				  success: function(player, node) {
				  	
					if (player.pluginType == 'flash') {
         				player.addEventListener('canplay', function() {
							player.play();
						}, false);
					}else{
						<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	}else {?>
						player.play();
						<?php } ?>
					}	
					
					<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
					$('.mejs-overlay-button, .mejs-overlay, .ewp-player-overlay').bind("click mouseover",function() {
					   player.play();
					});
					<?php } ?>
					
					$('.ewp-player-overlay').bind('dblclick', function() {
						player.enterFullScreen();	
					});
					
					$('.ewp-player-overlay').bind('contextmenu', function(e) {return false;});
				  }
	 });
 });
</script>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(){$("#footer_container a").each(function(){$(this).attr("target","_blank");$(this).css("text-decoration","none");$(this).css("color","white")})});
</script>
<?php } //end of widget monitoring  ?>
<script type="text/javascript">
	   var	plugin_url				=	"<?php echo WEBINAR_PLUGIN_URL?>"; 
	   var is_right_now				=	0;
	   
	   function pad(number){
		   if(number>=0){
			 return (parseInt(number,10) < 10 ? '0' : '') + number;  
		   }else{
			 return (parseInt(Math.abs(number),10) < 10 ? '0' : '') + Math.abs(number);
		   }
	   }
	   
	   function local_time_clock(){
		   	
			var local_date = new Date();
			var hours = local_date.getHours();
			var minutes = local_date.getMinutes();
			var seconds = local_date.getSeconds();
			/* var suffix = "AM";
			if (hours >= 12) {
				  suffix = "PM";
				  hours = hours - 12;
			}
			if (hours == 0) {
			  	hours = 12;
			}
			if (minutes < 10){
				minutes = "0" + minutes;
			}

			$("#local_time").html( hours + ":" + minutes + " " + suffix); */
			$("#local_time").val( hours + ":" + minutes + ":" + seconds);
	   }
		
		  function set_local_timezone_for_webinar(){
			local_time_clock();
			var local_date = new Date();
			var local_timezone_offset = local_date.getTimezoneOffset();
		    
			local_timezone_offset = parseFloat((-1)*(parseInt(local_timezone_offset,10)/60)).toFixed(2);
			
			var timezone_array = local_timezone_offset.split('.');
			var local_timezone = pad(timezone_array[0])+':'+pad(parseInt((timezone_array[1]*60)/100,10))+':'+"00";
			var local_timezone_operation = '';
			
			if(parseInt(timezone_array[0],10)>=0){
				local_timezone_operation = '+';
			}else{
				local_timezone_operation = '-';
			}
			
			var localtime = local_date.toTimeString();
			
			localtimezone = localtime.substring(localtime.indexOf(' '));
		
			localtimezone =	localtimezone.split("(").join("").split(")").join("");
			
			$("#timezone_string").val(localtimezone);
			$("#timezone_gmt").val(local_timezone);	
			$("#your_timezone").val(local_timezone);
			$("#timezone_gmt_operation").val(local_timezone_operation);
			$("#your_timezone").attr('timezone_operation',local_timezone_operation);
			
			
		}
		function time_to_timestamp(time){
		var time_array=	time.split(":");	
		var	hours			=	time_array[0]*3600;
		var minutes		=	time_array[1]*60;
		var seconds		=	time_array[2];
		return (parseInt(hours)+parseInt(minutes)+parseInt(seconds))*1000;
	}		
	    function convert_date(getdate){
		var date_array=	getdate.split("-");			
		return date_array[1]+"/"+date_array[2]+"/"+date_array[0];
	}	
	function toTimestamp(strDate){
		return Date.parse(strDate);
	}
	function check_integer(string){
		if(string<10){
			string	=	"0"+string;	
		}
		return string; 
	}
		function get_user_time_in_gmt() {
		    
			set_local_timezone_for_webinar();
			            
						var d = new Date();
                        var a = d.toTimeString();
					    var user_time_array  = a.split(" ");
						
						var user_time  =  user_time_array[0];  
						
						var umonth      = d.getMonth();
						if(month==12) var m_name	=	1;	
						else var m_name	=	parseInt(umonth)+1;
						var udate       = d.getDate();
						var uyear       = d.getFullYear();	
						
						var user_date  =  uyear+"-"+m_name+"-"+udate; 
						
		                var timezone_gmt_operation	=	$("#your_timezone").attr("timezone_operation")+"1";
						
						$("#timezone_gmt").val($("#your_timezone").val());	
						$("#timezone_gmt_operation").val(timezone_gmt_operation);
						
						//var webinar_selected_session=	jQuery.trim($("#select_webinar_session option:selected").attr("wb_date"));
						var	timezone_gmt_diffrence	=	jQuery.trim($("#timezone_gmt").val());
						var	timezone_operation			=	parseInt(jQuery.trim($("#timezone_gmt_operation").val()))+1;
						var	timezone_gmt_timestamp	=	time_to_timestamp(timezone_gmt_diffrence);
						
						//var webinar_date			=	$('#webinar_dates :selected').attr('value');
						var converted_date			=	convert_date(user_date);
						
						var date_timestamp			=	toTimestamp(''+converted_date+' '+user_time);
						if(timezone_operation>0){
							var total_timestamp		=	parseInt(date_timestamp)-parseInt(timezone_gmt_timestamp);	
						}else{
							var total_timestamp		=	parseInt(date_timestamp)+parseInt(timezone_gmt_timestamp);	
						}
						
						var timezone = timezone_gmt_diffrence.split(':');
						
						var timezone_hrs = parseInt(timezone[0]==24 ? 0 : timezone[0],10);
						var timezone_min = parseInt(timezone[0]!=24 ? timezone[1] : 0,10);
			
						var timezone_hrs = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_hrs));
						var timezone_min = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_min));
						
						var webinar_date = user_date.split('-');
						var webinar_time = user_time.split(':');
						var new_date 		 = new Date(parseInt(webinar_date[0],10),parseInt(webinar_date[1],10)-1,parseInt(webinar_date[2],10),(parseInt(webinar_time[0],10)==24 ? 0 : parseInt(webinar_time[0],10)),parseInt(webinar_time[1],10),0,0);
						var new_date 		 = new_date.valueOf();
						var ms_in_hour 	 = 60000*60;
						var ms_in_min    = 60000;
						var ms_to_add_or_deduct = -((timezone_hrs*ms_in_hour)+(timezone_min*ms_in_min));
			
						new_date 	= new_date + ms_to_add_or_deduct;
						
						var date		=	new Date(new_date);
						var hours 	= check_integer(date.getHours());
						var minutes = check_integer(date.getMinutes());
						var seconds = check_integer(date.getSeconds());
						var day		  = date.getDate();		
						var month	  = date.getMonth();
			
						if(month==12) var month_name	=	1;	
						else var month_name	=	parseInt(month)+1;	
						
						month_name		=	check_integer(month_name);	
						var year			=	date.getFullYear();
						var final_date=	year+"-"+month_name+"-"+day;
			            
						
						var formattedTime = hours + ':' + minutes + ':' + seconds;
						
						
						
						var x = [ formattedTime, final_date];
						return x;
						
		   
		}
	   $(document).ready(function(){
		  
		       
				var user_time_gmt    =    get_user_time_in_gmt()[0];
				var user_date_gmt    =    get_user_time_in_gmt()[1];
				
					
				$('#webinar_dates').change(function(event){
				if($("#webinar_dates option:selected").index()>0){
					$("[name=DATE]").val($(this).val());
					$("[name=webinar_real_date]").val($(this).val());
				}
				
				<?php if($is_right_now_enable == true){ ?>
					if($("#webinar_dates option:selected").index()==1){
						is_right_now=1;
					}
					else{
						is_right_now=0;
					}
				<?php } ?>
				if(is_right_now!=1 && $("#webinar_dates option:selected").index()>0){
					
					var webinar_date				=	jQuery.trim($(this).val());
					var webinar_id					=	'<?php echo $wbnr_id = isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id)) ?  $widget_options_webinar_id : $post->webinar_id;?>';
					var max_attendee				= '<?php echo $webinar_detail[0]->max_number_of_attendees; ?>';
					var webinar_schedule		=	'<?php echo $webinar_schedule_id; ?>';
					var webinar_timezone_id     =   '<?php echo $webinar_timezone_id;?>';
					var webinar_video_length    =   '<?php echo $video_length;?>';
		
					var timezone_difference	=	$("#your_timezone").val();
					var timezone_operation	=	$("#your_timezone").attr("timezone_operation");
					
					$.ajax({
						url		:	plugin_url+"webinar-db-interaction/get_webinar_session.php",		
						type	:	"GET",	
						crossDomain : true,
						dataType: 	"jsonp",
						data	:	"webinar_id="+webinar_id+"&webinar_date="+webinar_date+"&max_attendee="+max_attendee+"&webinar_schedule="+webinar_schedule+"&timezone_difference="+timezone_difference+"&timezone_operation="+timezone_operation+"&webinar_timezone_id="+webinar_timezone_id+"&user_time_gmt="+user_time_gmt+"&user_date_gmt="+user_date_gmt+"&webinar_video_length="+webinar_video_length,
						async	:	false,
						success	:	function(response){
										if(response==1){
											 $('#select_webinar_session').children().remove().end();
											 $('#select_webinar_session').append("<option value='0'><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_option'],'Select desired time'); ?></option>");
											 alert('No vacant slot is available for selected date');
 										}else if(response==2){
											 $('#select_webinar_session').children().remove().end();
											 $('#select_webinar_session').append("<option value='0'><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_option'],'Select desired time'); ?></option>");
											 alert('No vacant slot is available for this timezone and date.'); 
										} else {
										<?php if($webinar_detail[0]->webinar_timezone_id_fk > 0){ ?>
											var time_zone = "<?php echo substr($timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->name,strrpos($timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->name,'-')+1,strlen($timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->name)) ?>";
										<?php }else{ ?>
											var local_date     = new Date();
											var localtime      = local_date.toTimeString();
											var time_zone      = localtime.substring(localtime.indexOf('(')).split("(").join("").split(")").join("");
										<?php } ?> 
										
										var parsed_response = response;
										
										$('#select_webinar_session').children().remove().end();
										
										for(var i=0;i<parsed_response.length;i++){
											var single 		= parsed_response[i]; 
											var wb_time 	= single.start_time;
											var time_array=	wb_time.split(":");
										<?php if($webinar_time_format==0){ ?>	
											if (time_array[0]>12){
												var hour = time_array[0]-12;
												if(hour==12) {
													var time = hour+":"+time_array[1]+" AM";
												}else if(hour>9){ 
													var time = hour+":"+time_array[1]+" PM";
												}else{
													var time = "0"+hour+":"+time_array[1]+" PM";
												}
											}else {
												if(time_array[0]==12){
													var time = time_array[0]+":"+time_array[1]+" PM";
												}else {
													var time = time_array[0]+":"+time_array[1]+" AM";
												}
											}
										<?php }elseif($webinar_time_format==1){ ?>
												var time =	time_array[0]+":"+time_array[1];
										<?php } ?>
											if(i==0){$('#select_webinar_session').append($("<option value='0'><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_option'],'Select desired time'); ?></option>"))}
											$('#select_webinar_session').append($("<option></option>").attr("value",single.webinar_schedule_id_pk).attr("wb_date",single.start_time).text(time+" "+time_zone)); 
											$('#select_webinar_session').removeAttr('disabled');
											$("#webinar_real_date").val(webinar_date);
										}
										
											<?php if($webinar_detail[0]->event_type==1) { ?>
											if(parsed_response.length==1){
												<?php if($selected_template_dir=='classic_theme') {?>
													$("#select_webinar_session").hide();
													$(".reg_form_heading_block_2").hide();
													$(".reg_form_heading_block_3 .reg_heading_bullet_3").html('<span class="number2"></span>');
													$(".webinar_times").show();
												<?php }else{ ?>
													$("#select_webinar_session").parent().hide();
													$(".webinar_times_hint").hide();
													$(".webinar_times").show();
												<?php } ?>
												var one_time =  parsed_response[0].start_time;
												var one_time_schedule = parsed_response[0].webinar_schedule_id_pk;
												var one_time_array = one_time.split(":");
												<?php if($webinar_time_format==0){ ?>	
													if (one_time_array[0]>12){
														var hour = one_time_array[0]-12;
														if(hour==12) {
															var onetime = hour+":"+one_time_array[1]+" AM";
														}else if(hour>9){ 
															var onetime = hour+":"+one_time_array[1]+" PM";
														}else{
															var onetime = "0"+hour+":"+one_time_array[1]+" PM";
														}
													}else {
														if(one_time_array[0]==12){
															var onetime = one_time_array[0]+":"+one_time_array[1]+" PM";
														}else {
															var onetime = one_time_array[0]+":"+one_time_array[1]+" AM";
														}
													}
												<?php }elseif($webinar_time_format==1){ ?>
														var onetime =	one_time_array[0]+":"+one_time_array[1];
												<?php } ?>
												
												$(".webinar_times").html(onetime+' '+time_zone);
												
												if(one_time_schedule!=''){
													$("#select_webinar_session option").each(function(){
														if($(this).val() == one_time_schedule){
															$(this).attr("selected","selected");
															$("#select_webinar_session").trigger('change');
														}
													});
												}
											}
										<?php } ?>
										
										
										}
									}
					});
				}else{
					
					$('#select_webinar_session').children().remove().end();
					if($("#webinar_dates option:selected").index()==0){
						$('#select_webinar_session').prepend("<option value='0'><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_option'],'Select desired time'); ?></option>");
						$('#select_webinar_session').removeAttr('disabled');
					}else{
						$('#select_webinar_session').prepend("<option value='0' wb_date='00:00:00' selected='selected'>00:00</option>");
						$('#select_webinar_session').attr('disabled','disabled');
					}
				}
			});
		});
</script>

<script type="text/javascript">

$(document).ready(function() {
	
	var default_name = $('[name=NAME]').val();
	
	function time_to_timestamp(time){
		var time_array=	time.split(":");	
		var	hours			=	time_array[0]*3600;
		var minutes		=	time_array[1]*60;
		var seconds		=	time_array[2];
		return (parseInt(hours)+parseInt(minutes)+parseInt(seconds))*1000;
	}			
	
	function convert_date(getdate){
		var date_array=	getdate.split("-");			
		return date_array[1]+"/"+date_array[2]+"/"+date_array[0];
	}			
				
	function toTimestamp(strDate){
		return Date.parse(strDate);
	}
	function check_integer(string){
		if(string<10){
			string	=	"0"+string;	
		}
		return string; 
	}
	
	$('#select_webinar_session').change(function(event){
		set_date_time();
	});
    
	function set_date_time(){
		
		var session_value						=	jQuery.trim($("#select_webinar_session option:selected").val());
		
		if(session_value!=0){
			var webinar_selected_session_date = jQuery.trim($("#webinar_date").val());
			var webinar_selected_session_time	=	jQuery.trim($("#select_webinar_session option:selected").attr("wb_date"));
			
			<?php if(isset($webinar_detail[0]->webinar_timezone_id_fk) and $webinar_detail[0]->webinar_timezone_id_fk>0){ ?>
			if(parseInt(<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>)>0){	
				$.ajax({
				url		:	plugin_url+"webinar-db-interaction/set_date_time_for_attendee.php",		
				type	:	"GET",	
				data	:	"wbr_sel_dt="+webinar_selected_session_date+"&wbr_sel_tm="+webinar_selected_session_time+"&wbr_sel_tz_id="+<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>,
				async	:	false,
				crossDomain : true,
				dataType:	"jsonp",
				success	:	function(response){
									var splitResponseResult = response.split(" ");
									$('input[name="DATE"]').val(splitResponseResult[0]);
									$('input[name="TIME"]').val(splitResponseResult[1]);
									}
				});
			}
			<?php }elseif(isset($webinar_detail[0]->webinar_timezone_id_fk) and $webinar_detail[0]->webinar_timezone_id_fk==0){ ?>	
						
						var timezone_gmt_operation	=	$("#your_timezone").attr("timezone_operation")+"1";
						$("#timezone_gmt").val($("#your_timezone").val());	
						$("#timezone_gmt_operation").val(timezone_gmt_operation);
						
						var webinar_selected_session=	jQuery.trim($("#select_webinar_session option:selected").attr("wb_date"));
						var	timezone_gmt_diffrence	=	jQuery.trim($("#timezone_gmt").val());
						var	timezone_operation			=	parseInt(jQuery.trim($("#timezone_gmt_operation").val()))+1;
						
						var	timezone_gmt_timestamp	=	time_to_timestamp(timezone_gmt_diffrence);
						
						var webinar_date				=	$('#webinar_dates :selected').attr('value');
						var converted_date			=	convert_date(webinar_date);
						var date_timestamp			=	toTimestamp(''+converted_date+' '+webinar_selected_session);
						if(timezone_operation>0){
							var total_timestamp		=	parseInt(date_timestamp)-parseInt(timezone_gmt_timestamp);	
						}else{
							var total_timestamp		=	parseInt(date_timestamp)+parseInt(timezone_gmt_timestamp);	
						}
						
						var timezone = timezone_gmt_diffrence.split(':');
						
						var timezone_hrs = parseInt(timezone[0]==24 ? 0 : timezone[0],10);
						var timezone_min = parseInt(timezone[0]!=24 ? timezone[1] : 0,10);
			            
						var timezone_hrs = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_hrs));
						var timezone_min = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_min));
						
						var webinar_date = webinar_date.split('-');
						var webinar_time = webinar_selected_session.split(':');
						var new_date 		 = new Date(parseInt(webinar_date[0],10),parseInt(webinar_date[1],10)-1,parseInt(webinar_date[2],10),(parseInt(webinar_time[0],10)==24 ? 0 : parseInt(webinar_time[0],10)),parseInt(webinar_time[1],10),0,0);
						var new_date 		 = new_date.valueOf();
						var ms_in_hour 	 = 60000*60;
						var ms_in_min    = 60000;
						var ms_to_add_or_deduct = -((timezone_hrs*ms_in_hour)+(timezone_min*ms_in_min));
			            
						new_date 	= new_date + ms_to_add_or_deduct;
						
						var date		=	new Date(new_date);
						var hours 	= check_integer(date.getHours());
						var minutes = check_integer(date.getMinutes());
						var seconds = check_integer(date.getSeconds());
						var day		  = date.getDate();		
						var month	  = date.getMonth();
			
						if(month==12) var month_name	=	1;	
						else var month_name	=	parseInt(month)+1;	
						
						month_name		=	check_integer(month_name);	
						var year			=	date.getFullYear();
						var final_date=	year+"-"+month_name+"-"+day;
			
						var formattedTime = hours + ':' + minutes + ':' + seconds;
						
						$('input[name="TIME"]').val(formattedTime);
						$('input[name="DATE"]').val(final_date);
		<?php } ?>				
			
		} //end of if(session_value!=0)
	} //end of func set_date_time()

	function base64_encode (data) {
	  if (typeof this.window['btoa'] == 'function') {
	      return btoa(data);
	  }
	  var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
		ac = 0,
		enc = "",
		tmp_arr = [];
	
	  if (!data) {
		return data;
	  }
	
	  do { // pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);
	
		bits = o1 << 16 | o2 << 8 | o3;
	
		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	  } while (i < data.length);
	
	  enc = tmp_arr.join('');
	  var r = data.length % 3;
	  return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
	}
	
	var	plugin_url =	"<?php echo WEBINAR_PLUGIN_URL ?>"; 
	
	$("form").submit(function(event){
		//event.preventDefault();
		var video_length		=	"<?php echo $video_length  ?>";
		var webinar_date		=	$('input[name="DATE"]').val();
		var webinar_time		=	$('input[name="TIME"]').val();
		var webinar_selected_date=	$('#webinar_dates option:selected').index();
		var webinar_today_date=	$("#webinar_real_date").val();
		var webinar_real_time	=	$("#select_webinar_session option:selected").text();
		var webinar_check_time = jQuery.trim($("#select_webinar_session option:selected").attr("wb_date"));
		var webinar_real_date	=	$("#webinar_dates option:selected").text();
		var webinar_id				=	"<?php echo $wbnr_id = isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id)) ?  $widget_options_webinar_id : $post->webinar_id;?>";	
		var local_time = $("#local_time").val();
		var localtimezone 		=	$("#timezone_string").val();
		var webinar_schedule	=	jQuery.trim($("#select_webinar_session").val());		
		var notification			=	"<?php echo $webinar_detail[0]->notification_type;  ?>";

		if(notification==2 || notification==3){	
			var attendee_field_name	=	jQuery.trim($('#webinar_username').val());
			var attendee_field_email=	jQuery.trim($('#webinar_email').val());
			
			<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {?>
			var attendee_name			=	jQuery.trim($('#easywebinarplugin_widget_form form input[name="'+attendee_field_name+'"]').val());
			var attendee_email		=	jQuery.trim($('#easywebinarplugin_widget_form form input[name="'+attendee_field_email+'"]').val());
			<?php  }else{?>
			var attendee_name			=	jQuery.trim($('input[name="'+attendee_field_name+'"]').val());
			var attendee_email		=	jQuery.trim($('input[name="'+attendee_field_email+'"]').val());
			<?php } ?>
		}else {
			var attendee_name			=	jQuery.trim($('input[name="NAME"]').val()); 
			var attendee_email		=	jQuery.trim($('input[name="EMAIL"]').val()); 
		}

		var max_attendee					=	jQuery.trim($("#max_attendee").text());
		var selected_timezone_id	=	jQuery.trim($('#admin_tz').val());
		var after_webinar_enabled	=	"<?php echo $webinar_detail[0]->notification_after_webinar_enabled; ?>";
		var after_webinar_hours		=	"<?php echo $webinar_detail[0]->after_webinar_notification_hours;  ?>";
		var alpha_num_reg 				=	/^[a-zA-Z ]+$/g;
		var reg 									=	/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(selected_timezone_id==''){
			$("#message_div").css("display","block");
			$("#message_div").text("Unable to detect timezone");
			event.preventDefault();				
		}else if(webinar_date==0 || webinar_selected_date==0){
			$("#message_div").css("display","block");
			$("#message_div").text("Please select webinar date");
			event.preventDefault();				
		}else if(webinar_schedule==0 && is_right_now!=1 ){
			$("#message_div").css("display","block");
			$("#message_div").text("Please select webinar time");
			event.preventDefault();				
		}/* else if(alpha_num_reg.test(attendee_name)==false){
			$("#message_div").css("display","block");
			$("#message_div").text("Only alphabetic characters allowed as name");
			event.preventDefault();	
		} */  else if(attendee_name=="" || attendee_name == default_name){
			$("#message_div").css("display","block");
			$("#message_div").text("Name is mandatory");
			event.preventDefault();	
		}else if(attendee_email==""){
			$("#message_div").css("display","block");
			$("#message_div").text("Email is mandatory");
			event.preventDefault();
		}else if(reg.test(attendee_email) == false) {
			$("#message_div").css("display","block");
			$("#message_div").text("Enter a valid email");
 	   		event.preventDefault();
		}else{
			<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {?>
				$("div#loader_wrapper").css("display","table-cell");
				$("#reg_name").html(attendee_name);
				$("#reg_email").html(attendee_email);
				$("#reg_date").html(webinar_real_date);
				$("#reg_time").html(webinar_real_time);
			<?php }else{ ?>
				$("input#ewp_submit").attr("disabled","disabled");
				<?php if($selected_template_dir=='custom_theme'){ ?>
				var current_submit_val = $("input#ewp_submit").val();
				$("input#ewp_submit").val(current_submit_val+'...');
				<?php } else if($selected_template_dir=='new_theme') { ?>
					$("#ques-loading").show();
					$("#ewp_submit").hide();
				<?php } else{ ?>
				$("input#ewp_submit").hide();
				<?php } ?>
				$("#query_button_loading").css('display','block');
				$("#loading").css('display','block');
			<?php }?>
			
			attendee_email 	  = base64_encode(attendee_email);
			webinar_real_time = base64_encode(webinar_real_time);
			webinar_real_date =	base64_encode(webinar_real_date);
			localtimezone 	  = base64_encode(localtimezone);
			
			var user_time_gmt =    get_user_time_in_gmt()[0];
		    var user_date_gmt =    get_user_time_in_gmt()[1];
			var attendee_date_time_in_gmt = base64_encode(user_date_gmt+" "+user_time_gmt);
			
			$.ajax({
				url		:	plugin_url+"webinar-db-interaction/webinar-ajax-file.php",		
				type	:	"GET",	
				crossDomain : true,
				dataType: 	"jsonp",
				data	:	"webinar_id="+webinar_id+"&localtime="+local_time+ "&webinar_check_time=" + webinar_check_time +"&schedule_id="+webinar_schedule+"&aten_name="+attendee_name+"&aten_email="+attendee_email+"&webinar_date="+webinar_date+"&webinar_start_date="+webinar_today_date+"&max_attendee="+max_attendee+"&webinar_time="+webinar_time+"&after_webinar_enabled="+after_webinar_enabled+"&after_webinar_hours="+after_webinar_hours+"&video_length="+"<?php echo $video_length;  ?>&selected_timezone_id="+selected_timezone_id+"&webinar_real_time="+webinar_real_time+"&webinar_real_date="+webinar_real_date+"&attendee_local_timezone="+localtimezone+"&attendee_date_time_in_gmt="+attendee_date_time_in_gmt,
				async	:	false,
				success	:	function(response){
					             $("#ques-loading").hide();
								var parsed_response = response;
								var response = parsed_response.response;
								var res = parsed_response.redirect;
								
								var redirect_to_event = parsed_response.redirect_to_event; //0 or 1 direct to event page
								var notification_type	= "<?php echo $webinar_detail[0]->notification_type;?>";
								$("#message_div").text("");
								var max_limit_message				=	"Max limit for this webinar session is reached. Please register to another session";	
								var attende_register_messgae=	"You are already registered for this webinar sessions";
								
								if(response==1){
									$("#message_div").css("display","block");	
									$("#message_div").text(max_limit_message);
									$('input[name="NAME"]').val("");
									$('input[name="EMAIL"]').val("");

									$(".reg_form_submit_disabled").hide();
									$(".reg_form_loading").hide();
									<?php if($selected_template_dir=='custom_theme'){ ?>
									$("input#ewp_submit").val(current_submit_val);
									<?php }else{ ?>
									$("input#ewp_submit").show();
									<?php } ?>
									$("input#ewp_submit").removeAttr("disabled","disabled");
									event.preventDefault();
								}else if(response==2){
									$("#message_div").css("display","block");
									$("#message_div").text(attende_register_messgae);
									$('input[name="NAME"]').val("");
									$('input[name="EMAIL"]').val("");

									$(".reg_form_submit_disabled").hide();
									$(".reg_form_loading").hide();
									<?php if($selected_template_dir=='custom_theme'){ ?>
									$("input#ewp_submit").val(current_submit_val);
									<?php }else{ ?>
									$("input#ewp_submit").show();
									<?php } ?>
									$("input#ewp_submit").removeAttr("disabled","disabled");
									event.preventDefault();
								}else{				
													
									var page_to_redirect = "<?php echo $thank_page_link; ?>";
									
									if(notification_type==1){
										$("#message_div").css("display","none");
										<?php if($is_right_now_enable == true){  ?>
										      //  alert("1");
												if($("#webinar_dates option:selected").index()==1){
													//alert("2");
													var id = parsed_response.key;
													var url = "<?php echo $login_link ?>";
													page_to_redirect =  url + (url.indexOf('?') != -1 ? "&right_now=1&key="+id : "?right_now=1&key="+id);
													<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {?>
														window.parent.location	=	page_to_redirect;
													<?php }else{  ?>
														window.location.replace(page_to_redirect);
													<?php } ?>
												}
												else{
													//alert("3");
													page_to_redirect = "<?php echo $thank_page_link; ?>";
													<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {?>
													   // alert("4");
														var id = parsed_response.key;
														var url = "<?php echo $login_link ?>";
														page_to_redirect =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);
														if(redirect_to_event == 1)
														 {
														 window.location.replace(page_to_redirect);
														 }
														$("#easywebinarplugin_widget_form").hide();
														$("a#easywebinarplugin_event_link").attr("href",page_to_redirect);
														$("a#easywebinarplugin_event_link").html(page_to_redirect);
														$("#easywebinarplugin_event_box").show();
													<?php }else{  ?>
													    //For ongoing webinar redirect to thank-you page or event page
											            if(redirect_to_event == 1)
														 {
													     var id = parsed_response.key;
														  var url = "<?php echo $login_link ?>";
														  page_to_redirect =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);		 
														 window.location.replace(page_to_redirect);
														 }
														window.location.replace(page_to_redirect);
													<?php } ?>
												}
										<?php }else{ ?>
											<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {  ?>
												var id = parsed_response.key;
												var url = "<?php echo $login_link ?>";
												page_to_redirect =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);
												$("#easywebinarplugin_widget_form").hide();
												$("a#easywebinarplugin_event_link").attr("href",page_to_redirect);
												$("a#easywebinarplugin_event_link").html(page_to_redirect);
												$("#easywebinarplugin_event_box").show();
											<?php }else{  ?>
											//For ongoing webinar redirect to thank-you page or event page
											        if(redirect_to_event == 1)
														 {
													     var id = parsed_response.key;
														  var url = "<?php echo $login_link ?>";
														  page_to_redirect =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);		 
														 window.location.replace(page_to_redirect);
														 }
												window.location.replace(page_to_redirect);
											<?php } ?>
										<?php } ?>			
									}else if(notification_type==2 || notification_type==3){
										//alert(notification_type);
										<?php if($is_right_now_enable == true){  ?>
										
												if($("#webinar_dates option:selected").index()==1){
													var id = parsed_response.key;
													var url = "<?php echo $login_link ?>";
													//alert(url);
													page_to_redirect =  url + (url.indexOf('?') != -1 ? "&right_now=1&key="+id : "?right_now=1&key="+id);
													//alert(page_to_redirect);
										<?php if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {?>
														window.parent.location	=	page_to_redirect;
													<?php }else{  ?>
														window.location.replace(page_to_redirect);
													<?php } ?>
													event.preventDefault();
												}
										<?php } ?>
										if(res==5){
												var id = parsed_response.key;
												$('input[name*="URL"]').each(function(){
												  if($.trim($(this).val())==''){
													 var url = "<?php echo $login_link ?>";
													 var unique_url =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);
													 //alert(unique_url);
												   $(this).val(unique_url);
													}
													
												});
												//easywebinarplugin_widget_form
												$("#message_div").css("display","none");
												<?php  if(isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id))) {  ?>
														 $.ajax({
															  type: "POST",
															  url: $('#easywebinarplugin_widget_form form').attr("action"),   
															  data: $('#easywebinarplugin_widget_form form').serialize(),
															  async : true,
															  datatype: "html",
															  
															  success: function (result) {
																 //event.preventDefault();
																	 
															  }
														 });
														var url = "<?php echo $login_link ?>";
														//alert(url);
														var unique_url =  url + (url.indexOf('?') != -1 ? "&key="+id : "?key="+id);
														$("#easywebinarplugin_widget_form").hide();
														$("a#easywebinarplugin_event_link").attr("href",unique_url);
														$("a#easywebinarplugin_event_link").html(unique_url);
														$("#easywebinarplugin_event_box").show();
														event.preventDefault();
												<?php   }else{  ?>
														$('form').attr('target','_self');
												<?php } ?>
										}
											
									}								
								}
			
							}	
			});	
			var notification	=	"<?php echo $webinar_detail[0]->notification_type;?>";	
			if(notification==1){
				event.preventDefault();	
			}
		//event.preventDefault();		
		}	
	}); 	

	<?php if($webinar_detail[0]->notification_type!=1){ ?>
		var attendee_field_names		=	jQuery.trim($('#webinar_username').val());
		$('input[name="'+attendee_field_names+'"]').attr('value',"<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>").attr('onfocus',"if(this.value == '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>') {this.value = '';}").attr('onblur',"if (this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>';}").addClass("RegisInput paddingLeft5 inputbox default-value textbox nobg");
	
		var attendee_field_emails		=	jQuery.trim($('#webinar_email').val());
		$('input[name="'+attendee_field_emails+'"]').attr('value',"<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>").attr('onfocus',"if(this.value == '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>') {this.value = '';}").attr('onblur',"if (this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>';}").addClass("RegisInput paddingLeft5 inputbox default-value textbox nobg");
		
		var default_values = new Array();
		$("input.default-value").focus(function(){
			if (!default_values[this.value]) {
		      default_values[this.value] = this.value;
			}
		    if (this.value == default_values[this.value]) {
      			this.value = '';
			}
			$(this).blur(function(){
				if(this.value == '' && this.name==$("#webinar_username").val()){
					this.value="<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>";
				}else if (this.value == '' && this.name==$("#webinar_email").val()) {
					this.value="<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>";
				}
			});
		});
	<?php } ?>

	<?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
		var requested_name = $.trim($("#ewp_requested_name").val());
		var requested_mail = $.trim($("#ewp_requested_mail").val());
		if(requested_name!='' && requested_mail!=''){
			<?php if($webinar_detail[0]->notification_type==1){ ?>
				$("#NAME").val(requested_name);
				$("#EMAIL").val(requested_mail);
			<?php }else{ ?>
				var attendee_field_names		=	jQuery.trim($('#webinar_username').val());
				var attendee_field_emails		=	jQuery.trim($('#webinar_email').val());
				$('input[name="'+attendee_field_names+'"]').val(requested_name);
				$('input[name="'+attendee_field_emails+'"]').val(requested_mail);
			<?php } ?>
		}
	<?php } ?>
	
	function check_valid_webinar_date_for_timezone(){
		
		set_local_timezone_for_webinar();
		var user_time_gmt    =    get_user_time_in_gmt()[0];
		var user_date_gmt    =    get_user_time_in_gmt()[1];
				
		var webinar_id					=	"<?php echo $wbnr_id = isset($widget_options_webinar_id) && (!empty($widget_options_webinar_id)) ?  $widget_options_webinar_id : $post->webinar_id;?>";	
		var timezone_difference	=	$("#your_timezone").val();
		var timezone_operation	=	$("#your_timezone").attr("timezone_operation");
		
		$.ajax({
				url		:	plugin_url+"webinar-db-interaction/remove_webinar_session.php",		
				type	:	"GET",	
				data	:	"webinar_id="+webinar_id+"&timezone_difference="+timezone_difference+"&timezone_operation="+timezone_operation+"&user_time_gmt="+user_time_gmt+"&user_date_gmt="+user_date_gmt,
				async	:	false,
				crossDomain : true,
				dataType: 	"jsonp",
				success	:	function(response){
							var default_option  = '<option value="0"><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_date_option'],'Select desired date'); ?></option>';
							var available_dates = '';
							<?php if($is_right_now_enable == true ){ ?>
              		available_dates = available_dates+'<option value="<?php echo date('Y-m-d'); ?>">Watch yesterday\'s replay now</option>';
              <?php } ?>
							var months_array	=	new Array("January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December");
							
							for(var i=0;i<response.length;i++){
								var temp_array		=	new Array();
								temp_array				=	response[i].split('-');
								var webinar_date	= new Date(parseInt(temp_array[0],10),parseInt(temp_array[1],10)-1,parseInt(temp_array[2],10));
								
								available_dates 	= available_dates+'<option value="'+response[i]+'">'+months_array[webinar_date.getMonth()]+" "+parseInt(webinar_date.getDate(),10)+", "+webinar_date.getFullYear()+'</option>';
							}
							
							<?php if((!isset($widget_options_webinar_id)) && (empty($widget_options_webinar_id))) { ?>
								if(available_dates==''){
									$("#message_div").show();
									$("#message_div").html('Webinar registration date is unavailable for your current date and timezone');
								}
							<?php }else{ ?>
								if(available_dates==''){
									$("#message_div").show();
									$("#message_div").html('Registration date is unavailable');
								}
							<?php } ?>
							
							available_dates = default_option+available_dates;
							$("#webinar_dates").html(available_dates);
							
							<?php if($webinar_detail[0]->event_type==1) { ?>
							if(response.length==1){
								var one_time_even_date	= response[0];
								$("#webinar_dates option").each(function(){
									if($(this).val() == one_time_even_date && $.trim($(this).text()) != "Watch yesterday's replay now"){
										$(this).attr("selected","selected");
										$("#webinar_dates").trigger('change');
									}
								});
								var one_time_date_array	=	response[0].split('-');
								var one_time_date	= new Date(parseInt(one_time_date_array[0],10),parseInt(one_time_date_array[1],10)-1,parseInt(one_time_date_array[2],10));
								var days_array = new Array("Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday");
								var curr_day 	 = one_time_date.getDay();
								var curr_date  = one_time_date.getDate();
								var curr_month = one_time_date.getMonth();
								var curr_year  = one_time_date.getFullYear();
								$(".onetime_event_cal_month").html(months_array[curr_month]);
								$(".onetime_event_cal_date").html(curr_date);
								$(".onetime_event_cal").css('visibility','visible');
								$(".webinar_dates").html(days_array[curr_day] + ", " + curr_date + " " + months_array[curr_month] + " " + curr_year);
							}
							<?php } ?>
							
							var widget_date = $.trim($("#webinar_widget_date").val());
							if(widget_date!=''){
								 $("#webinar_dates option").each(function(){
									if($(this).val() == widget_date && $.trim($(this).text()) != "Watch yesterday's replay now"){
										$(this).attr("selected","selected");
										$("#webinar_dates").trigger('change');
									}
								});
							}
				}
		});
	}
	check_valid_webinar_date_for_timezone();
});

</script>