<link rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelementplayer.css'?>"/>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.jodometer.js' ?>"></script>
<?php if($service_type=='vimeo'){?>
<script type="text/javascript" src="//a.vimeocdn.com/js/froogaloop2.min.js"></script>
<?php } ?>
<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player-responsive.js'?>"></script>
<?php }else{ ?>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player.js'?>"></script>
<?php } ?>        
<script type="text/javascript">var is_livestream = "<?php echo $webinar_detail[0]->is_livestream;?>"; var SEEK_TO ="<?php echo $seek_to; ?>";</script>

<script type="text/javascript">
$(document).ready(function(){$("#scarcity_events").hide();$("#delayed_events").hide();$("#scarcity-delayed").hide();$("#footer_container a").each(function(){$(this).attr("target","_blank");$(this).css("text-decoration","none")})});
</script>

<script type="text/javascript">
      $(function() {
		
		<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet || preg_match('/(?i)msie/',$_SERVER['HTTP_USER_AGENT']) || preg_match('/(?i)trident/',$_SERVER['HTTP_USER_AGENT'])){	?>
					setTimeout('onVideoReady()',2000);
		  <?php }?>
		
       $('video').mediaelementplayer({
				  mode: 'auto',
				  autoplay : true,
				   <?php if($service_type=='vimeo'){?>
				  isClickable : true,
				  <?php }else{ ?>
				  isClickable : false,
				  <?php } ?>
				  clickToPlayPause : false,
				  iPhoneUseNativeControls: true, 
				  iPadUseNativeControls: true,
				  AndroidUseNativeControls: true,
				  enableKeyboard: false,
				  alwaysShowControls : false,
				  enableAutosize: true,
				  enablePseudoStreaming : false,
				  <?php if($service_type=='vimeo'){?>
				  hideControlsForVimeo:true,
				  <?php } ?>
				  <?php if( $iPod || $iPhone || $iPad ){	?>
				  features: ['current','volume'],
				  <?php }elseif($Android || $AndroidTablet){ ?>
				  features: ['current','volume'],
				  <?php }else{ ?>
				  		<?php if($service_type=='youtube' || $service_type=='stream'){ ?>
							features: ['current','volume'],
							<?php }else{ ?>
							features: ['current','volume','fullscreen'],
							<?php } ?>
				  <?php } ?>
				  success: function(player, node) {
					  
					var lastUpdatedTime  = 0;
					var hasPlayerStarted = false;
					var percentage 	 	 = [10,20,30,40,50,60,70,80,90,100];
					var lastPercentagePlayed = 0;
					
					if (player.pluginType == 'flash') {
         				player.addEventListener('canplay', function() {
							player.play();
						}, false);
					}else{
						<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	}else {?>
						player.play();
						<?php } ?>
					}						
					
					<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
					  $('.mejs-overlay-button, .mejs-overlay, .ewp-player-overlay').bind("click",function() {
						if(player.paused==true){
						 	player.play();
						 	
						}
					  });
					  <?php }else{ ?>
					  $('.ewp-player-overlay').bind("click",function() {	
						if(player.paused==true){
							player.play();
						}
					  });
					  <?php } ?>
					
					player.addEventListener("playing", function(e) {
						if(typeof onVideoReady == "function"){
							onVideoReady();
						}
        			});
					
				   player.addEventListener('timeupdate', function(e) {
			 		
						
						if(player.paused) return;
						<?php if($webinar_detail[0]->is_livestream==0 || $webinar_detail[0]->is_livestream==1){ ?>
						if(!hasPlayerStarted){
							if(player.currentTime<SEEK_TO){
								player.setCurrentTime(SEEK_TO);
								player.play();
								hasPlayerStarted = true;	
							}
						}
					<?php } ?>
						if(typeof addEvent == "function"){
							var currentTime =  Math.floor(player.currentTime);
							
							if(!(currentTime==lastUpdatedTime)){
								addEvent(currentTime);
								lastUpdatedTime = currentTime; 
							}
						}
						
						<?php if(isset($_session_attendee_id)){ ?>
						
						  var percentagePlayed =  Math.floor(((parseInt(player.currentTime) / parseInt(player.duration)) * 100));
						 
						 if(!(percentagePlayed==lastPercentagePlayed)){
						  if($.inArray(percentagePlayed,percentage)> -1 ){
							  updateWatchedStats(percentagePlayed);
							  lastPercentagePlayed = percentagePlayed;
						  }
						 }
					  
						<?php } ?>
						
				    },false);
										
					player.addEventListener("ended", function( event ) {
						
						if(typeof track_attendee_status == "function"){
							track_attendee_status();
						}
						
						if(typeof onVideoEnd == "function"){
							onVideoEnd();
						}
        			});
										
					$('.ewp-player-overlay').bind('contextmenu', function(e) {return false;});
				  }
			});
		
		
		  $("#delayed_events_text").on("click",".call_to_action",function(){
			var selectedTag = $(this).attr("tag_id");
			var selectedrTag = $(this).attr("tag_rid");
		    var selectedEvent = $(this).attr('event_id');
		    var selectedWebinar = $(this).attr('webinar_id');
		    var getPostsData = {
					action  : 'store_event_in_infusionsoft',
					attendeeEmail: '<?php echo $currentattendee[0]->attendee_email_id; ?>',
					attendeeName  : '<?php echo $currentattendee[0]->attendee_name; ?>',
					webinarId : selectedWebinar,
					delayedEvent: selectedTag,
					deltag:selectedrTag
				}
		   
				$.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
					});
		    
            
		});  
     });
</script>

<script type="text/javascript">
		
			<?php if($webinar_detail[0]->is_livestream==1){ ?>
				var eventTime = 0;
				var ewpInterval = setInterval(function () {
					++eventTime;
					addEvent(eventTime);
				},1000);
			<?php } ?>
			<?php
			if($webinar_scarcity[0]->is_scarcity_enabled==1 || count($webinar_delayed_events)>0 ) {
				$arrEventCalling[] 	= 	'function addEvent(time){';
				$count=1;
			if(count($webinar_delayed_events)>0){
				
				foreach($webinar_delayed_events as $events){
						
						$html 		= 	array();
						$content 	= 	trim($events->text_to_display);
						$content	= 	str_replace ("/r/n", "", $content);
						$content	=		nl2br($content);
						$content	=		explode ("<br />",$content);
						$current_delayed_event_id = $events->webinar_delayed_events_id_pk;
						$webinar_idd = $events->webinar_id_fk;
						$attendeeName = $currentattendee[0]->attendee_name;
						$attendeeEmail = $currentattendee[0]->attendee_email_id;
						$infusion_tag_id = $events->infusionsoft_delayed_tag;
						$infusion_tag_clicked_id = $events->infusionsoft_delayed_clicked_tag;
						foreach($content as $value){
							$html[] = trim($value);
						}
						$content	=	implode("",$html);
						$content	=	addslashes($content);
						
						$start_time_array	=	explode(":",$events->start_time);		
						$start_time				=	$start_time_array[0]*3600+$start_time_array[1]*60+$start_time_array[2];
						$end_time_array		=	explode(":",$events->end_time);		
						$end_time					=	$end_time_array[0]*3600+$end_time_array[1]*60+$end_time_array[2];
						$event_start_time	=	$start_time;		
						$event_end_time		=	$end_time;
						$delayed_event_button_link =	$events->chosen_button_link;
					if($events->button_source_path!=''){
						if(trim($events->chosen_button_link)!=""){
							$delayed_button_image	=	'<a class=\"call_to_action event_image'.$count.'\" href=\"'.$delayed_event_button_link.'\" target=\"_blank\" tag_id=\"'.$infusion_tag_clicked_id .'\" tag_rid=\"'.$infusion_tag_id .'\" webinar_id=\"'.$events->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$events->webinar_delayed_events_id_pk.'\" button_id=\"'.$events->webinar_button_id_fk.'\" ><img src=\"'.$events->button_source_path.'\" width=\"200\" height=\"50\" /></a>';
						}
						else{
							$delayed_button_image	=	'<img src=\"'.$events->button_source_path.'\"  width=\"200\" height=\"50\" webinar_id=\"'.$events->webinar_id_fk.'\" tag_id=\"'.$infusion_tag_clicked_id .'\" tag_rid=\"'.$infusion_tag_id .'\" is_event=\"1\" event_id=\"'.$events->webinar_delayed_events_id_pk.'\" button_id=\"'.$events->webinar_button_id_fk.'\" class=\"call_to_action\" />';
						}
						
					}else { 
						$delayed_button_image=''; 
					}
					
					
					if($seeking_point>$event_start_time && $seeking_point<$event_end_time && ( !empty($content) || !empty($delayed_button_image) ) ){
                         $emarketing_settings			= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar_idd));
                         $infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
					     $infusionsoft_enabled			= (int)$emarketing_settings['infusionsoft']['enabled'];
						if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
						$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
					    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
						
						}
						
						$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
					    $authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
						if($authorized_credentials)
						{
							
							update_option('ewp_emarketing_status',0);
							$iui	=           $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendeeName,'Email'=>$attendeeEmail));
	                       
							$obj_eII->applyTagToUser($iui,$infusion_tag_id);
						}
						else{
							/* $emarket_status = get_option('ewp_emarketing_status');
					         if($emarket_status != 1){
							update_option('ewp_emarketing_status',1);
							$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/change_infusionsoft_status.php'; 
				  	        $response_message_array['error_message'] = 0;
				  	        $response_message_array['message'] = 0;
				  	        $response_message_array['domain'] = site_url();
				  	        $ch = curl_init ($website_file_url);
			               curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
						   curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
						   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch,CURLOPT_REFERER,site_url());
						   curl_setopt($ch,CURLOPT_POST,true );
						   curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
						   $infusionsoft_reponse_error = curl_exec($ch);
						   curl_close($ch); 
						} */
						}
						$arrEvents[]			=	'var eventCall'.$count.' = function(){';
						$arrEvents[]			=	'$("#scarcity-delayed").show();';
						$arrEvents[]			=	'$("#delayed_events").show();';
						$arrEvents[]			=	'var width_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(width_scarcity_events>0){';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'	}else{';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'$("#delayed_events_text").append("<div style=\"margin-bottom:20px;padding-bottom:20px;display:none;\" class=event_'.$count.'>'.$content.''.$delayed_button_image.'</div>");';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeIn(1000);';
						$arrEvents[]			=	' };';
						$arrEventCalling[]		=	'if(time == '.$seeking_point.'){ if(ewpEventArray.indexOf("eventCall'.$count.'")!= -1){ eventCall'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventCall'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventCall".$count."'";
						
						$arrEvents[]			=	'var eventRemove'.$count.' = function(){';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeOut(1000,function(){$(".event_'.$count.'").remove();';
						$arrEvents[]			=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
						$arrEvents[]			=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(remaining_delayed_events==0){';
						$arrEvents[]			=	'	$("#delayed_events").hide();';
						$arrEvents[]			=	'	} ';
						$arrEvents[]			=	'if(remaining_scarcity_events==0  ){';
						$arrEvents[]			=	'	$("#scarcity_events").hide();';
						$arrEvents[]			=	'	} ';
						$arrEvents[]			=	'	});';
						$arrEvents[]			=	'};';
						$arrEventCalling[]		= 	'if(time == '.$end_time.'){  if(ewpEventArray.indexOf("eventRemove'.$count.'")!= -1){ eventRemove'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventRemove'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventRemove".$count."'";
						$count++;
					}
					else if($seeking_point<$event_end_time && ( !empty($content) or !empty($delayed_button_image) ) ){
						 $emarketing_settings			= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar_idd));
					    $infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
					    $infusionsoft_enabled			= (int)$emarketing_settings['infusionsoft']['enabled'];
						if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
						$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
					    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
						}
						
						$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);						
                        $authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
						if($authorized_credentials)
						{
							update_option('ewp_emarketing_status',0);
							$iui	=    $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendeeName,'Email'=>$attendeeEmail));
	                        
							$obj_eII->applyTagToUser($iui,$infusion_tag_id);
						}
						else{
							/* $emarket_status = get_option('ewp_emarketing_status');
					        if($emarket_status != 1){
							update_option('ewp_emarketing_status',1);
							$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/change_infusionsoft_status.php'; 
				  	        $response_message_array['error_message'] = 0;
				  	        $response_message_array['message'] = 0;
				  	        $response_message_array['domain'] = site_url();
				  	        $ch = curl_init ($website_file_url);
			               curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
						   curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
						   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch,CURLOPT_REFERER,site_url());
						   curl_setopt($ch,CURLOPT_POST,true );
						   curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
						   $infusionsoft_reponse_error = curl_exec($ch);
						   curl_close($ch); 
						} */
						}
						$arrEvents[]			=	'var eventCall'.$count.' = function(){';
						$arrEvents[]			=	'$("#scarcity-delayed").show();';
						$arrEvents[]			=	'$("#delayed_events").show();';
						$arrEvents[]			=	'var width_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(width_scarcity_events>0){';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'	}else{';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'$("#delayed_events_text").append("<div style=\"margin-bottom:20px;padding-bottom:20px;\" class=event_'.$count.'>'.$content.''.$delayed_button_image.'</div>");';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeIn(1000);';
						$arrEvents[]			=	'	};';
						$arrEventCalling[]		=	'if(time == '.$start_time.'){ if(ewpEventArray.indexOf("eventCall'.$count.'")!= -1){ eventCall'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventCall'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventCall".$count."'";
												
						$arrEvents[]			=	'var eventRemove'.$count.' = function(){';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeOut(1000,function(){$(".event_'.$count.'").remove();';
						$arrEvents[]			=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
						$arrEvents[]			=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(remaining_delayed_events==0){';
						$arrEvents[]			=	'	$("#delayed_events").hide();';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'if(remaining_scarcity_events==0  ){';
						$arrEvents[]			=	'	$("#scarcity-delayed").hide();';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'	} });';
						$arrEvents[]			=	'};';
						$arrEventCalling[]		= 	'if(time == '.$end_time.'){  if(ewpEventArray.indexOf("eventRemove'.$count.'")!= -1){ eventRemove'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventRemove'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventRemove".$count."'";
						$count++;
					}
				}	
			}
			if($webinar_scarcity[0]->is_scarcity_enabled==1){
					$scarcity_start_time_array		=	explode(":",$webinar_scarcity[0]->start_time);		
					$scarcity_start_time			=	$scarcity_start_time_array[0]*3600+$scarcity_start_time_array[1]*60+$scarcity_start_time_array[2];
					$scarcity_end_time_array		=	explode(":",$webinar_scarcity[0]->end_time);		
					$scarcity_end_time				=	$scarcity_end_time_array[0]*3600+$scarcity_end_time_array[1]*60+$scarcity_end_time_array[2];
					if($scarcity_end_time==0){
						$scarcity_end_time = $video_length;
					}
					
					$scarcity_event_start_time		=	$scarcity_start_time;		
					$scarcity_event_end_time		=	$scarcity_end_time;
					
					$scarcity_start_number			=	$webinar_scarcity[0]->start_number;
					$scarcity_end_number			=	$webinar_scarcity[0]->end_number;
					
										
					$slots_per_min					=	5;
					$interval						=   12;
					$end_time						= 	$scarcity_start_time+12;
					$time_difference				=	$scarcity_end_time - $scarcity_start_time;
					$random_numbers					=	$ob_webinar_functions->generate_random_series($scarcity_start_number,$scarcity_end_number,$time_difference,$slots_per_min);
					if($current_time_seconds>$scarcity_event_start_time && $current_time_seconds<$scarcity_event_end_time){
						$scarcity_current_start_number	=	$ob_webinar_functions->calculate_scracity_start_number($time_difference,$seeking_point,$scarcity_start_number,$scarcity_end_number);
						
						$scarcity_start_number			=	$scarcity_current_start_number;
						
						$random_numbers					=	$ob_webinar_functions->generate_random_series($scarcity_start_number,$scarcity_end_number,$time_difference,$slots_per_min);
					}
					
					$image_url							=	WEBINAR_PLUGIN_URL.'templates/images/jodometer-numbers.png';
					
					if($webinar_scarcity[0]->button_source_path!=''){
						if(trim($webinar_scarcity[0]->choose_button_link)!="")
							$button_image	=	'<a href='.$webinar_scarcity[0]->choose_button_link.' target=\"_blank\" webinar_id=\"'.$webinar_scarcity[0]->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$webinar_scarcity[0]->webinar_scarcity_id_pk.'\" button_id=\"'.$webinar_scarcity[0]->button_id_fk.'\"  class=\"call_to_action\" ><img src=\"'.$webinar_scarcity[0]->button_source_path .'\" width=\"200\" height=\"50\" ></a>';
						else
							$button_image	=	'<img src=\"'.$webinar_scarcity[0]->button_source_path .'\" width=\"200\" height=\"50\" webinar_id=\"'.$webinar_scarcity[0]->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$webinar_scarcity[0]->webinar_scarcity_id_pk.'\" button_id=\"'.$webinar_scarcity[0]->button_id_fk.'\"  class=\"call_to_action\" >';
					}else { $button_image	=	'';}
					
					for($i=0;$i<count($random_numbers);$i++){
						$number	=  $scarcity_start_number-$random_numbers[$i];
						$width	=	strlen($scarcity_start_number)*40;
						$width	=	$width."px";
						
						$content 	= trim($webinar_scarcity[0]->description);
						$html = array();
						$content	= 	str_replace ("/r/n", "", $content);
						$content	=	nl2br($content);
						$content	=	explode ("<br />",$content);
						foreach($content as $value)
						{
							$html[] = trim($value);
						}
						$content	=	implode("",$html);
						$content	=	addslashes($content);
						
						if($seeking_point>$scarcity_event_start_time && $seeking_point<$scarcity_event_end_time){
							$arrEvents[]				=	'var scarcityEvent'.$i.' = function(){';
							$arrEvents[]				=	'var width_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'$("#scarcity-delayed").css("display","block");';
							$arrEvents[]				=	'if(width_delayed_events>0){';
							$arrEvents[]				=	'$("#delayed_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'}else{ ';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	' };';
							$arrEvents[]				=	'$("#scarcity_events").fadeIn(1000);';
							$arrEvents[]				=	'$(".scarcity_counter").css("width","'.$width.'");';
							$arrEvents[]				=	'$(".scarcity_counter").jOdometer({ counterStart:"'.$scarcity_start_number.'", counterEnd:"'.$number.'", numbersImage: "'.$image_url.'"});';
							$arrEvents[]				=	'$("#scarcity_events_text").html("<div class=scarcity_'.$i.'>'.$content.''.$button_image.'</div>");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			= 	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcityEvent'.$i.'") != -1) { scarcityEvent'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEvent'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEvent".$i."'";
							
							$arrEvents[]				=	'var scarcityEventRemove'.$i.' = function(){';
							$arrEvents[]				=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
							$arrEvents[]				=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if('.$number.'=='.$scarcity_end_number.'){';
							$arrEvents[]				=	'$("#scarcity_events").fadeOut(1000,function(){ $("#scarcity_events").remove();  ';
							$arrEvents[]				=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	'if(remaining_delayed_events==0 ){';
							$arrEvents[]				=	'	$("#scarcity-delayed").hide();';
							$arrEvents[]				=	'	}; });';
							$arrEvents[]				=	'	};';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			= 	'if(time == '.$end_time.'){ if (ewpEventArray.indexOf("scarcityEventRemove'.$i.'") != -1) { scarcityEventRemove'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEventRemove'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEventRemove".$i."'";
							
						}else if($seeking_point<$scarcity_event_end_time){
						
							$arrEvents[]				=	'var scarcity_event_start'.$i.'= function(){';
							$arrEvents[]				=	'$("#scarcity-delayed").css("display","block");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			=	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcity_event_start'.$i.'") != -1) { scarcity_event_start'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcity_event_start'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcity_event_start".$i."'";
							
							$arrEvents[]				=	'var scarcityEvent'.$i.' = function(){';
							$arrEvents[]				=	'var width_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if(width_delayed_events>0){';
							$arrEvents[]				=	'$("#delayed_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	' }else{ ';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	' };';
							$arrEvents[]				=	'$("#scarcity_events").fadeIn(1000);';
							$arrEvents[]				=	'$(".scarcity_counter").css("width","'.$width.'");';
							$arrEvents[]				=	'$(".scarcity_counter").jOdometer({ counterStart:"'.$scarcity_start_number.'", counterEnd:"'.$number.'", numbersImage: "'.$image_url.'"});';
							$arrEvents[]				=	'$("#scarcity_events_text").html("<div class=scarcity_'.$i.'>'.$content.''.$button_image.'</div>");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			= 	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcityEvent'.$i.'") != -1) { scarcityEvent'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEvent'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEvent".$i."'";
							
							$arrEvents[]				=	'var scarcityEventRemove'.$i.' = function(){';
							$arrEvents[]				=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
							$arrEvents[]				=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if('.$number.'=='.$scarcity_end_number.'){';
							$arrEvents[]				=	'$("#scarcity_events").fadeOut(1000,function(){ $("#scarcity_events").delay(1000).remove();  ';
							$arrEvents[]				=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	'if(remaining_delayed_events==0 ){';
							$arrEvents[]				=	'	$("#scarcity-delayed").hide();';
							$arrEvents[]				=	'	}; });';
							$arrEvents[]				=	'	};';
							$arrEvents[]				=	'};';
							$arrEventCalling[]		= 	'if(time == '.$end_time.'){ if (ewpEventArray.indexOf("scarcityEventRemove'.$i.'") != -1) { scarcityEventRemove'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEventRemove'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEventRemove".$i."'";
							
						}
							
							$scarcity_start_time = $scarcity_start_time + 12;
							$end_time	= $scarcity_start_time +12;
							$scarcity_start_number				= 	$scarcity_start_number-$random_numbers[$i];
					}
			}
			$arrEventCalling[] =  '};';
		
			echo implode('',$arrEvents);
			echo 'var ewpEventArray = new Array('.implode(',',$arrEventAray).');';
			echo implode('',$arrEventCalling);
			}?>
			
		function onVideoEnd(){
			<?php if(isset($webinar_detail[0]->automated_redirect_url) && !empty($webinar_detail[0]->automated_redirect_url)) { ?>
				window.location.replace("<?php echo $webinar_detail[0]->automated_redirect_url;?>");
			<?php } ?>
		}
</script>

<script type="text/javascript">
	var webinar_id				=	"<?php echo $post->webinar_id; ?>";
	var webinar_schedule_id		=	"<?php echo $_session_schedule_id; ?>";
	var webinar_date			=	"<?php echo $_session_date; ?>";
	var all_ids					=	"<?php echo $attendee_ids;  ?>";
	
	function update_attendee_list(){
			<?php if($webinar_detail[0]->attendees_list_enabled ==1){ ?>
			setTimeout("manage_default_attendee()",90000);	
			<?php } ?>
	}
	
	update_attendee_list();
 
function manage_default_attendee( ){
	var default_attendee_ids		=	$("#total_logged_attendee").val();
	var attendee_operation			=	$("#default_attendee_operation").val();
	var webinar_id					=	"<?php echo $post->webinar_id; ?>";
	var max_limit					=	'<?php echo $webinar_detail[0]->max_number_of_attendees; ?>';
	var randomnumber				=	Math.floor(Math.random()*11);
	
	$.ajax({
		url		:	"<?php echo WEBINAR_PLUGIN_URL ?>"+"webinar-db-interaction/webinar-default-attendee-ajax.php",	
		data	:	"webinar_id="+webinar_id+"&attendee_ids="+default_attendee_ids+"&attendee_operation="+attendee_operation+"&randomnumber="+randomnumber+"&max_limit="+max_limit,
		type	:	"GET",	
		async	:	true,
		crossDomain : true,
		dataType: 	"jsonp",
		success	:	function(response){
						
						var default_attendee_response	=	response;
						
						if(default_attendee_response["attendee_add"]=='1'){	
							var total_new_attendees			=	default_attendee_response["new_attendee_name"].length;
							var default_attendee_new_id		=	default_attendee_response["new_ids"];	
							var default_attendee_operation	=	default_attendee_response["set_attendee_operation"];	
						
							if(total_new_attendees>0){	
								for(var j=0;j<total_new_attendees;j++){
									var attendee_name		=	default_attendee_response["new_attendee_name"][j];		
									var attendee_id			=	default_attendee_response["new_attendee_ids"][j];		
									$("#tot_logged_in").prepend('<li class="dotted-border" attendee_id='+attendee_id+'>'+attendee_name+'</li>');				
								}	
							}

							$("#total_logged_attendee").val(default_attendee_new_id);
							$("#default_attendee_operation").val(default_attendee_operation);
						
						}else if(default_attendee_response["attendee_add"]=='0'){
							var deleted_attendees			=	default_attendee_response["deleted_ids"].length;
							var default_attendee_new_id		=	default_attendee_response["new_ids"];	
							var default_attendee_operation	=	default_attendee_response["set_attendee_operation"];
							
							if(deleted_attendees>0){
								for(var k=0;k<deleted_attendees;k++){
									$("li#"+default_attendee_response["deleted_ids"][k]).remove();
								}
							}
							$("#total_logged_attendee").val(default_attendee_new_id);
							$("#default_attendee_operation").val(default_attendee_operation);
						}
						set_total_attendees();
						update_attendee_list();
					}
	});			
}

function set_total_attendees(){
	$("#total_presenter").text(parseInt($("#tot_logged_in li").length)+parseInt($("#real_attendees li").length));
	$( "ul#real_attendees li:odd").addClass("color");
	$( "ul#tot_logged_in li:odd").addClass("color");
}

function track_attendee_status(){
	<?php if(isset($_session_attendee_id)){ ?>
		var getPostsData = {
			action  : 'track_attendee_status',
			attendee: '<?php echo $_session_attendee_id; ?>',
			status  : '3',
			live	: '1'
		}
						
		$.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
		});
	<?php } ?>
}

function onVideoReady(){
	
	<?php if(isset($_session_attendee_id)){ ?>
		var getPostsData = {
			action  : 'track_attendee_status',
			attendee: '<?php echo $_session_attendee_id; ?>',
			status  : '2',
			live	: '1'
		}
						
		$.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
		});
	<?php } ?>
	
}

function updateWatchedStats(percentagePlayed){
	
	<?php if(isset($_session_attendee_id)){ ?>
		var getPostsData = {
			action  : 'track_attendee_watched_status',
			attendee: '<?php echo $_session_attendee_id; ?>',
			watched : percentagePlayed
		}
						
		$.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
		});
	<?php } ?>
}

</script>
<script type="text/javascript">
	$(document).ready(function (){
	
	<?php if($webinar_scarcity[0]->is_scarcity_enabled==1 || count($webinar_delayed_events)>0 ) { ?>
		var last_event_id  = '';
		var last_button_id = '';
		
		$(document.body).on('click', ".call_to_action", function(e){
            var web_id   = $(this).attr("webinar_id");
			var btn_id   = $(this).attr("button_id");
			var event_id = $(this).attr("event_id");
			var is_event = $(this).attr("is_event");
			var ajaxurl  = "<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>";
			
			if(last_event_id!=event_id && last_button_id!=event_id){
			  var getPostsData = {
				  action  : 'add_call_to_action',
				  webinar : web_id,
				  attendee: '<?php if(isset($_session_attendee_id)) echo $_session_attendee_id; else echo 0; ?>',
				  event_id: event_id,
				  is_event: is_event,
				  btn_id  : btn_id,
				  live	: 0
			  }
			  
			  $.post('<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>', getPostsData, function(response) {
				 last_event_id = event_id;
				  last_button_id= btn_id;
				  if(response==1){
				   
				  }
			  });
			}
			
        });
	<?php } ?>
	
	set_total_attendees();
	
	<?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
		$("#send_your_question").submit(function(event){
		event.preventDefault();	

		var admin_email		=	"<?php echo $admin_email; ?>";	
		var your_name		=	jQuery.trim($("#your_name").val());
		var your_email		=	jQuery.trim($("#your_email").val());
		var your_question	=	jQuery.trim($("#your_question").val());		

		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var alpha_num_reg = /^[a-zA-Z ]+$/g;
		
		 if(your_name=="" || your_name=="<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>" ){
			$("#message").removeClass("alert-success success");
			$("#message").addClass("alert-error errors error");
			$("#message").text("Name is mandatory");
			$("#message").show();
			$("#your_name").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>");
			$("#your_name").focus();
			event.preventDefault();											
		}
		else if(alpha_num_reg.test(your_name)==false){
			$("#message").removeClass("alert-success success");
			$("#message").addClass("alert-error errors error");
			$("#message").text("Fill valid name");
			$("#message").show();
			$("#your_name").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>");
			$("#your_name").focus();
			event.preventDefault();	
		}
		else if(your_email=="" || your_email=="Your Email"){
			$("#message").removeClass("alert-success success");
			$("#message").addClass("alert-error errors error");
			$("#message").text("Email is mandatory");
			$("#message").show();
			$("#your_email").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>");
			$("#your_email").focus();
			event.preventDefault();										
		}
		else if(reg.test(your_email) == false) {
			$("#message").removeClass("alert-success success");
			$("#message").addClass("alert-error errors error");
			$("#message").text("Fill valid email");
			$("#message").show();
			$("#your_email").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>");
			$("#your_email").focus();
			event.preventDefault();											
		}
		else if(your_question.trim()=="" || your_question=="<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>"){
			$("#message").removeClass("alert-success success");
			$("#message").addClass("alert-error errors error");
			$("#message").text("What is your question?");
			$("#message").show();
			$("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
			$("#your_question").focus();
			event.preventDefault();	
		}
		else{

		$("#message").hide();
		$("#send_question").attr("disabled","disabled");
		$("#ques-loading").show();
		var getPostsData = {
			action : 'add_attendees_questions',
			webinar: '<?php echo $post->webinar_id;?>',
			attendee:'<?php if(isset($attendee_webinar_session_detail[0]->registration_id_pk)) echo $attendee_webinar_session_detail[0]->registration_id_pk; else echo 0; ?>',
			question: your_question,
			live	: 1,
			name  : your_name,
			email	: your_email,
			admin : admin_email
		}
						
		$.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
			$("#ques-loading").hide();
			if(response>=1){
				$("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
						$("#message").removeClass("alert-error errors error");
						$("#message").addClass("alert-success success");
				  	$("#message").show();
									
						$("#message").text("Thanks for your question, we will get back to you soon");
						$("#your_name").val(your_name);
						$("#your_email").val(your_email);
						$("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
						$("#message").fadeOut(5000);
						setTimeout(function(){
							$("#send_question").removeAttr("disabled","disabled");
						},7000);
			}else{
						$("#message").removeClass("alert-success success");
						$("#message").addClass("alert-error errors error");
						$("#message").text("Your question has not sent. Please try to sent it again");
						$("#message").show();
						setTimeout(function(){
							$("#send_question").removeAttr("disabled","disabled");
						},7000);
			}
		});
		event.preventDefault();	
		}
	});
	<?php }  ?>
	});
</script>
