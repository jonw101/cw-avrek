<?php if((isset($_GET['live'])===true || $webinar_detail[0]->live_page_type==1) && isset($_GET['countdown'])===false) {?>
	<script type="text/javascript">var is_livestream = "<?php echo $webinar_detail[0]->is_livestream;?>"; var SEEK_TO ="<?php echo $seek_to; ?>";</script>
  <?php if($selected_template_dir  == 'new_theme'){ ?>
  <script src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/js/jquery-1.10.1.min.js'; ?>"></script>
  <script src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/js/jquery.counteverest.js'; ?>"></script>
  <?php } else{ ?>
  <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
  <?php } ?>
  <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.jodometer.js' ?>"></script>
  <link rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelementplayer.css'?>"/>
  <?php if($service_type=='vimeo'){?>
  <script type="text/javascript" src="//a.vimeocdn.com/js/froogaloop2.min.js"></script>
  <?php } ?>
  <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
  <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player-responsive.js'?>"></script>
  <?php }else{ ?>
  <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'webinar-flash-player/mediaelement-and-player.js'?>"></script>
  <?php } ?>
  <script type="text/javascript">$(document).ready(function(){$("#scarcity_events").hide();$("#delayed_events").hide();$("#scarcity-delayed").hide();$("#footer_container a").each(function(){$(this).attr("target","_blank");$(this).css("text-decoration","none")})});</script>
  <script type="text/javascript">
		  $(function() {
			  
				<?php if(empty($_GET['gdt']) || empty($_GET['gtm'])){?>
					 getUtcDateTime = function() {
      			
						var local_date=new Date;
						var local_timezone_offset = local_date.getTimezoneOffset();
						local_timezone_offset = parseFloat(-1 * (parseInt(local_timezone_offset, 10) / 60)).toFixed(2);
						var timezone_array = local_timezone_offset.split(".");
						
						var local_timezone = pad(timezone_array[0]) + ":" + pad(parseInt(timezone_array[1] * 60 / 100, 10)) + ":" + "00";
						var local_timezone_operation = "";
						if (parseInt(timezone_array[0], 10) >= 0) local_timezone_operation = "+";
						else local_timezone_operation = "-";
						
						var time_string 		= local_date.toTimeString();
						var user_time_array = time_string.split(" ");
						var user_time  			= user_time_array[0];  
						
						var umonth      = local_date.getMonth();
						if(month==12) var m_name	=	1;	
						else var m_name	=	parseInt(umonth)+1;
						var udate       = local_date.getDate();
						var uyear       = local_date.getFullYear();	
						
						var user_date   =  uyear+"-"+m_name+"-"+udate;
						var timezone 	  =  local_timezone.split(':');
						
						var timezone_hrs = parseInt(timezone[0]==24 ? 0 : timezone[0],10);
						var timezone_min = parseInt(timezone[0]!=24 ? timezone[1] : 0,10);
						
						var timezone_hrs = parseInt(local_timezone_operation+timezone_hrs);
						var timezone_min = parseInt(local_timezone_operation+timezone_min);
						
						var webinar_date = user_date.split('-');
						var webinar_time = user_time.split(':');
						var new_date 		 = new Date(parseInt(webinar_date[0],10),parseInt(webinar_date[1],10)-1,parseInt(webinar_date[2],10),(parseInt(webinar_time[0],10)==24 ? 0 : parseInt(webinar_time[0],10)),parseInt(webinar_time[1],10),parseInt(webinar_time[2],10),0);
						
						var new_date 		 = new_date.valueOf();
						var ms_in_hour 	 = 60000*60;
						var ms_in_min    = 60000;
						var ms_to_add_or_deduct = -((timezone_hrs*ms_in_hour)+(timezone_min*ms_in_min));
			
						new_date 	= new_date + ms_to_add_or_deduct;
						
						var date		=	new Date(new_date);
						var hours 	= check_integer(date.getHours());
						var minutes = check_integer(date.getMinutes());
						var seconds = check_integer(date.getSeconds());
						var day		  = date.getDate();		
						var month	  = date.getMonth();
			
						if(month==12) var month_name	=	1;	
						else var month_name	=	parseInt(month)+1;	
						
						month_name		=	check_integer(month_name);	
						var year			=	date.getFullYear();
						var gmtDate		=	year+"-"+month_name+"-"+day;
			      var gmtTime   = hours + ':' + minutes + ':' + seconds;
						
						return gmtDate+" "+gmtTime;
						
   				 }
					 		 						
						function pad(number){
							if(number>=0){
							 return (parseInt(number,10) < 10 ? '0' : '') + number;  
							}else{
							 return (parseInt(Math.abs(number),10) < 10 ? '0' : '') + Math.abs(number);
							}
						}
						 
						function check_integer(string){
							if(string<10){
							 string	=	"0"+string;	
							}
							return string; 
						}
							 
						function setWebPlayCookie(name,value,seconds){
				    	var expiry = new Date();
							expiry		 = new Date(expiry .getTime() + 1000*seconds);
							var value = escape(value) +  ("; expires="+expiry);
						  document.cookie=name + "=" + value;
   					}
						
						function unsetWebPlayCookie(name){
							 document.cookie = name +"=; max-age=0;" ;
						}
				<?php }	?>
				
				$('video').mediaelementplayer({
					mode: 'auto',
					autoplay : true,
					 <?php if($service_type=='vimeo'){?>
					isClickable : true,
					<?php }else{ ?>
					isClickable : false,
					<?php } ?>
					clickToPlayPause : false,
					iPhoneUseNativeControls: true, 
					iPadUseNativeControls: true,
					AndroidUseNativeControls: true,
					enableKeyboard: false,
					alwaysShowControls : false,
					enableAutosize: true,
					enablePseudoStreaming : true,
					 <?php if($service_type=='vimeo'){?>
					hideControlsForVimeo:true,
					<?php } ?>
					<?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
					features: ['playpause','current','volume'],
					<?php }else{ ?>
						<?php if($service_type=='youtube' || $service_type=='stream'){ ?>
						features: ['current','volume'],
						<?php }else{ ?>
						features: ['current','volume','fullscreen'],
						<?php } ?>
					<?php } ?>
					success: function(player, node) {
						
						<?php if(empty($_COOKIE['_ep_'.$post->webinar_id])) { ?>
						 if(typeof setWebPlayCookie == "function"){
							setWebPlayCookie('_ep_<?php echo $post->webinar_id; ?>',getUtcDateTime(),<?php echo $video_length; ?>);
						 }
						<?php }?>
						
						var hasPlayerStarted = false;
					  var lastUpdatedTime  = 0;
					  if (player.pluginType == 'flash') {
						  player.addEventListener('canplay', function() {
							  player.play();
						  }, false);
					  }else{
						  <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	}else {?>
						  player.play();
						  
						  <?php } ?>
					  }					
					  
					  <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
					  $('.mejs-overlay-button, .mejs-overlay, .ewp-player-overlay').bind("click",function() {
						if(player.paused===true){
						 	player.play();
						 	
						}
					  });
					  <?php }else{ ?>
					  $('.ewp-player-overlay').bind("click",function() {	
						if(player.paused===true){
							player.play();
						}
					  });
					  <?php } ?>
												  
					  player.addEventListener("playing", function( event ) {
						 if(typeof onVideoReady == "function"){
							  onVideoReady();
						  }
					  });
					  
					 player.addEventListener('timeupdate', function(e) {
						 
						  if(player.paused) return;
						  
						  <?php if($webinar_detail[0]->is_livestream==0 || $webinar_detail[0]->is_livestream==1){ ?>
							if(!hasPlayerStarted){
								if(player.currentTime<SEEK_TO){
									player.setCurrentTime(SEEK_TO);
									player.play();
									hasPlayerStarted = true;	
								}
							}
						<?php } ?>
						
						  if(typeof addEvent == "function"){
							  var currentTime =  Math.floor(player.currentTime);
							  
							  if(!(currentTime==lastUpdatedTime)){
								  addEvent(currentTime);
								  lastUpdatedTime = currentTime; 
							  }
						  }
						  
					  },false);
										  
					  player.addEventListener("ended", function( event ) {
						  if(typeof unsetWebPlayCookie == "function"){
							  unsetWebPlayCookie('_ep_<?php echo $post->webinar_id; ?>');
						  }
							if(typeof onVideoEnd == "function"){
							  onVideoEnd();
						  }
					  });
					  
					  $('.ewp-player-overlay, .mejs-overlay').bind('contextmenu', function(e) {return false;});
					
					},
					
					error: function() {
						 alert('Unable to start webinar due to unkown error');
					}
			  });
		});
 
  </script>
  
	<script type="text/javascript">
		
			<?php if($webinar_detail[0]->is_livestream==1){ ?>
				var eventTime = 0;
				var ewpInterval = setInterval(function () {
					++eventTime;
					addEvent(eventTime);
				},1000);
			<?php } ?>
			<?php
			
			 	$admin_email						=	$ob_webinar_functions->_validate_email($webinar_detail[0]->chatbox_code) ? $webinar_detail[0]->chatbox_code : $webinar_detail[0]->admin_email_value;
			  $webinar_delayed_events	=	$ob_webinar_db_interaction->get_webinar_delayed_events($post->webinar_id);
			  $webinar_scarcity				=	$ob_webinar_db_interaction->get_webinar_scarcity($post->webinar_id);
			
			if($webinar_scarcity[0]->is_scarcity_enabled==1 || count($webinar_delayed_events)>0 ) {
				
				$arrEventCalling[] 	= 	'function addEvent(time){';
				$count=1;
			if(count($webinar_delayed_events)>0){
				foreach($webinar_delayed_events as $events){
						
						$html 		= 	array();
						$content 	= 	trim($events->text_to_display);
						$content	= 	str_replace ("/r/n", "", $content);
						$content	=		nl2br($content);
						$content	=		explode ("<br />",$content);
						foreach($content as $value){
							$html[] = trim($value);
						}
						$content	=	implode("",$html);
						$content	=	addslashes($content);
						
						$start_time_array	=	explode(":",$events->start_time);		
						$start_time				=	$start_time_array[0]*3600+$start_time_array[1]*60+$start_time_array[2];
						$end_time_array		=	explode(":",$events->end_time);		
						$end_time					=	$end_time_array[0]*3600+$end_time_array[1]*60+$end_time_array[2];
						$event_start_time	=	$start_time;		
						$event_end_time		=	$end_time;
						$delayed_event_button_link =	$events->chosen_button_link;
					if($events->button_source_path!=''){
						if(trim($events->chosen_button_link)!=""){
							$delayed_button_image	=	'<a class=\"call_to_action event_image'.$count.'\" href=\"'.$delayed_event_button_link.'\" target=\"_blank\" webinar_id=\"'.$events->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$events->webinar_delayed_events_id_pk.'\" button_id=\"'.$events->webinar_button_id_fk.'\" ><img src=\"'.$events->button_source_path.'\" width=\"200\" height=\"50\" /></a>';
						}
						else{
							$delayed_button_image	=	'<img src=\"'.$events->button_source_path.'\"  width=\"200\" height=\"50\" webinar_id=\"'.$events->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$events->webinar_delayed_events_id_pk.'\" button_id=\"'.$events->webinar_button_id_fk.'\" class=\"call_to_action\" />';
						}
						
					}else { 
						$delayed_button_image=''; 
					}
					
					
					if($seeking_point>$event_start_time && $seeking_point<$event_end_time && ( !empty($content) || !empty($delayed_button_image) ) ){

						$arrEvents[]			=	'var eventCall'.$count.' = function(){';
						$arrEvents[]			=	'$("#scarcity-delayed").show();';
						$arrEvents[]			=	'$("#delayed_events").show();';
						$arrEvents[]			=	'var width_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(width_scarcity_events>0){';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'	}else{';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'$("#delayed_events_text").append("<div style=\"margin-bottom:20px;padding-bottom:20px;display:none;\" class=event_'.$count.'>'.$content.''.$delayed_button_image.'</div>");';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeIn(1000);';
						$arrEvents[]			=	' };';
						$arrEventCalling[]		=	'if(time == '.$seeking_point.'){ if(ewpEventArray.indexOf("eventCall'.$count.'")!= -1){ eventCall'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventCall'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventCall".$count."'";
						
						$arrEvents[]			=	'var eventRemove'.$count.' = function(){';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeOut(1000,function(){$(".event_'.$count.'").remove();';
						$arrEvents[]			=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
						$arrEvents[]			=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(remaining_delayed_events==0){';
						$arrEvents[]			=	'	$("#delayed_events").hide();';
						$arrEvents[]			=	'	} ';
						$arrEvents[]			=	'if(remaining_scarcity_events==0  ){';
						$arrEvents[]			=	'	$("#scarcity_events").hide();';
						$arrEvents[]			=	'	} ';
						$arrEvents[]			=	'	});';
						$arrEvents[]			=	'};';
						$arrEventCalling[]		= 	'if(time == '.$end_time.'){  if(ewpEventArray.indexOf("eventRemove'.$count.'")!= -1){ eventRemove'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventRemove'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventRemove".$count."'";
						$count++;
					}
					else if($seeking_point<$event_end_time && ( !empty($content) or !empty($delayed_button_image) ) ){
						
						$arrEvents[]			=	'var eventCall'.$count.' = function(){';
						$arrEvents[]			=	'$("#scarcity-delayed").show();';
						$arrEvents[]			=	'$("#delayed_events").show();';
						$arrEvents[]			=	'var width_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(width_scarcity_events>0){';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span12").addClass("span6");';
						$arrEvents[]			=	'	}else{';
						$arrEvents[]			=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'$("#delayed_events_text").append("<div style=\"margin-bottom:20px;padding-bottom:20px;\" class=event_'.$count.'>'.$content.''.$delayed_button_image.'</div>");';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeIn(1000);';
						$arrEvents[]			=	'	};';
						$arrEventCalling[]		=	'if(time == '.$start_time.'){ if(ewpEventArray.indexOf("eventCall'.$count.'")!= -1){ eventCall'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventCall'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventCall".$count."'";
												
						$arrEvents[]			=	'var eventRemove'.$count.' = function(){';
						$arrEvents[]			=	'$(".event_'.$count.'").fadeOut(1000,function(){$(".event_'.$count.'").remove();';
						$arrEvents[]			=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
						$arrEvents[]			=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
						$arrEvents[]			=	'if(remaining_delayed_events==0){';
						$arrEvents[]			=	'	$("#delayed_events").hide();';
						$arrEvents[]			=	'   $("#scarcity_events").removeClass("span6").addClass("span12");';
						$arrEvents[]			=	'if(remaining_scarcity_events==0  ){';
						$arrEvents[]			=	'	$("#scarcity-delayed").hide();';
						$arrEvents[]			=	'	};';
						$arrEvents[]			=	'	} });';
						$arrEvents[]			=	'};';
						$arrEventCalling[]	= 	'if(time == '.$end_time.'){  if(ewpEventArray.indexOf("eventRemove'.$count.'")!= -1){ eventRemove'.$count.'(); ewpEventArray.splice(ewpEventArray.indexOf("eventRemove'.$count.'"), 1);} }';
						$arrEventAray[]			=	"'eventRemove".$count."'";
						$count++;
					}
				}	
			}
			if($webinar_scarcity[0]->is_scarcity_enabled==1){
					$scarcity_start_time_array	=	explode(":",$webinar_scarcity[0]->start_time);		
					$scarcity_start_time				=	$scarcity_start_time_array[0]*3600+$scarcity_start_time_array[1]*60+$scarcity_start_time_array[2];
					$scarcity_end_time_array		=	explode(":",$webinar_scarcity[0]->end_time);		
					$scarcity_end_time					=	$scarcity_end_time_array[0]*3600+$scarcity_end_time_array[1]*60+$scarcity_end_time_array[2];
					if($scarcity_end_time==0){
						$scarcity_end_time = $video_length;
					}
					
					$scarcity_event_start_time	=	$scarcity_start_time;		
					$scarcity_event_end_time		=	$scarcity_end_time;
					
					$scarcity_start_number			=	$webinar_scarcity[0]->start_number;
					$scarcity_end_number				=	$webinar_scarcity[0]->end_number;
					
										
					$slots_per_min					=	5;
					$interval								= 12;
					$end_time								= $scarcity_start_time+12;
					$time_difference				=	$scarcity_end_time - $scarcity_start_time;
					$random_numbers					=	$ob_webinar_functions->generate_random_series($scarcity_start_number,$scarcity_end_number,$time_difference,$slots_per_min);
					if($current_time_seconds>$scarcity_event_start_time && $current_time_seconds<$scarcity_event_end_time){
						$scarcity_current_start_number	=	$ob_webinar_functions->calculate_scracity_start_number($time_difference,$seeking_point,$scarcity_start_number,$scarcity_end_number);
						
						$scarcity_start_number	=	$scarcity_current_start_number;
						
						$random_numbers					=	$ob_webinar_functions->generate_random_series($scarcity_start_number,$scarcity_end_number,$time_difference,$slots_per_min);
					}
					
					$image_url								=	WEBINAR_PLUGIN_URL.'templates/images/jodometer-numbers.png';
					
					if($webinar_scarcity[0]->button_source_path!=''){
						if(trim($webinar_scarcity[0]->choose_button_link)!="")
							$button_image	=	'<a href='.$webinar_scarcity[0]->choose_button_link.' target=\"_blank\" webinar_id=\"'.$webinar_scarcity[0]->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$webinar_scarcity[0]->webinar_scarcity_id_pk.'\" button_id=\"'.$webinar_scarcity[0]->button_id_fk.'\"  class=\"call_to_action\" ><img src=\"'.$webinar_scarcity[0]->button_source_path .'\" width=\"200\" height=\"50\" ></a>';
						else
							$button_image	=	'<img src=\"'.$webinar_scarcity[0]->button_source_path .'\" width=\"200\" height=\"50\" webinar_id=\"'.$webinar_scarcity[0]->webinar_id_fk.'\" is_event=\"1\" event_id=\"'.$webinar_scarcity[0]->webinar_scarcity_id_pk.'\" button_id=\"'.$webinar_scarcity[0]->button_id_fk.'\"  class=\"call_to_action\" >';
					}else { $button_image	=	'';}
					
					for($i=0;$i<count($random_numbers);$i++){
						$number	=  $scarcity_start_number-$random_numbers[$i];
						$width	=	strlen($scarcity_start_number)*40;
						$width	=	$width."px";
						
						$content 	= trim($webinar_scarcity[0]->description);
						$html = array();
						$content	= 	str_replace ("/r/n", "", $content);
						$content	=	nl2br($content);
						$content	=	explode ("<br />",$content);
						foreach($content as $value)
						{
							$html[] = trim($value);
						}
						$content	=	implode("",$html);
						$content	=	addslashes($content);
						
						if($seeking_point>$scarcity_event_start_time && $seeking_point<$scarcity_event_end_time){
							
							$arrEvents[]				=	'var scarcityEvent'.$i.' = function(){';
							$arrEvents[]				=	'var width_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'$("#scarcity-delayed").css("display","block");';
							$arrEvents[]				=	'if(width_delayed_events>0){';
							$arrEvents[]				=	'$("#delayed_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'}else{ ';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	' };';
							$arrEvents[]				=	'$("#scarcity_events").fadeIn(1000);';
							$arrEvents[]				=	'$(".scarcity_counter").css("width","'.$width.'");';
							$arrEvents[]				=	'$(".scarcity_counter").jOdometer({ counterStart:"'.$scarcity_start_number.'", counterEnd:"'.$number.'", numbersImage: "'.$image_url.'"});';
							$arrEvents[]				=	'$("#scarcity_events_text").html("<div class=scarcity_'.$i.'>'.$content.''.$button_image.'</div>");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]	= 	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcityEvent'.$i.'") != -1) { scarcityEvent'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEvent'.$i.'"), 1); } }';
							$arrEventAray[]			=	"'scarcityEvent".$i."'";
							
							$arrEvents[]				=	'var scarcityEventRemove'.$i.' = function(){';
							$arrEvents[]				=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
							$arrEvents[]				=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if('.$number.'=='.$scarcity_end_number.'){';
							$arrEvents[]				=	'$("#scarcity_events").fadeOut(1000,function(){ $("#scarcity_events").remove();  ';
							$arrEvents[]				=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	'if(remaining_delayed_events==0 ){';
							$arrEvents[]				=	'	$("#scarcity-delayed").hide();';
							$arrEvents[]				=	'	}; });';
							$arrEvents[]				=	'	};';
							$arrEvents[]				=	'};';
							$arrEventCalling[]		= 	'if(time == '.$end_time.'){ if (ewpEventArray.indexOf("scarcityEventRemove'.$i.'") != -1) { scarcityEventRemove'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEventRemove'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEventRemove".$i."'";
							
						}else if($seeking_point<$scarcity_event_end_time){
						
							$arrEvents[]				=	'var scarcity_event_start'.$i.'= function(){';
							$arrEvents[]				=	'$("#scarcity-delayed").css("display","block");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			=	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcity_event_start'.$i.'") != -1) { scarcity_event_start'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcity_event_start'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcity_event_start".$i."'";
							
							$arrEvents[]				=	'var scarcityEvent'.$i.' = function(){';
							$arrEvents[]				=	'var width_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if(width_delayed_events>0){';
							$arrEvents[]				=	'$("#delayed_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span12").addClass("span6");';
							$arrEvents[]				=	' }else{ ';
							$arrEvents[]				=	'$("#scarcity_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	' };';
							$arrEvents[]				=	'$("#scarcity_events").fadeIn(1000);';
							$arrEvents[]				=	'$(".scarcity_counter").css("width","'.$width.'");';
							$arrEvents[]				=	'$(".scarcity_counter").jOdometer({ counterStart:"'.$scarcity_start_number.'", counterEnd:"'.$number.'", numbersImage: "'.$image_url.'"});';
							$arrEvents[]				=	'$("#scarcity_events_text").html("<div class=scarcity_'.$i.'>'.$content.''.$button_image.'</div>");';
							$arrEvents[]				=	'};';
							$arrEventCalling[]			= 	'if(time == '.$scarcity_start_time.'){ if (ewpEventArray.indexOf("scarcityEvent'.$i.'") != -1) { scarcityEvent'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEvent'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEvent".$i."'";
							
							$arrEvents[]				=	'var scarcityEventRemove'.$i.' = function(){';
							$arrEvents[]				=	'var remaining_scarcity_events = $("#scarcity_events_text").children().size();';
							$arrEvents[]				=	'var remaining_delayed_events = $("#delayed_events_text").children().size();';
							$arrEvents[]				=	'if('.$number.'=='.$scarcity_end_number.'){';
							$arrEvents[]				=	'$("#scarcity_events").fadeOut(1000,function(){ $("#scarcity_events").delay(1000).remove();  ';
							$arrEvents[]				=	'   $("#delayed_events").removeClass("span6").addClass("span12");';
							$arrEvents[]				=	'if(remaining_delayed_events==0 ){';
							$arrEvents[]				=	'	$("#scarcity-delayed").hide();';
							$arrEvents[]				=	'	}; });';
							$arrEvents[]				=	'	};';
							$arrEvents[]				=	'};';
							$arrEventCalling[]		= 	'if(time == '.$end_time.'){ if (ewpEventArray.indexOf("scarcityEventRemove'.$i.'") != -1) { scarcityEventRemove'.$i.'(); ewpEventArray.splice(ewpEventArray.indexOf("scarcityEventRemove'.$i.'"), 1); } }';
							$arrEventAray[]				=	"'scarcityEventRemove".$i."'";
							
						}
							
							$scarcity_start_time = $scarcity_start_time + 12;
							$end_time	= $scarcity_start_time +12;
							$scarcity_start_number				= 	$scarcity_start_number-$random_numbers[$i];
					}
			}
			$arrEventCalling[] =  '};';
		
			echo implode('',$arrEvents);
			echo 'var ewpEventArray = new Array('.implode(',',$arrEventAray).');';
			echo implode('',$arrEventCalling);
			}?>
			
		function onVideoEnd(){
			<?php if(isset($webinar_detail[0]->automated_redirect_url) && !empty($webinar_detail[0]->automated_redirect_url)) { ?>
				window.location.replace("<?php echo $webinar_detail[0]->automated_redirect_url;?>");
			<?php } ?>
		}
</script>

  <script type="text/javascript">
	  $(document).ready(function (){
		  
		       <?php /*?><?php if($webinar_detail[0]->webinar_timezone_id_fk > 0) { ?>
	           document.cookie="_ewpc_"+<?php echo $post->webinar_id; ?>=<?php echo $seek_to; ?>";
			   <?php } ?><?php */?>
	    <?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
			  var last_question = '';
			  $("#send_your_question").submit(function(event){
			  event.preventDefault();	
  
			  var admin_email		=	"<?php echo $admin_email; ?>";	
			  var your_name			=	jQuery.trim($("#your_name").val());
			  var your_email		=	jQuery.trim($("#your_email").val());
			  var your_question	=	jQuery.trim($("#your_question").val());		
			  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			  var alpha_num_reg = /^[a-zA-Z ]+$/g;
			  
			  if(your_name=="" || your_name=="<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>" ){
				  $("#message").removeClass("alert-success success");
				  $("#message").addClass("alert-error errors error");
				  $("#message").text("Name is mandatory");
				  $("#message").show();
				  $("#your_name").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>");
				  $("#your_name").focus();
				  event.preventDefault();											
			  }
			  else if(alpha_num_reg.test(your_name)==false){
				  $("#message").removeClass("alert-success success");
				  $("#message").addClass("alert-error errors error");
				  $("#message").text("Fill valid name");
				  $("#message").show();
				  $("#your_name").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>");
				  $("#your_name").focus();
				  event.preventDefault();	
			  }
			  else if(your_email=="" || your_email=="Your Email"){
				  $("#message").removeClass("alert-success success");
				  $("#message").addClass("alert-error errors error");
				  $("#message").text("Email is mandatory");
				  $("#message").show();
				  $("#your_email").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>");
				  $("#your_email").focus();
				  event.preventDefault();										
			  }
			  else if(reg.test(your_email) == false) {
				  $("#message").removeClass("alert-success success");
				  $("#message").addClass("alert-error errors error");
				  $("#message").text("Fill valid email");
				  $("#message").show();
				  $("#your_email").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>");
				  $("#your_email").focus();
				  event.preventDefault();											
			  }
			  else if(your_question.trim()=="" || your_question=="<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>"){
				  $("#message").removeClass("alert-success success");
				  $("#message").addClass("alert-error errors error");
				  $("#message").text("What is your question?");
				  $("#message").show();
				  $("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
				  $("#your_question").focus();
				  event.preventDefault();	
			  }
			  else{
  
			  $("#message").hide();
			  $("#send_question").attr("disabled","disabled");
			  
			  var getPostsData = {
				  action : 'add_attendees_questions',
				  webinar: '<?php echo $post->webinar_id;?>',
				  attendee: '<?php if(isset($attendee_id)) echo $attendee_id; else echo 0; ?>',
				  question: your_question,
				  live	: 0,
				  name  : your_name,
				  email	: your_email,
					admin : admin_email
			  }
					  
			  $.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
				  
				 	if(response>=1){
						$("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
								$("#message").removeClass("alert-error errors error");
								$("#message").addClass("alert-success success");
								$("#message").show();
											
								$("#message").text("Thanks for your question, we will get back to you soon");
								$("#your_name").val(your_name);
								$("#your_email").val(your_email);
								$("#your_question").val("<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>");
								$("#message").fadeOut(5000);
								setTimeout(function(){
									$("#send_question").removeAttr("disabled","disabled");
								},7000);
					}else{
								$("#message").removeClass("alert-success success");
								$("#message").addClass("alert-error errors error");
								$("#message").text("Your question has not sent. Please try to sent it again");
								$("#message").show();
								setTimeout(function(){
									$("#send_question").removeAttr("disabled","disabled");
								},7000);
					}
			  });
			  event.preventDefault();	
			  }
	  
		  });
		  <?php } ?>
	  });
  </script>
  
<?php }else{ ?>

	<link rel="Stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/css/countdown.css'; ?>" />
	<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
	<?php if($selected_template_dir == 'new_theme') { ?>
<script src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/js/jquery-1.10.1.min.js'; ?>"></script>
<script src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/js/jquery.counteverest.js'; ?>"></script>
<?php } ?>
	<script language="javascript" type="text/javascript">
	
	function pad(number){
		 if(number>=0){
			 return (parseInt(number,10) < 10 ? '0' : '') + number;  
		 }else{
			 return (parseInt(Math.abs(number),10) < 10 ? '0' : '') + Math.abs(number);
		 }
 }
 
 function time_to_timestamp(time){
		var time_array=	time.split(":");	
		var	hours			=	time_array[0]*3600;
		var minutes		=	time_array[1]*60;
		var seconds		=	time_array[2];
		return (parseInt(hours)+parseInt(minutes)+parseInt(seconds))*1000;
	}		
	
	function convert_date(getdate){
		var date_array=	getdate.split("-");			
		return date_array[1]+"/"+date_array[2]+"/"+date_array[0];
	}	
	
	function toTimestamp(strDate){
		return Date.parse(strDate);
	}
	
	function check_integer(string){
		if(string<10){
			string	=	"0"+string;	
		}
		return string; 
	}
  
	function get_user_time_in_gmt() {
		    
			 var local_date=new Date;var local_timezone_offset=local_date.getTimezoneOffset();local_timezone_offset=parseFloat(-1*(parseInt(local_timezone_offset,10)/60)).toFixed(2);var timezone_array=local_timezone_offset.split(".");var local_timezone=pad(timezone_array[0])+":"+pad(parseInt(timezone_array[1]*60/100,10))+":"+"00";var local_timezone_operation="";if(parseInt(timezone_array[0],10)>=0)local_timezone_operation="+";else local_timezone_operation="-";var localtime=local_date.toTimeString();
localtimezone=localtime.substring(localtime.indexOf(" "));localtimezone=localtimezone.split("(").join("").split(")").join("");$("#your_timezone").val(local_timezone);$("#your_timezone").attr("timezone_operation",local_timezone_operation);
			
					  var time_string 		= local_date.toTimeString();
						var user_time_array = time_string.split(" ");
						var user_time  			= user_time_array[0];  
						
						var umonth      = local_date.getMonth();
						if(month==12) var m_name	=	1;	
						else var m_name	=	parseInt(umonth)+1;
						var udate       = local_date.getDate();
						var uyear       = local_date.getFullYear();	
						
						var user_date  =  uyear+"-"+m_name+"-"+udate; 
						
		        var timezone_gmt_operation	=	$("#your_timezone").attr("timezone_operation")+"1";
						
						$("#timezone_gmt").val($("#your_timezone").val());	
						$("#timezone_gmt_operation").val(timezone_gmt_operation);
						
						var	timezone_gmt_diffrence	=	jQuery.trim($("#your_timezone").val());
					    
						var	timezone_operation			=	parseInt(jQuery.trim($("#timezone_gmt_operation").val()))+1;
						
						var	timezone_gmt_timestamp	=	time_to_timestamp(timezone_gmt_diffrence);
						
						var converted_date			=	convert_date(user_date);
						
						var date_timestamp			=	toTimestamp(''+converted_date+' '+user_time);
						if(timezone_operation>0){
							var total_timestamp		=	parseInt(date_timestamp)-parseInt(timezone_gmt_timestamp);	
						}else{
							var total_timestamp		=	parseInt(date_timestamp)+parseInt(timezone_gmt_timestamp);	
						}
						
						var timezone = timezone_gmt_diffrence.split(':');
						
						var timezone_hrs = parseInt(timezone[0]==24 ? 0 : timezone[0],10);
						var timezone_min = parseInt(timezone[0]!=24 ? timezone[1] : 0,10);
			
						var timezone_hrs = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_hrs));
						var timezone_min = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_min));
						
						var webinar_date = user_date.split('-');
						var webinar_time = user_time.split(':');
						var new_date 		 = new Date(parseInt(webinar_date[0],10),parseInt(webinar_date[1],10)-1,parseInt(webinar_date[2],10),(parseInt(webinar_time[0],10)==24 ? 0 : parseInt(webinar_time[0],10)),parseInt(webinar_time[1],10),0,0);
						var new_date 		 = new_date.valueOf();
						var ms_in_hour 	 = 60000*60;
						var ms_in_min    = 60000;
						var ms_to_add_or_deduct = -((timezone_hrs*ms_in_hour)+(timezone_min*ms_in_min));
			
						new_date 	= new_date + ms_to_add_or_deduct;
						
						var date		=	new Date(new_date);
						var hours 	= check_integer(date.getHours());
						var minutes = check_integer(date.getMinutes());
						var seconds = check_integer(date.getSeconds());
						var day		  = date.getDate();		
						var month	  = date.getMonth();
			
						if(month==12) var month_name	=	1;	
						else var month_name	=	parseInt(month)+1;	
						
						month_name		=	check_integer(month_name);	
						var year			=	date.getFullYear();
						var final_date=	year+"-"+month_name+"-"+day;
			      var formattedTime = hours + ':' + minutes + ':' + seconds;
						return [formattedTime, final_date];
		   
		}
    
    $(document).ready(function() {
    	scheduled_time = '';
        $("#countdown_dashboard").hide();
        var user_time_gmt =    get_user_time_in_gmt()[0];
				var user_date_gmt =    get_user_time_in_gmt()[1];
	    	var attendee_date_in_gmt = user_date_gmt;
				var attendee_time_in_gmt = user_time_gmt;
		
        var getPostsData = {
                action : 'available_webinar_registration_dates',
                webinar_id: '<?php echo $post->webinar_id;?>',
                timezone_difference : $("#your_timezone").val(),
                timezone_operation	: $("#your_timezone").attr("timezone_operation"),
								attendee_date_in_gmt : attendee_date_in_gmt,
								attendee_time_in_gmt : attendee_time_in_gmt,
        }
                    
            $.post("<?php if(is_admin()) echo admin_url('admin-ajax.php'); else echo home_url('wp-admin/admin-ajax.php'); ?>", getPostsData, function(response) {
              	
								var eventurl = $.trim($("#webinar_video_link").val());
									
								try{
                	var response_array  = $.parseJSON(response);
                	var valid_timestamp = (typeof response_array.remaining_time !== 'undefined') ? response_array.remaining_time : 0;
									var scheduled_date	= (typeof response_array.date !== 'undefined') ? response_array.date : '';
									var scheduled_time	= (typeof response_array.time !== 'undefined') ? response_array.time : '';
									if(scheduled_date!='' && scheduled_time!=''){
										eventurl = eventurl + (eventurl.indexOf('?') != -1 ? "&gdt="+scheduled_date+"&gtm="+scheduled_time : "?gdt="+scheduled_date+"&gtm="+scheduled_time);
										$("#webinar_video_link").val(eventurl);
										console.log(eventurl);
									}else{
										eventurl = eventurl + (eventurl.indexOf('?') != -1 ? "&gdt="+attendee_date_in_gmt+"&gtm="+attendee_time_in_gmt : "?gdt="+attendee_date_in_gmt+"&gtm="+attendee_time_in_gmt);
										$("#webinar_video_link").val(eventurl);
									}
									
								}catch(e){
									var valid_timestamp = '0';
										eventurl = eventurl + (eventurl.indexOf('?') != -1 ? "&gdt="+attendee_date_in_gmt+"&gtm="+attendee_time_in_gmt : "?gdt="+attendee_date_in_gmt+"&gtm="+attendee_time_in_gmt);
										$("#webinar_video_link").val(eventurl);
								}
							      scheduled_time	=	parseInt(valid_timestamp);
							      var date = new Date();
					                
				                    date.setTime(date.getTime()+(scheduled_time));
				                    var day 	=	date.getDate();
				                    var year 	=	date.getFullYear();
				                    var month =	date.getMonth()+1;
				                    var hour 	=	date.getHours();
				                    var minutes	=	date.getMinutes();	
                    <?php if($selected_template_dir != 'new_theme') { ?>
                    
										$('#countdown_dashboard').countDown({
                        targetDate: {
                            'day': 		day,
                            'month': 	month,
                            'year': 	year,
                            'hour': 	hour,
                            'min': 		minutes,
                            'sec': 		0,
                        }	
                    });
					                    <?php } else { ?>

					                    $(".countdown").countEverest({
					            			//Set your target date here!
					            			day: day,
					            			month: month,
					            			year: year,
					            			hour:hour,
					            			minute:minutes,
					            			leftHandZeros: false,
					            			onChange: function() {
					            				drawCircle(document.getElementById('days'), this.days, 365);
					            				drawCircle(document.getElementById('hours'), this.hours, 24);
					            				drawCircle(document.getElementById('minutes'), this.minutes, 60);
					            				drawCircle(document.getElementById('seconds'), this.seconds, 60);
					            			},
					            			onComplete: function() {
					            				var redirection_link = $("#webinar_video_link").val();
					            				if(!redirection_link==''){
					            				//window.location.href	=	redirection_link;
					            				window.location.replace(redirection_link);
					            				}
					            				}
					            			
					            		});

					            		function deg(v) {
					            			return (Math.PI/180) * v - (Math.PI/2);
					            		}

					            		function drawCircle(canvas, value, max) {
					            			var	circle = canvas.getContext('2d');
					            			
					            			circle.clearRect(0, 0, canvas.width, canvas.height);
					            			circle.lineWidth = 3;

					            			circle.beginPath();
					            			circle.arc(
					            					canvas.width / 2, 
					            					canvas.height / 2, 
					            					canvas.width / 2 - circle.lineWidth, 
					            					deg(0), 
					            					deg(360 / max * (max - value)), 
					            					false);
					            			circle.strokeStyle = '#ced1d6';
					            			circle.stroke();

					            			circle.beginPath();
					            			circle.arc(
					            					canvas.width / 2, 
					            					canvas.height / 2, 
					            					canvas.width / 2 - circle.lineWidth, 
					            					deg(0), 
					            					deg(360 / max * (max - value)), 
					            					true);
					            			
					            			/*Colors for Day Canvas Circle*/
					            			var day = document.getElementById('days').getContext('2d');
					            			day.strokeStyle = '#1da2ce';
					            			day.stroke();
					            			
					            			/*Colors for Hours Canvas Circle*/
					            			var day = document.getElementById('hours').getContext('2d');
					            			day.strokeStyle = '#68d387';
					            			day.stroke();
					            			
					            			/*Colors for Minutes Canvas Circle*/
					            			var day = document.getElementById('minutes').getContext('2d');
					            			day.strokeStyle = '#ec585f';
					            			day.stroke();
					            			
					            			/*Colors for Seconds Canvas Circle*/
					            			var day = document.getElementById('seconds').getContext('2d');
					            			day.strokeStyle = '#fcae2f';
					            			day.stroke();
					            			
					            		}

					                    
					                    <?php } ?>
									 $("#countdown_dashboard").show();
            });
    });
    <?php if($selected_template_dir != 'new_theme') { ?>
(function($){$.fn.countDown=function(options){config={};$.extend(config,options);diffSecs=this.setCountDown(config);if(config.onComplete)$.data($(this)[0],"callback",config.onComplete);if(config.omitWeeks)$.data($(this)[0],"omitWeeks",config.omitWeeks);$("#"+$(this).attr("id")+" .digit").html('<div class="top"></div><div class="bottom"></div>');$(this).doCountDown($(this).attr("id"),diffSecs,500);return this};$.fn.stopCountDown=function(){clearTimeout($.data(this[0],"timer"))};$.fn.startCountDown=
function(){this.doCountDown($(this).attr("id"),$.data(this[0],"diffSecs"),500)};$.fn.setCountDown=function(options){var targetTime=new Date;if(options.targetDate)targetTime=new Date(options.targetDate.month+"/"+options.targetDate.day+"/"+options.targetDate.year+" "+options.targetDate.hour+":"+options.targetDate.min+":"+options.targetDate.sec+(options.targetDate.utc?" UTC":""));else if(options.targetOffset){targetTime.setFullYear(options.targetOffset.year+targetTime.getFullYear());targetTime.setMonth(options.targetOffset.month+
targetTime.getMonth());targetTime.setDate(options.targetOffset.day+targetTime.getDate());targetTime.setHours(options.targetOffset.hour+targetTime.getHours());targetTime.setMinutes(options.targetOffset.min+targetTime.getMinutes());targetTime.setSeconds(options.targetOffset.sec+targetTime.getSeconds())}var nowTime=new Date;diffSecs=Math.floor((targetTime.valueOf()-nowTime.valueOf())/1E3);$.data(this[0],"diffSecs",diffSecs);return diffSecs};$.fn.doCountDown=function(id,diffSecs,duration){$this=$("#"+
id);if(diffSecs<=0){var redirection_link=$("#webinar_video_link").val();if(!redirection_link=="")window.location.replace(redirection_link);diffSecs=0;if($.data($this[0],"timer"))clearTimeout($.data($this[0],"timer"))}secs=diffSecs%60;mins=Math.floor(diffSecs/60)%60;hours=Math.floor(diffSecs/60/60)%24;if($.data($this[0],"omitWeeks")==true){days=Math.floor(diffSecs/60/60/24);weeks=Math.floor(diffSecs/60/60/24/7)}else{days=Math.floor(diffSecs/60/60/24)%7;weeks=Math.floor(diffSecs/60/60/24/7)}$this.dashChangeTo(id,
"seconds_dash",secs,duration?duration:800);$this.dashChangeTo(id,"minutes_dash",mins,duration?duration:1200);$this.dashChangeTo(id,"hours_dash",hours,duration?duration:1200);$this.dashChangeTo(id,"days_dash",days,duration?duration:1200);$this.dashChangeTo(id,"weeks_dash",weeks,duration?duration:1200);$.data($this[0],"diffSecs",diffSecs);if(diffSecs>0){e=$this;t=setTimeout(function(){e.doCountDown(id,diffSecs-1)},1E3);$.data(e[0],"timer",t)}else if(cb=$.data($this[0],"callback"))$.data($this[0],"callback")()};
$.fn.dashChangeTo=function(id,dash,n,duration){$this=$("#"+id);for(var i=$this.find("."+dash+" .digit").length-1;i>=0;i--){var d=n%10;n=(n-d)/10;$this.digitChangeTo("#"+$this.attr("id")+" ."+dash+" .digit:eq("+i+")",d,duration)}};$.fn.digitChangeTo=function(digit,n,duration){if(!duration)duration=800;if($(digit+" div.top").html()!=n+""){$(digit+" div.top").css({"display":"none"});$(digit+" div.top").html(n?n:"0").slideDown(duration);$(digit+" div.bottom").animate({"height":""},duration,function(){$(digit+
" div.bottom").html($(digit+" div.top").html());$(digit+" div.bottom").css({"display":"block","height":""});$(digit+" div.top").hide().slideUp(10)})}}})(jQuery);
<?php } ?>
</script>
<?php  } ?>