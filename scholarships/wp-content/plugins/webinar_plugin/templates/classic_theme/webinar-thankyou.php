<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo plugins_url().'/webinar_plugin/templates/' .$selected_template_dir.'/css/main.css';?>" rel="stylesheet" type="text/css"  />
<link  href="<?php echo plugins_url().'/webinar_plugin/templates/' . $selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css';?>" rel="stylesheet" type="text/css"  />
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->thankyou_meta_title)) echo "Thank you"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_title);?>" />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title>
<?php if(empty($webinar_detail[0]->thankyou_webinar_page_title)) echo "Thank you"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_webinar_page_title);?>
</title>
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<style>
@media print {.noPrint { display:none; }}
 		<?php  if($service == 'vimeo' || $service_sharing == 'vimeo'){ ?>  
    #me_vimeo_0_container, #me_vimeo_1_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe, #me_vimeo_1_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
    fb_ltr.fb_iframe_widget_lift {z-index: 1000000;}
</style>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_header_analytics); ?>
</head>
<body style="background:#ededed;">
<noscript>
<center>
  <h1>JavaScript must be enabled in order for you to view webinar.</h1>
</center>
</noscript>
<div class="reg_topbar"></div>
<!--header-->
<div class="reg_header">
  <?php if($webinar_detail[0]->header_logo!='') { ?>
  <div class="reg_logo"> <img src="<?php echo $webinar_detail[0]->header_logo;?>"  width="170" height="100" /></div>
  <div class="header_heading_block">
    <?php } else { ?>
    <div class="header_heading_block_full">
      <?php } ?>
      <h1>
        <?php if(empty($webinar_detail[0]->thankyou_title)) echo "WEBINAR BROADCAST EVENT"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_title);  ?>
      </h1>
      <h2><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_subtitle);  ?></h2>
    </div>
  </div>
</div>
<!--end header--> 

<!--content-->
<div class="reg_content1 clearfix"> 
  <!--video-->
  
  <div class="left">
    <div class="thanku_heading">Congratulations! You are Registered</div>
    <?php
			
			if($webinar_detail[0]->thankyou_attachment_type==1){ ?>
    <div class="video-outside">
      <?php
				if($webinar_detail[0]->thankyou_attachment_thumb_path!='') { 

		?>
      <div class="video-inside"><img width="507" height="339" alt="player" src="<?php echo $webinar_detail[0]->thankyou_attachment_thumb_path; ?>" ></div>
      <?php } ?>
    </div>
    <?php } else {  if(empty($thankyou_attachment_embed_script)){ ?>
    <div id="player" <?php if($webinar_detail[0]->thankyou_video_aspect_ratio==1) echo 'class="thankyou_video_block_for_4X3"'; else echo 'class="thankyou_video_block"';?> style="height:<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>px;width:500px;" >
      <section id="main_content" class="inner video_section">
        <?php if($service == 'youtube'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
        </video>
        <?php }else{ ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
        </video>
        <?php } ?>
        <?php }elseif($service == 'other'){ ?>
        <?php if($thankyou_attachment_format=='mp4'){ ?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
        </video>
        <?php }else{ ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
        </video>
        <?php } ?>
        <?php }elseif($thankyou_attachment_format=='flv'){ ?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <?php if($alternate_video_format=='youtube'){ ?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php  }elseif($alternate_video_format=='mp4'){ ?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php } ?>
        <?php }else{ ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/flv" src="<?php echo $thankyou_attachment_path;?>" />
        </video>
        <?php } ?>
        <?php }?>
        <?php }else if($service == 'stream'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <?php if($alternate_video_format=='youtube'){ ?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php  }elseif($alternate_video_format=='mp4'){ ?>
        <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php } ?>
        <?php }else{ ?>
        <?php if($thankyou_attachment_format=='mp4'){ ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $thankyou_attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
        </video>
        <?php }elseif($thankyou_attachment_format=='flv'){ ?>
        <?php $flv_stream = str_replace('.flv','',$stream); ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $thankyou_attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
        </video>
        <?php }?>
        <?php } ?>
        <?php }elseif($service=='vimeo'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
        </video>
        <?php }else{ ?>
        <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
        </video>
        <?php } ?>
        <?php }?>
      </section>
    </div>
    <?php } else{
    echo $thankyou_attachment_embed_script;
    }
    } ?>
  </div>
  <!--end video--> 
  
  <!-- tickets block start-->
  <div class="ticket_block">
    <div class="ticket_head">
      <div class="bonus_head_icon"><img src="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/images/tickets.png';?>" width="32" height="34" /></div>
      <div class="bonus_head_seprator"></div>
      <h2>Your Webinar Tickets</h2>
      <a href="javascript:window.print()"><img src="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/images/print-icn.png';?>" style="margin-top:-30px; margin-left:290px;"/></a> </div>
    <form class="ticket_mid_pannel">
      <ul>
        <?php if($webinar_detail[0]->presenters_box==1){ ?>
        <?php if($webinar_detail[0]->topic) { ?>
        <li>
          <div class="ticket_labels">Webinar:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); ?> </div>
        </li>
        <?php } ?>
        <?php if($webinar_detail[0]->presenters_name) { ?>
        <li>
          <div class="ticket_labels">Host:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></div>
        </li>
        <?php } }?>
        <?php if(isset($result[0]->key)){
			    if(isset($result[0]->attendee_name)) { ?>
        <li>
          <div class="ticket_labels">Name:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($result[0]->attendee_name); ?> </div>
        </li>
        <?php } ?>
        <?php if(isset($result[0]->attendee_email_id)) { ?>
        <li>
          <div class="ticket_labels">Email:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($result[0]->attendee_email_id); ?> </div>
        </li>
        <?php } ?>
        <?php if(isset($result[0]->webinar_real_date)) { ?>
        <li>
          <div class="ticket_labels">Date:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_date); ?> </div>
        </li>
        <?php } ?>
        <?php if(isset($result[0]->webinar_real_time)) { ?>
        <li>
          <div class="ticket_labels">Time:</div>
          <div class="ticket_values"><?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_time);  ?></div>
        </li>
        <?php } } ?>
      </ul>
    </form>
    <div class="clear"></div>
    <?php  if($sharing_detail[0]->is_sharing_enabled==1){ ?>
    <div class="noPrint">
      <div class="ticket_head">
        <div class="bonus_head_icon"><img src="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/images/bonus-head-left-arrow.png';?>" width="32" height="34" /></div>
        <div class="bonus_head_seprator"></div>
        <h2>Share to get your Bonus</h2>
      </div>
    </div>
    <?php } ?>
    <?php  if($sharing_detail[0]->is_sharing_enabled==1){ ?>
    <div class="ticket_mid_pannel2">
      <div class="noPrint">
        <div class="social_links1">
          <div id="fb-root"></div>
          <div class="fb-like" data-href="<?php echo get_permalink($reg_id); ?>" data-layout="button_count" data-action="like" data-share="flase" data-width="100" data-show-faces="true" data-font="arial"></div>
        </div>
        <div class="social_links2">
          <g:plusone callback="reward" href="<?php  echo get_permalink($reg_id); ?>"></g:plusone>
        </div>
        <div class="social_links"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $sharing_detail[0]->share_text; ?>" data-url="<?php  echo get_permalink($reg_id); ?>" data-count="horizontal">Tweet</a> </div>
      </div>
    </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="ticket_head" style="line-height:normal;">
      <div class="bonus_head_icon"><img src="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/images/link.png';?>" width="32" height="34" /></div>
      <div class="bonus_head_seprator"></div>
      <h3 style="margin-top:11px;">Your Custom Link to the Webinar</h3>
    </div>
    <div class="ticket_mid_pannel1">
      <p class="video_link_text">
        <?php if(isset($result[0]->key))echo add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id));?>
      </p>
    </div>
  </div>
  
  <!-- tickets block end-->
  <?php  if(!empty($webinar_detail[0]->thankyou_webinar_description) ) { ?>
  <div class="video_description_thanq_page">
    <div class="custm_link_head left"> Description</div>
    <p class="marginright15"> <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thankyou_webinar_description); ?> </p>
  </div>
  <div class="borderbtm1"></div>
  <?php } ?>
  <div class="noPrint">
    <?php if($sharing_detail[0]->is_video_enabled==1 || $sharing_detail[0]->is_document_enabled==1  ){ ?>
    <div style=" display:none;" id="document-reward">
      <div class="custm_link_head margintop20" style=" clear:both; text-align:center">Congratulations! You have unlocked your</div>
      <div class="free_bonus_txt"> Free Bonus</div>
      <?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
      <div><a target="_blank" href="<?php echo $sharing_detail[0]->document; ?>" >
        <input name="" type="button" class="download_btn"  id="reward-doc"/>
        </a></div>
      <?php } ?>
    </div>
    <?php } ?>
    <?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
    <div style="display:none;margin-left:130px;" id="video-reward" >
      <div class="video_block_center margintop10">
        <div <?php if($webinar_detail[0]->thankyou_video_aspect_ratio==1) echo 'class="thankyou_video_block_for_4X3"'; else echo 'class="thankyou_video_block"';?> style="width:585px;">
          <section>
          <?php if($service_sharing == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service_sharing == 'other'){ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service_sharing == 'stream_sharing'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/mp4:<?php if(isset($stream_sharing)){echo $stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php $sharing_flv_stream_sharing = str_replace('.flv','',$stream_sharing); ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/<?php if(isset($sharing_flv_stream_sharing)){echo $sharing_flv_stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service_sharing=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="clear"></div>
  </div>
</div>
<!--end content--> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_body_analytics); ?>
<div class="reg_footer">
  <div class="footer_content clearfix" id="footer_container">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
</div>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_footer_analytics); ?>
</body>
</html>