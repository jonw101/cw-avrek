<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/css/main.css';?>" rel="stylesheet" type="text/css"  />
<link  href="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css';?>" rel="stylesheet" type="text/css"  />
<?php
if(!empty($webinar_detail[0]->presenters_thumbnail_path)){$facebook_image= $webinar_detail[0]->presenters_thumbnail_path;}
elseif(!empty($webinar_detail[0]->header_logo )){$facebook_image =$webinar_detail[0]->header_logo;}
?>
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->reg_meta_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title>
<?php if(empty($webinar_detail[0]->registration_page_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_page_title);?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->header_analytics); ?>
<style type="text/css">
<?php if($webinar_detail[0]->custom_reg_styling != ''){
    	$custom_reg_css = $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->custom_reg_styling);
    	$custom_reg_css = str_replace("<style type='text/css'>",'',$custom_reg_css);
    	$custom_reg_css = str_replace('<style type="text/css">','',$custom_reg_css);
    	$custom_reg_css = str_replace('<style>','',$custom_reg_css);
    	$custom_reg_css = str_replace('</style>','',$custom_reg_css);
    	echo $custom_reg_css;
    } ?>
    <?php  if($service == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
</style>
</head>
<body style="background:#ededed;">
<noscript>
<center>
  <h2>JavaScript must be enabled in order for you to view webinar.</h2>
</center>
</noscript>
<?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
<input type="hidden" name="ewp_requested_name" id="ewp_requested_name" value="<?php if(!empty($_REQUEST['ew-name'])) echo trim($_REQUEST['ew-name'])?>" />
<input type="hidden" name="ewp_requested_mail" id="ewp_requested_mail" value="<?php if(!empty($_REQUEST['ew-email'])) echo trim($_REQUEST['ew-email'])?>" />
<?php } ?>
<input type="hidden" id="local_time" />
<input type="hidden" name="webinar_widget_date" id="webinar_widget_date" value="<?php echo $ob_webinar_functions->_sanitise_string($_GET['webinar_date']);?>"  />
<input type="hidden" name="admin_tz" id="admin_tz" value="<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>" />
<input type="hidden" name="timezone_gmt" id="timezone_gmt"/>
<input type="hidden" name="timezone_string" id="timezone_string"/>
<input type="hidden" name="timezone_gmt_operation" id="timezone_gmt_operation"/>
<input type="hidden" name="webinar_real_date" id="webinar_real_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>" />
<input type="hidden" name="TIME" id="webinar_time" />
<input type="hidden" name="DATE" id="webinar_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>"  />
<?php if($webinar_detail[0]->webinar_timezone_id_fk>0) { ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->timezone_gmt_symbol; ?>" value="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->GMT; ?>" />
<?php }else{ ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
<?php } ?>
<div class="reg_topbar"></div>
<!--header-->
<div class="reg_header">
  <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
  <div class="reg_logo"> <img src="<?php echo $webinar_detail[0]->header_logo; ?>"  width="250" height="100" /></div>
  <div class="header_heading_block">
    <?php } else { ?>
    <div class="header_heading_block_full">
      <?php } ?>
      <h1>
        <?php if(empty($webinar_detail[0]->registration_headline)) echo "WEBINAR BROADCAST EVENT"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_headline);  ?>
      </h1>
      <h2><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_subheadline);?></h2>
    </div>
  </div>
</div>
<!--end header--> 

<!--content-->
<div class="reg_content clearfix">
  <div class="left"> 
    <!--video-->
    <?php  if(!empty($webinar_detail[0]->attachment_thumb_path) || !empty($webinar_detail[0]->attachment_path)){ ?>
    <div id="player" <?php if($webinar_detail[0]->registration_video_aspect_ratio==1) echo 'class="reg_video_block_for_4X3"'; else 'class="reg_video_block"';?>>
      <?php
			if($attachment_type==1){
		?>
      <img src="<?php echo $webinar_detail[0]->attachment_thumb_path; ?>" alt="" width="507" height="339" />
      <?php
			}else{ 
				if(empty($attachment_embed_script)) {
		?>
      <section id="main_content" class="inner video_section">
        <?php if($attachment_type==1){  ?>
        <?php if(!empty($webinar_detail[0]->attachment_thumb_path)) { ?>
        <section class="video_section"> <img src="<?php echo $webinar_detail[0]->attachment_thumb_path; ?>" alt="" /> </section>
        <?php } ?>
        <?php }else{  ?>
        <?php if(!empty($attachment_path)){  ?>
        <section class="video_section">
          <?php if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        <?php } ?>
        <?php } ?>
      </section>
      <?php
			} else { echo $attachment_embed_script; }  }
		?>
    </div>
    <?php } else {
    	echo $attachment_embed_script;
    	
    } ?>
    <!--end video--> 
    <!--arrow-->
    <div class="reg_arrow"></div>
    <!--end arrow--> 
    <!--presenter description-->
    <?php if($webinar_detail[0]->presenters_box==1) { ?>
    <div class="reg_presenter_block">
      <div class="reg_presenter_img">
        <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
        <div class="imgthumb"> <img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="118" height="150" alt="Presenter"  /> </div>
        <?php } ?>
      </div>
      <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
      <h3 class="dark"><b>PRESENTER(S):</b></h3>
      <p><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></p>
      <?php } ?>
      <?php if(!empty($webinar_detail[0]->presenters_description)) { ?>
      <h3 class="dark"><b>DESCRIPTION:</b></h3>
      <p><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?></p>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="margintop10 descri">
      <div class="clear"></div>
      <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->description); ?> 
      <!-- webinar description end--> 
    </div>
    <!-- webinar description end--> 
    <!--presenter description--> 
  </div>
  <div class="left">
  <div class="reg_registration_block">
  <div class="reg_registration_head">
    <h4>Register Now</h4>
  </div>
  <?php if($webinar_detail[0]->notification_type==1){ ?>
  <form name="" id="" action="" method="post" >
    <?php }else{ ?>
    <?php echo $starting_form; ?>
    <?php } ?>
    <div class="reg_form_heading_block">
      <div class="reg_heading_bullet"><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/bullet-1.png';?>" width="39" height="39" alt="bullet1" /></div>
      <h5><?php if($webinar_detail[0]->event_type==1){ echo 'Webinar Date'; }else{ echo 'Select your date';} ?></h5>
     <?php if($webinar_detail[0]->event_type==1){ ?>
     <table class="calender_setting onetime_event_cal" style="visibility:hidden;">
      <tr>
         <td><div class="calender_wrapper">
             <div class="calender_month onetime_event_cal_month"></div>
             <div class="calender_date onetime_event_cal_date"></div>
           </div></td>
         <td><div class="webinar_dates"></div>
          <h5 class="webinar_times" style="display:none;"></h5>
         </td>
       </tr>
    </table>
     <?php } ?>
    </div>
    <select class="reg_date_dropdown" id="webinar_dates"  name="webinar_dates" style=" <?php if($webinar_detail[0]->event_type==1) echo 'display:none'?>">
    </select>
    <div class="reg_form_heading_block reg_form_heading_block_2">
      <div class="reg_heading_bullet reg_heading_bullet_2"><span class="number2"></span></div>
      <h5>Which time works best for you?</h5>
    </div>
    <select  name="select_webinar_session" id="select_webinar_session" class="reg_timezone_dropdown">
      <option value="0">Select desired time</option>
    </select>
    <!--<div style="margin-left:70px;clear:both;float:left;padding:10px 0 0; font-size: 16px; color:#000;"> <span>Your Local Time &nbsp;&nbsp;-&nbsp;&nbsp;</span> <span id="local_time"></span> </div>-->
    <div class="reg_form_heading_block reg_form_heading_block_3">
      <div class="reg_heading_bullet reg_heading_bullet_3"><span class="number3"></span></div>
      <h5>Where to send the invitation?</h5>
    </div>
    <?php if($webinar_detail[0]->notification_type==1){ ?>
    <span>
    <input autocomplete="off" type="text" class="input " value="Enter your name" onfocus="if(this.value == 'Enter your name') {this.value = ''; this.className='input1';}" onblur="if (this.value == '') {this.value = 'Enter your name';this.className='input';}"    name="NAME" id="NAME" />
    </span> <span>
    <input autocomplete="off"  value="Enter your email" onfocus="if(this.value == 'Enter your email') {this.value = ''; this.className='input1';}" onblur="if(this.value == '') {this.value = 'Enter your email'; this.className='input';}" type="text" class="input" name="EMAIL" id="EMAIL" />
    </span> <span>
    <div id="message_div" class="alert-error" style="display:none;"></div>
    <input type="submit" name="register" id="ewp_submit" class="reg_form_submit" value="" />
    </span>
    <?php }else{ ?>
    <?php if($name_field_name!='' || $email_field_name!='') { ?>
    <span > <?php echo $name_field_tag; ?> </span> <span> <?php echo $email_field_tag; ?> </span>
    <div id="message_div" class="alert-error" style="display:none;"></div>
    <span><?php echo '<div id="autoresponder_extra_fields" style="display:none">'.implode('',$other_input_fields).'</div>'; ?>
    <input class="submit register_button" type="submit" tabindex="503" value="" id="ewp_submit" name="submit">
    </span> <span id="query_button_loading" class="reg_form_submit_disabled" ></span> <span id="loading" class="reg_form_loading" ><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME; ?>/templates/classic_theme/images/loader.gif" /></span> </span>
    <input type="hidden" name="webinar_username" id="webinar_username" value="<?php echo $name_field_name; ?>"  />
    <input type="hidden" name="webinar_email" id="webinar_email" value="<?php echo $email_field_name; ?>"  />
    <?php } else { ?>
    <div class="invalid_form">Something went wrong with registration fields.</div>
    <?php } ?>
    <?php } ?>
    <span id="query_button_loading" class="reg_form_submit_disabled" ></span> <span id="loading" class="reg_form_loading" ><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME; ?>/templates/classic_theme/images/loader.gif" /></span> </span>
    </div>
    </div>
  </form>
  <div class="clear"></div>
</div>

<!--end content--> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->body_analytics); ?>
<div class="reg_footer">
  <div class="footer_content clearfix" id="footer_container">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
</div>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_analytics);?>
</body>
</html>