<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/css/main.css';?>" rel="stylesheet" type="text/css"  />
<link  href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css';?>" rel="stylesheet" type="text/css"  />
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if($webinar_detail[0]->event_meta_title=='') echo "Webinar Event"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title>
<?php if(empty($webinar_detail[0]->webinar_page_title)) echo "Webinar Event"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_page_title);  ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php include_once(WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template);?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_header_analytics); ?>
</head>
<body style="background:#ededed;">
<noscript>
<center>
  <h1>JavaScript must be enabled in order for you to view webinar.</h1>
</center>
</noscript>
<?php if(isset($webinar_detail[0]->chatbox_enabled) && $webinar_detail[0]->chatbox_enabled==2){?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code); ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
<style type="text/css">
.fb-comments, .fb-comments span, .fb-comments iframe {width: 100% !important;}

   <?php if($service_type == 'youtube' || $alternate_video_format == 'youtube'){
				if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	
  ?>
     .video_section{position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
  <?php }else{ ?>
		.video_section{position:relative;padding-bottom:54%;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
	<?php } }?> 
	
    <?php  if($service_type == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:56.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
</style>
<?php }?>
<div class="reg_topbar"></div>
<!--header-->
<div class="reg_header">
  <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
  <div class="reg_logo"> <img src="<?php echo $webinar_detail[0]->header_logo; ?>"  width="170" height="100" /></div>
  <div class="header_heading_block">
    <?php } else { ?>
    <div class="header_heading_block_full">
      <?php } ?>
      <h1>
        <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic);  ?>
      </h1>
      <h2><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->topic);  ?></h2>
    </div>
  </div>
</div>
<!--end header--> 

<!--content-->
<div class="reg_content clearfix"> 
  <!-- video presenter block-->
  <?php if($webinar_detail[0]->presenters_box==1){ ?>
  <?php if(!empty($webinar_detail[0]->webinar_main_topic) || !empty($webinar_detail[0]->presenters_name) || !empty($webinar_detail[0]->presenters_thumbnail_path) || !empty($webinar_detail[0]->presenters_description)) { ?>
  <div class="video_presenter_block">
    <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)) { ?>
    <div class="imgthumb"> <img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" alt="presenter" width="95" height="95" /> </div>
    <?php }?>
    
    <!-- video presenter block Content-->
    <div class="left video_presenter_description_box">
      <div class="left" style="width:728px">
        <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
        <h3><b>Presenter(s):</b></h3>
        <p class="left"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name);  ?></p>
        <?php } ?>
      </div>
      <div class="left" style="width:728px">
        <?php if(!empty($webinar_detail[0]->presenters_description)) { ?>
        <h3><b>Description:</b></h3>
        <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description);  
	 	 ?>
        <?php } ?>
      </div>
    </div>
    <!-- video presenter block Content end--> </div>
  <?php } } ?>
  <!-- video presenter block end--> 
  
  <!--video-->
  <div class="video_block_center margintop10">
    <div class="video-inside" style="height:<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>px;width:<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>px;">
      <?php  if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
		<div class="notice_for_mobile" >Please click to start the event</div>
		<?php } ?>
      <section id="main_content" class="inner video_section">
        <?php if($service_type == 'youtube'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $video_url;?>" />
        </video>
        <?php }else{ ?>
        <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/youtube" src="<?php echo $video_url;?>" />
        </video>
        <?php } ?>
        <?php }elseif($service_type == 'other'){ ?>
        <?php if($video_format=='mp4'){ ?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $video_url;?>" />
        </video>
        <?php }else{ ?>
        <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/mp4" src="<?php echo $video_url;?>" />
        </video>
        <?php } ?>
        <?php }elseif($video_format=='flv'){ ?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <?php if($alternate_video_format=='youtube'){ ?>
        <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php  }elseif($alternate_video_format=='mp4'){ ?>
        <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php } ?>
        <?php }else{ ?>
        <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source type="video/flv" src="<?php echo $video_url;?>" />
        </video>
        <?php } ?>
        <?php }?>
        <?php }else if($service_type == 'stream'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <?php if($alternate_video_format=='youtube'){ ?>
        <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php  }elseif($alternate_video_format=='mp4'){ ?>
        <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
        </video>
        <?php } ?>
        <?php }else{ ?>
        <?php if($video_format=='mp4'){ ?>
        <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $video_url;?>/mp4:<?php if(isset($video_stream)){echo $video_stream;}?>" type="video/rtmp" />
        </video>
        <?php }elseif($video_format=='flv'){ ?>
        <?php $flv_stream = str_replace('.flv','',$video_stream); ?>
        <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $video_url;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
        </video>
        <?php }?>
        <?php } ?>
        <?php }elseif($service_type=='vimeo'){?>
        <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
        <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
          <source src="<?php echo $video_url;?>" type="video/vimeo" />
        </video>
        <?php }else{ ?>
        <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
          <source src="<?php echo $video_url;?>" type="video/vimeo" />
        </video>
        <?php } ?>
        <?php }?>
      </section>
    </div>
  </div>
  <!--end video--> 
  
  <!-- row-fluid-->
  <div class="row-fluid" id="scarcity-delayed"> 
    
    <!-- delayed events-->
    <div id="delayed_events" class="span12 webinar events">
      <div id="delayed_events_text"></div>
    </div>
    <!-- delayed events ends--> 
    
    <!--scarcity events-->
    <div id="scarcity_events" class="span6 webinar events">
      <div> <span class="counter_heading_text"> <?php echo $webinar_scarcity[0]->scarcity_start_text;?></span>
        <div class="scarcity_counter" style=""></div>
        <span class="counter_heading_text"><?php echo $webinar_scarcity[0]->scarcity_end_text; ?></span>
        <div id="scarcity_events_text"></div>
      </div>
    </div>
    <!--scarcity events ends--> 
    
  </div>
  <!-- row-fluid--> 
  
  <!-- video submit query block start-->
  <input type="hidden" name="attendee_to_add" id="attendee_to_add" value="<?php echo $attendee_ids; ?>"  />
  <input type="hidden" name="default_attendee_operation" id="default_attendee_operation" value="<?php echo $attendee_operation;?>"/>
  <input type="hidden" name="default_attendees" id="default_attendees" value="<?php echo $attenees_ids;   ?>"/>
  <input type="hidden" value="<?php echo $attenees_ids;?>" id="total_logged_attendee"  />
  <?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
  <form action="" method="post" class="submit_query_form" name="send_your_question" id="send_your_question">
    <div class="video_submit_query_block left" style=" <?php if($attendees_list_enabled==0) { echo "margin-left:145px;" ;}  ?>">
      <div class="submit_query_heading">
        <div class="video_page_head_left"></div>
        <div class="video_page_head_mid"> Submit Your Question </div>
        <div class="video_page_head_right"></div>
      </div>
      <div class="submit_query_cnt">
        <p><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?></p>
        <div id="message"></div>
        <ul class="left">
          <li>
            <input name="your_name" id="your_name" type="text" value="<?php if(isset($attendee_name)) { echo $attendee_name;} else {echo 'Your Name';} ?>" class="submit_query_form_input" maxlength="150"onfocus="if (this.value == 'Your Name') {this.value = '';this.className='submit_query_form_change_color'}" onblur="if (this.value == '') {this.value = 'Your Name';this.className = 'submit_query_form_input'}" />
          </li>
          <li>
            <input  name="your_email" id="your_email" type="text"  class="submit_query_form_input" value="<?php if(isset($attendee_email)) {echo $attendee_email;} else {echo 'Your Email';} ?>" maxlength="150" onfocus="if (this.value == 'Your Email') {this.value = '';this.className='submit_query_form_change_color'}" onblur="if (this.value == '') {this.value = 'Your Email';this.className = 'submit_query_form_input'}"/>
          </li>
        </ul>
        <ul class="right">
          <li>
            <textarea id="your_question" name="your_question" class="submit_txtarea" onfocus="if(this.value == 'Your Question') { this.value = ''; this.className='submit_txtarea_change_color'}" onblur="if(this.value == '') {this.value='Your Question'; this.className = 'submit_txtarea'} " cols="" rows="0" >Your Question</textarea>
          </li>
         <li>
            <input name="send_question" value=""  id="send_question" type="submit" class="submit_query_btn" />
         </li>   
        </ul>
      </div>
    </div>
  </form>
  <?php 
	} elseif($webinar_detail[0]->chatbox_enabled==2){
	?>
  <div class="video_submit_query_block left" style=" margin-left: 145px;">
    <div class="submit_query_cnt">
      <p><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?> </p>
      <br/>
      <div class="fb-comments" data-href="<?php echo get_permalink($post->ID); ?>" data-width="642" order_by="reverse_time" data-num-posts="10"></div>
    </div>
  </div>
  <?php	  	
	  }elseif($webinar_detail[0]->chatbox_enabled==3){
		 echo '<div class="video_submit_query_block left" style=" margin-left: 145px;"><div class="submit_query_cnt">'; 
		 echo '<p>'.$ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title).'</p><br/>';
		 echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
         echo '<div></div>';
	  }elseif($webinar_detail[0]->chatbox_enabled==4){
		  echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
	  }
 	?>
  <!-- video submit query block end--> 
  <!-- Registered Participants  start-->
  <?php if($attendees_list_enabled>0){ ?>
  <?php  $logged = count($logged_in_attendees); 
				$defaults = count($default_attendee_list);
				$tot =  $logged+$defaults;
  ?>
  <div class="registered_participants_main right">
    <div class="registered_participants_heading">
      <div class="video_page_head_left"></div>
      <div class="video_page_head_mid1"> Registered Participants : <span style="width:auto !important;float:none !important" id="total_presenter"><?php echo $tot;?></span> </div>
      <div class="video_page_head_right"></div>
    </div>
    <div class="clear"></div>
    <div class="registered_participants_cnt1">
      <?php if(count($logged_in_attendees)>0){ ?>
      <ul id="real_attendees">
        <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
        <li class="dotted-border presenter" ><b><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></b></li>
        <?php } ?>
        <?php
					foreach($logged_in_attendees as $logged_in){
					$registration_detail	=	$ob_webinar_db_interaction->get_webinar_session_registration_id($logged_in->registration_id_pk);											
			 ?>
        <li id="reg_id<?php echo $logged_in->webinar_logged_in_id_pk; ?>"><?php echo $registration_detail[0]->attendee_name; ?></li>
        <?php } ?>
      </ul>
      <?php } ?>
      <?php if(count($default_attendee_list)>0){ ?>
      <ul id="tot_logged_in">
        <?php for($j=0;$j<count($default_attendee_list);$j++){    ?>
        <li attendee_id="<?php echo $default_attendee_ids[$j]; ?>"><?php echo $default_attendee_list[$j];  ?></li>
        <?php } ?>
      </ul>
      <?php }?>
    </div>
  </div>
  <!-- Registered Participants  end-->
  <?php } ?>
  <div class="clear"></div>
</div>
<!--end content--> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_body_analytics); ?>
<div class="reg_footer">
  <div class="footer_content clearfix" id="footer_container">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
</div>
<?php
	echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_footer_analytics);
?>
</body>
</html>