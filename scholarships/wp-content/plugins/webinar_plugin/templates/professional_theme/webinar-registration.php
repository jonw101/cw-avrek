<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){$facebook_image= $webinar_detail[0]->presenters_thumbnail_path;}
elseif(! empty($webinar_detail[0]->header_logo )){$facebook_image =$webinar_detail[0]->header_logo;}?>
<meta content="text/html;charset=UTF-8" http-equiv="content-type" />
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->reg_meta_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<link media="screen" rel="stylesheet" href="<?php echo plugins_url().'/webinar_plugin/webinar-view-control/css/colorbox.css' ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css'  ?>"  />
<title><?php if(empty($webinar_detail[0]->registration_page_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_page_title);  ?></title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->header_analytics); ?>
<!---Dynamic css starts here-->
<style type="text/css">
    <?php  if($service == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
</style>
</head>
<body class="bodyWrapper">
<noscript>
<center>
  <h2>JavaScript must be enabled in order for you to view webinar.</h2>
</center>
</noscript>
<?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
<input type="hidden" name="ewp_requested_name" id="ewp_requested_name" value="<?php if(!empty($_REQUEST['ew-name'])) echo trim($_REQUEST['ew-name'])?>" />
<input type="hidden" name="ewp_requested_mail" id="ewp_requested_mail" value="<?php if(!empty($_REQUEST['ew-email'])) echo trim($_REQUEST['ew-email'])?>" />
<?php } ?>
<input type="hidden" name="webinar_widget_date" id="webinar_widget_date" value="<?php echo $ob_webinar_functions->_sanitise_string($_GET['webinar_date']);?>"  />
<input type="hidden" name="admin_tz" id="admin_tz" value="<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>" />
<input type="hidden" name="timezone_gmt" id="timezone_gmt"/>
<input type="hidden" name="timezone_string" id="timezone_string"/>
<input type="hidden" name="timezone_gmt_operation" id="timezone_gmt_operation"/>
<input type="hidden" name="webinar_real_date" id="webinar_real_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>" />
<input type="hidden" name="TIME" id="webinar_time"  />
<input type="hidden" name="DATE" id="webinar_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>"  />
<?php if($webinar_detail[0]->webinar_timezone_id_fk>0) { ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->timezone_gmt_symbol; ?>" value="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->GMT; ?>" />
<?php }else{ ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
<?php } ?>
<!--Main Wrapper start here-->
<div class="topbar1"></div>
<div class="topbar2"></div>
<div class="header clearfix">
<?php if(!empty($webinar_detail[0]->header_logo)) { ?>
<div class="logo_container"><span><img src="<?php echo $webinar_detail[0]->header_logo; ?>" width="250" height="100" /></span> </div>
<div class="page_headings">
  <?php } else { ?>
  <div class="page_headings_full">
    <?php } ?>
    <h1 class="darkgrey"> <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_headline);?> </h1>
    <h3 class="darkgrey"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_subheadline);  ?></h3>
  </div>
</div>
<div class="registration-content clearfix">
  <div class="round clearfix">
    <?php  if(count($webinar_sessions)==0){?>
    <div style="color:#FF0000;font-size:26px;text-align:center; padding-bottom:20px;">Webinar is expired</div>
    <?php }?>
    <div class="sidebar1 bodyWrapper left">
      <?php if(!empty($webinar_detail[0]->attachment_thumb_path) || !empty($webinar_detail[0]->attachment_path)){ ?>
      <div id="player" <?php if($webinar_detail[0]->registration_video_aspect_ratio==1) echo 'class="player4X3 margintop20"'; else echo 'class="player margintop20"'; ?>>
        <?php if($attachment_type==1){ ?>
        <img src="<?php echo $webinar_detail[0]->attachment_thumb_path; ?>" alt="" width="500" height="300" />
        <?php }else{?>
        <section class="video_section">
          <?php if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        <?php }?>
      </div>
      <?php } else {
    	echo htmlspecialchars_decode($attachment_embed_script);
    	
    } ?>
      <div class="register-for margintop20"> <img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/images/registrationfor.png'; ?>"  /> </div>
      <!-- presenterbox-->
      <?php if($webinar_detail[0]->presenters_box==1) { ?>
      <div class="marginleft50 margintop20">
        <div class="heading-grey-middle">Presenter(s) Info</div>
        <div class="prsnt-infobox clearfix">
          <div class="prsnt-info">
            <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
            <img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="109" height="109" alt="Presenter(s)" align="left" class="paddingright10 margintop10" />
            <?php }?>
            <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
            <div class="prsnt-name"><b>Presenter(s):</b> <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></div>
            <?php } ?>
            <div class="prsnt-descp"> <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?> </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- presenterbox end-->
      <div class="clear"></div>
      
      <!-- webinar description-->
      <div class="margintop20" style="width:500px; margin-left:50px;">
        <div class="clear"></div>
        <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->description); ?> </div>
      <!-- webinar description end--> 
    </div>
    <?php
		if(count($webinar_sessions)>0){    
		?>
    <div class="sidebar2 right">
      <div class="arrowdown"></div>
      <div class="spot">
        <div class="centeralign font25">RESERVE YOUR SPOT!</div>
        <div class="centeralign darkgrey font22">WEBINAR REGISTRATION</div>
      </div>
      <div class="margintop20">
        <label>
          <?php if($webinar_detail[0]->event_type!=1){ echo 'Which Day Do You Want to Attend?';} ?>
        </label>
      </div>
      <div class="bottomline margintop10"></div>
      <?php if($webinar_detail[0]->notification_type==1){?>
      <form name="" id="" action="" method="post" >
        <?php } else { echo $starting_form; }?>
        <div>
          <?php if($webinar_detail[0]->event_type==1){ ?>
          <table class="calender_setting onetime_event_cal" style="visibility:hidden;">
            <tr>
              <td><div class="calender_wrapper">
                  <div class="calender_month onetime_event_cal_month"></div>
                  <div class="calender_date onetime_event_cal_date"></div>
                </div>
              </td>
              <td>
                <div class="webinar_dates"></div>
                <h5 class="webinar_times" style="display:none;"></h5>
              </td>
            </tr>
          </table>
          <?php } ?>
          <div class="select-inputbox" <?php if($webinar_detail[0]->event_type==1) echo 'style="display:none"'?>>
            <select name="webinar_dates" id="webinar_dates" class="selectopt" style="margin-top:0px !important;">
            </select>
          </div>
        </div>
        <div class="margintop10 webinar_times_hint">
          <label>Which Time Works Best For You?</label>
        </div>
        <div class="select-inputbox">
          <select name="select_webinar_session" id="select_webinar_session" class="selectopt">
            <option value="0">Select desired time</option>
          </select>
        </div>
        <!--
        <div class="bottomline margintop10"></div>
        <div style="padding-top:5px;"><span>Your Local Time </span> <span id="local_time"></span></div>
        -->
        <div class="margintop10">
          <label>Where To Send The Invitation?</label>
        </div>
        <div class="bottomline margintop10"></div>
        <div id="message_div" class="error  bold fontSize13 marginBottom5" style="display:none;"></div>
        <div id="max_attendee" style="display:none"><?php echo $webinar_detail[0]->max_number_of_attendees  ?></div>
        <?php if($webinar_detail[0]->notification_type==1){?>
        <div class="margintop10">
          <input autocomplete="off" value="Enter your name here..." onfocus="if(this.value == 'Enter your name here...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter your name here...';}" class="RegisInput paddingLeft5 inputbox default-value" type="text"  name="NAME" id="NAME"/>
        </div>
        <div class="margintop20">
          <input autocomplete="off" class="RegisInput paddingLeft5 inputbox default-value" type="text" value="Enter your email here..." onfocus="if(this.value == 'Enter your email here...') {this.value = '';}" onblur="if(this.value == '') {this.value = 'Enter your email here...';}" name="EMAIL" id="EMAIL"/>
        </div>
        <div class="margintop20 centeralign">
          <input type="submit" name="register" id="ewp_submit" value="" class="register_button"/>
        </div>
        <div class="margintop20 centeralign" > <span id="loading" class="reg_form_loading" ><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME; ?>/templates/professional_theme/images/loader.gif" /></span> <span id="query_button_loading" class="reg_form_submit_disabled"  ></span> </div>
        <?php }else{ ?>
        <?php  if($name_field_name!='' && $email_field_name!='') { ?>
        <div class="margintop10"> <?php echo $name_field_tag; ?> </div>
        <div class="margintop20"> <?php echo $email_field_tag; ?> </div>
        <div class="margintop20 centeralign"><?php echo '<div id="autoresponder_extra_fields" style="display:none">'.implode('',$other_input_fields).'</div>'; ?>
          <input class="submit register_button" type="submit" tabindex="503" value="" name="submit" id="ewp_submit">
        </div>
        <div class="margintop20 centeralign" > <span id="loading" class="reg_form_loading" ><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME; ?>/templates/professional_theme/images/loader.gif" /></span> <span id="query_button_loading" class="reg_form_submit_disabled"  ></span></div>
        <input type="hidden" name="webinar_username" id="webinar_username" value="<?php echo $name_field_name; ?>"  />
        <input type="hidden" name="webinar_email" id="webinar_email" value="<?php echo $email_field_name; ?>"  />
        <?php }else{ ?>
        <div class="invalid_form">Something went wrong with registration fields.</div>
        <?php } ?>
        <?php } ?>
      </form>
    </div>
    <?php } ?>
  </div>
</div>
<!--body analytics --> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->body_analytics); ?>
<div class="footer">
  <div class="topbar1"></div>
  <div class="footer_content clearfix" id="footer_container">
    <div class="footer_links_block">
      <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
    </div>
  </div>
</div>
<!-- footer analytics--> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_analytics);?>
</body>
</html>