<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css';?>"  />
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->thankyou_meta_title)) echo "Thank you"; else echo stripslashes($webinar_detail[0]->thankyou_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title>
<?php if($webinar_detail[0]->thankyou_webinar_page_title=='') echo "Thank you"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_webinar_page_title);  ?>
</title>
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<style>@media print {.noPrint {	display:none;}} </style>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_header_analytics);?>
<style type="text/css">
	 <?php  if($service == 'vimeo' || $service_sharing == 'vimeo'){ ?>  
    #me_vimeo_0_container, #me_vimeo_1_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe, #me_vimeo_1_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
    fb_ltr.fb_iframe_widget_lift {z-index: 1000000;}
</style>
</head>
<body>
<noscript>
<center>
  <h1>JavaScript must be enabled in order for you to view webinar.</h1>
</center>
</noscript>
<div class="topbar1"></div>
<div class="topbar2"></div>
<div class="header clearfix">
  <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
  <div class="logo_container"><span><img src="<?php echo $webinar_detail[0]->header_logo; ?>" width="250" height="100" /></span></div>
  <div class="page_headings">
    <?php } 
			else { ?>
    <div class="page_headings_full">
      <?php } ?>
      <h1 class="darkgrey">
        <?php if(empty($webinar_detail[0]->thankyou_title)) echo "Your Event Will Start In"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_title);?>
      </h1>
      <h3 class="darkgrey"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_subtitle);  ?></h3>
    </div>
  </div>
</div>
<div class="registration-content clearfix">
  <div class="video-round clearfix"> 
    <!--sidebar1 start-->
    <div class="thank-you-sidebar1 left">
      <h1>Congratulations! You are Registered</h1>
      <?php
			
			if($webinar_detail[0]->thankyou_attachment_type==1){
				if(!empty($webinar_detail[0]->thankyou_attachment_thumb_path)) {
		?>
      <div class="player margintop20"> <img src="<?php echo $webinar_detail[0]->thankyou_attachment_thumb_path;?>" alt=""  width="500" height="300"/> </div>
      <?php } ?>
      <?php
			}else{ if(empty($thankyou_attachment_embed_script)){
		?>
      <div id="player" <?php if($webinar_detail[0]->thankyou_video_aspect_ratio==1) echo 'class="thankyou_player4X3 margintop20"'; else echo 'class="player margintop20"';?>>
        <section class="video_section">
          <?php if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
      </div>
      <?php } else { echo $thankyou_attachment_embed_script; } }	?>
      <div class="custom margintop10">
        <textarea name="" cols="" rows="" readonly="readonly" id="key" onClick="javascript:this.select();"><?php if(isset($result[0]->key))echo add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id));  ?>
</textarea>
      </div>
      <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thankyou_webinar_description);?> </div>
    <!--sidebar1 end--> 
    <!--sidebar2 start-->
    <div class="thank-you-sidebar2 right">
      <div class="tickets-cont"> <img class="left" src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/tickets.png';?>" height="71" width="75" alt="tickets icon" />
        <h1>Your Webinar Ticket</h1>
        <div class="left margintop10"><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/orange-separator.png';?>" height="1" width="299" alt="separator" /></div>
        <ul style="list-style:none;">
          <?php if($webinar_detail[0]->presenters_box==1){ if($webinar_detail[0]->webinar_main_topic) { ?>
          <li><span>Webinar</span>: <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); ?></li>
          <?php } ?>
          <?php if($webinar_detail[0]->presenters_name) { ?>
          <li><span>Host</span>: <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></li>
          <?php } } ?>
          <?php if(isset($_session_user_id)){ ?>
          <?php if($result[0]->attendee_name) { ?>
          <li><span>Name</span>: <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->attendee_name); ?></li>
          <?php } ?>
          <?php if($result[0]->attendee_email_id) { ?>
          <li><span>Email</span>: <?php echo $ob_webinar_functions->_sanitise_email($result[0]->attendee_email_id); ?></li>
          <?php } ?>
          <?php if($result[0]->webinar_real_date) { ?>
          <li><span>Date</span>: <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_date); ?></li>
          <?php } ?>
          <?php if($result[0]->webinar_real_time) { ?>
          <li><span>Time</span>: <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_time); ?></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
      <?php  if($sharing_detail[0]->is_sharing_enabled==1){ ?>
      <div class="noPrint">
        <div class="fb-links margintop20">
          <div class="fb-icon">
          	<div id="fb-root"></div>
            <div class="fb-like" data-href="<?php echo get_permalink($reg_id); ?>" data-layout="button_count" data-action="like" data-share="false" data-width="100" data-show-faces="true" data-font="arial"></div>
          </div>
          <div class="left marginleft10" style="margin-top:-10px"><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/fb-separator.png';?>" height="85" width="2" alt="separator" /></div>
          <div class="left" style="margin-left:7px"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $sharing_detail[0]->share_text; ?>" data-url="<?php echo get_permalink($reg_id); ?>"data-count="vertical">Tweet</a> </div>
          <div class="left marginleft10" style="margin-top:-10px"><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/fb-separator.png';?>" height="85" width="2" alt="separator" /></div>
          <div class="left" style="margin-left:7px; margin-top:23px;"> 
            <!-- Place this tag where you want the +1 button to render -->
            <g:plusone callback="reward" href="<?php echo get_permalink($reg_id); ?>">
          </div>
        </div>
        <?php } ?>
        <div class="left margintop20"><a href="javascript:window.print()"><img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/theme/'.$selected_template_color;?>/images/print-this.png" height="60" width="346" alt="print icon" /></a></div>
      </div>
    </div>
    <!--sidebar2 end-->
    <?php if($sharing_detail[0]->is_video_enabled==1 || $sharing_detail[0]->is_document_enabled==1  ){ ?>
    <div class="noPrint">
      <div class="clear"></div>
      <div align="center" style="display:none;" id="document-reward" > <img src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/theme/'.$selected_template_color; ?>/images/bonus.png" width="306" height="64" class="margintop20" /><br />
      	<div class="clear"></div>
       	<?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
          <div align="center">
            <h4>Please Download Your Gift.</h4>
            <a target="_blank" href="<?php echo $sharing_detail[0]->document; ?>" >
            <input type="image" src="<?php echo plugins_url() .'/'.WEBINAR_PLUGIN_NAME.'/templates/'.$selected_template_dir.'/images/b-download-bonus.png';?>" height="52" width="226" />
            </a> </div>
          <?php } ?>
      </div>
      <?php } ?>
      <?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
      <div class="clear"></div>
      <div style="width:585px;margin-left:213px;">
        <div align="center" style="display:none;" id="video-reward" >
          <section class="video_section">
          <?php if($service_sharing == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service_sharing == 'other'){ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service_sharing == 'stream_sharing'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/mp4:<?php if(isset($stream_sharing)){echo $stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php $sharing_flv_stream_sharing = str_replace('.flv','',$stream_sharing); ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/<?php if(isset($sharing_flv_stream_sharing)){echo $sharing_flv_stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service_sharing=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_body_analytics); ?>
<div class="footer">
  <div class="topbar1"></div>
  <div class="footer_content clearfix" id="footer_container">
    <div class="footer_links_block">
      <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else{ echo POWERED_BY_SOFTOBIZ; }?>
    </div>
  </div>
  <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_footer_analytics); ?> </div>
<!-- footer analytics-->
</body>
</html>