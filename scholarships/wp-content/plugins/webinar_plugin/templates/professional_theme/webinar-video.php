<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/theme/'.$selected_template_color.'/css/styles.css'?>" />
<link href="<?php echo plugins_url().'/webinar_plugin/templates/'.$selected_template_dir.'/css/main.css';?>" rel="stylesheet" type="text/css"  />
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if($webinar_detail[0]->event_meta_title=='') echo "Webinar Event"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title><?php if(empty($webinar_detail[0]->webinar_page_title)) echo "Webinar Event"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_page_title);  ?></title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_header_analytics);?>
<?php include WEBINAR_PLUGIN_PATH.'templates/js/webinar-video.php';?>
<style type="text/css">
 		<?php  if($service_type == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:56.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
</style>
</head>
<body>
<noscript>
<center>
  <h1>JavaScript must be enabled in order for you to view webinar.</h1>
</center>
</noscript>
<?php if(isset($webinar_detail[0]->chatbox_enabled) && $webinar_detail[0]->chatbox_enabled==2){?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code); ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <style type="text/css">.fb-comments, .fb-comments span, .fb-comments iframe {width: 100% !important;}</style>
<?php }?>

<div class="topbar1"></div>
<div class="topbar2"></div>
<div class="header clearfix">
  <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
  <div class="logo_container"><span><img src="<?php echo $webinar_detail[0]->header_logo; ?>" width="250" height="100" /></span> </div>
  <div class="page_headings">
    <?php } else { ?>
    <div class="page_headings_full">
      <?php } ?>
      <h1 class="darkgrey">
        <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic);  ?>
      </h1>
      <h3 class="darkgrey"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->topic); ?></h3>
    </div>
  </div>
  <!--header end-->
  <div class="registration-content clearfix">
    <div class="video-round clearfix"> 
      <!--sidebar1 start-->
     <div class="video-sidebar1 <?php if($webinar_detail[0]->presenters_box==1 || $attendees_list_enabled>0) echo 'left' ?>" <?php if($webinar_detail[0]->presenters_box==0 && $attendees_list_enabled==0) echo ' style="margin:0 auto;"'; ?> >
        <div <?php if($webinar_detail[0]->event_video_aspect_ratio==1) echo 'class="event-video-player-4X3 margintop20"'; else echo 'class="event-video-player margintop20"'; ?> style="margin-left:23px">
          <div id="main_content_wrap" class="outer">
          <?php  if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
		<div class="notice_for_mobile" >Please click to start the event</div>
		<?php } ?>
           <section class="video_section">
          <?php if($service_type == 'youtube'){?>
   
		   <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php }else{ ?>
            <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php } ?>
            
            
        <?php }elseif($service_type == 'other'){ ?>
        
          <?php if($video_format=='mp4'){ ?>
            
           <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php }else{ ?>
            <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php } ?>
            
          <?php }elseif($video_format=='flv'){ ?>
            
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
            
            <?php if($alternate_video_format=='youtube'){ ?>
              <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php  }elseif($alternate_video_format=='mp4'){ ?>
              <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php } ?>
          
          <?php }else{ ?>
              <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/flv" src="<?php echo $video_url;?>" />
              </video>
          <?php } ?>
          
          <?php }?>
        
        <?php }else if($service_type == 'stream'){?>
        
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          
              <?php if($alternate_video_format=='youtube'){ ?>
                <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php  }elseif($alternate_video_format=='mp4'){ ?>
                <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php } ?>
           
           <?php }else{ ?>
              <?php if($video_format=='mp4'){ ?>
                <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
                <source src="<?php echo $video_url;?>/mp4:<?php if(isset($video_stream)){echo $video_stream;}?>" type="video/rtmp" />
                </video>
              <?php }elseif($video_format=='flv'){ ?>
              <?php $flv_stream = str_replace('.flv','',$video_stream); ?>
               <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
               <source src="<?php echo $video_url;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
               </video>
              <?php }?>
          <?php } ?>
        
        <?php }elseif($service_type=='vimeo'){?>
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
             <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php }else{ ?>
            <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php } ?>
        
        <?php }?>
        </section>
          </div>
        </div>
        <div class="clear"></div>
        <!-- row-fluid-->
        <div class="row-fluid" id="scarcity-delayed"> 
          
          <!-- delayed events-->
          <div id="delayed_events" class="span12 webinar events">
            <div id="delayed_events_text"></div>
          </div>
          <!-- delayed events ends--> 
          
          <!--scarcity events-->
          <div id="scarcity_events" class="span6 webinar events">
            <div> <span class="counter_heading_text"> <?php echo $webinar_scarcity[0]->scarcity_start_text;?></span>
              <div class="scarcity_counter" style=""></div>
              <span class="counter_heading_text"><?php echo $webinar_scarcity[0]->scarcity_end_text; ?></span>
              <div id="scarcity_events_text"></div>
            </div>
          </div>
          <!--scarcity events ends--> 
          
        </div>
      	<!-- row-fluid-->
        <div class="clear"></div>
        <input type="hidden" name="attendee_to_add" id="attendee_to_add" value="<?php echo $attendee_ids?>"  />
        <input type="hidden" name="default_attendee_operation" id="default_attendee_operation" value="<?php echo $attendee_operation?>"/>
        <input type="hidden" name="default_attendees" id="default_attendees" value="<?php echo $attenees_ids?>"/>
        <input type="hidden" value="<?php echo $attenees_ids;?>" id="total_logged_attendee"  />
        
        <!---------------Scarcity Delayed Box----------------------->
        <?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
        <form action="" method="post" name="send_your_question" id="send_your_question">
          <div class="query-form">
            <p><b><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?></b></p>
            <div id="message" class="alert-error error bold fontSize13 marginBottom5"></div>
            <ul style="width:608px; list-style:none">
              <li><span><span class="mandatory-query-form">*</span>Name :</span>
                <input type="text" name="your_name" id="your_name" value="<?php if(isset($attendee_name)) { echo $attendee_name ;} else {echo 'Your Name';} ?>" onfocus="if (this.value == 'Your Name') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Your Name';}"/>
              </li>
              <li><span><span class="mandatory-query-form">*</span>Email :</span>
                <input  name="your_email" id="your_email" type="text" value="<?php if(isset($attendee_email)) {echo $attendee_email;} else {echo 'Your Email';} ?>" onfocus="if (this.value == 'Your Email') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Your Email';}" />
              </li>
              <li><span><span class="mandatory-query-form">*</span>Question :</span>
                <textarea id="your_question" name="your_question"cols="45" rows="5" onfocus="if(this.value == 'Your Question') { this.value = '';}" onblur="if(this.value == '') {this.value='Your Question'; } ">Your Question</textarea>
              </li>
            </ul>
            <input name="send_question" id="send_question" value=""  type="submit"  class="submit-button" style="cursor:pointer; margin-left:119px;display:block;"/>
            </div>
        </form>
      <?php }  
	  		elseif($webinar_detail[0]->chatbox_enabled==2){
	  ?>
       <div class="query-form" style="max-width:642px;min-width:642px;">
       	<p><b><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?></b></p>
        <div class="fb-comments" data-href="<?php echo get_permalink($post->ID); ?>" data-width="642" order_by="reverse_time" data-num-posts="10"></div>
        </div>
	  <?php	  	
	  }elseif($webinar_detail[0]->chatbox_enabled==3){
		  echo '<div class="query-form" style="max-width:642px;min-width:642px;">';
		  echo '<p><b>'.$ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title).'</b></p>';
		  echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
		  echo '</div>';
	  }elseif($webinar_detail[0]->chatbox_enabled==4){
		  echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
	  }
 	  ?>
      </div>
      <!--sidebar1 end--> 
      <!--sidebar2 start-->
      
      <div class="video-sidebar2 right" <?php if($webinar_detail[0]->presenters_box==0 && $attendees_list_enabled==0) echo 'style="display:none;"'; ?>>
        <?php if($webinar_detail[0]->presenters_box==1){ ?>
        <?php if(!empty($webinar_detail[0]->webinar_main_topic) || !empty($webinar_detail[0]->presenters_name) || !empty($webinar_detail[0]->presenters_thumbnail_path) || !empty($webinar_detail[0]->presenters_description)) { ?>
        <div class="video-left-heading"><span>Webinar Presentation</span>
          <div class="video-left-mid">
            <ul style="list-style:none;">
              <?php if(!empty($webinar_detail[0]->webinar_main_topic)) { ?>
              <li><span>Topic:&nbsp;</span><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic);  ?></li>
              <?php } ?>
              <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
              <li><span>Presenter(s):&nbsp;</span><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name);  ?></li>
              <?php } ?>
              <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)) { ?>
              <li><span>&nbsp;</span><img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" height="80" width="76" alt="Presenter(s) image" /></li>
              <?php } ?>
              <?php if(!empty($webinar_detail[0]->presenters_description)) { ?>
              <li><span>Description:&nbsp;</span><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description);  ?></li>
              <?php } ?>
            </ul>
          </div>
          <div class="video-left-btm"></div>
        </div>
        <?php } ?>
        <?php } ?>
        <?php if($attendees_list_enabled>0){ ?>
        <?php  $logged = count($logged_in_attendees); 
				$defaults = count($default_attendee_list);
				$tot =  $logged+$defaults;
			?>
        <div <?php if($webinar_detail[0]->presenters_box==1) echo 'class="video-left-heading margintop20"'; else echo 'class="video-left-heading"'; ?>><span>Registered Participants : <span style="width:auto !important;float:none !important" id="total_presenter"><?php echo $tot;?></span></span>
          <div class="scroll-list">
            <div class="video-left-mid">
              <?php if(count($logged_in_attendees)>0){ ?>
              <ul id="real_attendees">
                <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
        		<li class="dotted-border presenter" ><b><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></b></li>
        		<?php } ?>
				<?php
					foreach($logged_in_attendees as $logged_in){
					$registration_detail =	$ob_webinar_db_interaction->get_webinar_session_registration_id($logged_in->registration_id_pk);											
				?>
                <li class="dotted-border" id="reg_id<?php echo $logged_in->webinar_logged_in_id_pk; ?>"><?php echo $registration_detail[0]->attendee_name; ?></li>
                <?php } ?>
              </ul>
              <?php } ?>
              <?php if(count($default_attendee_list)>0){ ?>
              <ul class="" id="tot_logged_in">
                <?php for($j=0;$j<count($default_attendee_list);$j++){ ?>
                <li class="dotted-border" attendee_id="<?php echo $default_attendee_ids[$j]; ?>"><?php echo $default_attendee_list[$j];  ?></li>
                <?php } ?>
              </ul>
              <?php }?>
            </div>
          </div>
          <div class="video-left-btm"></div>
        </div>
        <?php } ?>
      </div>
      <!--sidebar2 end--> 
    </div>
  </div>
  <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_body_analytics);?>
</div>
<div class="footer">
  <div class="topbar1"></div>
  <div class="footer_content clearfix" id="footer_container">
    <div class="footer_links_block">
      <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
    </div>
  </div>
</div>
<!-- footer analytics-->
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_footer_analytics);?>
</body>
</html>