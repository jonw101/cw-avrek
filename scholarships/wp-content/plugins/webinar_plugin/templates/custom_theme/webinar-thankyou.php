<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if(empty($webinar_detail[0]->thankyou_webinar_page_title)) bloginfo('name'); else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_webinar_page_title); ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<!-- page and OG meta -->
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text);?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc);?>" />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_thumbnail_path);?>" />
<meta name="title" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_title); ?>" />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_desc);?>" />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_keys);?>" />
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>" />
<!-- page and OG meta end-->
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap-responsive.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_style.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_responsive.css'?>" />
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/js/html5.js'?>"></script>
<![endif]-->

<?php include WEBINAR_PLUGIN_PATH.'templates/js/webinar-thankyou.php';?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_header_analytics); ?>

<!--- custom style starts here -->
<style type="text/css">
@media print {.share_description { display:none; }div#ticket center.head { display:none; }center.icons_links { display:none; }.span8{ display:none;}.right_section { display:block;}}
fb_ltr.fb_iframe_widget_lift {z-index: 1000000;}
</style>
<style type="text/css">
  	<?php if($service == 'youtube' || $alternate_video_format == 'youtube' || $service_sharing == 'youtube'){ 
					if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	
		?>
    .video_section{position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php }else{ ?>
		.video_section{position:relative;padding-bottom:53.26%;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
  	<?php } } ?>
  	
    <?php  if($service == 'vimeo' || $service_sharing == 'vimeo'){ ?>  
    #me_vimeo_0_container, #me_vimeo_1_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe, #me_vimeo_1_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
    
.navbar {
 <?php  if(!empty($webinar_thank_template_options['thank_header_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_header_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_header_rep_image'])) {
 echo "background-image:url('".$webinar_thank_template_options['thank_header_rep_image']."');";
}
 ?>
}

.navbar .navbar_inner {  <?php  if(!empty($webinar_thank_template_options['thank_header_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_header_text_color'].';';
}
 ?>
}

.navbar .navbar_inner center {  <?php if(!empty($webinar_thank_template_options['thank_header_height'])) {
 echo 'height:'.$webinar_thank_template_options['thank_header_height'].'px;';
}
 if(!empty($webinar_thank_template_options['thank_header_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_header_text_color'].';';
}
 ?>
}

.navbar_inner center img {  <?php if(!empty($webinar_thank_template_options['thank_header_height'])) {
 echo 'height:'.$webinar_thank_template_options['thank_header_height'].'px;';
}
?>
}

body {  <?php if(!empty($webinar_thank_template_options['thank_body_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_body_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_body_bg_rep_image'])) {
 echo "background-image:url('".$webinar_thank_template_options['thank_body_bg_rep_image']."');";
}
 if(!empty($webinar_thank_template_options['thank_body_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_body_text_color'].';';
}
 ?>
}

.main_heading, .sub_heading {  <?php  if(!empty($webinar_thank_template_options['thank_body_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_body_text_color'].';';
}
 ?>
}

.video_section {  <?php if(!empty($webinar_thank_template_options['thank_player_bg_color'])) {
 echo 'border: 14px solid '.$webinar_thank_template_options['thank_player_bg_color'].';';
 echo 'background-color:'.$webinar_thank_template_options['thank_player_bg_color'].';';
}
?>
}

section.description.person_details {  <?php  if(!empty($webinar_thank_template_options['thank_desc_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_desc_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_desc_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_desc_color'].';';
}
 ?>
}

.leftpanel {  <?php  if(!empty($webinar_thank_template_options['thank_left_panel_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_left_panel_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_left_panel_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_left_panel_text_color'].';';
}
 ?>
}

.rightpanel, .right_section {  <?php  if(!empty($webinar_thank_template_options['thank_right_panel_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_right_panel_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_right_panel_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_right_panel_text_color'].';';
}
 ?>
}

.register_title .ticket, .register_title .ticket .head, .register_title .share_description {  <?php  if(!empty($webinar_thank_template_options['thank_ticket_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_ticket_bg_color'].';';
 ?>  -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 <?php
}
 if(!empty($webinar_thank_template_options['thank_ticket_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_ticket_text_color'].';';
}
 ?>
}

section.person_details.url, .person_details.url p a, .person_details.url p a {  <?php  if(!empty($webinar_thank_template_options['thank_ticket_link_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_ticket_link_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_ticket_link_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_ticket_link_text_color'].';';
}
 ?>
}
 <?php if(!empty($webinar_thank_template_options['thank_ticket_link_bg_color'])) {
?>  section.person_details.url h4 {
 background:none;
 -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
 margin:0px;
 <?php if(!empty($webinar_thank_template_options['thank_ticket_link_text_color'])) {
 	echo 'color:'.$webinar_thank_template_options['thank_ticket_link_text_color'].';';
	}
 ?>
}
 <?php
}
?>  footer {
 <?php  if(!empty($webinar_thank_template_options['thank_footer_bg_color'])) {
 echo 'background-color:'.$webinar_thank_template_options['thank_footer_bg_color'].';';
}
 if(!empty($webinar_thank_template_options['thank_footer_rep_image'])) {
 echo "background-image:url('".$webinar_thank_template_options['thank_footer_rep_image']."');";
}
 if(!empty($webinar_thank_template_options['thank_footer_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_footer_text_color'].';';
}
 ?>
}

footer p a, footer p a:hover {  <?php if(!empty($webinar_thank_template_options['thank_footer_text_color'])) {
 echo 'color:'.$webinar_thank_template_options['thank_footer_text_color'].';';
}
 ?>
}
</style>
<!-- custom style ends here-->

</head>

<body>

<!-- Header Starts --> 

<!-- Navigation -->

<div class="navbar">
  <div class="navbar_inner">
    <center>
      <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
      <img src="<?php echo $webinar_detail[0]->header_logo; ?>"/>
      <?php }else{ ?>
      <?php if(!empty($webinar_thank_template_options['thank_header_text'])){ 
	      echo '<div style="font-size:24px;margin-top:12px">'.$webinar_thank_template_options['thank_header_text'].'</div>';
        }?>
      <?php } ?>
    </center>
  </div>
</div>

<!-- Navigation Ends --> 

<!-- Header Ends --> 

<!-- Content Section -->
<div class="container"> 
  
  <!-- Main Title -->
  <div class="row heading">
    <div class="span12">
      <noscript>
      <center>
        <p class="alert alert-error errors">JavaScript must be enabled in order to view webinar.</p>
      </center>
      </noscript>
      <?php if(!empty($webinar_detail[0]->thankyou_title)){ ?>
      <h1 class="main_heading"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_title); ?></h1>
      <?php } ?>
      <?php if(!empty($webinar_detail[0]->thankyou_subtitle)){ ?>
      <h3 class="sub_heading"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_subtitle); ?></h3>
      <?php } ?>
    </div>
    <!-- Main Title Ends --> 
  </div>
  
  <!-- Main Title Ends --> 
  
  <!-- Content Panel -->
  <div class="row"> 
    <!-- Left Section  Starts -->
    <section class="span8">
      <?php /* if(!empty($webinar_detail[0]->thankyou_attachment_thumb_path) || !empty($thankyou_attachment_path) || !empty($webinar_detail[0]->thankyou_webinar_description) ) { */?>
      <div class="leftpanel">
        <?php if($webinar_detail[0]->thankyou_attachment_type==1){ ?>
        <?php if(!empty($webinar_detail[0]->thankyou_attachment_thumb_path)){ ?>
        <!-- Video Section -->
        <section class="video_section"> <img src="<?php echo $webinar_detail[0]->thankyou_attachment_thumb_path;?>" alt="video" /> </section>
        <!-- Video Section Ends -->
        <?php } ?>
        <?php }else{ ?>
        <?php if(empty($thankyou_attachment_embed_script)){ ?>
        <!-- Video Section -->
        <section class="video_section">
          <?php if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        
        <!-- Video Section Ends -->
        <?php } else { 
        	echo $thankyou_attachment_embed_script;
         }
         }?>
        <?php if(!empty($webinar_detail[0]->thankyou_webinar_description) ) { ?>
        <!-- person details -->
        <section class="description person_details thankyou clearfix">
          <p><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thankyou_webinar_description); ?></p>
        </section>
        <!-- Person Details Ends -->
        <?php } ?>
        <?php if($sharing_detail[0]->is_video_enabled==1 || $sharing_detail[0]->is_document_enabled==1  ){ ?>
        <?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
        <section class="video_section" style="display:none;" id="video-reward">
          <?php if($service_sharing == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service_sharing == 'other'){ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service_sharing == 'stream_sharing'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/mp4:<?php if(isset($stream_sharing)){echo $stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php $sharing_flv_stream_sharing = str_replace('.flv','',$stream_sharing); ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/<?php if(isset($sharing_flv_stream_sharing)){echo $sharing_flv_stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service_sharing=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        <?php } ?>
        <?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
        <div class="person_details thankyou clearfix" style="display:none;" id="document-reward"> <a target="_blank" href="<?php echo $sharing_detail[0]->document; ?>" >
          <button type="submit" class="btn btn-large btn-success" id="reward-doc" style="width:100%"> <i class="icon-gift icon-white"></i> Reward document <i class="icon-download icon-white"></i> </button>
          </a> </div>
        <?php } ?>
        <?php } ?>
      </div>
      <?php /* } */ ?>
    </section>
    <!-- Left Section Ends --> 
    
    <!-- Right Section Starts -->
    <section class="span4 margin0 rightpanel">
      <div class="right_section">
        <div class="register_title">
          <?php  if($sharing_detail[0]->is_sharing_enabled==1){ ?>
          <div class="share_description"> <span>
            <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['sharing_text_title'],'Share News of This Webinar and Unlock Your Gift!'); ?>
            </span>
            <center>
              <div class="social_links_g">
                <g:plusone callback="reward" href="<?php  echo get_permalink($reg_id); ?>"></g:plusone>
              </div>
              <div class="social_links_t"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $sharing_detail[0]->share_text; ?>" data-url="<?php  echo get_permalink($reg_id); ?>" data-count="horizontal">Tweet</a> </div>
              <div class="social_links_fb">
                <div id="fb-root"></div>
                <div class="fb-like" data-href="<?php echo get_permalink($reg_id); ?>" data-layout="button_count" data-action="like" data-share="false" data-width="100" data-show-faces="true" data-font="arial"></div>
              </div>
            </center>
            <div style="clear:both;"></div>
          </div>
          <?php } ?>
          
          <div class="ticket" id="ticket">
            <center class="head" >
              <img src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir.'/img/pin.png'?>" alt="pin" />
            </center>
            <ul>
              <?php if($webinar_detail[0]->presenters_box==1){ ?>
              <?php if($webinar_detail[0]->webinar_main_topic){ ?>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_webinar_title'],'Webinar'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); ?> </li>
              <?php } ?>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_host_title'],'Host'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></li>
              <?php } ?>
              <?php if(isset($result[0]->key)){ ?>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_name_title'],'Name'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->attendee_name); ?></li>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_email_title'],'Email'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->attendee_email_id); ?></li>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_date_title'],'Date'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_date); ?></li>
              <li><span>
                <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_time_title'],'Time'); ?>
                : </span> <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_time); ?></li>
              <?php }?>
            </ul>
            <?php if(isset($result[0]->key)){ ?>
            <center class="icons_links">
              <div class="ical_icon"><a href="<?php echo bloginfo('url').'/'.WEBINAR_PLUGIN_PATH; ?>webinar-ical.php?ical=<?php echo base64_encode($ical_content);?>" target="_blank" >
                <h5>
                  <?php if(isset($result[0]->key)) echo date('F',strtotime($result[0]->webinar_real_date));?>
                </h5>
                <h6>
                  <?php if(isset($result[0]->key)) echo (int)date('d',strtotime($result[0]->webinar_real_date));?>
                </h6>
                </a> </div>
              <div class="gcal_icon"> <a href="<?php echo $gcal_url; ?>" target="_blank">
                 <h5>
                  <?php if(isset($result[0]->key)) echo date('F',strtotime($result[0]->webinar_real_date));?>
                </h5>
                <h6>
                  <?php if(isset($result[0]->key)) echo date('d',strtotime($result[0]->webinar_real_date));?>
                </h6>
                </a></div>
              <div class="print_icon"> <a href="" onclick="return false" class="print-ticket"> <img src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir.'/img/printer_icn.png'?>" alt="printer-icon" /> </a> </div>
              <div style="clear:both;"></div>
            </center>
            <?php } ?>
          </div>
        </div>
        <section class="person_details url clearfix">
          <h4>
            <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['thank_ticket_link_title'],'Your Custom url to the Webinar'); ?>
          </h4>
          <p style="text-decoration:none !important;word-wrap: break-word;"> <a href="<?php if(isset($result[0]->key) && isset($webinar_link)) echo $webinar_link;?>" target="_blank" ><?php if(isset($result[0]->key))echo add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id));?>
            </a>
          </p>
        </section>
      </div>
    </section>
    
    <!-- Right Section Ends --> 
    
  </div>
  
  <!-- Content Panel Ends --> 
  
</div>
<!-- Content Section Ends --> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_body_analytics); ?>
<!-- Footer Starts -->
<footer>
  <div align="center">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
  <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_footer_analytics);?> </footer>
<!-- Footer Ends -->

</body>
</html>