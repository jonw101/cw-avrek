<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if(empty($webinar_detail[0]->webinar_page_title)) bloginfo('name'); else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_page_title); ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />

<!-- page and OG meta -->
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->event_meta_title);?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->event_meta_desc);?>" />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_thumbnail_path);?>" />
<meta name="title" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_title); ?>" />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_desc);?>" />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->event_meta_keys);?>" />
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>" />
<!-- page and OG meta end-->

<meta content="width=device-width, initial-scale=1.0" name="viewport">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap-responsive.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_style.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_responsive.css'?>" />
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/js/html5.js'?>"></script>
<![endif]-->

<?php include WEBINAR_PLUGIN_PATH.'templates/js/webinar-video.php';?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_header_analytics); ?>

<!--- custom style starts here -->
<style type="text/css">
   
   <?php if($service_type == 'youtube' || $alternate_video_format == 'youtube'){
				if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	
  ?>
     .video_section{position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
  <?php }else{ ?>
		.video_section{position:relative;padding-bottom:54%;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
	<?php } }?> 
	
    <?php  if($service_type == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:56.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
   
   .navbar{
     <?php
	if(!empty($webinar_event_template_options['event_header_bg_color'])){ 
	echo 'background-color:'.$webinar_event_template_options['event_header_bg_color'].';';
	}
	if(!empty($webinar_event_template_options['event_header_rep_image'])){ 
	echo "background-image:url('".$webinar_event_template_options['event_header_rep_image']."');";
	} 
	 ?>
   }
   
   .navbar .navbar_inner{
  	<?php
	if(!empty($webinar_event_template_options['event_header_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_header_text_color'].';';
	}
	?> 
  }
  
  .navbar .navbar_inner center{
  	<?php if(!empty($webinar_event_template_options['event_header_height'])){ 
      echo 'height:'.$webinar_event_template_options['event_header_height'].'px;';
	} 
	
	if(!empty($webinar_event_template_options['event_header_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_header_text_color'].';';
	}
	?>
  }
  
  .navbar_inner center img{
	<?php if(!empty($webinar_event_template_options['event_header_height'])){ 
      echo 'height:'.$webinar_event_template_options['event_header_height'].'px;';
  } ?>    
  }

  body{
   <?php if(!empty($webinar_event_template_options['event_body_bg_color'])){ 
	echo 'background-color:'.$webinar_event_template_options['event_body_bg_color'].';';
	}

	if(!empty($webinar_event_template_options['event_body_bg_rep_image'])){ 
	echo "background-image:url('".$webinar_event_template_options['event_body_bg_rep_image']."');";
	}
	
	if(!empty($webinar_event_template_options['event_body_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_body_text_color'].';';
	}
	
   ?>
  }
  
  .main_heading, .sub_heading{
    <?php
	if(!empty($webinar_event_template_options['event_body_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_body_text_color'].';';
	}
   ?>
  }
  
  .leftpanel{
     <?php
     if(!empty($webinar_event_template_options['event_player_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_player_bg_color'].';';
	}
	?>
  }
  
  .video_section{
    <?php if(!empty($webinar_event_template_options['event_player_bg_color'])){ 
      echo 'border: 14px solid '.$webinar_event_template_options['event_player_bg_color'].';';
			echo 'background-color:'.$webinar_event_template_options['event_player_bg_color'].';';
  	} ?>  
  }
 
  .bottompanel #delayed_events.webinar.events, .bottompanel #scarcity_events.webinar.events{
    <?php
     if(!empty($webinar_event_template_options['event_timed_event_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_timed_event_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_timed_event_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_timed_event_text_color'].';';
	}
    ?>
  }
  
  div.span12.webinar.presenter_box h3{
     <?php
     if(!empty($webinar_event_template_options['event_webinar_desc_header_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_webinar_desc_header_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_webinar_desc_header_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_webinar_desc_header_text_color'].';';
	}
    ?>
  }
  
  div.span12.webinar.presenter_box{
     <?php
     if(!empty($webinar_event_template_options['event_webinar_desc_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_webinar_desc_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_webinar_desc_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_webinar_desc_text_color'].';';
	}
    ?>
  }
  
  div.webinar.participants{
    <?php
     if(!empty($webinar_event_template_options['event_attendee_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_attendee_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_attendee_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_attendee_text_color'].';';
	}
    ?>
  }
  
  .bottompanel .webinar.participants h3{
    <?php
     if(!empty($webinar_event_template_options['event_attendee_header_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_attendee_header_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_attendee_header_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_attendee_header_text_color'].';';
	}
    ?>
  }
  
   .bottompanel .webinar.participants li.color{
    <?php
     if(!empty($webinar_event_template_options['event_attendee_bg_color'])){ 
		echo 'background-color: rgba(0, 0, 0, 0.1);border-bottom: 1px solid rgba(0, 0, 0, 0.1);';
  	}?>
   <?php
	if(!empty($webinar_event_template_options['event_attendee_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_attendee_text_color'].';';
	}
    ?>
   }
  
  .bottompanel div.webinar.question{
    <?php
     if(!empty($webinar_event_template_options['event_chat_bg_color'])){ 
		echo 'background-color:'.$webinar_event_template_options['event_chat_bg_color'].';';
	}
	?>
  
    <?php
	if(!empty($webinar_event_template_options['event_chat_text_color'])){ 
 		echo 'color:'.$webinar_event_template_options['event_chat_text_color'].';';
	}
    ?>
  }
  
 <?php if($webinar_detail[0]->chatbox_enabled>1){ ?>
  .bottompanel div.webinar.question {
    padding: 5px 15px;
    overflow-x: auto;
	}
 <?php } ?> 
    
  footer{
    <?php 
    if(!empty($webinar_event_template_options['event_footer_bg_color'])){ 
	echo 'background-color:'.$webinar_event_template_options['event_footer_bg_color'].';';
	}
	
	if(!empty($webinar_event_template_options['event_footer_rep_image'])){ 
	echo "background-image:url('".$webinar_event_template_options['event_footer_rep_image']."');";
	}
	
	if(!empty($webinar_event_template_options['event_footer_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_footer_text_color'].';';
	}
	 ?>
  }
  
  footer p a, footer p a:hover{
   <?php  if(!empty($webinar_event_template_options['event_footer_text_color'])){ 
	echo 'color:'.$webinar_event_template_options['event_footer_text_color'].';';
	}
   ?>
  }
 
</style>
<!-- custom style ends here-->

</head>

<body>


<?php if(isset($webinar_detail[0]->chatbox_enabled) && $webinar_detail[0]->chatbox_enabled==2){?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code); ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <style type="text/css">.fb-comments, .fb-comments span, .fb-comments iframe {width: 100% !important;}</style>
<?php }?>


<!-- Header Starts --> 

<!-- Navigation -->

<div class="navbar">
  <div class="navbar_inner">
    <center>
       <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
      <img src="<?php echo $webinar_detail[0]->header_logo; ?>"/>
      <?php }else{ ?>
      	<?php if(!empty($webinar_thank_template_options['event_header_text'])){ 
	      echo '<div style="font-size:24px;margin-top:12px">'.$webinar_thank_template_options['event_header_text'].'</div>';
        }?>
      <?php } ?>
    </center>
  </div>
</div>

<!-- Navigation Ends --> 

<!-- Header Ends --> 

<!-- Content Section -->
<div class="container video"> 
  
  <!-- Main Title -->
  <div class="row">
    <div class="span12">
    <noscript><center><p class="alert alert-error errors">JavaScript must be enabled in order to view webinar</p></center></noscript>
      <?php if(!empty($webinar_detail[0]->webinar_main_topic)){ ?>
      <h1 class="main_heading"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_main_topic); ?> </h1>
      <?php } ?>
      <?php if(!empty($webinar_detail[0]->topic)){ ?>
      <h3 class="sub_heading"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->topic); ?></h3>
      <?php } ?>
    </div>
    <!-- Main Title Ends --> 
  </div>
  
  <!-- Main Title Ends --> 
  
  <!-- Content Panel -->
  <div class="row"> 
    <!-- Left Section  Starts -->
    <section class="span12">
      <div class="leftpanel"> 
       <?php  if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
		<div class="notice_for_mobile" >Please click to start the event</div>
		<?php } ?>
        <!-- Video Section -->
         <section class="video_section">
          <?php if($service_type == 'youtube'){?>
   
		   <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php }else{ ?>
            <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php } ?>
            
            
        <?php }elseif($service_type == 'other'){ ?>
        
          <?php if($video_format=='mp4'){ ?>
            
           <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php }else{ ?>
            <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php } ?>
            
          <?php }elseif($video_format=='flv'){ ?>
            
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
            
            <?php if($alternate_video_format=='youtube'){ ?>
              <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php  }elseif($alternate_video_format=='mp4'){ ?>
              <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php } ?>
          
          <?php }else{ ?>
              <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/flv" src="<?php echo $video_url;?>" />
              </video>
          <?php } ?>
          
          <?php }?>
        
        <?php }else if($service_type == 'stream'){?>
        
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          
              <?php if($alternate_video_format=='youtube'){ ?>
                <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php  }elseif($alternate_video_format=='mp4'){ ?>
                <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php } ?>
           
           <?php }else{ ?>
              <?php if($video_format=='mp4'){ ?>
                <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
                <source src="<?php echo $video_url;?>/mp4:<?php if(isset($video_stream)){echo $video_stream;}?>" type="video/rtmp" />
                </video>
              <?php }elseif($video_format=='flv'){ ?>
              <?php $flv_stream = str_replace('.flv','',$video_stream); ?>
               <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
               <source src="<?php echo $video_url;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
               </video>
              <?php }?>
          <?php } ?>
        
        <?php }elseif($service_type=='vimeo'){?>
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
             <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php }else{ ?>
            <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php } ?>
        
        <?php }?>
        </section>
        <!-- Video Section Ends -->
        
      </div>
    </section>
    <!-- Left Section Ends -->
    
    <div class="span12 invite"> 
    <span style="line-height:normal;">&nbsp;</span>
    <!--<span>Invite Your Friends to the Webinar:</span> <a href="" onclick="return false" ><img src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir ?>/img/fb-invite.png" alt="" /></a> <a href="" onclick="return false"><img src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir ?>/img/tw-invite.png" alt="" /></a> <a href="" onclick="return false"><img src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir ?>/img/gplus-invite.png" alt="" /></a> --></div>
    <section class="span12">
      <div class="bottompanel">
        <div class="row-fluid"> 
          
          <!-- span4 leftside bar-->
         <?php if($webinar_detail[0]->presenters_box==1 || $attendees_list_enabled>0) { ?>
          <div class="span4 leftsidebar"> 
            
            
            <?php if($webinar_detail[0]->presenters_box==1) { ?>
            <!-- row-fluid-->
            <div class="row-fluid">
              <div class="span12 webinar presenter_box">
                <h3><?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_webinar_desc_headline'],'Webinar Presentation'); ?></h3>
                <div class="row inner">
                   <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?><div class="span5"> <img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" alt="Webinar Host" /> </div><?php } ?>
                  <div class="span7">
                    <ul>
                      <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
                      <li><span><?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_webinar_presenter_title'],'Presenter(s)'); ?>: </span><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name);?></li>
                      <?php } ?>
                    </ul>
                  </div>
                  <?php if(!empty($webinar_detail[0]->presenters_description)) { ?>
                   <div class="span12" style="margin-left:0;">
                      <span>
                        <?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_webinar_desc_title'],'Description'); ?>
                        : </span> <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description);?>
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- row-fluid--> 
            <?php } ?>
            
            
            <!-- row-fluid-->
            <!-- Registered Participants  start-->
			<?php if($attendees_list_enabled>0){ ?>
            <?php  $logged 		= count($logged_in_attendees); 
                   $defaults	= count($default_attendee_list);
                   $tot 			= $logged+$defaults;
            ?>
            <div class="row-fluid">
              <div class="span12 webinar participants">
                <h3> <?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_attendee_name_title'],'Registered Participants'); ?> : <span style="width:auto !important;float:none !important" id="total_presenter"><?php echo $tot;?></span></h3>
								<div class="participants_sec">            
                  <!--logged in attendees-->
				  					<?php if(count($logged_in_attendees)>0){ ?>
                      <ul id="real_attendees">
					 						 <?php if(!empty($webinar_detail[0]->presenters_name)){?>
                      <li class="color"><strong><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?></strong></li>
                      <?php }?>
                      <?php foreach($logged_in_attendees as $logged_in){
						  							$registration_detail	=	$ob_webinar_db_interaction->get_webinar_session_registration_id($logged_in->registration_id_pk);
                      ?>
        			  					<li id="reg_id<?php echo $logged_in->webinar_logged_in_id_pk; ?>" ><?php echo $registration_detail[0]->attendee_name; ?></li>
						          <?php } ?>
                      </ul>
				  				 <?php } ?>
                  <!--logged in attendees ends -->
                  
                  <!--simulated attendees-->
                  <?php if(count($default_attendee_list)>0){ ?>
                  <ul id="tot_logged_in">
                    <?php for($j=0;$j<count($default_attendee_list);$j++){    ?>
                    <li attendee_id="<?php echo $default_attendee_ids[$j]; ?>"><?php echo $default_attendee_list[$j];?></li>
                    <?php } ?>
                  </ul>
                  <?php }?>
                  <!--simulated attendees ends-->
                  </div>
              </div>
            </div>
            <?php } ?>
   			<!-- Registered Participants  end-->
            <!-- row-fluid--> 
            
          </div>
          <!-- span4 leftsidebar--> 
          <?php } ?>
         
         
          <!-- span8 rightside bar-->
          <div class="<?php if($webinar_detail[0]->presenters_box==1 || $attendees_list_enabled>0) echo 'span8'; else echo 'span12' ?>  rightsidebar"> 
            
            
           
            <!-- row-fluid-->
            <div class="row-fluid" id="scarcity-delayed">
               
              
              <!-- delayed events-->
               <div id="delayed_events" class="span12 webinar events">
               <div id="delayed_events_text"></div>
               </div>
              <!-- delayed events ends--> 
              
                
                <!--scarcity events-->
                <div id="scarcity_events" class="span6 webinar events">
                    <div> <span class="counter_heading_text"> <?php echo $webinar_scarcity[0]->scarcity_start_text;?></span>
                      <div class="scarcity_counter" style=""></div>
                      <span class="counter_heading_text"><?php echo $webinar_scarcity[0]->scarcity_end_text; ?></span>
                      <div id="scarcity_events_text"></div>
                    </div>
                </div>
              <!--scarcity events ends-->
          
            </div>
            <!-- row-fluid--> 
            
            
            <!-- row-fluid-->
            <?php if($webinar_detail[0]->chatbox_enabled>0){ ?>
            <div class="row-fluid">
              <div class="span12 webinar question">
                <div class="row-fluid">
                  <?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
                  <table width="100%"><tr><td style="width:20px;" valign="top"><h3>&nbsp;</h3></td>
                  <td><span style="padding-left:0px;"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title);?></span></td></tr></table></div>
                 <form action="" method="POST" name="send_your_question" id="send_your_question">
               	   <p class="alert alert-error errors hide" style="width:83%" id="message"></p>   
                  <input name="your_name" id="your_name" type="text" value="<?php if(isset($attendee_name)) { echo $attendee_name;} else {$ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name');} ?>" maxlength="150" onfocus="if (this.value == '<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>') {this.value = ''};" onblur="if (this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_name_hint'],'Your Name'); ?>';}" />
                  <input  name="your_email" id="your_email" type="text" value="<?php if(isset($attendee_email)) {echo $attendee_email;} else {$ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email');} ?>" maxlength="150" onfocus="if (this.value == '<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_email_hint'],'Your Email'); ?>';}"/>
                  <textarea id="your_question" class="your_question" name="your_question" onfocus="if(this.value == '<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>') { this.value = '';}" onblur="if(this.value == '') {this.value='<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?>'; } " cols="" rows="0" ><?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_desc_hint'],'Your Question'); ?></textarea>
                  <br/>
                  <input type="submit" value="<?php $ob_webinar_functions->get_template_option($webinar_event_template_options['event_chat_button_text'],'Submit Your Question'); ?>" name="send_question" id="send_question" class="btn btn-large btn-primary">
                </form>
                <?php 
				  }elseif($webinar_detail[0]->chatbox_enabled==2){
				  ?>
					  <div>
					  <div>
					  <p><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?> </p><br/>
					  <div class="fb-comments" data-href="<?php echo get_permalink($post->ID); ?>" data-width="642" order_by="reverse_time" data-num-posts="10"></div>
					  </div>
					  </div>
				  <?php	  	
					}elseif($webinar_detail[0]->chatbox_enabled==3){
					   echo '<div class="video_submit_query_block left" style=" margin-left: 145px;"><div>'; 
					   echo '<p>'.$ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title).'</p><br/>';
					   echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
					   echo '<div></div>';
					}elseif($webinar_detail[0]->chatbox_enabled==4){
						echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
					}
				  ?>
              </div>
            </div>
            <?php } ?>
            <!-- row-fluid--> 
            
          </div>
          <!-- span8 rightside bar--> 
          
        </div>
      </div>
    </section>
  </div>
  
  <!-- Content Panel Ends --> 
  
</div>
<!-- Content Section Ends --> 
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_body_analytics);?>
<!-- Footer Starts -->
<footer>
  <div align="center">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
  <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_footer_analytics);?> 
  <input type="hidden" name="attendee_to_add" id="attendee_to_add" value="<?php echo $attendee_ids; ?>"  />
  <input type="hidden" name="default_attendee_operation" id="default_attendee_operation" value="<?php echo $attendee_operation;?>"/>
  <input type="hidden" name="default_attendees" id="default_attendees" value="<?php echo $attenees_ids;?>"/>
  <input type="hidden" value="<?php echo $attenees_ids;?>" id="total_logged_attendee" />
  <input type="hidden" value="0" id="webinar_played_per" name="webinar_played_per" />
</footer>

<!-- Footer Ends -->
</body>
</html>