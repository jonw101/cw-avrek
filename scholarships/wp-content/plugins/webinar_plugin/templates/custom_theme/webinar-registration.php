<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- page and OG meta -->
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="text/html;charset=UTF-8" http-equiv="content-type" />
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->reg_meta_title);?>" />
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->reg_meta_desc);?>" />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_thumbnail_path);?>" />
<meta name="title" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_title); ?>" />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_desc);?>" />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_keys);?>" />
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>" />
<!-- page and OG meta end-->
<title>
<?php if(empty($webinar_detail[0]->registration_page_title)) bloginfo('name'); else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_page_title); ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/bootstrap-responsive.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_style.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/css/custom_responsive.css'?>" />
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo plugins_url().'/webinar_plugin/templates/custom_theme/js/html5.js'?>"></script>
<![endif]-->

<?php include WEBINAR_PLUGIN_PATH.'templates/js/webinar-registration.php';?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->header_analytics); ?>

<!--- custom style starts here -->
<style type="text/css">
   
	 <?php if($service == 'youtube' || $alternate_video_format == 'youtube'){ 
	 				if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	
	 ?>
    .video_section{position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
   <?php }else{ ?>
   .video_section{position:relative;padding-bottom:52.26%;height:0;overflow:hidden}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
   <?php } 
	 }?>
	
    <?php  if($service == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
   
   .navbar{
     <?php
	if(!empty($webinar_reg_template_options['reg_header_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_header_bg_color'].';';
	}
	if(!empty($webinar_reg_template_options['reg_header_rep_image'])){ 
	echo "background-image:url('".$webinar_reg_template_options['reg_header_rep_image']."');";
	} 
	 ?>
   }
   
   .navbar .navbar_inner{
  	<?php
	if(!empty($webinar_reg_template_options['reg_header_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_header_text_color'].';';
	}
	?> 
  }
  
  .navbar .navbar_inner center{
  	<?php if(!empty($webinar_reg_template_options['reg_header_height'])){ 
      echo 'height:'.$webinar_reg_template_options['reg_header_height'].'px;';
	} 
	
	if(!empty($webinar_reg_template_options['reg_header_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_header_text_color'].';';
	}
	?>
  }
  
  .navbar_inner center img{
	<?php if(!empty($webinar_reg_template_options['reg_header_height'])){ 
      echo 'height:'.$webinar_reg_template_options['reg_header_height'].'px;';
  } ?>    
  }

  body{
   <?php if(!empty($webinar_reg_template_options['reg_body_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_body_bg_color'].';';
	}

	if(!empty($webinar_reg_template_options['reg_body_bg_rep_image'])){ 
	echo "background-image:url('".$webinar_reg_template_options['reg_body_bg_rep_image']."');";
	}
	
	if(!empty($webinar_reg_template_options['reg_body_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_body_text_color'].';';
	}
	
   ?>
  }
  
  .main_heading, .sub_heading{
    <?php
	if(!empty($webinar_reg_template_options['reg_body_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_body_text_color'].';';
	}
   ?>
  }
  
  .leftpanel{
    <?php 
	if(!empty($webinar_reg_template_options['reg_left_section_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_left_section_bg_color'].';';
	}
	?>
  }
  
  .video_section{
    <?php if(!empty($webinar_reg_template_options['reg_player_bg_color'])){ 
      echo 'border: 14px solid '.$webinar_reg_template_options['reg_player_bg_color'].';';
			echo 'background-color:'.$webinar_reg_template_options['reg_player_bg_color'].';';
  	} ?>  
  }
 
 /* arrow customization */
  section.registration_btn span{
    <?php
	if(!empty($webinar_reg_template_options['reg_arrow_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_arrow_text_color'].';';
	}
	?>
  }
  
  section.registration_btn a{
    <?php
	if(!empty($webinar_reg_template_options['reg_arrow_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_arrow_text_color'].';';
	}
	?>
  }
  
  .btn-register{
    <?php
    if(!empty($webinar_reg_template_options['reg_arrow_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_arrow_bg_color'].';';
	}
	?>
  
  }
 
 .arrow-right{
   <?php
    if(!empty($webinar_reg_template_options['reg_arrow_bg_color'])){ 
	echo 'border-left:41px solid '.$webinar_reg_template_options['reg_arrow_bg_color'].';';
	}
	?>
 }
  /*arrow customization ends */
  
  
  section.person_details, .person_details .row-fluid .span12 .row-fluid{
   <?php 
    if(!empty($webinar_reg_template_options['reg_presenter_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_presenter_bg_color'].';';
	}
	
	if(!empty($webinar_reg_template_options['reg_presenter_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_presenter_color'].';';
	}
	?>
  }
  
  /*for description section*/
  .second div.content{
    <?php 
    if(!empty($webinar_reg_template_options['reg_desc_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_desc_bg_color'].';';
	}
	
	if(!empty($webinar_reg_template_options['reg_desc_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_desc_color'].' !important;';
	}
	?>
	border-radius:6px;
  }
  /*end of description section*/
  
  .leftpanel{
    <?php 
    if(!empty($webinar_reg_template_options['reg_body_left_panel_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_body_left_panel_bg_color'].';';
	}
	?>
  }
  
  .color{
    <?php if(!empty($webinar_reg_template_options['reg_form_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_form_bg_color'].';';
	}
	
	if(!empty($webinar_reg_template_options['reg_form_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_form_text_color'].';';
	}
	
	?>
  }
  
  input#ewp_submit.btn-primary{
    <?php
	if(!empty($webinar_reg_template_options['reg_form_button_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_form_button_bg_color'].';';
	}
	
	if(!empty($webinar_reg_template_options['reg_form_button_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_form_button_text_color'].';';
	}
		
	?>
  }
  footer{
    <?php 
    if(!empty($webinar_reg_template_options['reg_footer_bg_color'])){ 
	echo 'background-color:'.$webinar_reg_template_options['reg_footer_bg_color'].';';
	}
	
	if(!empty($webinar_reg_template_options['reg_footer_rep_image'])){ 
	echo "background-image:url('".$webinar_reg_template_options['reg_footer_rep_image']."');";
	}
	
	if(!empty($webinar_reg_template_options['reg_footer_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_footer_text_color'].';';
	}
	 ?>
  }
  
  footer p a, footer p a:hover{
   <?php  if(!empty($webinar_reg_template_options['reg_footer_text_color'])){ 
	echo 'color:'.$webinar_reg_template_options['reg_footer_text_color'].';';
	}
   ?>
  }
</style>
<!-- custom style ends here-->

</head>

<body>
<!-- Header Starts --> 

<!-- Navigation -->

<div class="navbar">
  <div class="navbar_inner">
    <center>
      <?php if(!empty($webinar_detail[0]->header_logo)) { ?>
      <img src="<?php echo $webinar_detail[0]->header_logo; ?>"/>
      <?php }else{ ?>
      <?php if(!empty($webinar_reg_template_options['reg_header_text'])){ 
	      echo '<div style="font-size:24px;margin-top:12px">'.$webinar_reg_template_options['reg_header_text'].'</div>';
        }?>
      <?php } ?>
    </center>
  </div>
</div>

<!-- Navigation Ends --> 

<!-- Header Ends --> 

<!-- Content Section -->
<div class="container"> 
  
  <!-- Main Title -->
  <div class="row heading">
    <div class="span12">
      <noscript>
      <center>
        <p class="alert alert-error errors">JavaScript must be enabled in order to register for webinar.</p>
      </center>
      </noscript>
      <?php if(!empty($webinar_detail[0]->registration_headline)){ ?>
      <h2 class="main_heading"><?php echo $ob_webinar_functions-> _esc_decode_string($webinar_detail[0]->registration_headline); ?></h2>
      <?php } ?>
      <?php if(!empty($webinar_detail[0]->registration_subheadline)){ ?>
      <h3 class="sub_heading"><?php echo $ob_webinar_functions-> _esc_decode_string($webinar_detail[0]->registration_subheadline);?></h3>
      <?php } ?>
    </div>
    <!-- Main Title Ends --> 
  </div>
  
  <!-- Main Title Ends --> 
  
  <!-- Content Panel -->
  <div class="row"> 
    <!-- Left Section  Starts -->
    <section class="span8">
      <div class="leftpanel"> 
        
        <!-- Video Section -->
        <?php if($attachment_type==1){ ?>
        <?php if(!empty($webinar_detail[0]->attachment_thumb_path)) { ?>
        <section class="video_section"> <img src="<?php echo $webinar_detail[0]->attachment_thumb_path; ?>" alt="" /> </section>
        <?php } ?>
        <?php }else{  if(empty($attachment_embed_script)) { ?>
        <?php if(!empty($attachment_path)){ ?>
        <section class="video_section">
          <?php if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        <?php } ?>
        <?php } else {
        	echo $attachment_embed_script;
        	
        } }  ?>
        
        <!-- Video Section Ends --> 
        <!-- Registration Big Button -->
        <section class="registration_btn clearfix"> <a href="" onclick="return false" class="btn-register"> <span>
          <?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_arrow_text'],'Register for the Webinar'); ?>
          </span> </a>
          <div class="arrow-right"></div>
        </section>
        <!-- Registration Big Button Ends --> 
        
        <!-- Host details -->
        <?php if($webinar_detail[0]->presenters_box==1) { ?>
        <section class="person_details clearfix">
          <div class="row-fluid">
            <div class="span12">
              <div class="row-fluid">
                <div class="span12">
                  <div class="span3 image">
                    <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
                    <img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" alt="Webinar Host" />
                    <?php } ?>
                    <?php if(!empty($webinar_detail[0]->presenters_name)) { ?>
                    <span class="bold_name"><?php echo $ob_webinar_functions-> _esc_decode_string($webinar_detail[0]->presenters_name); ?></span>
                    <?php } ?>
                  </div>
                  <div style="padding-left:4px;">
                    <?php if(!empty($webinar_detail[0]->presenters_description)) { ?>
                    <p class="content"><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?></p>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <?php } ?>
        <!-- Host Details Ends -->
        
        <section class="second clearfix">
          <?php if(!empty($webinar_detail[0]->description)){ ?>
          <div class="content span6 margin0 "> 
            <!--<h4></h4>-->
            <p><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->description); ?></p>
          </div>
          <?php } ?>
        </section>
      </div>
    </section>
    <!-- Left Section Ends --> 
    
    <!-- Right Section Starts -->
    <section class="span4 margin0">
      <div class="right_section color">
        <div class="register_title">
          <?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_headline'],'Reserve Your Spot'); ?>
          <br />
          <span>
          <?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_subheadline'],'Webinar Registration'); ?>
          </span> </div>
        <div class="inner_form"> 
          <!--webinar registration fields-->
          <?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
          <input type="hidden" name="ewp_requested_name" id="ewp_requested_name" value="<?php if(!empty($_REQUEST['ew-name'])) echo trim($_REQUEST['ew-name'])?>" />
          <input type="hidden" name="ewp_requested_mail" id="ewp_requested_mail" value="<?php if(!empty($_REQUEST['ew-email'])) echo trim($_REQUEST['ew-email'])?>" />
          <?php } ?>
		  <input type="hidden" id="local_time" />
          <input type="hidden" name="webinar_widget_date" id="webinar_widget_date" value="<?php echo $ob_webinar_functions->_sanitise_string($_GET['webinar_date']);?>"  />
          <input type="hidden" name="admin_tz" id="admin_tz" value="<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>" />
          <input type="hidden" name="timezone_gmt" id="timezone_gmt"/>
          <input type="hidden" name="timezone_string" id="timezone_string"/>
          <input type="hidden" name="timezone_gmt_operation" id="timezone_gmt_operation"/>
          <input type="hidden" name="webinar_real_date" id="webinar_real_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>" />
          <input type="hidden" name="TIME" id="webinar_time"  />
          <input type="hidden" name="DATE" id="webinar_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>"  />
          <?php if($webinar_detail[0]->webinar_timezone_id_fk>0) { ?>
          <input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->timezone_gmt_symbol; ?>" value="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->GMT; ?>" />
          <?php }else{ ?>
          <input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
          <?php } ?>
          <?php if($webinar_detail[0]->notification_type==1){ ?>
          <form name="" id="" action="" method="post" >
            <?php }else{ ?>
            <?php echo $starting_form; ?>
            <?php } ?>
            <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td class="pad12">
                <?php if($webinar_detail[0]->event_type==1){ ?>
                <table class="calender_setting onetime_event_cal" style="visibility:hidden;">
                    <tr>
                      <td><div class="calender_wrapper">
                          <div class="calender_month onetime_event_cal_month"></div>
                          <div class="calender_date onetime_event_cal_date" style="border:none;"></div>
                        </div></td>
                      <td>
                        <div class="webinar_dates"></div>
                        <h5 class="webinar_times" style="display:none;"></h5>
                      </td>
                    </tr>
                </table>
                <?php } ?>
                <div class="select_wrap" <?php if($webinar_detail[0]->event_type==1) echo 'style="display:none"' ?>> 
                <img class="select" src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir ?>/img/date_bg.png" />
                 <select id="webinar_dates"  name="webinar_dates" ></select>
                </div>
                </td>
              </tr>
              <tr>
                <td class="fieldname webinar_times_hint"><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_title'],'Which Time Works Best For You?'); ?>
                  <br /></td>
              </tr>
              <tr>
                <td class="pad12">
                  <div class="select_wrap"><img class="select" src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir ?>/img/select_bg.png" />
                    <select  name="select_webinar_session" id="select_webinar_session" class="reg_timezone_dropdown">
                      <option value="0">
                      <?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_time_option'],'Select desired time'); ?>
                      </option>
                    </select>
                  </div>
                 <!-- <span>
                  <?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_localtime_title'],'Your Local Time'); ?>
                  : <span id="local_time"></span></span>--></td>
              </tr>
              <tr>
                <td class="fieldname"><?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_invitation_title'],'Where To Send The Invitation?'); ?></td>
              </tr>
              <tr>
                <td class="pad12"><p id="message_div" class="alert alert-error errors hide"></p></td>
              </tr>
              <tr>
                <td class="pad12"><?php if($webinar_detail[0]->notification_type==1){ ?>
                  <input autocomplete="off" type="text" value="<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>" onfocus="if(this.value == '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>') {this.value = ''; }" onblur="if (this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_name_text'],'Enter your name here..'); ?>'}" name="NAME" id="NAME" class="default-value" />
                  <input autocomplete="off" type="text" value="<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>" onfocus="if(this.value == '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>') {this.value = '';}" onblur="if(this.value == '') {this.value = '<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_email_text'],'Enter your email here..'); ?>';}" name="EMAIL" id="EMAIL" class="default-value" />
                  <?php }else{ ?>
                  <?php if($name_field_name!='' && $email_field_name!=''){ ?>
                  <?php echo $name_field_tag; ?> <?php echo $email_field_tag; ?> <?php echo '<div id="autoresponder_extra_fields" style="display:none">'.implode('',$other_input_fields).'</div>'; ?>
                  <input type="hidden" name="webinar_username" id="webinar_username" value="<?php echo $name_field_name; ?>"  />
                  <input type="hidden" name="webinar_email" id="webinar_email" value="<?php echo $email_field_name; ?>"  />
                  <?php }else{?>
                  <p class="alert alert-error errors">Something went wrong with autoresponder fields.</p>
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <tr>
                <td class="submit"><input type="submit" name="register" id="ewp_submit" class="btn btn-primary" value="<?php $ob_webinar_functions->get_template_option($webinar_reg_template_options['reg_form_button_text'],'Register Now'); ?>" /></td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </section>
    
    <!-- Right Section Ends --> 
    
  </div>
  
  <!-- Content Panel Ends --> 
  
</div>
<!-- Content Section Ends --> 

<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->body_analytics); ?> 

<!-- Footer Starts -->
<footer>
  <div align="center">
    <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)){ echo  $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text);} else { echo POWERED_BY_SOFTOBIZ; }?>
  </div>
  <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_analytics);?>
</footer>
<!-- Footer Ends -->

</body>
</html>