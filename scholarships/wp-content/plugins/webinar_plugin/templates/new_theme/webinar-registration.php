<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){$facebook_image= $webinar_detail[0]->presenters_thumbnail_path;}
elseif(! empty($webinar_detail[0]->header_logo )){$facebook_image =$webinar_detail[0]->header_logo;}?>
<meta content="text/html;charset=UTF-8" http-equiv="content-type" />
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->reg_meta_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->reg_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<link media="screen" rel="stylesheet" href="<?php echo WEBINAR_PLUGIN_URL.'webinar-view-control/css/colorbox.css' ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir.'/css/style.css';  ?>"  />
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir.'/css/jquery.fancybox.css?v=2.1.5';  ?>"  />
<title><?php if(empty($webinar_detail[0]->registration_page_title)) echo "Webinar Registration"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_page_title);  ?></title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->header_analytics); ?>
<!---Dynamic css starts here-->
<style type="text/css">
    <?php  if($service == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
</style>

<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/'.$selected_template_dir. '/js/jquery.fancybox.js?v=2.1.5'; ?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#register").fancybox({
			'closeBtn' : false
		});
		jQuery("#register1").fancybox({
			'closeBtn' : false
		});
	});
</script>
</head>
<body class="bodyWrapper">
<noscript>
<center>
  <h2>JavaScript must be enabled in order for you to view webinar.</h2>
</center>
</noscript>
<?php if(!empty($_REQUEST['ew-name']) && !empty($_REQUEST['ew-email'])){ ?>
<input type="hidden" name="ewp_requested_name" id="ewp_requested_name" value="<?php if(!empty($_REQUEST['ew-name'])) echo trim($_REQUEST['ew-name'])?>" />
<input type="hidden" name="ewp_requested_mail" id="ewp_requested_mail" value="<?php if(!empty($_REQUEST['ew-email'])) echo trim($_REQUEST['ew-email'])?>" />
<?php } ?>
<input type="hidden" name="webinar_widget_date" id="webinar_widget_date" value="<?php echo $ob_webinar_functions->_sanitise_string($_GET['webinar_date']);?>"  />
<input type="hidden" name="admin_tz" id="admin_tz" value="<?php echo $webinar_detail[0]->webinar_timezone_id_fk; ?>" />
<input type="hidden" name="timezone_gmt" id="timezone_gmt"/>
<input type="hidden" name="timezone_string" id="timezone_string"/>
<input type="hidden" name="timezone_gmt_operation" id="timezone_gmt_operation"/>
<input type="hidden" name="webinar_real_date" id="webinar_real_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>" />
<input type="hidden" name="TIME" id="webinar_time"  />
<input type="hidden" name="DATE" id="webinar_date" value="<?php if($is_right_now_enable==true){ echo date('Y-m-d'); }else{echo $webinar_date; } ?>"  />
<?php if($webinar_detail[0]->webinar_timezone_id_fk>0) { ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->timezone_gmt_symbol; ?>" value="<?php echo $timezones_detail[$webinar_detail[0]->webinar_timezone_id_fk-1]->GMT; ?>" />
<?php }else{ ?>
<input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
<?php } ?>
<!--Main Wrapper start here-->
<div class="topbar1"></div>
<div class="topbar2"></div>



	<div class="rainbow-border">
    	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/Rainbow-Border.png" width="100%" />
    </div>
	<div class="top-text">
	<div class="wrapper">
    	<div class="reg-text">
    	<?php if(!empty($webinar_detail[0]->header_logo)) { ?> <img src="<?php echo $webinar_detail[0]->header_logo; ?>" width="250" height="100" /> <?php } ?>
    	<br /><?php if($ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_headline) != ''){ echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->registration_headline);?>
       <?php } ?>
        </div>
        </div>
    </div>
    <div class="wrapper">
    <div class="video-section">
    <?php if(!empty($webinar_detail[0]->attachment_thumb_path) || !empty($webinar_detail[0]->attachment_path)){ ?>
    <?php if($attachment_type==1){ ?>
    <img src="<?php echo $webinar_detail[0]->attachment_thumb_path; ?>" alt="" width="960" height="540" />
    <?php } else { if($service == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="youtube" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($attachment_format=='mp4'){ ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->registration_video_aspect_ratio]['registration_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php } }?>
          <?php } else { 
          echo $attachment_embed_script;
           } ?>
    <?php /* ?>	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/video-image.png" width="100%" alt="Video" /> <?php */ ?>
    </div>
    
    <div class="register-button">
    	<a href="#register-block" id="register">Register For The Event!</a>
    </div>
    
    <!--Register Pop-Up Block Starts-->
    <div class="register-pop" id="register-block">
    	<div class="top-bar">
        	<div class="reg">
            	<div class="top-bar-txt">Register</div>
                <div class="top-bar-no">1</div>
            </div>
            <div class="attend">
            	<div class="top-bar-txt1">Attend</div>
                <div class="top-bar-no1">2</div>
            </div>
            <div class="line">&nbsp;</div>
        </div>
        <?php if($webinar_detail[0]->notification_type==1){?>
      <form name="" id="register-new-attendee" action="" method="post" >
        <?php } else { echo $starting_form; }?>
          <?php if($webinar_detail[0]->event_type==1){ ?>
          <table class="calender_setting onetime_event_cal" style="visibility:hidden;">
            <tr>
              <td><div class="calender_wrapper">
                  <div class="calender_month onetime_event_cal_month"></div>
                  <div class="calender_date onetime_event_cal_date"></div>
                </div>
              </td>
              <td>
                <div class="webinar_dates"></div>
                <h5 class="webinar_times" style="display:none;"></h5>
              </td>
            </tr>
          </table>
          <?php } ?>
        <div class="input-cal" <?php if($webinar_detail[0]->event_type==1) echo 'style="display:none"'?>>
        	<?php /* ?><input type="text" placeholder="Choose Your Date" /> <?php */ ?>
        	<select name="webinar_dates" id="webinar_dates" class="selectopt" style="margin-top:0px !important;">
            </select>
        </div>
        <div class="input-time">
        	<?php /* <input type="text" placeholder="Choose Your Time" />  <?php */ ?>
        	<select name="select_webinar_session" id="select_webinar_session" class="selectopt">
            <option value="0">Select desired time</option>
          </select>
           <div class="bottomline margintop10"></div>
        <div id="message_div" class="alert-danger error  bold fontSize13 marginBottom5" style="display:none;"></div>
        </div>
       <?php /* ?> <div class="input">
        	<input type="text" placeholder="First Name" />
        </div>
        <div class="input">
        	<input type="text" placeholder="Email Address" />
        </div>
        
        <button type="submit" class="pop-register-btn">Register For The Event!</button>
        <?php */ ?>
       
        <div id="max_attendee" style="display:none"><?php echo $webinar_detail[0]->max_number_of_attendees  ?></div>
        <?php if($webinar_detail[0]->notification_type==1){?>
        <div class="margintop10 input">
          <input autocomplete="off" value="Enter your name here..." onfocus="if(this.value == 'Enter your name here...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter your name here...';}" class="RegisInput paddingLeft5 inputbox default-value" type="text"  name="NAME" id="NAME"/>
        </div>
        <div class="margintop20 input">
          <input autocomplete="off" class="RegisInput paddingLeft5 inputbox default-value" type="text" value="Enter your email here..." onfocus="if(this.value == 'Enter your email here...') {this.value = '';}" onblur="if(this.value == '') {this.value = 'Enter your email here...';}" name="EMAIL" id="EMAIL"/>
        </div>
        
        <div class="margintop20 centeralign register-button">
      <a  href="#register-block" id="ques-loading" style="display:none;" class="ques-loader" onclick="return false"><img style="margin: 0px 10px 0 0px;float:left;"   src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/images/reg_loading.gif'; ?>" />Please Wait...</a>  
          <input type="submit" name="register" id="ewp_submit" value="Register For The Event!" class="register_button pop-register-btn"/> 
        </div>
        <div class="margintop20 centeralign" style="display:none;"> <span id="loading" class="reg_form_loading" ><img src="<?php echo get_bloginfo('url').'/'.WEBINAR_PLUGIN_PATH; ?>templates/professional_theme/images/loader.gif" /></span> <span id="query_button_loading" class="reg_form_submit_disabled"  ></span> </div>
        <?php }else{ ?>
        <?php  if($name_field_name!='' && $email_field_name!='') { ?>
        <div class="margintop10 input"> <?php echo $name_field_tag; ?> </div>
        <div class="margintop20 input"> <?php echo $email_field_tag; ?> </div>
        <div class="margintop20 centeralign register-button"><?php echo '<div id="autoresponder_extra_fields" style="display:none">'.implode('',$other_input_fields).'</div>'; ?>
          <a  href="#register-block" id="ques-loading" style="display:none;" class="ques-loader" onclick="return false"><img style="margin: 0px 10px 0 0px;float:left;"   src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/images/reg_loading.gif'; ?>" />Please Wait...</a>  
          <input class="submit register_button pop-register-btn" type="submit" tabindex="503" value="Register For The Event!" name="submit" id="ewp_submit">
        </div>
        <div class="margintop20 centeralign" >  <span id="query_button_loading" class="reg_form_submit_disabled"  ></span></div>
        <input type="hidden" name="webinar_username" id="webinar_username" value="<?php echo $name_field_name; ?>"  />
        <input type="hidden" name="webinar_email" id="webinar_email" value="<?php echo $email_field_name; ?>"  />
        <?php }else{ ?>
        <div class="invalid_form">Something went wrong with registration fields.</div>
        <?php } ?>
        <?php } ?>
        
        </form>
    </div>
    <!--Register Pop-Up Block Ends-->
    <?php if($ob_webinar_functions->_esc_decode_html($webinar_detail[0]->description) != ''){ ?>
    <div class="description">
    	<div class="desc-heading">
        	When You Attend, You Will Learn...
        </div>
        
        <div class="desc">
      <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->description); ?>
        <!--  	<div class="desc-line">
            	The <span class="bold">100 Membership Site Ideas</span>&nbsp;that you can "copy and clone" so you can 
                <span class="underline">jumpstart your success</span> with building a <span class="bold">Successful Membership Site in just 28 days or less...</span>
            </div>
			<div class="desc-line">
            	The&nbsp;<span class="bold">3 Power Pillars</span>&nbsp;your membership site&nbsp;<span class="underline">must have</span>&nbsp;that will give you the ability to systematically and predictably turn it into a&nbsp;<span class="bold">Passive Income Generating Machine...</span>
            </div>
			<div class="desc-line">
            	Why&nbsp;<span class="bold">Sales pages, Sales Videos and Sales Funnels are DEAD...</span>&nbsp;and if you are using any of these, you're setting "your business" up for&nbsp;<span class="underline">catastrophic</span>&nbsp;disaster...
            </div>
			<div class="desc-line">
            	Why&nbsp;<span class="underline">not having</span>&nbsp;anything to sell, teach or talk about is a "Good Thing" when you apply my Membership Method (and&nbsp;still&nbsp;<span class="bold">Become The&nbsp;"Go to" Celebrity Authority</span>)...
            </div>
			<div class="desc-line">
            	How to&nbsp;<span class="bold">Strategically Position Your Content</span>&nbsp;to entice, entertain and educate your audience... and why "Gurus" are&nbsp;<span class="underline">afraid</span>&nbsp;to do this...
            </div>
            <div class="desc-line">
            	The membership site&nbsp;<span class="underline">currently generating</span>&nbsp;over&nbsp;<span class="bold">$100,000,000.00 PER MONTH</span>&nbsp;that inspired me to drop everything and focus 100% on Membership Sites back in 2007...
            </div>
			<div class="desc-line"> 
            	The&nbsp;<span class="bold">7&nbsp;Money Making Membership Models</span>&nbsp;that you can quickly and easily create, scale and automate... "even if" you've&nbsp;<span class="underline">never built</span>&nbsp;a membership site before...
            </div>
			<div class="desc-line"> ...And so much more! </div>
-->
        </div>
        
    </div>
    
    <div class="register-button">
    	<a href="#register-block" id="register1">Register For The Event!</a>
    </div>
    <?php } ?>
    </div>
    <div class="footer">
    <?php if($webinar_detail[0]->presenters_box==1) { ?>
       <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
       <div class="wrapper">
    	<div class="footer-img">
        	<div class="host-image">
        		<img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="109" height="109" alt="img" />
       		</div>
        </div>
        <?php } ?>
        <div class="host-text">Your Host</div>
        <div class="host-name"><?php if(!empty($webinar_detail[0]->presenters_name)) { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); } ?></div>
        <div class="host-desc">
        	<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?>
        </div>
        </div>
        <?php } ?>
    </div>
    <div class="copyright">
    	<div class="wrapper">
    		 <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
        </div>
    </div>
    

</body>
</html>