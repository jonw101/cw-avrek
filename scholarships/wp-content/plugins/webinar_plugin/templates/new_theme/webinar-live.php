<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if(empty($webinar_detail[0]->webinar_page_title)) bloginfo('name'); else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->webinar_page_title); ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<?php include WEBINAR_PLUGIN_PATH.'templates/js/webinar-live.php';?>
     <link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/new_theme/css/style.css'?>" />
    <link rel="Stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/css/main.css'; ?>" />
 
<?php if((isset($_GET['live'])===true || $webinar_detail[0]->live_page_type==1) && isset($_GET['countdown'])===false) {?>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="text/html;charset=UTF-8" http-equiv="content-type">

    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/js/html5.js'?>"></script>
    <![endif]-->
    
    <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->event_header_analytics); ?>
    
    <!--- custom style starts here -->
    <style type="text/css">
    
    <?php if($service_type == 'youtube' || $alternate_video_format == 'youtube'){
				if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	
  ?>
     .video-section {position:relative;padding-bottom:56.25%;padding-top:30px;height:0;margin-bottom:20px;}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
  <?php }else{ ?>
		.video-section{position:relative;padding-bottom:54%;height:0;overflow:hidden;margin-bottom:20px;}.video_section iframe,.video_section object,.video_section embed{position:absolute;top:0;left:0;width:100%;height:100%}
	<?php } }?> 
    
    <?php  if($service_type == 'vimeo'){ ?>  
    #me_vimeo_0_container{position:relative;padding-bottom:56.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
    </style>
</head>

<body>

<noscript>
    <center>
      <h2>JavaScript must be enabled in order for you to view webinar.</h2>
    </center>
    </noscript>
    <?php if(isset($webinar_detail[0]->chatbox_enabled) && $webinar_detail[0]->chatbox_enabled==2){?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code); ?>";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <style type="text/css">.fb-comments, .fb-comments span, .fb-comments iframe {width: 100% !important;}</style>
    <?php }?>
    
    <div class="topbar1"></div>
    <div class="topbar2"></div>

	<div class="rainbow-border">
    	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/Rainbow-Border.png" width="100%" />
    </div>

	<div class="wrapper">	
        <div class="video-section">
		 
         <?php if($service_type == 'youtube'){?>
   
		   <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="youtube" width="100%" height="100%" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php }else{ ?>
            <video id="youtube" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/youtube" src="<?php echo $video_url;?>" />
            </video>
           <?php } ?>
            
            
        <?php }elseif($service_type == 'other'){ ?>
        
          <?php if($video_format=='mp4'){ ?>
            
           <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?> 
            <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php }else{ ?>
            <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $video_url;?>" />
            </video>
            <?php } ?>
            
          <?php }elseif($video_format=='flv'){ ?>
            
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
            
            <?php if($alternate_video_format=='youtube'){ ?>
              <video id="h264" width="100%" height="100%" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php  }elseif($alternate_video_format=='mp4'){ ?>
              <video id="h264" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
              <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
              </video>
            <?php } ?>
          
          <?php }else{ ?>
              <video id="h264" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source type="video/flv" src="<?php echo $video_url;?>" />
              </video>
          <?php } ?>
          
          <?php }?>
        
        <?php }else if($service_type == 'stream'){?>
        
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          
              <?php if($alternate_video_format=='youtube'){ ?>
                <video id="rtmp" width="100%" height="100%" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php  }elseif($alternate_video_format=='mp4'){ ?>
                <video id="rtmp" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
                <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
                </video>
              <?php } ?>
           
           <?php }else{ ?>
              <?php if($video_format=='mp4'){ ?>
                <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
                <source src="<?php echo $video_url;?>/mp4:<?php if(isset($video_stream)){echo $video_stream;}?>" type="video/rtmp" />
                </video>
              <?php }elseif($video_format=='flv'){ ?>
              <?php $flv_stream = str_replace('.flv','',$video_stream); ?>
               <video id="rtmp" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
               <source src="<?php echo $video_url;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
               </video>
              <?php }?>
          <?php } ?>
        
        <?php }elseif($service_type=='vimeo'){?>
            <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
             <video id="vimeo" width="100%" height="100%" poster="" controls="false" preload="none" autoplay="true">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php }else{ ?>
            <video id="vimeo" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->event_video_aspect_ratio]['video_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
              <source src="<?php echo $video_url;?>" type="video/vimeo" />
            </video>
            <?php } ?>
        
        <?php }?>
    
            <?php /* <img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/video-image.png" width="100%" alt="Video" /> */ ?>
        </div>
         <!-- row-fluid-->
        <div class="row-fluid" id="scarcity-delayed"> 
          
          <!-- delayed events-->
          <div id="delayed_events" class="span12 webinar events">
            <div id="delayed_events_text" style="float:left;width:96%;text-align:center"></div>
          </div>
          <!-- delayed events ends--> 
          
          <!--scarcity events-->
          <div id="scarcity_events" class="span6 webinar events">
            <div style="float:left;width:96%;text-align:center"> <span class="counter_heading_text"> <?php echo $webinar_scarcity[0]->scarcity_start_text;?></span>
              <div class="scarcity_counter" style=""></div>
              <span class="counter_heading_text"><?php echo $webinar_scarcity[0]->scarcity_end_text; ?></span>
              <div id="scarcity_events_text"></div>
            </div>
          </div>
          <!--scarcity events ends--> 
          
        </div>
      	<!-- row-fluid-->
        <input type="hidden" name="attendee_to_add" id="attendee_to_add" value="<?php echo $attendee_ids?>"  />
          <input type="hidden" name="default_attendee_operation" id="default_attendee_operation" value="<?php echo $attendee_operation?>"/>
          <input type="hidden" name="default_attendees" id="default_attendees" value="<?php echo $attenees_ids?>"/>
    <?php if($webinar_detail[0]->chatbox_enabled==1){ ?>
    <form action="" method="post" name="send_your_question" id="send_your_question">
        <div class="question">
            <div class="ques-heading">Do You Have A Question?</div>
            <div class="ques-sub-heading">
                <?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?>
            </div>
             
            <div class="ques-form">
            <div id="message" style="display:none;" class="alert-error alert-danger error bold fontSize13 marginBottom5"></div>
                <div class="ques-row">
                    <input type="text" name="your_name" id="your_name" class="f-name" placeholder="First Name" >
                    <input type="text" name="your_email" id="your_email" class="mail" placeholder="Best Email Address" >
                </div>
                <div class="ques-row">
                    <textarea id="your_question" name="your_question" class="query" placeholder="Ask Your Question Here..." style="width:100%"></textarea>
                </div>
                <div class="ques-submit">
                    <img id="ques-loading" class="ques-loader" src="<?php echo WEBINAR_PLUGIN_URL.'templates/' . $selected_template_dir . '/images/fancybox_loading.gif'; ?>" /><a><button type="submit" name="send_question" id="send_question" class="ques-btn">Submit Your Question</button></a>
                </div>
            </div>
        </div>
        </form>
        <?php }  elseif($webinar_detail[0]->chatbox_enabled==2){
          ?>
           <div class="ques-form" style="width:100%">
            <p><b><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title); ?></b></p>
            <div class="fb-comments" data-href="<?php echo get_permalink($post->ID); ?>" data-width="642" data-num-posts="10" order_by="reverse_time"></div>
            </div>
          <?php	  	
          }elseif($webinar_detail[0]->chatbox_enabled==3){
              echo '<div class="ques-form" style="width:100%">';
              echo '<p><b>'.$ob_webinar_functions->_esc_decode_string($webinar_detail[0]->chat_title).'</b></p>';
              echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
              echo '</div>';
          }elseif($webinar_detail[0]->chatbox_enabled==4){
              echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->chatbox_code);
          }
         ?>
         </div>
               <div class="footer">
    <?php if($webinar_detail[0]->presenters_box==1) { ?>
    <div class="wrapper">
       <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
    	<div class="footer-img">
        	<img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="109" height="109" alt="img" />
        </div>
        <?php } ?>
        <div class="host-text">Your Host</div>
        <div class="host-name"><?php if(!empty($webinar_detail[0]->presenters_name)) { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); } ?></div>
        <div class="host-desc">
        	<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?>
        </div>
        </div>
        <?php } ?>
    </div>
        <div class="enroll" style="display:none;">
            <a><button type="button" class="enroll-now">Enroll Now</button></a>
            <div class="enroll-text">
                You Will Get Access To The Exclusive 5 Week Membership Method Workshop...
                <span class="enroll-txtb">Instantly Save $500 (And) Get The 8 Bonus Training Modules!</span>
            </div>
        </div>

   
   
</body>
 <?php } else{ ?>
 
 </head>
 <body>
 <div class="rainbow-border">
    	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/Rainbow-Border.png" width="100%" />
    </div>
	<div class="top-text">
        <div class="wrapper"> 
            <div class="cong-text">Whoops!  You're A Bit Too Early :-)
                <span class="cong-sub-text">Bookmark This Page And When The Timer Reaches Zero You Will Be Taken Straight To The Online Event.</span>
            </div>
        </div>
    </div>
    
    <div class="wrapper">
        <div class="countdown-timer">
            <input type="hidden" name="your_timezone" id="your_timezone" timezone_operation="" />
    <input type="hidden" name="webinar_video_link" id="webinar_video_link"  value="<?php echo add_query_arg(array('live'=>''),get_permalink($post->ID));?>" />
            <div id="countdown_dashboard" class="countdown" style="width:auto;height:auto;margin:auto;">
                <div class="circle">
                    <canvas id="days" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-days digit">0</span>
                        <span class="ce-label ce-days-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="hours" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-hours digit">0</span>
                        <span class="ce-label ce-hours-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="minutes" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-minutes digit">0</span>
                        <span class="ce-label ce-minutes-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="seconds" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-seconds digit">0</span>
                        <span class="ce-label ce-seconds-label digit">0</span>
                    </div>
                </div>
            </div>
            
        </div>    
    </div>
     <div class="footer">
    <?php if($webinar_detail[0]->presenters_box==1) { ?>
    <div class="wrapper">
       <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
    	<div class="footer-img">
    	<div class="host-image">
        	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/footer-image.png" width="109" height="109" alt="img" />
        </div>
        </div>
        <?php } ?>
        <div class="host-text">Your Host</div>
        <div class="host-name"><?php if(!empty($webinar_detail[0]->presenters_name)) { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); } ?></div>
        <div class="host-desc">
        	<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?>
        </div>
    </div>
    <?php } ?>
    </div>
    <div class="copyright">
    	<div class="wrapper">
    		 <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
        </div>
    </div>

<!-- Init Count Everest plugin -->

<script>
	/* $(document).ready(function() {
		
		var date = new Date();
        
       alert(scheduled_time);
		date.setTime(date.getTime()+(scheduled_time));
		var pday 	=	date.getDate();
		var pyear 	=	date.getFullYear();
		var pmonth 	=	date.getMonth()+1;
		alert(pday);
		
		var phour 	=	date.getHours();
		var pminutes	=	date.getMinutes();
		
		
	}); */
</script>
 </body>
 <?php } ?>
</html>
