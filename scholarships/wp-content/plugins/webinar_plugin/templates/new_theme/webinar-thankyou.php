<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('url').'/'.WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir. '/css/style.css';?>"  />
<meta content="text/html;charset=UTF-8" http-equiv="content-type">
<meta property="og:title" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_text); ?>"/>
<meta name="og:description" content="<?php echo $ob_webinar_functions->_esc_decode_string($sharing_detail[0]->share_desc); ?>"  />
<meta property="og:image" content="<?php echo $ob_webinar_functions->_esc_decode_string($facebook_image); ?>" />
<meta name="title" content="<?php if(empty($webinar_detail[0]->thankyou_meta_title)) echo "Thank you"; else echo stripslashes($webinar_detail[0]->thankyou_meta_title);  ?>"  />
<meta name="description" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_desc); ?>"  />
<meta name="keywords" content="<?php  echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_meta_keys);?>">
<meta name="author" content="<?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); ?>">
<title>
<?php if($webinar_detail[0]->thankyou_webinar_page_title=='') echo "Thank you"; else echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_webinar_page_title);  ?>
</title>
<?php include WEBINAR_PLUGIN_PATH.'templates/js/'.$current_template;?>
<style>@media print {.noPrint {	display:none;}} </style>
<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_header_analytics);?>
<style type="text/css">
	 <?php  if($service == 'vimeo' || $service_sharing == 'vimeo'){ ?>  
    #me_vimeo_0_container, #me_vimeo_1_container{position:relative;padding-bottom:51.25%;padding-top:25px;height:0}#me_vimeo_0_container iframe, #me_vimeo_1_container iframe{position:absolute;top:0;left:0;width:100%;height:100%}
    <?php } ?>
    fb_ltr.fb_iframe_widget_lift {z-index: 1000000;}
</style>
</head>
<body>
<noscript>
<center>
  <h1>JavaScript must be enabled in order for you to view webinar.</h1>
</center>
</noscript>




	<div class="rainbow-border">
    	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/Rainbow-Border.png" width="100%" />
    </div>
	<div class="top-text">
	<div class="wrapper">
    	<div class="cong-text"><?php if(empty($webinar_detail[0]->thankyou_title)) { echo 'Congratulations!';  } else { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_title); } ?>
        	<span class="cong-sub-text"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->thankyou_subtitle);  ?></span>
        </div>
        </div>
    </div>
    <div class="wrapper">
    <div class="video-section">
    
    <?php
    if($webinar_detail[0]->thankyou_attachment_type==1){
				if(!empty($webinar_detail[0]->thankyou_attachment_thumb_path)) {
		?>
       <img src="<?php echo $webinar_detail[0]->thankyou_attachment_thumb_path;?>" alt=""  width="100%" /> 
      <?php } 
			} else{
				if(empty($thankyou_attachment_embed_script)){
					 if($service == 'youtube'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service == 'other'){ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $thankyou_attachment_path;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service == 'stream'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="thankyou" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $alternate_video_url;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($thankyou_attachment_format=='mp4'){ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/mp4:<?php if(isset($stream)){echo $stream;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($thankyou_attachment_format=='flv'){ ?>
          <?php $flv_stream = str_replace('.flv','',$stream); ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>/<?php if(isset($flv_stream)){echo $flv_stream;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="thankyou" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $thankyou_attachment_path;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }
				}
				else{
					echo $thankyou_attachment_embed_script;
				}
			}
			
			?>
    <?php /* ?>	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/video-image.png" width="100%" alt="Video" /> <?php */ ?>
    </div>
    
    <div class="event-detail">
    <?php if($ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_time) != ''){ ?>
    	<div class="when">
        	
            <div class="when-det">
            	<div class="when-head">This Event Takes Place On...</div>
                <div class="when-desc"><?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_date); ?> at <?php echo $ob_webinar_functions->_esc_decode_string($result[0]->webinar_real_time); ?> (Your Time)</div>
            </div>
        </div>
    <?php } if($ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thankyou_webinar_description) != ''){ ?>
    
    
     <div class="learn">
                <!--<div class="when-img"><img src="images/confirmation-calender.png" alt="img" /></div>-->
                <div class="when-det">
                    <div class="when-head">You Will Learn...</div>
                    <div class="when-desc"><?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thankyou_webinar_description);?></div>
                </div>
            </div>
            <?php } if($result[0]->key != ''){ ?>
            <div class="url">
                <!--<div class="when-img"><img src="images/confirmation-calender.png" alt="img" /></div>-->
                <div class="when-det">
                    <div class="when-head">The URL For This Event Is...</div>
                    <div class="when-desc" style="cursor:pointer;" onclick="window.open('<?php echo add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id)); ?>')"><?php if(isset($result[0]->key))echo add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id));  ?></div>
                </div>
            </div>
            <?php } ?>
            <?php  if($sharing_detail[0]->is_sharing_enabled==1){ ?>
            <div class="social">
                <div class="social-det">
                    <div class="when-head">
                     
                    <?php $ob_webinar_functions->get_template_option($webinar_thank_template_options['sharing_text_title'],'Share News of This Webinar and Unlock Your Gift!'); ?>
                    </div>
                    <div class="social-desc">
                    <div class="fb-icon">
          	<div id="fb-root"></div>
            <div class="fb-like" data-href="<?php echo get_permalink($reg_id); ?>" data-layout="button_count" data-action="like" data-share="false" data-width="100" data-show-faces="true" data-font="arial"></div>
          </div>
           <div class="left marginleft10" style="margin-top:-10px;margin-left:7px;"><img src="<?php echo get_bloginfo('url').'/'.WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/images/fb-separator.png';?>" height="85" width="2" alt="separator" /></div>
          <div class="left social_links" style="margin-left:7px"> <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $sharing_detail[0]->share_text; ?>" data-url="<?php echo get_permalink($reg_id); ?>"data-count="horizontal">Tweet</a> </div>
          <div class="left marginleft10" style="margin-left:7px;"><img src="<?php echo get_bloginfo('url').'/'.WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/images/fb-separator.png';?>" height="85" width="2" alt="separator" /></div>
          <div class="left" style="margin-left:7px; margin-top:23px;"> 
            <!-- Place this tag where you want the +1 button to render -->
            <g:plusone callback="reward" href="<?php echo get_permalink($reg_id); ?>">
          </div>
          
                    	<!-- Social Link here--->
                   </div>
                </div>
            </div>
            <?php if($sharing_detail[0]->is_video_enabled==1 || $sharing_detail[0]->is_document_enabled==1  ){ ?>
            <div class="social-content">
            <div class="noprint">
                <div align="center" style="display:none;" id="document-reward" > <img src="<?php echo bloginfo('url').'/'.WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir. '/images/bonus.png'; ?>" width="306" height="64" class="margintop20" /><br />
      	<div class="clear"></div>
       	<?php if($sharing_detail[0]->is_document_enabled==1 && !empty($sharing_detail[0]->document)) { ?>
          <div align="center">
            <h4>Please Download Your Gift.</h4>
            <a target="_blank" href="<?php echo $sharing_detail[0]->document; ?>" >
            <input type="image" src="<?php echo bloginfo('url').'/'.WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/images/b-download-bonus.png';?>" height="52" width="226" />
            </a> </div>
          <?php } ?>
      </div>
            
            
            <?php if($sharing_detail[0]->is_video_enabled==1 && !empty($sharing_detail[0]->video_attachment_path)){ ?>
      <div class="clear"></div>
      <div style="float:left">
        <div align="center" style="display:none;" id="video-reward" >
          <section class="video_section">
          <?php if($service_sharing == 'youtube'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($service_sharing == 'other'){ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source type="video/flv" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }?>
          <?php }else if($service_sharing == 'stream_sharing'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <?php if($alternate_video_format=='youtube'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/youtube" src="<?php echo $sharing_video;?>" />
          </video>
          <?php  }elseif($alternate_video_format=='mp4'){ ?>
          <video id="sharing_video" width="100%" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source type="video/mp4" src="<?php echo $sharing_video;?>" />
          </video>
          <?php } ?>
          <?php }else{ ?>
          <?php if($sharing_video_format=='mp4'){ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/mp4:<?php if(isset($stream_sharing)){echo $stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }elseif($sharing_video_format=='flv'){ ?>
          <?php $sharing_flv_stream_sharing = str_replace('.flv','',$stream_sharing); ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>/<?php if(isset($sharing_flv_stream_sharing)){echo $sharing_flv_stream_sharing;}?>" type="video/rtmp" />
          </video>
          <?php }?>
          <?php } ?>
          <?php }elseif($service_sharing=='vimeo'){?>
          <?php if( $iPod || $iPhone || $iPad || $Android || $AndroidTablet){	?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php }else{ ?>
          <video id="sharing_video" width="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thankyou_page_video_width']; ?>" height="<?php echo $webinar_layout_variable[$webinar_detail[0]->thankyou_video_aspect_ratio]['thnkyou_page_video_height']; ?>" poster="" controls="false" preload="none" autoplay="true" style="width: 100%; height: 100%;">
            <source src="<?php echo $sharing_video;?>" type="video/vimeo" />
          </video>
          <?php } ?>
          <?php }?>
        </section>
        </div>
      </div>
      <?php } ?>
            </div>
            </div>
            <?php } ?>
            <?php  } ?>
    </div>
    </div>
    
     <div class="footer">
    <?php if($webinar_detail[0]->presenters_box==1) { ?>
    <div class="wrapper">
       <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
    	<div class="footer-img">
    	<div class="host-image">
        	<img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="109" height="109" alt="img" />
        </div>
        </div>
        <?php } ?>
        <div class="host-text">Your Host</div>
        <div class="host-name"><?php if(!empty($webinar_detail[0]->presenters_name)) { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); } ?></div>
        <div class="host-desc">
        	<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?>
        </div>
        </div>
        <?php } ?>
    </div>
    <div class="copyright">
    	<div class="wrapper">
    		 <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
        </div>
    </div>
     <?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->thank_footer_analytics); ?> </div>

</body>
</html>