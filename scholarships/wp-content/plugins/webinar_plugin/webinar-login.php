<?php
/**
 * Template Name: Webinar Login
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
date_default_timezone_set('GMT');
global $post;
	
	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions				=	new webinar_functions();

	$purge_cache						= $ob_webinar_functions->purge_cache();
	$webinar_detail					=	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);
	$countdown_page_id			=	$ob_webinar_db_interaction->get_webinar_template_page_id("countdown");	
	$countdown_page_link		=	get_permalink($countdown_page_id);
	$webinar_pages					=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);
	$error_page							=	get_permalink($ob_webinar_db_interaction->get_error_page_id());
	$affilate_param = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param);
    $affilate_param_val = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param_val);
    if($affilate_param_val != '' && $affilate_param != ''){
    	$affilate_param_query = '&' . $affilate_param  . '=' . $affilate_param_val;
    }
    else {
    	$affilate_param_query = '';
    	$affilate_param_val = '';
    	$affilate_param ='';
    }
if(isset($_GET['key']) and isset($_GET['right_now']) ){
  $key = $ob_webinar_functions->_sanitise_string($_GET['key']);
  $right_now = $ob_webinar_functions->_sanitise_string($_GET['right_now']);
  
  $is_right_now_enable 	=	$ob_webinar_db_interaction->check_webinar_right_now_enable($webinar_detail[0]->is_right_now_enable,$webinar_detail[0]->webinar_start_date);	
  
  if($is_right_now_enable){

	  $webinar_registration_id  	 	 =  $ob_webinar_db_interaction->get_webinar_registration_id($webinar_detail[0]->webinar_id_pk);
	  $attendee_webinar_registration =	$ob_webinar_db_interaction->do_attendee_register_for_webinar_key($webinar_detail[0]->webinar_id_pk,$key);
	  $attendee_webinar_session			 =	$ob_webinar_db_interaction->get_todays_unexpired_session_by_attendee_key($post->webinar_id,$key,$right_now);
	
	  if($attendee_webinar_registration>0 && $attendee_webinar_session!=0){
		
		  $_session_webinar_registration_key	=	$attendee_webinar_session->key;
		  $_session_start_time				=	date("H:i:s");
		 
		  $webinar_video_page_id			=	$ob_webinar_db_interaction->get_webinar_video_id($post->webinar_id);	
		  $webinar_video_link					=	get_permalink($webinar_video_page_id);
		   if($affilate_param_val != '' && $affilate_param != ''){
		  $redirect_to_webinar_video_link = add_query_arg(array('attendee'=>$_session_webinar_registration_key,'start'=>base64_encode($_session_start_time),$affilate_param =>$affilate_param_val),$webinar_video_link);
		   }
		   else {
		   $redirect_to_webinar_video_link = add_query_arg(array('attendee'=>$_session_webinar_registration_key,'start'=>base64_encode($_session_start_time)),$webinar_video_link);
		   }
		  header("Location:$redirect_to_webinar_video_link");
		  exit;
	  }
	  else{
		  header("Location:$error_page");
		  exit;
	  }
  }
  else{
	  header("Location:$error_page");
	  exit;
  }
}
if(isset($_GET["key"]) and (!isset($_GET['right_now']))){
	$key 							= $ob_webinar_functions->_sanitise_string($_GET['key']);
	$webinar_registration_id  			= $ob_webinar_db_interaction->get_webinar_registration_id($webinar_detail[0]->webinar_id_pk);
	$attendee_webinar_registration	= $ob_webinar_db_interaction->do_attendee_register_for_webinar_key($webinar_detail[0]->webinar_id_pk,$key);	
	
	if($attendee_webinar_registration>0){
		
		$attendee_webinar_session	=	$ob_webinar_db_interaction->get_latest_webinar_session_for_attendee_key($webinar_detail[0]->webinar_id_pk,$key);		
	
		if(count($attendee_webinar_session)==0){
		
			$attendee_webinar_session =	$ob_webinar_db_interaction->get_todays_unexpired_session_by_attendee_key($post->webinar_id,$key);		
			
			if($attendee_webinar_session!=0){
			
				$_session_webinar_registration_id		=	$attendee_webinar_session->registration_id_pk;	
				$_session_webinar_id								=	$attendee_webinar_session->webinar_id_fk;	
				$_session_webinar_schedule_id_fk		=	$attendee_webinar_session->webinar_schedule_id_fk;	
				$_session_webinar_date							=	$attendee_webinar_session->webinar_date;					
				$_session_webinar_registration_key	=	$attendee_webinar_session->key;
				if($affilate_param_val != '' && $affilate_param != ''){
				$redirect_to_countdown_page	= add_query_arg(array('attendee'=>$_session_webinar_registration_key,$affilate_param => $affilate_param_val),$countdown_page_link);
				}
				else{
					$redirect_to_countdown_page	= add_query_arg(array('attendee'=>$_session_webinar_registration_key),$countdown_page_link);
				}
				header("Location:$redirect_to_countdown_page");
				exit;
				
			}else{
				
				$attendee_webinar_session	=	$ob_webinar_db_interaction->get_todays_unexpired_session_by_attendee_key($post->webinar_id,$key);		
			
				if($attendee_webinar_session==0){
					$replay_page			=	get_permalink($webinar_pages[3]->ID);
					$replay_link			=	add_query_arg(array('attendee'=>$key),$replay_page);
					$_SESSION["error"]		=	VIDEO_ENDS.'. Please <a href="'.$replay_link.'" >click here</a> to watch the recorded webinar';	
					header("Location:$error_page");
					exit;

				}else{
					$_session_webinar_registration_id	=	$attendee_webinar_session->registration_id_pk;	
					$_session_webinar_id							=	$attendee_webinar_session->webinar_id_fk;	
					$_session_webinar_schedule_id_fk	=	$attendee_webinar_session->webinar_schedule_id_fk;	
					$_session_webinar_date						=	$attendee_webinar_session->webinar_date;		
					
					$webinar_video_page_id						=	$ob_webinar_db_interaction->get_webinar_video_id($attendee_webinar_session->webinar_id_fk);	
					$webinar_video_link								=	get_permalink($webinar_video_page_id);
					if($affilate_param_val != '' && $affilate_param != ''){
					$redirect_to_video_page						=	add_query_arg(array('attendee'=>$_session_webinar_registration_id,$affilate_param =>$affilate_param_val),$webinar_video_link);
					}else{
						$redirect_to_video_page						=	add_query_arg(array('attendee'=>$_session_webinar_registration_id),$webinar_video_link);
					}
					header("Location:$redirect_to_video_page");
					exit;
				}								
			}
		}else{
				$_session_webinar_registration_key	=	$attendee_webinar_session->key;
				if($affilate_param_val != '' && $affilate_param != ''){
				$redirect_to_countdown_page					= add_query_arg(array('attendee'=>$_session_webinar_registration_key,$affilate_param =>$affilate_param_val),$countdown_page_link);
				} else{
					$redirect_to_countdown_page					= add_query_arg(array('attendee'=>$_session_webinar_registration_key),$countdown_page_link);
				}
				header("Location:$redirect_to_countdown_page");
				exit;
		}
	}else{
		$reg_page = get_permalink($webinar_registration_id);
		header("Location:".$reg_page);
		exit;		
	}
}
?>