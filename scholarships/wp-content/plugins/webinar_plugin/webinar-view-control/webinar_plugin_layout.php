<?php
error_reporting(0);
$ob_webinar_plugin	=	new webinar_plugin();
$Plugin_register=$ob_webinar_plugin->redirect_if_not_registered();
$see_all_webinar_page = get_bloginfo('url');

if($Plugin_register!='Plugin_register'){
?>
<link media="all" type="text/css" href="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/css/dashboard.css" rel="stylesheet">

<div class="wordpress_dashboard_header clearfix" style="background-color:#015885;">
  <div class="ewp_logo"> <img height="51" src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/ewp-logo.png" alt="Easywebinar" /></div>
</div>
<div class="dashboard_content_right clearfix">
  <div class="width534"></div>
  <div class="width534"></div>
  <div class="clear"></div>
  <div class="dashboard_front_icons2">
    <div class="dashboard_front_icons1">
      <ul>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=create-webinar">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/create-webinar1.png" width="48" height="50" alt="create-webinar" /></div>
                <p class="dashbord_icon_text">Create Webinar</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=dashboard">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/webinars1.png" width="48" height="50" alt="webinar" /></div>
                <p class="dashbord_icon_text">Webinars</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=webinar-drafts">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/drafts1.png" width="48" height="50" alt="drafts" /></div>
                <p class="dashbord_icon_text">Drafts</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a  target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/list-your-domain">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/admin1.png" width="48" height="50" alt="admin" /></div>
                <p class="dashbord_icon_text">EWP Admin</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
      </ul>
    </div>
    <div class="dashboard_front_video">
      <div class="dashboard_front_video_block">
        <div class="video_url" style="border:1px solid #ccc; height:400px">
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="646" height="401">
            <param name="wmode" value="opaque">
            <param name="timed_link" value="0">
            <param name="allowfullscreen" value="true">
            <param name="allowscriptaccess" value="always">
            <param name="movie" value="http://ezs3.s3.amazonaws.com/player/58/player.swf">
            <param name="flashvars" value="aboutlink=http://www.ezs3.com/about&amp;backcolor=000000&amp;skin=http://ezs3.s3.amazonaws.com/player/skins/glow.zip&amp;mute=false&amp;frontcolor=ffffff&amp;screencolor=000000&amp;stretching=uniform&amp;icons=true&amp;lightcolor=000099&amp;repeat=none&amp;controlbar=over&amp;file=http://d3t21e94xmb6gv.cloudfront.net/AutoWebinarVideos/EWPwpdashvideo.mp4&amp;image=http://d3t21e94xmb6gv.cloudfront.net/gettingstartedewp.png&amp;provider=video&amp;autostart=false&amp;dock=false&amp;abouttext=eZs3&amp;logo.hide=false&amp;logo.file=http://d3t21e94xmb6gv.cloudfront.net/EasyWebinarPlugin100x18.png&amp;logo.linktarget=_blank&amp;logo.position=bottom-right&amp;plugins=gapro-1&amp;gapro.accountid=UA-12244947">
            <embed width="646" height="402" wmode="opaque" timed_link="0" allowfullscreen="true" allowscriptaccess="always" src="http://ezs3.s3.amazonaws.com/player/58/player.swf" flashvars="aboutlink=http://www.ezs3.com/about&amp;backcolor=000000&amp;skin=http://ezs3.s3.amazonaws.com/player/skins/glow.zip&amp;mute=false&amp;frontcolor=ffffff&amp;screencolor=000000&amp;stretching=uniform&amp;icons=true&amp;lightcolor=000099&amp;repeat=none&amp;controlbar=over&amp;file=http://d3t21e94xmb6gv.cloudfront.net/AutoWebinarVideos/EWPwpdashvideo.mp4&amp;image=http://d3t21e94xmb6gv.cloudfront.net/gettingstartedewp.png&amp;provider=video&amp;autostart=false&amp;dock=false&amp;abouttext=eZs3&amp;logo.hide=false&amp;logo.file=http://d3t21e94xmb6gv.cloudfront.net/EasyWebinarPlugin100x18.png&amp;logo.linktarget=_blank&amp;logo.position=bottom-right&amp;plugins=gapro-1&amp;gapro.accountid=UA-12244947" />
          </object>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clear"></div>
<?php
} ?>
