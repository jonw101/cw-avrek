<?php
error_reporting(0);
$ob_webinar_plugin	=	new webinar_plugin();
if(isset($_POST['key'])){

	$key_info = $ob_webinar_plugin->get_key_info(trim($_POST['key']),get_bloginfo('url'));
	//echo 'ht' . $key_info->is_key_valid;
	if($key_info->is_key_valid==1){
  		if($key_info->is_domain_registered==1){
			$ob_webinar_plugin->register_webinar_plugin(trim($_POST['key']),$key_info);
			$ob_webinar_plugin->show_webinar_messages('Your plugin has been activated');
			header('Location:'.get_bloginfo('url').'/wp-admin/admin.php?page=webinar-admin-view');
		}else {
			$error = 'This domain is not registered for the given key. <a href="http://www.easywebinarplugin.com/login" target="_blank"> Click here</a> to register this domain';
		}
	}else{
		$error = 'You have entered an invalid license key';
	}
}
?>
<link media="all" type="text/css" href="<?php echo bloginfo('url'); ?>/wp-content/plugins/webinar_plugin/webinar-view-control/css/dashboard.css" rel="stylesheet" />
<div class="main-container">
  <div class="outer-grey-top-round"></div>
  <form method="post" action="">
    <div class="main-greybox">
      <div class="paddingtop15">
        <div class="blue-round-top" style="margin-top:20px;">&nbsp;</div>
        <div class="registration_header clearfix"> <span class="left marginleft10">Plugin Activation</span> </div>
        <div class="light_blue_box2">
          <div class="form-container">
            <table width="100%" border="0">
              <tr>
                <td width="35%"><label>Enter your license key</label></td>
                <td><input type="text" name="key" class="borderblue textbox450"/></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="inner-grey-round-bottom"></div>
      </div>
      <div>
        <table width="100%">
          <tr>
            <td colspan="2" align="center"  ><input type="submit" name="sbmt_register_key" class="submit" value=""/></td>
          </tr>
        </table>
      </div>
    </div>
  </form>
  <div class="warning" id="warning" style="<?php if(empty($error)){ ?>display:none<?php } ?>"><?php echo $error; ?></div>
</div>
<div class="outer-grey-bottom-round"></div>
<p><a class='example8' href="#"></a></p>
<div style='display:none'>
  <div id='inline_example1' style='padding:10px; background:#fff;'>
    <p> <span style="margin-left:40px;">Please wait <img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/ajax-loader.gif" /></span> </p>
  </div>
</div>
</div>
<div class="clear"></div>
