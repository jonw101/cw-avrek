<?php
error_reporting(0);
$ob_webinar_plugin	=	new webinar_plugin();
$Plugin_register=$ob_webinar_plugin->redirect_if_not_registered();
$see_all_webinar_page = get_bloginfo('url');
if($Plugin_register!='Plugin_register'){
?>
<script type="text/javascript" src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/js/see-all-webinar.js"></script>
<link media="all" type="text/css" href="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/css/dashboard.css" rel="stylesheet">
<div class="wordpress_dashboard_header clearfix" style="background-color:#015885;">
  <div class="ewp_logo"> <img height="51" src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/ewp-logo.png" alt="Easywebinar" /></div>
</div>
<div class="dashboard_content_right" style="position: relative;">
  <div class="width534"></div>
  <div class="width534"></div>
  <div class="clear"></div>
  <div>
    <div class="dashboard_front_icons1">
      <ul>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=create-webinar">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/create-webinar1.png" width="48" height="50" alt="create-webinar" /></div>
                <p class="dashbord_icon_text">Create Webinar</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=dashboard">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/webinars1.png" width="48" height="50" alt="webinar" /></div>
                <p class="dashbord_icon_text">Webinars</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/?request_page=webinar-drafts">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/drafts1.png" width="48" height="50" alt="drafts" /></div>
                <p class="dashbord_icon_text">Drafts</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
        <li> <a  target="_blank" href="<?php echo EASYWEBINAR_WEBSITE_LINK; ?>/list-your-domain">
          <div class="dashbord_icon_block">
            <div class="dashboard_border_block">
              <div class="dashbord_icon_inner">
                <div class="dashbord_icon_image"><img src="<?php echo bloginfo('url')?>/wp-content/plugins/webinar_plugin/webinar-view-control/images/admin1.png" width="48" height="50" alt="admin" /></div>
                <p class="dashbord_icon_text">EWP Admin</p>
              </div>
            </div>
            <div class="shadow_block"></div>
          </div>
          </a> </li>
      </ul>
    </div>
    <table cellspacing="0" class="wp-list-table widefat fixed pages webinar-links" style="width:98%;">
      <thead>
        <tr>
          <th style="" class="manage-column column-cb check-column" colspan="5"></th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th style="" class="manage-column column-cb check-column" colspan="5"></th>
        </tr>
      </tfoot>
      <tbody id="the-list">
        <?php
						$ob_webinar_db_interaction	=	new webinar_db_interaction();
						$all_webinar				=	$ob_webinar_db_interaction->get_all_webinar_info();
						
						if(count($all_webinar)>0) {
						$count = 0;
						foreach($all_webinar as  $webinars){
						$webinar_pages[$webinars->webinar_id_pk]	=	$ob_webinar_db_interaction->get_webinar_all_pages_links($webinars->webinar_id_pk);
						}
						
						foreach($all_webinar as  $webinars){ 
						$count++;
						?>
            <tr <?php if($count%2==0) echo 'class="alternate"' ?> <?php echo ' id="'.$webinars->webinar_id_pk.'"'; ?>>
          	<td colspan="5" class="colspanchange">
						<?php 
						if(!empty($webinars->modified_date)){echo '<span style="float:right;margin-right:4px">Modified on: '.date('d M, Y',strtotime($webinars->modified_date)).'</span>';}else{ echo '<span style="float:right;margin-right:4px">Created on: '.date('d M, Y',strtotime($webinars->created_date)).'</span>'; }
						if(empty($webinars->webinar_event_name))echo "<span>No title</span>"; else echo "<span>Name : ".stripslashes(html_entity_decode($webinars->webinar_event_name,ENT_QUOTES))."</span>";		
						if(!empty($webinars->webinar_main_topic))echo "<p>".stripslashes(html_entity_decode($webinars->webinar_main_topic,ENT_QUOTES))."</p>";
						?>
            <p> <a href="<?php echo $webinar_pages[$webinars->webinar_id_pk]['registration_page']; ?>" class='load-customize' target='_blank'>Registration Page</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo $webinar_pages[$webinars->webinar_id_pk]['thankyou_page']; ?>" class='load-customize' target='_blank'>Thankyou Page</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo $webinar_pages[$webinars->webinar_id_pk]['video_page']; ?>" class="load-customize" target='_blank'/>Event Page</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo $webinar_pages[$webinars->webinar_id_pk]['after_webinar_page'];?>" class="load-customize" target='_blank'/>Replay Page</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo $webinar_pages[$webinars->webinar_id_pk]['live_page'];?>" class="load-customize" target='_blank'/>Public Event Page</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo add_query_arg('countdown','',$webinar_pages[$webinars->webinar_id_pk]['live_page']);?>" class="load-customize" target='_blank'/>Public Event With Countdown</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="javascript:void(0)" webinar="<?php echo $webinars->webinar_id_pk; ?>" class="set_homepage" >Set registration page as homepage</a> &nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0)" class="delete_webinar" webinar="<?php echo $webinars->webinar_id_pk ?>" >Delete Webinar</a> &nbsp;&nbsp;</p>
            </td>
            </tr>
          <?php				
						}
						}else{ ?>
          	<tr><td colspan="5" class="colspanchange"><p>No webinar(s) yet. Use <b> create webinar </b> tab to create one.</p></td></tr>
          <?php } ?>
      </tbody>
    </table>
  </div>
</div>
<?php
}
?>
