jQuery(document).ready(function($) {
    
    jQuery(".delete_webinar").click( function() {
       if (confirm("It can't be undone.!")) {
   			var webinar_id 	 = parseInt(jQuery(this).attr('webinar'));
			var getPostsData = {
				action: 'delete_webinar',
				webinar_to_delete: webinar_id
			}
			
			jQuery.post(ajaxurl, getPostsData, function(response) {
				if(response==1){
					jQuery("tr#"+webinar_id).hide("slow");
				}else{
					alert("Unable to delete");
				}
			});
		}
	})
	
	jQuery(".set_homepage").click( function() {
       if (confirm("It will set webinar registration page as your home page.!")) {
   			var webinar_id 	 = parseInt(jQuery(this).attr('webinar'));
			var getPostsData = {action: 'webinar_as_homepage', webinar_to_set_as_homepage: webinar_id}
			
			jQuery.post(ajaxurl, getPostsData, function(response) {
				if(response==1){
					jQuery("table.webinar-links tr#"+webinar_id).addClass('its_done');
					jQuery("table.webinar-links tr#"+webinar_id).delay(2000).queue(function(){jQuery("table.webinar-links tr#"+webinar_id).removeClass('its_done')});
					alert("Done");
				}else{
					alert("Already set");
				}
			});
		}
	})
	
	jQuery(".generate_report").click( function() {
      var webinar_id 	 = parseInt(jQuery(this).attr('webinar'));
			var getPostsData = {action: 'webinar_report', webinar_to_gen_report: webinar_id}
			
			jQuery.post(ajaxurl, getPostsData, function(response) {
				
			});
	})
	   
});
