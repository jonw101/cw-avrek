<?php
/**
 * Template Name: Webinar Live Video
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
if(!session_id())session_start();
error_reporting(0);
define('DONOTCACHEPAGE',true);
include('webinar-config.php');
date_default_timezone_set("GMT");
global $post;
global $wpdb;
    ?>
   
    <script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/js/jquery.js'?>"></script>
    <script type="text/javascript">	    
    </script>
        <script>
	 var	plugin_url				=	"<?php echo WEBINAR_PLUGIN_URL?>"; 
	 var    webinar_id              =   "<?php echo $post->webinar_id;?>";
	  function pad(number){
		   if(number>=0){
			 return (parseInt(number,10) < 10 ? '0' : '') + number;  
		   }else{
			 return (parseInt(Math.abs(number),10) < 10 ? '0' : '') + Math.abs(number);
		   }
	   }
	  function local_time_clock(){
		   	
			var local_date = new Date();
			var hours = local_date.getHours();
			var minutes = local_date.getMinutes();
			
			var suffix = "AM";
			if (hours >= 12) {
				  suffix = "PM";
				  hours = hours - 12;
			}
			if (hours == 0) {
			  	hours = 12;
			}
			if (minutes < 10){
				minutes = "0" + minutes;
			}

			$("#local_time").html( hours + ":" + minutes + " " + suffix);
	   }
		
		  function set_local_timezone_for_webinar(){
			local_time_clock();
			var local_date = new Date();
			var local_timezone_offset = local_date.getTimezoneOffset();
		
			local_timezone_offset = parseFloat((-1)*(parseInt(local_timezone_offset,10)/60)).toFixed(2);
			
			var timezone_array = local_timezone_offset.split('.');
			var local_timezone = pad(timezone_array[0])+':'+pad(parseInt((timezone_array[1]*60)/100,10))+':'+"00";
			var local_timezone_operation = '';
			
			if(parseInt(timezone_array[0],10)>=0){
				local_timezone_operation = '+';
			}else{
				local_timezone_operation = '-';
			}
			
			var localtime = local_date.toTimeString();
			
			localtimezone = localtime.substring(localtime.indexOf(' '));
		
			localtimezone =	localtimezone.split("(").join("").split(")").join("");
			
			$("#timezone_string").val(localtimezone);
			$("#timezone_gmt").val(local_timezone);	
			$("#your_timezone").val(local_timezone);
			$("#timezone_gmt_operation").val(local_timezone_operation);
			$("#your_timezone").attr('timezone_operation',local_timezone_operation);
			
			
		}
		function time_to_timestamp(time){
		var time_array=	time.split(":");	
		var	hours			=	time_array[0]*3600;
		var minutes		=	time_array[1]*60;
		var seconds		=	time_array[2];
		return (parseInt(hours)+parseInt(minutes)+parseInt(seconds))*1000;
	}		
	    function convert_date(getdate){
		var date_array=	getdate.split("-");			
		return date_array[1]+"/"+date_array[2]+"/"+date_array[0];
	}	
	function toTimestamp(strDate){
		return Date.parse(strDate);
	}
	function check_integer(string){
		if(string<10){
			string	=	"0"+string;	
		}
		return string; 
	}
		function get_user_time_in_gmt() {
		    
			set_local_timezone_for_webinar();
			            
						var d = new Date();
                        var a = d.toTimeString();
					    var user_time_array  = a.split(" ");
						
						var user_time  =  user_time_array[0];  
						
						var umonth      = d.getMonth();
						if(month==12) var m_name	=	1;	
						else var m_name	=	parseInt(umonth)+1;
						var udate       = d.getDate();
						var uyear       = d.getFullYear();	
						
						var user_date  =  uyear+"-"+m_name+"-"+udate; 
						
		                var timezone_gmt_operation	=	$("#your_timezone").attr("timezone_operation")+"1";
						
						$("#timezone_gmt").val($("#your_timezone").val());	
						$("#timezone_gmt_operation").val(timezone_gmt_operation);
						
						//var webinar_selected_session=	jQuery.trim($("#select_webinar_session option:selected").attr("wb_date"));
						var	timezone_gmt_diffrence	=	jQuery.trim($("#timezone_gmt").val());
						var	timezone_operation			=	parseInt(jQuery.trim($("#timezone_gmt_operation").val()))+1;
						var	timezone_gmt_timestamp	=	time_to_timestamp(timezone_gmt_diffrence);
						
						//var webinar_date			=	$('#webinar_dates :selected').attr('value');
						var converted_date			=	convert_date(user_date);
						
						var date_timestamp			=	toTimestamp(''+converted_date+' '+user_time);
						if(timezone_operation>0){
							var total_timestamp		=	parseInt(date_timestamp)-parseInt(timezone_gmt_timestamp);	
						}else{
							var total_timestamp		=	parseInt(date_timestamp)+parseInt(timezone_gmt_timestamp);	
						}
						
						var timezone = timezone_gmt_diffrence.split(':');
						
						var timezone_hrs = parseInt(timezone[0]==24 ? 0 : timezone[0],10);
						var timezone_min = parseInt(timezone[0]!=24 ? timezone[1] : 0,10);
			
						var timezone_hrs = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_hrs));
						var timezone_min = parseInt($('#your_timezone').attr('timezone_operation')+(timezone_min));
						
						var webinar_date = user_date.split('-');
						var webinar_time = user_time.split(':');
						var new_date 		 = new Date(parseInt(webinar_date[0],10),parseInt(webinar_date[1],10)-1,parseInt(webinar_date[2],10),(parseInt(webinar_time[0],10)==24 ? 0 : parseInt(webinar_time[0],10)),parseInt(webinar_time[1],10),0,0);
						var new_date 		 = new_date.valueOf();
						var ms_in_hour 	 = 60000*60;
						var ms_in_min    = 60000;
						var ms_to_add_or_deduct = -((timezone_hrs*ms_in_hour)+(timezone_min*ms_in_min));
			
						new_date 	= new_date + ms_to_add_or_deduct;
						
						var date		=	new Date(new_date);
						var hours 	= check_integer(date.getHours());
						var minutes = check_integer(date.getMinutes());
						var seconds = check_integer(date.getSeconds());
						var day		  = date.getDate();		
						var month	  = date.getMonth();
			
						if(month==12) var month_name	=	1;	
						else var month_name	=	parseInt(month)+1;	
						
						month_name		=	check_integer(month_name);	
						var year			=	date.getFullYear();
						var final_date=	year+"-"+month_name+"-"+day;
			            
						
						var formattedTime = hours + ':' + minutes + ':' + seconds;
						
						
						
						var x = [ formattedTime, final_date];
						return x;
						
		   
		}
		
	jQuery(document).ready(function(){
	
		set_local_timezone_for_webinar();
		var user_time_gmt    =    get_user_time_in_gmt()[0];
		var user_date_gmt    =    get_user_time_in_gmt()[1];
		var timezone_difference	=	$("#your_timezone").val();
		var timezone_operation	=	$("#your_timezone").attr("timezone_operation");
    jQuery.ajax({
		url		:	plugin_url+"webinar-db-interaction/remove_webinar_session.php",		
		type	:	"GET",	
		data	:	"webinar_id="+webinar_id+"&timezone_difference="+timezone_difference+"&timezone_operation="+timezone_operation+"&user_time_gmt="+user_time_gmt+"&user_date_gmt="+user_date_gmt+"&is_flag="+1,
	    async	:	false,
		crossDomain : true,
		dataType: 	"jsonp",
		success	:	function(response){
			
			        
			}
	    });
	});
		</script>
      
    <?php
    $ob_webinar_db_interaction	 =	new webinar_db_interaction();
	$ob_webinar_functions	 =	new webinar_functions();
	
	$purge_cache			 =	$ob_webinar_functions->purge_cache();

	$webinar_detail			 =	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);
	
	$webinar_pages		     =	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);
	
	if($webinar_detail[0]->webinar_timezone_id_fk > 0){
	$webinar_timezone_detail = $ob_webinar_db_interaction->get_timezone_detail($webinar_detail[0]->webinar_timezone_id_fk);
	$webinar_timezone_name   = stripslashes($webinar_timezone_detail[0]->timezone_name);
	}
	$dates_webinar           = json_decode($_SESSION['dates_array_live']);
    $user_date_time_gmt      = json_decode($_SESSION['user_date_time_gmt']);
    $user_date_time          = json_decode($_SESSION['user_date_time']);
	$session_webinar         = $ob_webinar_db_interaction->get_distinct_webinar_sessions($post->webinar_id);  
	
	$video_length			 =	$webinar_detail[0]->webinar_video_length;	
	$is_allowed_end_sessions =  (int)$webinar_detail[0]->allow_unexpired_end_sessions;
	$webinar_endpoint 		 =  ($is_allowed_end_sessions==1) ? $video_length : 0;
	$flag =0;
	foreach($dates_webinar as $dw){
		 foreach($session_webinar as $sw){
		
		  $webinar_date_time = $dw." ".$sw->start_time;
		  $dateinsec         = strtotime($webinar_date_time);
		  $newdate           = $dateinsec+$webinar_endpoint;
		  $date_in_format    = date('Y-m-d H:i:s',$newdate);
		  if($webinar_detail[0]->webinar_timezone_id_fk > 0) {
			  
			  $webinar_date_time_in_gmt = $ob_webinar_db_interaction->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
			  if(strtotime($webinar_date_time_in_gmt) > strtotime($user_date_time_gmt))
			  { 
				   $scheduled_webinar_date_time = $ob_webinar_db_interaction->convert_time_zone($webinar_date_time,$webinar_timezone_name,"GMT");
				   $flag = 1;
				   break;
			  }
		  }
		  else{
			    if(strtotime($date_in_format) > strtotime($user_date_time))
			  { 
			       
				   if(!empty($_GET['gdt']) && !empty($_GET['gtm']))
				   {
					 
                      if($ob_webinar_functions->is_valid_date($_GET['gdt'])&& $ob_webinar_functions->is_valid_time($_GET['gtm']))
				      {
					  $scheduled_webinar_date_time = $_GET['gdt']." ".$_GET['gtm'];
                      }
					   else{
							  if($ob_webinar_functions->is_valid_time($_GET['gtm']))
							   {
								  $scheduled_webinar_date_time = date('Y-m-d')." ".$_GET['gtm'];	
							   }
					       }
				   }
				   
				   $flag = 1;
				   break;
			  }
		  }
		}
		if($flag == 1)
		break;
	} 
  	
	$current_seconds	=	time();	
	if($webinar_detail[0]->webinar_timezone_id_fk > 0) {
		
				if(!empty($_GET['gdt']) && !empty($_GET['gtm'])){
					 
                  if($ob_webinar_functions->is_valid_date($_GET['gdt'])&& $ob_webinar_functions->is_valid_time($_GET['gtm']))
				      {
					  $scheduled_date_time = $_GET['gdt']." ".$_GET['gtm'];
					  $seeking_point		=	$current_seconds-strtotime($scheduled_date_time);
                      }
					  else{
							  if($ob_webinar_functions->is_valid_time($_GET['gtm']))
							   {
								  $scheduled_date_time = date('Y-m-d')." ".$_GET['gtm'];	
								  $seeking_point	   = time()-strtotime($scheduled_date_time);
							   }
							   else{
								$seeking_point = 0;
							   }
					  }
				 }else{
						if(!empty($_COOKIE['_ep_'.$post->webinar_id]))
						{
							  $scheduled_date_time = $_COOKIE['_ep_'.$post->webinar_id];
							  $scheduled_dt_array  =  explode(' ',$scheduled_date_time);
				  
							  if($ob_webinar_functions->is_valid_date($scheduled_dt_array[0]) && $ob_webinar_functions->is_valid_time($scheduled_dt_array[1])){
								  if(((strtotime($scheduled_date_time)+$video_length)-strtotime($scheduled_date_time))>0){
									  $seeking_point	 	 = time()-strtotime($scheduled_date_time);
								  }
							  }else{
								  $seeking_point = 0;
							  }
						 }else{
							$seeking_point = 0;
							unset($_COOKIE['_ep_'.$post->webinar_id]);
						     }
					    }
			
		
	}
	else{
		if(empty($_GET['gdt']) && empty($_GET['gtm'])){
						if(!empty($_COOKIE['_ep_'.$post->webinar_id]))
						{
							  $scheduled_date_time = $_COOKIE['_ep_'.$post->webinar_id];
							  $scheduled_dt_array  =  explode(' ',$scheduled_date_time);
				  
							  if($ob_webinar_functions->is_valid_date($scheduled_dt_array[0]) && $ob_webinar_functions->is_valid_time($scheduled_dt_array[1])){
								  if(((strtotime($scheduled_date_time)+$video_length)-strtotime($scheduled_date_time))>0){
									  $seeking_point	 	 = time()-strtotime($scheduled_date_time);
								  }
							  }else{
								  $seeking_point = 0;
							  }
						 }else{
							$seeking_point = 0;
							unset($_COOKIE['_ep_'.$post->webinar_id]);
						     }
			}
			else{
	              $seeking_point		=	$current_seconds-strtotime($scheduled_webinar_date_time);
			    }
	}
	
	$timezones_detail		 =	$ob_webinar_db_interaction->get_timezones_detail();
					 
	$video_url	             =	trim($webinar_detail[0]->webinar_video_url);		
	
	$service_type			 =	$webinar_detail[0]->video_type;	
	$video_format 			 =	pathinfo($video_url,PATHINFO_EXTENSION);

	$current_time					=	date("H:i:s");		
	$current_time_seconds	=	$ob_webinar_functions->time_to_sec($current_time);

	$video_stream					=	"";	
	
	$alternate_video_url		= trim($webinar_detail[0]->webinar_alternate_video_url);		
	$alternate_video_format	=	$ob_webinar_functions->get_video_type($alternate_video_url);
	
	if($service_type=="stream"){
		$rtmp_url					=	"rtmp:";
		$rtmp_url_array		=	explode("/",$video_url);
		$video_stream_node=	count($rtmp_url_array)-1;
		$video_stream			=	$rtmp_url_array[$video_stream_node];
		for($i=1;$i<$video_stream_node;$i++){
			$rtmp_url.="/".$rtmp_url_array[$i];	
		}
		$video_url				=	$rtmp_url;		
	}
	
	$device_type 			= 	$ob_webinar_functions->get_device_type();
	$iPod 						= 	$device_type['iPod'];
	$iPhone 					= 	$device_type['iPhone'];
	$iPad 						= 	$device_type['iPad'];
	$Android 					=   $device_type['Android'];
	$AndroidTablet  	=		$device_type['AndroidTablet'];
	
	
	if($seeking_point<0) $seeking_point =	0;
	
	if(($service_type=='youtube' || $service_type=='stream' || $service_type=='vimeo') && ($webinar_detail[0]->is_livestream==0 || $webinar_detail[0]->is_livestream==1)){
		if(($iPod || $iPhone || $iPad || $Android || $AndroidTablet) && $service_type=='stream'){
			$seek_to			=		0;	
		}else{
			$seek_to 			= 	$seeking_point;		
		}
	}else{
		$seek_to				=		0;
	}
	
	$webinar_event_template_options  =  $ob_webinar_db_interaction->get_event_template_data($post->webinar_id);
	include_once(WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/'.'webinar-live.php');
?>