<?php
/**
 * Template Name: Webinar Thank You
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
include('webinar-config.php');
global $post;
global $wpdb;
	
	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions		=	new webinar_functions();
	
	$purge_cache			=	$ob_webinar_functions->purge_cache();
		
 	$webinar_detail		=	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);
	$sharing_detail		=	$ob_webinar_db_interaction->get_sharing_detail($post->webinar_id);
	$webinar_pages		=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);
	
	$webinar_login_id	=	$webinar_pages[1]->ID;
	$reg_id						=	$ob_webinar_db_interaction->get_webinar_registration_id($post->webinar_id);
	$webinar_id 			=	$post->webinar_id;
	$_session_user_id	=	$_SESSION['user_id'];
	
	$webinar_thank_template_options = $ob_webinar_db_interaction->get_thankyou_template_data($post->webinar_id);
		
	$thankyou_attachment_path		=	trim($webinar_detail[0]->thankyou_attachment_path);
	$thankyou_attachment_format	=	pathinfo($thankyou_attachment_path, PATHINFO_EXTENSION);
	
	$youtube_pattern = '/youtube.com/';
	$rtmp_pattern 	 = '/rtmp:\/\//';
	$vimeo_pattern 	 = '/vimeo.com/';
	
	$stream 				 = "";
	$service 				 = "";
	
	$stream_sharing  = "";
	$service_sharing = "";
   $thankyou_attachment_embed_script =	stripslashes($webinar_detail[0]->thankyou_attachment_embed_script);
	$alternate_video_url   =    trim($webinar_detail[0]->thank_alternate_video);		
	$alternate_video_format=	$ob_webinar_functions->get_video_type($alternate_video_url);

	if(isset($thankyou_attachment_path) && trim($thankyou_attachment_path)!=""){
		if(preg_match($youtube_pattern, $thankyou_attachment_path)){
			$service 	= "youtube";
		}elseif(preg_match($rtmp_pattern, $thankyou_attachment_path)){
			$service 	=	"stream";
			$thankyou_attachment_path = explode('/', $thankyou_attachment_path);
			$stream 	= $thankyou_attachment_path[count($thankyou_attachment_path)-1];
			unset($thankyou_attachment_path[count($thankyou_attachment_path)-1]);
			$thankyou_attachment_path = implode('/', $thankyou_attachment_path);
		}elseif(preg_match($vimeo_pattern, $thankyou_attachment_path)){
			$service  = "vimeo";
		}else{
			$service  = "other";
		}
	}
	
	$sharing_video		  =	trim($sharing_detail[0]->video_attachment_path);
	$sharing_video_format =	pathinfo($sharing_video, PATHINFO_EXTENSION);
	
	if(isset($sharing_video) && trim($sharing_video)!=""){
		if(preg_match($youtube_pattern, $sharing_video)){
			$service_sharing = "youtube";
		}elseif(preg_match($rtmp_pattern, $sharing_video)){
			$service_sharing =	"stream";
			$sharing_video   = explode('/', $sharing_video);
			$stream_sharing  = $sharing_video[count($sharing_video)-1];
			unset($sharing_video[count($sharing_video)-1]);
			$sharing_video   = implode('/', $sharing_video);
		}elseif(preg_match($vimeo_pattern, $sharing_video)){
			$service_sharing = "vimeo";
		}else{
			$service_sharing = "other";
		}
	}
		
	$device_type	= 	$ob_webinar_functions->get_device_type();
	$iPod 				= 	$device_type['iPod'];
	$iPhone 			= 	$device_type['iPhone'];
	$iPad 				= 	$device_type['iPad'];
	$Android 			=   $device_type['Android'];
	$AndroidTablet=	$device_type['AndroidTablet'];
		
	global $wpdb;
	
	if(isset($_session_user_id)){
	$result = $wpdb->get_results($wpdb->prepare("SELECT webinar_schedule_id_fk,webinar_real_date,webinar_registered_users.webinar_real_time,webinar_registered_users.attendee_name,webinar_registered_users.attendee_email_id,webinar_registered_users.key,webinar_registered_users.webinar_date,webinar_registered_users.webinar_time,webinar_registered_users.webinar_end_date,webinar_registered_users.webinar_end_time FROM webinar_registered_users LEFT JOIN webinar_schedule ON webinar_registered_users.webinar_schedule_id_fk= webinar_schedule.webinar_schedule_id_pk WHERE registration_id_pk =%d AND webinar_registered_users.webinar_id_fk=%d",$_session_user_id,$webinar_id));
	}
	
	if(isset($result[0]->key)){
	  $event_start_date = str_replace("-","",$result[0]->webinar_date);
	  $event_start_time = str_replace(":","",$result[0]->webinar_time);
	  $event_end_date   = str_replace("-","",$result[0]->webinar_end_date);
	  $event_end_time   = str_replace(":","",$result[0]->webinar_end_time);
	  $webinar_link	    = add_query_arg('key',$result[0]->key,get_permalink($webinar_login_id));
	  $gcal_url 	  		= "https://www.google.com/calendar/event?action=TEMPLATE&text=".$webinar_detail[0]->webinar_main_topic."&dates=".$event_start_date."T".$event_start_time."Z/".$event_end_date."T".$event_end_time."Z&details=".$webinar_detail[0]->webinar_main_topic."&location=".$webinar_link."&trp=true&sprop=".$webinar_detail[0]->presenters_name."&sprop=name:".$webinar_link."";
	  $ical_content		= 
"BEGIN:VCALENDAR
BEGIN:VEVENT
DTSTART:".$event_start_date."T".$event_start_time."Z
DTEND:".$event_end_date."T".$event_end_time."Z
SUMMARY:".$webinar_detail[0]->webinar_main_topic."
DESCRIPTION:".$webinar_detail[0]->webinar_main_topic."
END:VEVENT
END:VCALENDAR";
	}
	include_once(WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/'.$current_template);
?>