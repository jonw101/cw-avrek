<?php
/**
 * Template Name: Webinar Video
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
//error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
include('webinar-config.php');
date_default_timezone_set('GMT');

global $current_user;
global $post;

	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions				=	new webinar_functions();
 	
	$purge_cache								= 	$ob_webinar_functions->purge_cache();
	
	$attendee_webinar_session_detail	=	$ob_webinar_db_interaction->get_registration_details($_GET['attendee']);
	
	$webinar_pages							=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);
	$error_page_id							=	$ob_webinar_db_interaction->get_error_page_id();
	$error_page									=	get_permalink($error_page_id);
	
	if(empty($attendee_webinar_session_detail)){
		if(!current_user_can('administrator')){
			header("Location:".get_permalink($webinar_pages[0]->ID));
			exit;
		}
	}else{
		$_session_schedule_id		=	$attendee_webinar_session_detail[0]->webinar_schedule_id_fk;
		$_session_date					=	$attendee_webinar_session_detail[0]->webinar_date;
		$_session_attendee_id 	=	$attendee_webinar_session_detail[0]->registration_id_pk;
		
		if($_session_schedule_id==0){
			if($ob_webinar_functions->is_valid_time(base64_decode($_GET['start']))){
				$_session_start_time= 	base64_decode($_GET['start']);
			}else{
				header("Location:".get_permalink($webinar_pages[0]->ID));
				exit;
			}
		}
		
	}
	
	if(!isset($_session_start_time)){
		$scheduled_date_time=	$attendee_webinar_session_detail[0]->webinar_date." ".$attendee_webinar_session_detail[0]->webinar_time;
	}else{
		$scheduled_date_time=	date('Y-m-d')." ".$_session_start_time;
	}
	
	$webinar_detail				=	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);

	$video_length					=	$webinar_detail[0]->webinar_video_length;
	
	$webinar_start_time		=	strtotime($scheduled_date_time);
	$webinar_end_time			=	strtotime($scheduled_date_time)+$video_length;
		
	$current_time					=	date("H:i:s");		
	$current_time_seconds	=	$ob_webinar_functions->time_to_sec($current_time);

	$current_seconds			=	time();	
	$seeking_point				=	$current_seconds-strtotime($scheduled_date_time);

	if($seeking_point<0)$seeking_point  =	0;
	
	if(isset($_session_start_time)){
		if(($webinar_end_time-$webinar_start_time)>0){
			$seeking_point		=	time()-strtotime($scheduled_date_time);
		}
		else{
			$_SESSION["error"]=	VIDEO_ENDS;
			header("Location:$error_page");
			exit;
		}
	}
	
	if($current_seconds>$webinar_end_time){
		$today = date('Y-m-d');
		$flag_check_webinar_end_date = $ob_webinar_db_interaction->check_webinar_validity_for_end_date($today,$webinar_detail[0]->webinar_end_date);
	
		if(isset($_session_start_time)){
			$_SESSION["not_found"]	 =	VIDEO_ENDS;
			header("Location:$error_page");
			exit;
		}else if($flag_check_webinar_end_date==true){
			$replay_page			=	get_permalink($webinar_pages[3]->ID);
			$replay_link			=	add_query_arg(array('attendee'=>$attendee_webinar_session_detail[0]->key),$replay_page);
			$_SESSION["not_found"]	=	VIDEO_ENDS;
			header("Location:$error_page");
			exit;
		}else{
			$_SESSION["not_found"] =	WEBINAR_EXPIRE;
			header("Location:$error_page");
			exit;
		}
	}elseif($current_seconds<$webinar_start_time){
			$countdown_page_id				 =	$ob_webinar_db_interaction->get_webinar_template_page_id("countdown");	
			$countdown_page_link			 =	get_permalink($countdown_page_id);
			$redirect_to_countdown_page=  add_query_arg(array('attendee'=>$attendee_webinar_session_detail[0]->key),$countdown_page_link);
			header("location:$redirect_to_countdown_page");
			exit;
	}
		 
	if(isset($_session_attendee_id)){
			$ob_webinar_db_interaction->set_attendee_event_activity_status($_session_attendee_id,1); 	 
	}
	
	$default_attendee_list	=	array();	
	$default_attendee_ids		=	array();	
	$attendee_operation			=	0;

	$attendee_name					=	$attendee_webinar_session_detail[0]->attendee_name;
	$attendee_email					=	$attendee_webinar_session_detail[0]->attendee_email_id;
	
	$emarketing_settings		= maybe_unserialize(get_option('ewp_emarketing_settings_'.$post->webinar_id));
	$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
	
	
	$infusionsoft_enabled		= (int)$emarketing_settings['infusionsoft']['enabled'];
	if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
						$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
					    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
						}
	if($infusionsoft_enabled){
			$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
	        $authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
						if($authorized_credentials)
						{
							update_option('ewp_emarketing_status',0);
							$iui		 =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendee_name,'Email'=>$attendee_email));
			                $obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['came']);
			                $obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['left_early']);
			                $obj_eII->grpRemoveFromUser($iui,$emarketing_settings['infusionsoft']['tags']['didnt_came']);
						}
	else{
							 $emarket_status = get_option('ewp_emarketing_status');
					        if($emarket_status != 1){
							update_option('ewp_emarketing_status',1);
							$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/change_infusionsoft_status.php'; 
				  	        $response_message_array['error_message'] = 0;
				  	        $response_message_array['message'] = 0;
				  	        $response_message_array['domain'] = site_url();
				  	        $ch = curl_init ($website_file_url);
			               curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
						   curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
						   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch,CURLOPT_REFERER,site_url());
						   curl_setopt($ch,CURLOPT_POST,true );
						   curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
						   $infusionsoft_reponse_error = curl_exec($ch);
						   curl_close($ch); 
						}
			
	}
	}
	if(empty($attendee_webinar_session_detail)){
		if(current_user_can('administrator')){
			$seeking_point=0;
			$attendee_name			=	"Webinar Administrator";
			$current_time_seconds= 0;
		}
	}
	
	$attendees_list_enabled	=	$webinar_detail[0]->attendees_list_enabled;
	$admin_email						=	$ob_webinar_functions->_validate_email($webinar_detail[0]->chatbox_code) ? $webinar_detail[0]->chatbox_code : $webinar_detail[0]->admin_email_value;
    
	$currentattendee = $ob_webinar_db_interaction->get_current_attendee_email($_GET['attendee']);
	
	$attendee_list					=	$ob_webinar_db_interaction->get_default_attendee_webinar_list($post->webinar_id);
	$max_number_of_attendees=	$webinar_detail[0]->max_number_of_attendees;
	$total_attedees					=	count($attendee_list);	
	

	if($total_attedees>3){
		$attendee_operation				=	1;
		if($max_number_of_attendees>count($attendee_list) or $max_number_of_attendees==0 )
			$get_half_attendees			=	(int)(count($attendee_list)/2);
		else	
			$get_half_attendees			=	(int)($max_number_of_attendees/2);	
		for($j=0;$j<$get_half_attendees;$j++){
			$default_attendee_list[]=	$attendee_list[$j]->attendee_name;			
			$default_attendee_ids[]	=	$attendee_list[$j]->webinar_attendee_id_pk;			
		}	
	}else{
		foreach($attendee_list as $attendee){
			$default_attendee_list[]=	$attendee->attendee_name;				
			$default_attendee_ids[]	=	$attendee->webinar_attendee_id_pk;			
		}	
	}

	$attenees_ids					=	implode(",",$default_attendee_ids);
	$logged_in_attendees	=	$ob_webinar_db_interaction->get_logged_in_attendees_for_webinar($post->webinar_id,$_session_schedule_id,$_session_date);
	$attendee_ids					=	$ob_webinar_functions->get_attendee_ids_from_array($logged_in_attendees);
 	$is_right_now_enable	=	$ob_webinar_db_interaction->check_webinar_right_now_enable($webinar_detail[0]->is_right_now_enable,$webinar_detail[0]->webinar_start_date);
	
	$service_type					=	$webinar_detail[0]->video_type;	
	$video_url						=	trim($webinar_detail[0]->webinar_video_url);		
	$video_format 				=	pathinfo($video_url, PATHINFO_EXTENSION);
	$video_stream					=	"";	
	$alternate_video_url	= trim($webinar_detail[0]->webinar_alternate_video_url);		
	$alternate_video_format	=	$ob_webinar_functions->get_video_type($alternate_video_url);

	if($service_type=="stream"){
		$rtmp_url					=	"rtmp:";
		$rtmp_url_array		=	explode("/",$video_url);
		$video_stream_node=	count($rtmp_url_array)-1;
		$video_stream			=	$rtmp_url_array[$video_stream_node];
		for($i=1;$i<$video_stream_node;$i++){
			$rtmp_url.="/".$rtmp_url_array[$i];	
		}
		$video_url				=	$rtmp_url;		
	}
	
	$device_type 			= 	$ob_webinar_functions->get_device_type();
	$iPod 						= 	$device_type['iPod'];
	$iPhone 					= 	$device_type['iPhone'];
	$iPad 						= 	$device_type['iPad'];
	$Android 					=   $device_type['Android'];
	$AndroidTablet  	=		$device_type['AndroidTablet'];
	
	if(($service_type=='youtube' || $service_type=='stream' || $service_type=='vimeo') && ($webinar_detail[0]->is_livestream==0 || $webinar_detail[0]->is_livestream==1) ){
		if(($iPod || $iPhone || $iPad || $Android || $AndroidTablet) && $service_type=='stream'){
			$seek_to			=		0;	
		}else{
			$seek_to 			= 	$seeking_point;		
		}
	}else{
		$seek_to				=		0;
	}
	
	$webinar_delayed_events	=	$ob_webinar_db_interaction->get_webinar_delayed_events($post->webinar_id);
	
	$webinar_scarcity				=	$ob_webinar_db_interaction->get_webinar_scarcity($post->webinar_id);
	
	$webinar_event_template_options = $ob_webinar_db_interaction->get_event_template_data($post->webinar_id);
	
	include_once(WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/'.$current_template);
?>