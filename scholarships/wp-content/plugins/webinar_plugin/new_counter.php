<div class="rainbow-border">
    	<img src="<?php echo WEBINAR_PLUGIN_URL; ?>/templates/<?php echo $selected_template_dir; ?>/images/Rainbow-Border.png" width="100%" />
    </div>
	<div class="top-text">
        <div class="wrapper"> 
            <div class="cong-text"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_headline);?>
                <span class="cong-sub-text"><?php echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->counter_subheadline);?></span>
            </div>
        </div>
    </div>
    
    <div class="wrapper">
        <div class="countdown-timer">
            
            <div id="countdown_dashboard" class="countdown">
                <div class="circle">
                    <canvas id="days" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-days digit">0</span>
                        <span class="ce-label ce-days-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="hours" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-hours digit">0</span>
                        <span class="ce-label ce-hours-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="minutes" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-minutes digit">0</span>
                        <span class="ce-label ce-minutes-label digit">0</span>
                    </div>
                </div>
                <div class="circle">
                    <canvas id="seconds" width="408" height="408"></canvas>
                    <div class="circle__values">
                        <span class="ce-digit ce-seconds digit">0</span>
                        <span class="ce-label ce-seconds-label digit">0</span>
                    </div>
                </div>
            </div>
            
        </div>    
    </div>
    
   
    <div class="footer">
     <?php if($webinar_detail[0]->presenters_box==1) { ?>
     <div class="wrapper">
       <?php if(!empty($webinar_detail[0]->presenters_thumbnail_path)){ ?>
    	<div class="footer-img">
    	<div class="host-image">
        	<img src="<?php echo $webinar_detail[0]->presenters_thumbnail_path; ?>" width="109" height="109" alt="img" />
        </div>
        </div>
        <?php } ?>
        <div class="host-text">Your Host</div>
        <div class="host-name"><?php if(!empty($webinar_detail[0]->presenters_name)) { echo $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->presenters_name); } ?></div>
        <div class="host-desc">
        	<?php echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->presenters_description); ?>
        </div>
        </div>
         <?php } ?>
    </div>
   
    <div class="copyright">
    	<div class="wrapper">
    		 <?php if(!empty($webinar_detail[0]->footer_links)&& $webinar_detail[0]->is_opted_for_affiliate_link==1) echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_links); elseif(!empty($webinar_detail[0]->footer_text)) { echo $ob_webinar_functions->_esc_decode_html($webinar_detail[0]->footer_text); } else  { echo POWERED_BY_SOFTOBIZ; }?>
        </div>
    </div>

<!-- Init Count Everest plugin -->

<script>
	$(document).ready(function() {
		var scheduled_time	=	<?php if(isset($desired_time_in_milliseconds)) echo $desired_time_in_milliseconds; else echo 0;?>;
		
		var date = new Date();

		date.setTime(date.getTime()+(scheduled_time));
		var pday 	=	date.getDate();
		var pyear 	=	date.getFullYear();
		var pmonth 	=	date.getMonth()+1;
		
		var phour 	=	date.getHours();
		var pminutes	=	date.getMinutes();
		$(".countdown").countEverest({
			//Set your target date here!
			day: pday,
			month: pmonth,
			year: pyear,
			hour:phour,
			minute:pminutes,
			leftHandZeros: false,
			onChange: function() {
				drawCircle(document.getElementById('days'), this.days, 365);
				drawCircle(document.getElementById('hours'), this.hours, 24);
				drawCircle(document.getElementById('minutes'), this.minutes, 60);
				drawCircle(document.getElementById('seconds'), this.seconds, 60);
			},
			onComplete: function() {
				var redirection_link = $("#webinar_video_link").val();
				if(!redirection_link==''){
				//window.location.href	=	redirection_link;
				window.location.replace(redirection_link);
				}
				}
			
		});

		function deg(v) {
			return (Math.PI/180) * v - (Math.PI/2);
		}

		function drawCircle(canvas, value, max) {
			var	circle = canvas.getContext('2d');
			
			circle.clearRect(0, 0, canvas.width, canvas.height);
			circle.lineWidth = 3;

			circle.beginPath();
			circle.arc(
					canvas.width / 2, 
					canvas.height / 2, 
					canvas.width / 2 - circle.lineWidth, 
					deg(0), 
					deg(360 / max * (max - value)), 
					false);
			circle.strokeStyle = '#ced1d6';
			circle.stroke();

			circle.beginPath();
			circle.arc(
					canvas.width / 2, 
					canvas.height / 2, 
					canvas.width / 2 - circle.lineWidth, 
					deg(0), 
					deg(360 / max * (max - value)), 
					true);
			
			/*Colors for Day Canvas Circle*/
			var day = document.getElementById('days').getContext('2d');
			day.strokeStyle = '#1da2ce';
			day.stroke();
			
			/*Colors for Hours Canvas Circle*/
			var day = document.getElementById('hours').getContext('2d');
			day.strokeStyle = '#68d387';
			day.stroke();
			
			/*Colors for Minutes Canvas Circle*/
			var day = document.getElementById('minutes').getContext('2d');
			day.strokeStyle = '#ec585f';
			day.stroke();
			
			/*Colors for Seconds Canvas Circle*/
			var day = document.getElementById('seconds').getContext('2d');
			day.strokeStyle = '#fcae2f';
			day.stroke();
			
		}
		
	});
</script>
