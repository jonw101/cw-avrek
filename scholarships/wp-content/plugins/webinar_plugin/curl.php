<?php
header('Content-Type: application/json');
require_once('../../../wp-load.php');
require_once('webinar-db-interaction/webinar-functions.php');
$ob_webinar_db_interaction = new webinar_db_interaction();
$ob_webinar_functions			 = new webinar_functions();
global $wpdb;
require_once('license.php');	
require_once('access-validation.php');

$storing_mode = $_POST['storing_mode'];
$data 					= $_POST['data'];
$query 					= json_decode(base64_decode($data));
$query_count		= 0;
$webinar_id 		= 0;
$response_array	=	array('response'=>1,'error'=>'');
if(isset($_POST['webinar_id'])){
	
$draft_webinar = (int) base64_decode($_POST['webinar_id']);
$record_webinar = $wpdb->get_results("SELECT storing_mode FROM webinar WHERE webinar_id_pk='$draft_webinar'");
$storing_opt = $record_webinar[0]->storing_mode;

}
$wpdb->query("begin;");
foreach($query as $value){
	
	if($query_count==0){
		$wpdb->query($value);
		$webinar_id =  $wpdb->insert_id;
		//$webinar_id = $wpdb->get_var($wpdb->prepare("SELECT webinar_id_pk FROM webinar ORDER BY webinar_id_pk DESC LIMIT 1"));
		if($wpdb->last_error) {
			$response_array['response']	= 0;
			$response_array['error']		= $wpdb->last_error.' <b>Query : '.$value.'</b>';
			$wpdb->query('rollback;');
			break;
		}
		
	}else {
		$value = str_replace("#webinar_id#",$webinar_id, $value);
		$value = str_replace("#button#",$button_id, $value);
		$wpdb->query($value);
		if ($wpdb->last_error) {
			$response_array['response']	= 0;
			$response_array['error']		= $wpdb->last_error.' <b>Query : '.$value.'</b>';
			$wpdb->query('rollback;');
			break;
		}
		$button_id =  $wpdb->insert_id;
	}
	$query_count++;
}
$wpdb->query("commit;");
update_option('sendgrid_status',$_POST['sendgrid_status']);
if($_POST['sendgrid_status'] == 0){
update_option("sendGrid_credentials",base64_decode($_POST['sendgrid_credentials']));
}
update_option('ewp_emarketing_status',$_POST['infusionsoft_status']);
update_option('infusionsoft_credentials',base64_decode($_POST['infusionsoft_credentials']));
if($storing_mode != 'draft'){
if((!empty($webinar_id) && (int)$webinar_id >0 ) || $storing_opt == 'draft' ) {
if($storing_opt == 'draft'){
	$webinar_id = $draft_webinar; 
	
}
	$ob_webinar_db_interaction->make_webinar_registration_page($webinar_id,$_POST['registration_page_permalink'],$storing_mode);
	$ob_webinar_db_interaction->make_webinar_login_page($webinar_id,$_POST['registration_page_permalink'],$storing_mode);
	$ob_webinar_db_interaction->make_webinar_thankyou_page($webinar_id,$_POST['thankyou_page_permalink'],$storing_mode);
	$ob_webinar_db_interaction->make_after_webinar_video_page($webinar_id,$_POST['event_replay_page_permalink'],$storing_mode);
	$ob_webinar_db_interaction->make_webinar_video_page($webinar_id,$_POST['event_page_permalink'],$storing_mode);
	$ob_webinar_db_interaction->make_webinar_live_page($webinar_id,$_POST['live_page_permalink'],$storing_mode);
	update_option('ewp_emarketing_settings_'.$webinar_id,base64_decode($_POST['emarketing_settings']));
	
}  elseif(($webinar_id==0) && isset($_POST['webinar_id'])){
	$webinar_id							=	(int) base64_decode($_POST['webinar_id']);
		
	if($webinar_id>0 ){ 
		$webinar_all_pages		=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($webinar_id);
		$registration_page_id	=	$webinar_all_pages[0]->ID;
		$login_page_id				=	$webinar_all_pages[1]->ID;
		$thankyou_page_id			=	$webinar_all_pages[2]->ID;
		$after_webinar_page_id=	$webinar_all_pages[3]->ID;
		$webinar_page_id			=	$webinar_all_pages[4]->ID;
		$webinar_live_page_id	=	$webinar_all_pages[5]->ID;
		
		$is_reg_page_updated 				=	$ob_webinar_db_interaction->update_webinar_registration_page($registration_page_id,$_POST['registration_page_permalink'],$storing_mode);
		//$is_login_page_updated			=	$ob_webinar_db_interaction->update_webinar_login_page($login_page_id,$_POST['registration_page_permalink']);
		$is_thankyou_page_updated	  =	$ob_webinar_db_interaction->update_webinar_thankyou_page($thankyou_page_id,$_POST['thankyou_page_permalink'],$storing_mode);
		$is_replay_page_updated		  =	$ob_webinar_db_interaction->update_after_webinar_page($after_webinar_page_id,$_POST['event_replay_page_permalink'],$storing_mode);
		$is_webinar_page_updated	  =	$ob_webinar_db_interaction->update_webinar_page($webinar_page_id,$_POST['event_page_permalink'],$storing_mode);
		$is_live_page_updated		  	=	$ob_webinar_db_interaction->update_webinar_live_page($webinar_live_page_id,$_POST['live_page_permalink'],$storing_mode);
		update_option('ewp_emarketing_settings_'.$webinar_id,base64_decode($_POST['emarketing_settings']));
	}
} 
}
echo json_encode($response_array);
exit;
?>