<?php
	header('Content-Type: application/json');
	require_once('../../../wp-load.php');
	require_once('webinar-db-interaction/webinar-db-interaction.php');

	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	
	require_once('license.php');	
	require_once('access-validation.php');
	
	$upload_max_filesize	=	ini_get('upload_max_filesize');
	$post_max_size				=	ini_get('post_max_size');
	$max_execution_time 	=	(int)ini_get('max_execution_time');
	
	if(isset($_POST['webinar_id'])){
		$webinar_id 								= 	(int)$_POST['webinar_id'];
		$webinar_detail							= 	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
		$webiar_default_attendees		=		$ob_webinar_db_interaction->get_default_attendee_webinar_list($webinar_id);
		$scheduled_times						=		$ob_webinar_db_interaction->get_all_schedules_for_particuler_webinar($webinar_id);
		$webinar_delayed_events			=		$ob_webinar_db_interaction->get_webinar_delayed_events($webinar_id);
		$webinar_scarcity						=		$ob_webinar_db_interaction->get_webinar_scarcity($webinar_id);
		$sharing_detail							=		$ob_webinar_db_interaction->get_sharing_detail($webinar_id);
		$notification_details				=		$ob_webinar_db_interaction->get_notification_details($webinar_id);
		$week_days									=		$ob_webinar_db_interaction->get_webinar_scheduled_days($webinar_id);
		$specific_date							=		$ob_webinar_db_interaction->get_specific_date($webinar_id);
		$delayed_events_paths				=		$ob_webinar_db_interaction->get_delayed_events_paths();
		$timezone_detail						=		$ob_webinar_db_interaction->get_timezones_detail();
		$webinar_notification_emails=	$ob_webinar_db_interaction->get_before_webinar_notification_emails($webinar_id);
		//$notification_timing_details=	$ob_webinar_db_interaction->get_notification_timing_details($webinar_id);
		$webinar_welcome_notification_emails=	$ob_webinar_db_interaction->get_welcome_webinar_notification_emails($webinar_id);
		$webinar_after_notification_emails	=	$ob_webinar_db_interaction->get_after_webinar_notification_emails($webinar_id);
		$specific_dates							=		$ob_webinar_db_interaction->get_specific_date($webinar_id);
		$slugname										=		$ob_webinar_db_interaction->get_webinar_slug($webinar_id);
		$registration_template			=   $ob_webinar_db_interaction->get_registration_template_data($webinar_id);
		$thankyou_template					=   $ob_webinar_db_interaction->get_thankyou_template_data($webinar_id);
		$event_template							=   $ob_webinar_db_interaction->get_event_template_data($webinar_id);
		$webinars_count		 					= 	$ob_webinar_db_interaction->get_webinars_count('')->total;
		$drafts_count		 					= 	$ob_webinar_db_interaction->get_webinars_count('draft')->total;
		$webinars_emarketing_settings=  maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar_id));
		$webinar = array(
			'webinar_detail'=>$webinar_detail,
			'webinar_default_attendees'=>$webiar_default_attendees,
			'scheduled_times'=>$scheduled_times,
			'webinar_delayed_events'=>$webinar_delayed_events,
			'webinar_scarcity'=>$webinar_scarcity,
			'sharing_detail'=>$sharing_detail,
			'notification_details'=>$notification_details,
			'week_days'=>$week_days,
			'specific_date'=>$specific_date,
			'delayed_events_paths'=>$delayed_events_paths,
			'timezone_detail'=>$timezone_detail,
			'webinar_notification_emails'=>$webinar_notification_emails,
			'webinar_welcome_notification_emails'=>$webinar_welcome_notification_emails,
			'webinar_after_notification_emails' => $webinar_after_notification_emails,
			//'notification_timing_details'=>$notification_timing_details,
			'specific_dates' => $specific_dates,
			'upload_max_filesize' => $upload_max_filesize,
			'post_max_size'=>$post_max_size,
			'slug_name'=>$slugname,
			'max_execution_time'=>$max_execution_time, 
			'registration_template'=>$registration_template,
			'thankyou_template'=>$thankyou_template,
			'event_template'=>$event_template,
			'emarketing_settings'=>$webinars_emarketing_settings,
			'webinars_count'=>$webinars_count,
		    'drafts_count' => $drafts_count
		);
		echo json_encode($webinar);
	}else{
		echo json_encode(array());
	}
	exit;
?>