<?php
/**
 * Template Name: error page
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
error_reporting(0);
if(!session_id())session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if(isset($_SESSION['not_found'])) {echo $_SESSION['not_found']; } else{ bloginfo('name'); } ?>
</title>
<link rel="shortcut icon" href="<?php echo FAVICON_URL ?>" type="image/x-icon" />
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/css/bootstrap.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/css/bootstrap-responsive.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/css/custom_style.css'?>" />
<link rel="stylesheet" type="text/css" href="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/css/custom_responsive.css'?>" />
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/js/html5.js'?>"></script>
<![endif]-->
</head>

<body>

<!-- Header Starts --> 

<!-- Navigation -->

<div class="navbar">
  <div class="navbar_inner">
    <center>
    </center>
  </div>
</div>

<!-- Navigation Ends --> 

<!-- Header Ends --> 

<!-- Content Section -->
<div class="container video error"> 
  
  <!-- Main Title Ends --> 
  
  <!-- Content Panel -->
  <div class="row"> 
    <!-- Left Section  Starts -->
    <section class="span12">
      <div class="leftpanel"> 
        
        <!-- Video Section -->
        
        <section class="errorsection">
          <center>
            <img src="<?php echo WEBINAR_PLUGIN_URL.'templates/custom_theme/img/error.png'; ?>" alt="" />
            <h3 class="colortxt">
              <?php if(isset($_SESSION['not_found'])) {echo ' '; } else{ echo $_SESSION['error'];  } ?>
            </h3>
            <h5 class="errormsg">
              <?php if(isset($_SESSION['not_found'])) {echo $_SESSION['not_found']; } ?>
            </h5>
          </center>
        </section>
      </div>
    </section>
    <!-- Left Section Ends --> 
    
  </div>
  
  <!-- Content Panel Ends --> 
  
</div>
<!-- Content Section Ends --> 

<!-- Footer Starts -->
<footer>
  <p align="center"></p>
</footer>

<!-- Footer Ends -->
</body>
</html>