<?php
/*
 *	Plugin Name: Easy Webinar Plugin
 *	Plugin URI:	 http://easywebinarplugin.com/
 *	Description: Start Easily Creating Automated Webinars
 *	Author:	<a href="http://caseyzemanonline.com/" target="_blank" >CSZ/Casey Zeman</a> and <a href="http://www.softobiz.com/" target="_blank" >Softobiz</a>.
 *	Version: EWP(3.8)
 */
error_reporting(0);
ob_start();

if(!session_id())session_start();
if(!class_exists('webinar_plugin')){
	class webinar_plugin{
		
		function webinar_plugin(){
		 
			date_default_timezone_set("GMT");
			$this->load_required_classes_files();
			add_action('admin_menu',array(&$this,'admin_menu'));
		}
	
		function make_dynamic_tables_for_webinar(){
			global $wpdb;
			
			$wpdb->webinar										=	"webinar";
			$wpdb->webinar_schedule						=	"webinar_schedule";
			$wpdb->webinar_schedule_type			=	"webinar_schedule_type";
			$wpdb->webinar_delayed_events			=	"webinar_delayed_events";
			$wpdb->webinar_buttons						=	"webinar_buttons";
			$wpdb->webinar_attendees					=	"webinar_attendees";
			$wpdb->webinar_timezone						=	"webinar_timezone";
			$wpdb->webinar_notification_emails=	"webinar_notification_emails";
			$wpdb->webinar_page_ids					  =	"webinar_page_ids";
			$wpdb->webinar_registered_users		=	"webinar_registered_users";
			$wpdb->webinar_logged_in_tracking	=	"webinar_logged_in_tracking";
			$wpdb->notification_timing				=	"notification_timing";
			$wpdb->notification_sent_to				=	"notification_sent_to";
			$wpdb->webinar_scarcity						= "webinar_scarcity";
			$wpdb->webinar_sharing						= "webinar_sharing";
			$wpdb->webinar_template						=	"webinar_template";
			$wpdb->webinar_events_tracking		=	"webinar_events_tracking";
			$wpdb->webinar_questions					=	"webinar_questions";
			$wpdb->webinar_visitor_tracking		=	"webinar_visitor_tracking";
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar'")==$wpdb->webinar)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_schedule'")==$wpdb->webinar_schedule)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_schedule} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_schedule_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_schedule_type'")==$wpdb->webinar_schedule_type)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_schedule_type} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_schedule_type_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_delayed_events'")==$wpdb->webinar_delayed_events)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_delayed_events} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_delayed_events_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_buttons'")==$wpdb->webinar_buttons)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_buttons} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_button_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_attendees'")==$wpdb->webinar_attendees)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_attendees} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_attendee_id_pk)");	
						
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_timezone'")==$wpdb->webinar_timezone)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_timezone} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_timezone_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_notification_emails'")==$wpdb->webinar_notification_emails)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_notification_emails} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_notification_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_page_ids'")==$wpdb->webinar_page_ids)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_page_ids} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_page_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_registered_users'")==$wpdb->webinar_registered_users)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_registered_users} DROP PRIMARY KEY, ADD UNIQUE KEY (registration_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_logged_in_tracking'")==$wpdb->webinar_logged_in_tracking)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_logged_in_tracking} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_logged_in_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->notification_timing'")==$wpdb->notification_timing)
				$wpdb->query( "ALTER TABLE {$wpdb->notification_timing} DROP PRIMARY KEY, ADD UNIQUE KEY (timing_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->notification_sent_to'")==$wpdb->notification_sent_to)
				$wpdb->query( "ALTER TABLE {$wpdb->notification_sent_to} DROP PRIMARY KEY, ADD UNIQUE KEY (notification_sent_to_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_scarcity'")==$wpdb->webinar_scarcity)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_scarcity} DROP PRIMARY KEY, ADD UNIQUE KEY (webinar_scarcity_id_pk)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_sharing'")==$wpdb->webinar_sharing)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_sharing} DROP PRIMARY KEY, ADD UNIQUE KEY (sharing_id)");	
			
		  if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_template'")==$wpdb->webinar_template)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_template} DROP PRIMARY KEY, ADD UNIQUE KEY (template_id)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_events_tracking'")==$wpdb->webinar_events_tracking)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_events_tracking} DROP PRIMARY KEY, ADD UNIQUE KEY (action_id)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_questions'")==$wpdb->webinar_questions)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_questions} DROP PRIMARY KEY, ADD UNIQUE KEY(quest_id)");	
			
			if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar_visitor_tracking'")==$wpdb->webinar_visitor_tracking)
				$wpdb->query( "ALTER TABLE {$wpdb->webinar_visitor_tracking} DROP PRIMARY KEY, ADD UNIQUE KEY (visitor_tracking_id)");	
			
			$webinar_sql = "CREATE TABLE ".$wpdb->webinar." (
						 webinar_id_pk int(11) NOT NULL AUTO_INCREMENT,
						  webinar_event_name varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  webinar_main_topic varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  topic varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  webinar_page_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  footer_text text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  admin_email int(11) NOT NULL,
						  admin_email_value varchar(255) NOT NULL,
						  footer_links text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  presenters_name varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  presenters_box enum('0','1') NOT NULL,
						  presenters_attachemnt_path varchar(233) NOT NULL,
						  presenters_thumbnail_path varchar(233) NOT NULL,
						  presenters_description text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  registration_page_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  registration_headline varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  registration_subheadline varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  header_logo varchar(255) NOT NULL,
						  header_attachment varchar(255) NOT NULL,
						  reg_alternate_video varchar(255) NOT NULL,
						  registration_video_aspect_ratio tinyint(1) NOT NULL COMMENT '0 for 16:9, 1 for 4:3',
						  description text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_webinar_page_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_subtitle varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_webinar_description text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_video_aspect_ratio tinyint(1) NOT NULL COMMENT '0 for 16:9, 1 for 4:3',
    					thank_alternate_video varchar(255) NOT NULL,
						  webinar_timezone_id_fk int(11) NOT NULL,
						  webinar_video_url varchar(255) NOT NULL,
						  webinar_video_length int(11) NOT NULL,
						  video_type varchar(255) NOT NULL,
						  webinar_alternate_video_url varchar(255) NOT NULL,
						  event_video_aspect_ratio tinyint(1) NOT NULL COMMENT '0 for 16:9, 1 for 4:3',
						  webinar_schedule_type_id_fk int(11) NOT NULL,
						  webinar_start_date date DEFAULT NULL,
						  webinar_end_date date DEFAULT NULL,
						  webinar_block_days int(11) NOT NULL,
						  webinar_days_to_show int(11) NOT NULL DEFAULT '1',
						  is_right_now_enable enum('0','1') NOT NULL DEFAULT '0',
						  attachment_type enum('1','2') DEFAULT NULL,
						  attachment_embed_script varchar(255) DEFAULT NULL,
						  attachment_path varchar(255) DEFAULT NULL,
						  attachment_thumb_path varchar(255) NOT NULL,
						  thankyou_attachment_type enum('1','2') DEFAULT NULL COMMENT '1 for image and 2 for video',
						  thankyou_attachment_embed_script varchar(255) DEFAULT NULL,
						  thankyou_attachment_path varchar(255) DEFAULT NULL,
						  thankyou_attachment_thumb_path varchar(255) NOT NULL,
						  automated_redirect_url varchar(255) DEFAULT NULL,
						  attendees_list_enabled tinyint(1) NOT NULL DEFAULT '0',
						  max_number_of_attendees int(215) NOT NULL,
						  stimulate_attendee_list int(213) NOT NULL,
						  chatbox_enabled tinyint(1) NOT NULL DEFAULT '0',
						  chatbox_code text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  chat_title text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  notification_after_webinar_enabled tinyint(1) NOT NULL DEFAULT '0',
						  after_webinar_notification_hours tinyint(3) NOT NULL,
						  sender_name varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  notification_from_address varchar(255) NOT NULL,
						  sharing enum('1','2') DEFAULT NULL,
						  is_presenter_link enum('1','2') NOT NULL,
						  is_webinar_link enum('1','2') NOT NULL,
						  notification_type enum('1','2','3') NOT NULL COMMENT 'This fields tell which notification will be used.1 stands for the bulit in notification and 2 stands for the autoresponder.',
						  template_style varchar(255) NOT NULL,
						  template_color varchar(233) NOT NULL,
						  created_date date NOT NULL,
						  modified_date date DEFAULT NULL,
						  autoresponder_text text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  header_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  body_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  counter_headline varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  counter_subheadline varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  footer_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
							thank_header_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thank_body_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thank_footer_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
							event_header_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  event_body_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  event_footer_analytics text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  reg_meta_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  reg_meta_keys text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  reg_meta_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  thankyou_meta_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  thankyou_meta_keys text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  thankyou_meta_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  event_meta_title varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  event_meta_keys text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  event_meta_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
						  is_opted_for_affiliate_link tinyint(1) NOT NULL,
						  is_livestream tinyint(1) NOT NULL DEFAULT '0',
						  live_page_type tinyint(1) NOT NULL DEFAULT '1',
							event_type tinyint(1) NOT NULL DEFAULT '0',
							webinar_time_format tinyint(1) NOT NULL DEFAULT '0',
							allow_unexpired_end_sessions tinyint(1) NOT NULL DEFAULT '0',
							allow_event_page tinyint(1) NOT NULL DEFAULT '0',
							affilate_param varchar(40) NOT NULL,
							affilate_param_val varchar(70) NOT NULL,
							storing_mode varchar(40) NOT NULL,
						  PRIMARY KEY (webinar_id_pk));";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_sql);
		
			$webinar_schedule_sql = "CREATE TABLE " .$wpdb->webinar_schedule. " (
					  webinar_schedule_id_pk int(11) NOT NULL AUTO_INCREMENT,
					  webinar_id_fk int(11) NOT NULL,
					  start_time time NOT NULL,
					  end_time time NOT NULL,
					  gmt_start_time time NOT NULL,
					  gmt_end_time time NOT NULL,
					  webinar_date date DEFAULT NULL,
					  week_day int(11) DEFAULT NULL,
					  PRIMARY KEY (webinar_schedule_id_pk))";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_schedule_sql);
		
			$webinar_schedule_type_sql = "CREATE TABLE " .$wpdb->webinar_schedule_type. " (
					 webinar_schedule_type_id_pk int(11) NOT NULL AUTO_INCREMENT,
					 schedule_name varchar(255) NOT NULL,
					  PRIMARY KEY (webinar_schedule_type_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_schedule_type_sql);
			$wpdb->query("TRUNCATE TABLE ".$wpdb->webinar_schedule_type."");
			$this->insert_schedule_types();
			
			$webinar_attendees_sql = "CREATE TABLE " .$wpdb->webinar_attendees. " (
					 webinar_attendee_id_pk int(11) NOT NULL AUTO_INCREMENT,
					 webinar_id_fk int(11) NOT NULL,
					 attendee_name varchar(255) NOT NULL Collate 'utf8_bin',
					 PRIMARY KEY (webinar_attendee_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_attendees_sql);
		
			$webinar_delayed_events_sql = "CREATE TABLE " .$wpdb->webinar_delayed_events. " (
				  webinar_delayed_events_id_pk int(11) NOT NULL AUTO_INCREMENT,
				  webinar_id_fk int(11) NOT NULL,
				  text_to_display text Collate 'utf8_bin',
				  start_time time NOT NULL,
				  end_time time NOT NULL,
				  webinar_button_id_fk int(11) DEFAULT NULL COMMENT 'button id of button which user want to show at a particular event',
				  chosen_button_link varchar(255) DEFAULT NULL COMMENT 'where chosen_button should link',
				  infusionsoft_delayed_tag int(20) NOT NULL,
				  infusionsoft_delayed_clicked_tag int(20) NOT NULL,
				  PRIMARY KEY (webinar_delayed_events_id_pk)
			   );";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_delayed_events_sql);

		
			$webinar_scarcity_sql = "CREATE TABLE " .$wpdb->webinar_scarcity. "  (
				  webinar_scarcity_id_pk int(12) NOT NULL AUTO_INCREMENT,
				  webinar_id_fk int(12) NOT NULL,
				  start_time time NOT NULL,
				  start_number text NOT NULL,
				  end_time time NOT NULL,
				  end_number text NOT NULL,
				  scarcity_start_text varchar(255) NOT NULL Collate 'utf8_bin',
				  scarcity_end_text varchar(255) NOT NULL Collate 'utf8_bin',
				  description text NOT NULL Collate 'utf8_bin',
				  is_scarcity_enabled enum('0','1') NOT NULL,
				  button_id_fk int(12) NOT NULL,
				  choose_button_link text NOT NULL,
				  PRIMARY KEY (webinar_scarcity_id_pk),
				  KEY (webinar_id_fk)
				 );";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_scarcity_sql);

			$webinar_sharing_sql = "CREATE TABLE " .$wpdb->webinar_sharing. " (
				  sharing_id int(12) NOT NULL AUTO_INCREMENT,
				  webinar_id_fk int(12) NOT NULL,
				  is_sharing_enabled enum('1','2') NOT NULL,
				  facebook int(12) NOT NULL,
				  twitter int(12) NOT NULL,
				  share_text varchar(255) NOT NULL Collate 'utf8_bin',
				  share_desc text NOT NULL Collate 'utf8_bin',
				  is_document_enabled int(12) NOT NULL,
				  document text NOT NULL,
				  is_video_enabled int(12) NOT NULL,
				  video_attachment_path text NOT NULL,
				  video_thumbnail_path text NOT NULL,
				  PRIMARY KEY (sharing_id),
				  KEY (webinar_id_fk)
				);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_sharing_sql);
		
			$webinar_buttons_sql = "CREATE TABLE " .$wpdb->webinar_buttons. " (
						webinar_button_id_pk int(11) NOT NULL AUTO_INCREMENT,
						button_name varchar(255) NOT NULL Collate 'utf8_bin',
						button_source_path varchar(255) NOT NULL,
						 PRIMARY KEY (webinar_button_id_pk)
					  );";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_buttons_sql);
			$wpdb->query("TRUNCATE TABLE ".$wpdb->webinar_buttons."");
			$this->insert_delayed_button_paths();
			
			$webinar_logged_in_tracking_sql = "CREATE TABLE " .$wpdb->webinar_logged_in_tracking." (
						webinar_logged_in_id_pk int(11) NOT NULL AUTO_INCREMENT,
						registration_id_fk int(11) NOT NULL,
						webinar_id_fk int(11) NOT NULL,
						webinar_schedule_id_fk int(11) NOT NULL,
						date_of_webinar date NOT NULL,
						watch_full_webinar int(11) NOT NULL DEFAULT '0',
						PRIMARY KEY (webinar_logged_in_id_pk)
					 );";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_logged_in_tracking_sql);
		
		
			$webinar_notification_emails_sql = "CREATE TABLE webinar_notification_emails (
							 webinar_notification_id_pk int(11) NOT NULL AUTO_INCREMENT,
							 notification_type_id enum('1','2','3') NOT NULL COMMENT '1 for registration mail, 2 for before webinar email, 3 for after webinar email',
							 subject varchar(255) NOT NULL Collate 'utf8_bin',
							 message text NOT NULL Collate 'utf8_bin',
							 webinar_id_fk int(11) NOT NULL,
							 notification_days int(11) NOT NULL DEFAULT '0',
							 notification_hours int(11) NOT NULL DEFAULT '0',
							 notification_minutes int(11) NOT NULL DEFAULT '0',
							 PRIMARY KEY (webinar_notification_id_pk)
							);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_notification_emails_sql);
		

			$notification_timing = "CREATE TABLE " . $wpdb->notification_timing	. " (
					  timing_id_pk int(11) NOT NULL AUTO_INCREMENT,
					  webinar_id int(11) NOT NULL,
					  notification_date date NOT NULL,
					  notification_time time NOT NULL,
					  notification_type int(11) NOT NULL,
					  webinar_time time NOT NULL,
					  webinar_date date NOT NULL,
					notification_status int(11) NOT NULL DEFAULT '1',
					  PRIMARY KEY (timing_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($notification_timing);
	
			$notification_sent_to_sql = "CREATE TABLE " .$wpdb->notification_sent_to. " (
					 notification_sent_to_id_pk int(11) NOT NULL AUTO_INCREMENT,
					 email_to varchar(255) NOT NULL Collate 'utf8_bin',
					 email_from varchar(255) NOT NULL Collate 'utf8_bin',
					 subject varchar(255) NOT NULL Collate 'utf8_bin',
					 body text NOT NULL,
					 stat tinyint(1) NOT NULL DEFAULT '0',
					 PRIMARY KEY (notification_sent_to_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($notification_sent_to_sql);
		
			$webinar_registered_users_sql = "CREATE TABLE " .$wpdb->webinar_registered_users. " (
					  registration_id_pk int(11) NOT NULL AUTO_INCREMENT,
					  webinar_id_fk int(11) NOT NULL,
					  webinar_schedule_id_fk int(11) NOT NULL,
					  attendee_name varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
					  attendee_email_id varchar(255) NOT NULL,
					  attendee_local_timezone varchar(255) DEFAULT NULL,
					  selected_timezone_id int(11) NOT NULL,
					  webinar_real_date varchar(255) NOT NULL,
					  webinar_real_time varchar(255) NOT NULL,
					  webinar_start_date date NOT NULL,
					  webinar_date date NOT NULL,
					  webinar_time time NOT NULL,
					  webinar_end_date date NOT NULL,
					  webinar_end_time time NOT NULL,
					  `key` varchar(255) NOT NULL,
					  activity_status tinyint(1) NOT NULL DEFAULT '0',
					  replay_activity_status tinyint(1) NOT NULL DEFAULT '0',
					  watched_percentage tinyint(1) NOT NULL DEFAULT '0',
					  signup_date date NOT NULL,
					  PRIMARY KEY (registration_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_registered_users_sql);
		
			$webinar_template_sql = "CREATE TABLE ".$wpdb->webinar_template." (
									  template_id int(11) NOT NULL AUTO_INCREMENT,
									  webinar_id int(11) NOT NULL,
									  theme_type varchar(50) NOT NULL,
									  template_part int(11) NOT NULL,
									  template_options text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
									  PRIMARY KEY (template_id)
									);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_template_sql);
			
			$webinar_events_tracking_sql = "CREATE TABLE ".$wpdb->webinar_events_tracking." (
									action_id int(11) NOT NULL AUTO_INCREMENT,
									webinar_id int(11) NOT NULL,
									user_id int(11) NOT NULL,
									is_event int(11) NOT NULL DEFAULT '0',
									event_id int(11) NOT NULL,
									button_id int(11) NOT NULL,
									is_live int(11) NOT NULL DEFAULT '0',
									PRIMARY KEY (action_id)
								  );";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_events_tracking_sql);
			
			$webinar_questions_sql = "CREATE TABLE ".$wpdb->webinar_questions." (
									quest_id int(11) NOT NULL AUTO_INCREMENT,
									webinar_id int(11) NOT NULL,
									user_id int(11) NOT NULL,
									quest text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
									is_live int(11) NOT NULL DEFAULT '0',
									user_name varchar(50) NOT NULL,
									user_email varchar(100) NOT NULL,
									PRIMARY KEY (quest_id)
								  );";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_questions_sql);
			
			
			$webinar_visitor_tracking_sql= "CREATE TABLE ".$wpdb->webinar_visitor_tracking." (
									  visitor_tracking_id int(11) NOT NULL AUTO_INCREMENT,
									  webinar_id int(11) NOT NULL,
									  visitor_ip varchar(50) NOT NULL,
									  visit_time varchar(50) NOT NULL,
									  PRIMARY KEY (visitor_tracking_id)
									);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_visitor_tracking_sql);
			
			$webinar_timezone_sql	= "CREATE TABLE webinar_timezone (
					webinar_timezone_id_pk int(11) NOT NULL AUTO_INCREMENT,
					GMT time NOT NULL,
					name varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL Collate 'utf8_bin',
					timezone_gmt_symbol varchar(255) NOT NULL,
					timezone_name varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
					PRIMARY KEY (webinar_timezone_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($webinar_timezone_sql);
			$wpdb->query("TRUNCATE TABLE webinar_timezone");
			$this->insert_timezones_detail();
		
			$create_webinar_page_ids = "CREATE TABLE webinar_page_ids (
					webinar_page_id_pk int(11) NOT NULL auto_increment,
					post_id int(11) NOT NULL,
					page_name varchar(11) NOT NULL,
					 PRIMARY KEY (webinar_page_id_pk)
					);";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($create_webinar_page_ids);
			
		$post_table = $wpdb->prefix.'posts';
		$meta_table_name = $wpdb->prefix.'postmeta';
		if(get_option('delete_notificaion_timing') == '')
		{
			$current_gm_time = gmdate("H:i:s",strtotime('-1 hour'));
			$current_gm_date =  gmdate("Y-m-d");
		$wpdb->query("DELETE FROM $wpdb->notification_timing WHERE notification_date < '" . $current_gm_date ."' OR (notification_date = '" . $current_gm_date ."' AND notification_time <= '" . $current_gm_time . "')");
		update_option('delete_notificaion_timing',1);
		}
		$wpdb->query("DELETE FROM $post_table WHERE post_title = 'Webinar Countdown Page (Mandatory page, please do not modify or delete [EasyWebinarPlugin])'");
		$wpdb->query("DELETE FROM $post_table WHERE post_title = 'Webinar Error Page (Mandatory page, please do not modify or delete [EasyWebinarPlugin])'");
		$wpdb->query("DELETE FROM $post_table WHERE post_title = 'Webinar Countdown (Mandatory page, please do not modify or delete [EasyWebinarPlugin])'");
		$wpdb->query("DELETE FROM $post_table WHERE post_title = 'Webinar Error (Mandatory page, please do not modify or delete [EasyWebinarPlugin])'");
		$wpdb->query("DELETE FROM $meta_table_name WHERE meta_key ='_wp_page_template' AND meta_value = 'webinar-countdown.php'");
		$wpdb->query("DELETE FROM $meta_table_name WHERE meta_key ='_wp_page_template' AND meta_value = 'webinar-error.php'");
		$wpdb->query("TRUNCATE TABLE webinar_page_ids");
		$countdown_page_id = $this->make_webinar_pages("Webinar Countdown","webinar-countdown.php");
		$this->save_webinar_pages("countdown",$countdown_page_id);
		$error_page_id = $this->make_webinar_pages("Webinar Error","webinar-error.php");
		$this->save_webinar_pages("error",$error_page_id);
					
		$post_table = $wpdb->prefix.'posts';
		$post_table_checkfields = $wpdb->get_col("SHOW FIELDS FROM $post_table");
		if (!in_array('webinar_id', $post_table_checkfields)){
			$hook_webinar_id_sql	=  "ALTER TABLE $post_table ADD COLUMN webinar_id int(11) NOT NULL";
			$wpdb->query($hook_webinar_id_sql);
		}

		if($wpdb->get_var("SHOW TABLES LIKE '$wpdb->webinar'")==$wpdb->webinar)
				$wpdb->query( "UPDATE webinar SET webinar_start_date = '".date('Y-m-d')."', webinar_end_date=NULL WHERE (webinar_schedule_type_id_fk=1 OR webinar_schedule_type_id_fk=4) AND (webinar_start_date IS NULL OR webinar_start_date='')");	
		
		$this->ewpUpdateCurrentVersionOption();
		$this->implement_live_page();
		
		}
		
		function implement_live_page(){
			global $wpdb;
			$post_table = $wpdb->prefix.'posts';
			
			$ob_webinar_db_interaction = new webinar_db_interaction();
			$webinar_ids = $wpdb->get_results("SELECT webinar_id_pk FROM webinar",OBJECT);
			if(!empty($webinar_ids)){
			  foreach($webinar_ids as $webinar){
				   $webinar_pages = $ob_webinar_db_interaction->get_webinar_all_pages_ids($webinar->webinar_id_pk);
				   if(!empty($webinar_pages[5]->ID)){
					  $meta_value = get_post_meta($webinar_pages[5]->ID,'_ewp_live_page',true);
					  if($meta_value!=1){
						  update_post_meta($webinar_pages[5]->ID,"_wp_page_template","webinar-live.php");
						  update_post_meta($webinar_pages[5]->ID,'_ewp_live_page',"1");
						  $ob_webinar_db_interaction->update_webinar_live_page($webinar_pages[5]->ID,'event-live');
					  }
				   }
			  }
			}
		}
		
		function ewpUpdateCurrentVersionOption(){
			update_option('ewp_current_version',get_ewp_plugin_version_from_file());
		}
		
		function insert_delayed_button_paths(){
			global $wpdb;
			$wpdb->delayed_button_paths		=	"webinar_buttons";
			$wpdb->query("INSERT INTO $wpdb->delayed_button_paths (webinar_button_id_pk, button_name, button_source_path) VALUES(1, 'Add To Cart','".WEBINAR_PLUGIN_URL."webinar-events-images/add-to-cart.png'),(2, 'Sign-Up Now','".WEBINAR_PLUGIN_URL."webinar-events-images/become-a-member.png'),(3, 'Buy Now','".WEBINAR_PLUGIN_URL."webinar-events-images/buy-now.png'),(4, 'Get Access Now','".WEBINAR_PLUGIN_URL."webinar-events-images/get-started.png'),(5, 'Download Now','".WEBINAR_PLUGIN_URL."webinar-events-images/get-your-copy.png'),(6, 'Order Now','".WEBINAR_PLUGIN_URL."webinar-events-images/start-trail.png');");
		}
	
		function insert_schedule_types(){
			global $wpdb;
			$wpdb->schedule_types	=	"webinar_schedule_type";
			$schedule_types			=	$wpdb->query("INSERT INTO $wpdb->schedule_types (webinar_schedule_type_id_pk, schedule_name) VALUES(1, 'Everyday'),(2,'Specific Dates'),(4, 'Selected days in week');");
		}
	
		function insert_timezones_detail(){
			global $wpdb;
			$wpdb->timezone			=	"webinar_timezone";
			$wpdb->query("INSERT INTO $wpdb->timezone (webinar_timezone_id_pk, GMT, name, timezone_gmt_symbol, timezone_name) VALUES (1, '12:00:00', '(GMT-12:00)-International Date Line West', '-', 'Etc/GMT+12'),(2, '11:00:00', '(GMT-11:00)-Midway Island, Samoa', '-', 'US/Samoa'),(3, '10:00:00', '(GMT-10:00)-Hawaii', '-', 'US/Hawaii'),(4, '09:00:00', '(GMT-09:00)-Alaska', '-', 'US/Alaska'),(5, '08:00:00', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana', '-', 'America/Tijuana'),(6, '07:00:00', '(GMT-07:00)-Arizona', '-', 'US/Arizona'),(7, '07:00:00', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan', '-', 'America/Chihuahua'),(8, '07:00:00', '(GMT-07:00)-Mountain Time (US & Canada)', '-', 'US/Mountain'),(9, '06:00:00', '(GMT-06:00)-Central America', '-', 'America/Managua'),(10, '06:00:00', '(GMT-06:00)-Central Time (US & Canada)', '-', 'US/Central'),(11, '06:00:00', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey', '-', 'America/Cancun'),(12, '06:00:00', '(GMT-06:00)-Saskatchewan', '-', 'Canada/Saskatchewan'),(13, '05:00:00', '(GMT-05:00)-Bogota, Lima, Quito', '-', 'America/Bogota'),(14, '05:00:00', '(GMT-05:00)-Eastern Time (US & Canada)', '-', 'America/New_York'),(15, '05:00:00', '(GMT-05:00)-Indiana (East)', '-', 'US/East-Indiana'),(16, '04:00:00', '(GMT-04:00)-Atlantic Time (Canada)', '-', 'Canada/Atlantic'),(17, '04:00:00', '(GMT-04:00)-Caracas, La Paz', '-', 'America/Caracas'),(18, '04:00:00', '(GMT-04:00)-Santiago', '-', 'America/Santiago'),(19, '03:30:00', '(GMT-03:30)-Newfoundland', '-', 'Canada/Newfoundland'),(20, '03:00:00', '(GMT-03:00)-Brasilia', '-', 'America/Sao_Paulo'),(21, '03:00:00', '(GMT-03:00)-Buenos Aires, Georgetown', '-', 'America/Argentina/Buenos_Aires'),(22, '03:00:00', '(GMT-03:00)-Greenland', '-', 'America/Godthab'),(23, '02:00:00', '(GMT-02:00)-Mid-Atlantic', '-', 'America/Noronha'),(24, '01:00:00', '(GMT-01:00)-Azores', '-', 'Atlantic/Azores'),(25, '01:00:00', '(GMT-01:00)-Cape Verde Is.', '-', 'Atlantic/Cape_Verde'),(26, '00:00:00', '(GMT)-Casablanca, Monrovia', '+', 'Africa/Casablanca'),(27, '00:00:00', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London', '+', 'Europe/London'),(28, '01:00:00', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', '+', 'Europe/Amsterdam'),(29, '01:00:00', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague', '+', 'Europe/Belgrade'),(30, '01:00:00', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris', '+', 'Europe/Brussels'),(31, '01:00:00', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb', '+', 'Europe/Sarajevo'),(32, '01:00:00', '(GMT+01:00)-West Central Africa', '+', 'Africa/Lagos'),(33, '02:00:00', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk', '+', 'Europe/Istanbul'),(34, '03:00:00', '(GMT+03:00)-Bucharest', '+', 'Europe/Bucharest'),(35, '02:00:00', '(GMT+02:00)-Cairo', '+', 'Africa/Cairo'),(36, '02:00:00', '(GMT+02:00)-Harare, Pretoria', '+', 'Africa/Johannesburg'),(37, '02:00:00', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius', '+', 'Europe/Helsinki'),(38, '02:00:00', '(GMT+02:00)-Jerusalem', '+', 'Asia/Jerusalem'),(39, '03:00:00', '(GMT+03:00)-Baghdad', '+', 'Asia/Baghdad'),(40, '03:00:00', '(GMT+03:00)-Kuwait, Riyadh', '+', 'Asia/Kuwait'),(41, '03:00:00', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd', '+', 'Europe/Moscow'),(42, '03:00:00', '(GMT+03:00)-Nairobi', '+', 'Africa/Nairobi'),(43, '04:30:00', '(GMT+04:30)-Tehran', '+', 'Asia/Tehran'),(44, '04:00:00', '(GMT+04:00)-Abu Dhabi, Muscat', '+', 'Asia/Muscat'),(45, '04:00:00', '(GMT+04:00)-Baku, Tbilisi, Yerevan', '+', 'Asia/Tbilisi'),(46, '04:30:00', '(GMT+04:30)-Kabul', '+', 'Asia/Kabul'),(47, '05:00:00', '(GMT+05:00)-Ekaterinburg', '+', 'Asia/Yekaterinburg'),(48, '05:00:00', '(GMT+05:00)-Islamabad, Karachi, Tashkent', '+', 'Asia/Karachi'),(49, '05:30:00', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi', '+', 'Asia/Kolkata'),(50, '05:45:00', '(GMT+05:45)-Kathmandu', '+', 'Asia/Kathmandu'),(51, '06:00:00', '(GMT+06:00)-Almaty, Novosibirsk', '+', 'Asia/Almaty'),(52, '06:00:00', '(GMT+06:00)-Astana, Dhaka', '+', 'Asia/Dhaka'),(53, '06:00:00', '(GMT+06:00)-Sri Jayawardenepura', '+', 'Asia/Colombo'),(54, '06:30:00', '(GMT+06:30)-Rangoon', '+', 'Asia/Rangoon'),(55, '07:00:00', '(GMT+07:00)-Bangkok, Hanoi, Jakarta', '+', 'Asia/Bangkok'),(56, '07:00:00', '(GMT+07:00)-Krasnoyarsk', '+', 'Asia/Krasnoyarsk'),(57, '08:00:00', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi', '+', 'Asia/Hong_Kong'),(58, '08:00:00', '(GMT+08:00)-Irkutsk, Ulaan Bataar', '+', 'Asia/Irkutsk'),(59, '08:00:00', '(GMT+08:00)-Kuala Lumpur, Singapore', '+', 'Asia/Singapore'),(60, '08:00:00', '(GMT+08:00)-Perth', '+', 'Australia/Perth'),(61, '08:00:00', '(GMT+08:00)-Taipei', '+', 'Asia/Taipei'),(62, '09:00:00', '(GMT+09:00)-Osaka, Sapporo, Tokyo', '+', 'Asia/Tokyo'),(63, '09:00:00', '(GMT+09:00)-Seoul', '+', 'Asia/Seoul'),(64, '09:00:00', '(GMT+09:00)-Vakutsk', '+', 'Asia/Yakutsk'),(65, '09:30:00', '(GMT+09:30)-Adelaide', '+', 'Australia/Adelaide'),(66, '09:30:00', '(GMT+09:30)-Darwin', '+', 'Australia/Darwin'),(67, '10:00:00', '(GMT+10:00)-Brisbane', '+', 'Australia/Brisbane'),(68, '10:00:00', '(GMT+10:00)-Canberra, Melbourne, Sydney', '+', 'Australia/Sydney'),(69, '10:00:00', '(GMT+10:00)-Guam, Port Moresby', '+', 'Pacific/Guam'),(70, '10:00:00', '(GMT+10:00)-Hobart', '+', 'Australia/Hobart'),(71, '10:00:00', '(GMT+10:00)-Vladivostok', '+', 'Asia/Vladivostok'),(72, '11:00:00', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia', '+', 'Asia/Magadan'),(73, '12:00:00', '(GMT+12:00)-Auckland, Wellington', '+', 'Pacific/Auckland'),(74, '12:00:00', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.', '+', 'Pacific/Fiji'),(75, '13:00:00', '(GMT+13:00)-Nuku''alofa ', '+', 'Pacific/Tongatapu');");
		}
	
		function make_webinar_pages($title,$template_name){
			global $userID,$wpdb;
			$my_post = array(
				'ID'			=>	0,
				'post_type'		=>	'page',
				'post_title'	=>	$title.' (Mandatory page, please do not modify or delete [EasyWebinarPlugin])',
				'post_status'   =>	'publish',
				'post_author'   =>	$userID,
				'post_name'		=>	$title
			);
			
			$post_id	=	wp_insert_post($my_post);
			add_post_meta($post_id,"_wp_page_template",$template_name);
			return $post_id;
		}
	
		function save_webinar_pages($page_name,$page_id){
			global $wpdb;
			$wpdb->insert_webinar_pages		=		"webinar_page_ids";
			$make_webinar_pages				=		$wpdb->query("INSERT INTO $wpdb->insert_webinar_pages (post_id,page_name) VALUES('".$page_id."','".$page_name."');");
			return $make_webinar_pages;
		}
	
		function admin_menu(){
			global $menu;
			add_menu_page('Easy Webinar Plugin', 'EWP','manage_options','webinar-admin-view', array(&$this, 'webinar_admin_view'),'');
			add_submenu_page('webinar-admin-view','See All Webinars','Webinars','10','see-all-webinar',array(&$this,'see_all_webinar'),'');
		}
		
		function see_all_webinar(){
				include_once("webinar-view-control/see-all-webinar.php");
		}
	
		function webinar_admin_view (){
				include_once("webinar-view-control/webinar_plugin_layout.php");
		}
	
		function webinar_options_general(){
			include_once("webinar-view-control/webinar-options-general.php");
		}
	
		function load_required_classes_files() {
			require_once('webinar-db-interaction/webinar-db-interaction.php');
			require_once('webinar-db-interaction/webinar-functions.php');
			require_once('webinar-config.php');
			require_once('lib/integration.infusionsoft.php');
		}
	
		function show_webinar_messages($message, $errormsg = false)
		{
			?>
			<script>
			
				jQuery(function($){
					jQuery('.close_sendgrid_notification').click(function(){
                            jQuery(this).parent().parent().hide();
						});
				});
			</script>
			<?php 
			if ($errormsg){echo '<div id="message" class="error " style="padding-left:50px;">';}
			else {echo '<div id="message" class="updated fade" style="poisition:relative;padding-left:50px;">';}
			echo "<p><strong>$message</strong><span style='cursor:pointer;margin-left:20px;' class='close_sendgrid_notification' >Hide</span></p></div>";
		}
		
		function is_webinar_registered(){
			$webinar_registerd = 0;
			if(isset($_SESSION['webinar_registered'])){
				$webinar_registerd = $_SESSION['webinar_registered']==1 ? 1 : 0;
			}else{
				$_SESSION['webinar_registered'] = 0;
				global $wpdb;
				if($wpdb->get_var("SHOW TABLES LIKE 'webinar_license'")){
				 $key_info = $wpdb->get_results("SELECT * FROM webinar_license");
				 if(count($key_info)){
					if($key_info[0]->key != ''){
						$referer = get_site_url();
						$key = $key_info[0]->key;
						$key_info = $this->get_key_info($key,$referer);
	
						if($key_info->is_key_valid==1){
							$webinar_registerd = 1;
							$_SESSION['webinar_registered'] = 1;
							
						}					
					}
				 }
				}
			}
			return $webinar_registerd;
		}
	
		function get_key_info($key,$referer){
			$version = $this->plugin_get_version();
			$version = base64_encode($version);
			$referer = base64_encode($referer);
			$ch = curl_init (EASYWEBINAR_WEBSITE_LINK."/webinar_check_key.php?key=".$key."&version=".$version."&referer=".$referer);
			curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			$result = curl_exec($ch);
			curl_close($ch);
			return json_decode($result);
		}
		
		function plugin_get_version() {
			$plugin_data = get_plugin_data( __FILE__ );
			$plugin_version = $plugin_data['Version'];
			return $plugin_version;
		}
	
		function register_webinar_plugin($key,$key_info){
			if($key_info->is_key_valid==1){
				global $wpdb;
				$create_table_webinar_license = $wpdb->query("CREATE TABLE IF NOT EXISTS webinar_license (`key` varchar(255) NOT NULL)");
				$delete_license_exists 				= $wpdb->query("DELETE FROM webinar_license;");
				$is_key_registered  					= $wpdb->query($wpdb->prepare("INSERT INTO webinar_license(`key`) VALUES(%s)",$key));
				if($is_key_registered){
					$_SESSION['webinar_registered'] = 1;
				}
				activate_widget_plugin('webinar_plugin/webinar-widget.php');
			}
		}
	
		function show_webinar_admin_messages(){
			if(!function_exists('curl_init')){
				$this->show_webinar_messages("Enable Curl in order to make Easy Webinar function properly.", true);
			}
		}
	
		function redirect_if_not_registered(){
			$path = get_bloginfo('url').'/wp-admin/admin.php?page=webinar-options-general';
			if(!function_exists('curl_init')){
				wp_redirect(get_admin_url());
			}
			if(!$this->is_webinar_registered()){
				$this->webinar_options_general();
				$data='Plugin_register';
				return $data;
			}
			
		}
	
		function webinar_logout(){
			unset($_SESSION['webinar_registered']);
		}
	
		function implement_template(){
			global $wpdb;
			if(is_page()){
				$page_id = get_the_ID();
				
				$current_template = get_post_meta($page_id,'_wp_page_template',true);
				$table_name 	  	= $wpdb->prefix.'posts';
				$results 		  		= $wpdb->get_results("SELECT * FROM webinar LEFT JOIN $table_name ON $table_name.webinar_id=webinar.webinar_id_pk WHERE $table_name.ID='$page_id'");

				$selected_template_dir 	 = $results[0]->template_style;
				$selected_template_color = $results[0]->template_color;
				
				$webinar_templates = array('webinar-countdown.php','webinar-error.php','webinar-login.php','webinar-registration.php','webinar-thankyou.php','webinar-video.php','after-webinar-video.php','webinar-live.php');
				
				if(in_array($current_template, $webinar_templates)){
					include WEBINAR_PLUGIN_PATH.$current_template;
					exit;
				}
			}	
		}
	
		function check_permissions(){

			$physical_file_path = $_SERVER['SCRIPT_FILENAME'];
			$physical_file_path = explode('/wp-admin',$physical_file_path);
			$physical_file_path = $physical_file_path[0].'/wp-content/plugins/webinar_plugin/webinar-events-images/add-to-cart.png';
			if (file_exists($physical_file_path) && (is_writable($physical_file_path)===false)){
				$this->show_webinar_messages("Easy Webinar doesn't have sufficient permissions to create or upload a file. Please ask your webmaster to allow recursive write permissions to <a href='#' onclick='return false' >".WEBINAR_PLUGIN_URL."</a> directory",true);
			}
		}
		function check_infusionsoft_status(){
		$api_credentials = get_option('ewp_emarketing_status');
            if($api_credentials == 1) {
            	$this->show_webinar_messages('Infusionsoft credentials are not valid.Please check and fill the correct credentials <a href="' . EASYWEBINAR_WEBSITE_LINK . '/integration-infusionsoft" target="_blank">Click Here</a>');
            }
		}
	    function check_sendgrid_integration(){
	    	
            $api_credentials = get_option('sendGrid_credentials');
            if($api_credentials == '') {
            	$this->show_webinar_messages('SendGrid Integration is available.It is recommendated to you integrate sendgrid  followed by this link.<a href="' . EASYWEBINAR_WEBSITE_LINK . '/sendgrid-integration" target="_blank">Click Here</a>');
            }
			
		}
	    function check_sendgrid_status(){
	    	
            $api_status = get_option('sendgrid_status');
            if($api_status == 1) {
            	$this->show_webinar_messages('SendGrid credentials are not valid.Please check and fill the correct credentials <a href="' . EASYWEBINAR_WEBSITE_LINK . '/sendgrid-integration" target="_blank">Click Here</a>');
            }
			
		} 
		
		function ewpRestoreAttachments(){
		  if(is_dir(EWP_TEMP_PATH)){
		  $src = EWP_TEMP_PATH;
		  $dst = EWP_ATTACHMENTS_PATH;
		  $ewp_temp_dir = @opendir($src); 
			  while(false !== ( $file = @readdir($ewp_temp_dir)) ) { 
				  if (( $file != '.' ) && ( $file != '..' )) { 
					  if ( is_dir($src . '/' . $file) ) { 
						  $this->ewpRestoreAttachments($src . '/' . $file,$dst . '/' . $file); 
					  } 
					  else { 
						  @copy($src . '/' . $file,$dst . '/' . $file); 
					  } 
				  } 
			  } 
			  closedir($ewp_temp_dir);
			  $this->deleteTempDirectory($src);
		 }
	  }
	  
	  function deleteTempDirectory($dirname) {
		   if (is_dir($dirname))
			  $dir_handle = @opendir($dirname);
		   if (!$dir_handle)
			  return false;
		   while($file = readdir($dir_handle)) {
			  if ($file != "." && $file != "..") {
				 if (!is_dir($dirname."/".$file))
					unlink($dirname."/".$file);
				 else
					$this->deleteTempDirectory($dirname.'/'.$file);    
			  }
		   }
		   closedir($dir_handle);
		   rmdir($dirname);
		   return true;
	 }
	 
	}
}
$ob_webinar_plugin	=	new webinar_plugin();
//webinar_functions::send_email_cron(); 
if (!class_exists('ewp_auto_update')){
 class ewp_auto_update{
		
		public $current_version;
		public $update_path;
		public $plugin_slug;
		public $slug;
		public $message;

		function __construct($current_version, $update_path, $plugin_slug){
			$this->update_path = $update_path;
			$this->current_version = $current_version;
			$this->plugin_slug = $plugin_slug;
			list($directory, $file) = explode('/', $plugin_slug);
			$this->slug = $directory;

			add_filter('pre_set_site_transient_update_plugins', array(&$this,'check_update'));
			add_filter('site_transient_update_plugins', array($this,'check_update')); //WP 3.0+
			add_filter('transient_update_plugins', array($this,'check_update')); //WP 2.8+
			add_filter('plugins_api', array(&$this,'check_info'),10,3);
		}
		

		public function check_update($transient){
			if (empty($transient->checked)){
				return $transient;
			}
			
			$remote_version = $this->getRemote_version();

			if(version_compare($this->current_version, $remote_version, '<')){
				$obj = new stdClass();
				$obj->slug = $this->slug;
				$obj->new_version = $remote_version;
				$obj->url = $this->update_path;
				$obj->package = $this->update_path;
				$transient->response[$this->plugin_slug] = $obj;
				$this->ewpInitaiteUpdateMsg($remote_version);
				$this->backupEwpAttcahments(EWP_ATTACHMENTS_PATH,EWP_TEMP_PATH);
			}
			return $transient;
		}

		public function check_info($false, $action, $arg){
			if($arg->slug === $this->slug){
				$information = $this->getRemote_information();
				return $information;
			}
		return false;
		}

		public function getRemote_version(){
			$request = wp_remote_post($this->update_path, array('body' => array('action' => 'ewp-version')));
			if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200){
				return $request['body'];
				}
			return false;
		}

		public function getRemote_information(){
			$request = wp_remote_post($this->update_path, array('body' => array('action' => 'ewp-info')));
			if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200){
				return unserialize($request['body']);
			}
		return false;
		}

		public function getRemote_license(){
			$request = wp_remote_post($this->update_path, array('body' => array('action' => 'ewp-license')));
			if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200){
				return $request['body'];
			}
			 return false;
		}
		
		private function backupEwpAttcahments($src,$dst){
			$dir = @opendir($src); 
			if(mkdir($dst,0777)){
				while(false !== ( $file = readdir($dir)) ) { 
					if (( $file != '.' ) && ( $file != '..' )) { 
						if ( is_dir($src . '/' . $file) ) { 
							@$this->backupEwpAttcahments($src . '/' . $file,$dst . '/' . $file); 
						} 
						else { 
							@copy($src . '/' . $file,$dst . '/' . $file); 
						} 
					} 
				} 
				closedir($dir);
			}
		}
		
		public function ewpInitaiteUpdateMsg($remote_version){
			$ewp_plugin = 'webinar_plugin/webinar-plugin.php';
     	$ewp_upgrade_path = wp_nonce_url('update.php?action=upgrade-plugin&amp;plugin=' . urlencode($ewp_plugin), 'upgrade-plugin_' . $ewp_plugin);
     	$this->message = 'There is a new version EWP('.$remote_version.') of EasyWebinar available.<br>We highly recommend to <a href="' . $ewp_upgrade_path . '">UPDATE NOW</a> automatically or visit <a target="_blank" href="'.EASYWEBINAR_WEBSITE_LINK.'/list-your-domain">EWP ADMIN</a> section to download the plugin for manual update.';
			add_action('admin_notices',  array(&$this, 'ewpShowMessage'));
     }
		
		public function ewpShowMessage() {
      echo '<div id="ewp-update-message" class="updated"><p><strong>'.$this->message.'</strong></p></div>';
    }
 }

	add_action('init', 'check_for_ewp_update');
	function check_for_ewp_update(){
		$wptuts_plugin_current_version = get_ewp_plugin_version_from_file();
		$wptuts_plugin_remote_path = EWP_UPDATE_FILE_PATH;
		$wptuts_plugin_slug = plugin_basename(__FILE__);
		
		new ewp_auto_update($wptuts_plugin_current_version, $wptuts_plugin_remote_path, $wptuts_plugin_slug);
	}
}

	function get_ewp_plugin_version_from_file() {
		require_once(ABSPATH.'wp-admin/includes/upgrade.php');
		$ewp_plugin_data = get_plugin_data( __FILE__ );
		$ewp_plugin_version = str_replace(')','',substr($ewp_plugin_data['Version'],4));
		return $ewp_plugin_version;
	}
	
	add_action('admin_init', 'ewp_reactivate_for_autoupdate' );
	function ewp_reactivate_for_autoupdate(){
		 global $wpdb;
		 $is_ewp_active = is_plugin_active('webinar_plugin/webinar-plugin.php');
		 $ewp_previous_version = get_option('ewp_current_version');
		 $ewp_current_version  = get_ewp_plugin_version_from_file();
		 if($is_ewp_active){
			 if($ewp_previous_version){
				if(version_compare($ewp_previous_version, $ewp_current_version,'<')){
					$ob_webinar_plugin	=	new webinar_plugin();
					$ob_webinar_plugin->ewpRestoreAttachments();
					$ob_webinar_plugin->make_dynamic_tables_for_webinar();
					if($wpdb->get_var("SHOW TABLES LIKE 'webinar_license'")){
						$key = $wpdb->get_var("SELECT * FROM webinar_license LIMIT 1");
						if($key){
						$referer = get_site_url();
						$ob_webinar_plugin->get_key_info($key, $referer);
						update_option('ewp_current_version', $ewp_current_version);
						activate_widget_plugin('webinar_plugin/webinar-widget.php' );
						}
					}
				}elseif(version_compare($ewp_previous_version, $ewp_current_version, '>')){
					update_option('ewp_current_version', $ewp_current_version);
				}
			 }
		 }
	}

	add_action('admin_notices', array($ob_webinar_plugin, 'check_permissions'));
	 add_action('admin_notices', array($ob_webinar_plugin, 'check_sendgrid_integration'));
	add_action('admin_notices', array($ob_webinar_plugin, 'check_sendgrid_status')); 
	add_action('admin_notices', array($ob_webinar_plugin, 'show_webinar_admin_messages'));
	add_action('admin_notices',array($ob_webinar_plugin, 'check_infusionsoft_status'));
	add_action('wp_logout', array($ob_webinar_plugin, 'webinar_logout'));
	add_action('template_redirect', array($ob_webinar_plugin, 'implement_template'));
	
	if(isset($ob_webinar_plugin)){
		register_activation_hook(__FILE__,array(&$ob_webinar_plugin,'make_dynamic_tables_for_webinar') );
	}
	
	function activate_widget_plugin($plugin_to_activate){
		$plugins = get_option('active_plugins'); 
		if ($plugins){
			if (!in_array($plugin_to_activate, $plugins ) ) {
				array_push( $plugins, $plugin_to_activate );
				update_option('active_plugins', $plugins );
			}
		}
	}
	
	
	register_activation_hook( __FILE__, 'webinar_activate' );
	register_deactivation_hook( __FILE__, 'webinar_deactivate');
	
	function webinar_activate(){
		
	}
	
	function webinar_deactivate(){
		global $wpdb;
	 	if($wpdb->get_var("SHOW TABLES LIKE 'webinar_license'")=='webinar_license')
		$wpdb->query("DELETE FROM webinar_license");
	  $_SESSION['webinar_registered'] = 0 ;
		unset($_SESSION['webinar_registered']);
		delete_option('ewp_current_version');
	}
	
	/* Exclude webinar Pages from wordpress menu bar */ 
	add_filter("wp_list_pages_excludes", "ewp_list_pages_excludes");
	function ewp_list_pages_excludes($excluded_ewp_page_ids){
		$ob_webinar_db_interaction = new webinar_db_interaction();
		
		$all_webinar_pages_ids = $ob_webinar_db_interaction->get_all_webinar_page_ids();
		foreach($all_webinar_pages_ids as $webinar_page_id){
			$excluded_ewp_page_ids[] = $webinar_page_id->ID;
		}
		
		$excluded_ewp_page_ids[] = $ob_webinar_db_interaction->get_countdown_page_id();
		$excluded_ewp_page_ids[] = $ob_webinar_db_interaction->get_error_page_id();
	
		 return $excluded_ewp_page_ids;
	}


	/* Hide ewp pages from wp-admin section */
	add_action( 'pre_get_posts','exclude_ewp_pages');
	function exclude_ewp_pages($query) {
		$ob_webinar_db_interaction = new webinar_db_interaction();
		$all_webinar_pages_ids = $ob_webinar_db_interaction->get_all_webinar_page_ids();
		foreach($all_webinar_pages_ids as $webinar_page_id){
			$excluded_ewp_page_ids[] = $webinar_page_id->ID;
		}
		$excluded_ewp_page_ids[] = $ob_webinar_db_interaction->get_countdown_page_id();
		$excluded_ewp_page_ids[] = $ob_webinar_db_interaction->get_error_page_id();
		if(!is_admin())return $query;
		global $pagenow;
		if( 'edit.php' == $pagenow && ( get_query_var('post_type') && 'page' == get_query_var('post_type') ) )
			$query->set( 'post__not_in', $excluded_ewp_page_ids ); 
		return $query;
	}
	
	
	 /**
		* Cron hook
		* for get_webinar_notification_cron, send_email_cron
		* 
		*/
	
		add_filter('cron_schedules','ewp_get_webinar_cron');
		function ewp_get_webinar_cron( $schedules ) {
			 $schedules['ewp_auto_cron_every_minute'] = array( 
			 'interval'=>60,
				'display' => __('EWP Cron on every Minute')
			 );
			 return $schedules; 
		}
		 
		//$ob_webinar_functions = new webinar_functions();
		
		add_action('ewp_get_webinar_notification_cron_hook', array('webinar_functions','get_webinar_notification_cron') );
		add_action('ewp_send_email_cron_hook', array('webinar_functions','send_email_cron') );
					
		if(!wp_next_scheduled('ewp_get_webinar_notification_cron_hook' ) ) {
			 wp_schedule_event(time(),'ewp_auto_cron_every_minute','ewp_get_webinar_notification_cron_hook');
		}
					
		if(!wp_next_scheduled('ewp_send_email_cron_hook') ) {
			 wp_schedule_event(time(),'ewp_auto_cron_every_minute','ewp_send_email_cron_hook');
		} 
		
	include_once('includes/ajax-callbacks.php');	
	 require_once('lib/sendgrid/lib/SendGrid.php');
	require_once('lib/sendgrid/vendor/autoload.php');
?>