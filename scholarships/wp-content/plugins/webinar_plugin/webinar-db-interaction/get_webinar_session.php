<?php
	header('Content-Type: application/json');
	require_once('../../../../wp-load.php');
	require_once('webinar-db-interaction.php');
	require_once('webinar-functions.php');

	date_default_timezone_set('UTC');
	
	global $wpdb;
	$ob_webinar_db_interaction= new webinar_db_interaction();
	$ob_webinar_functions		 	= new webinar_functions();
	
	    $webinar_timezone_id  = (int)$_GET['webinar_timezone_id'];  
		$webinar_timezone_detail= $ob_webinar_db_interaction->get_timezone_detail($webinar_timezone_id);
		$webinar_timezone_name  = stripslashes($webinar_timezone_detail[0]->timezone_name);
		
		$web_date	    		 			= date('Y-m-d',strtotime($ob_webinar_functions->_sanitise_string($_GET['webinar_date'])));
		
		$day_name	       		 		=		date("N", strtotime($web_date));
		$webinar_id 			 			=		(int)$_GET['webinar_id'];
		$max_attendee 			 		=		(int)$_GET['max_attendee'];
		$webinar_schedule_id 	 	=		(int)$_GET['webinar_schedule'];
		$webinar_video_length   =   (int)$_GET['webinar_video_length'];
		
		$webinar_details				=	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
		$is_allowed_end_sessions=  (int)$webinar_details[0]->allow_unexpired_end_sessions;
		$webinar_endpoint 			=  ($is_allowed_end_sessions==1) ? $webinar_video_length : 0;
		
		$timezone_operation		 	=		$ob_webinar_functions->_sanitise_string($_GET['timezone_operation']);
		$timezone_difference	 	=		$ob_webinar_functions->_sanitise_string($_GET['timezone_difference']);
       

		$timezone_date_time		 	=		$ob_webinar_db_interaction->get_date_from_timezone($timezone_difference,$timezone_operation);	
		$timezone_time			 		=		$timezone_date_time['timezone_time'];
		$timezone_date			 		=		$timezone_date_time['timezone_date'];
		
 		$user_time_gmt          =   $ob_webinar_functions->_sanitise_string($_GET['user_time_gmt']);
		$user_date_gmt          =   $ob_webinar_functions->_sanitise_string($_GET['user_date_gmt']);
		$user_date_time_gmt     =   $user_date_gmt." ".$user_time_gmt;
		
		
		$everyday_session_detail=	$ob_webinar_db_interaction->get_registered_attendees_for_everyday($webinar_id,$web_date);
		
		if(count($everyday_session_detail)>0){
			$invalid_sessions		=	"";	
			$total_rows					=	count($everyday_session_detail);
			$i=1;
			foreach($everyday_session_detail as $session_detail){
				if($session_detail->counts==$max_attendee){
					$invalid_sessions.=	$session_detail->webinar_schedule_id_fk;
					if($i!=$total_rows){
						$invalid_sessions.=	",";	
					}
					$i++;
				}	
				if($session_detail->counts!=0){
					$scheduled_date	= $web_date;
				}
			}
			$last_char = $invalid_sessions[strlen($invalid_sessions)-1];
			if($last_char==","){
				$invalid_sessions	=	substr($invalid_sessions,0,strlen($invalid_sessions)-1);	
			}
			if($webinar_schedule_id==4){
				
				if($max_attendee==0){
					$condition		=	 '';
				}else {
						$condition	=	 'having count<'.$max_attendee;
				}
				
				$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
			}else {
				$webinar_sessions	=	$ob_webinar_db_interaction->get_unexpired_webinar_sessions($webinar_id,$invalid_sessions,$webinar_schedule_id,$web_date);
			}
			
			$webinar_date	=	$scheduled_date;			
		}else{
			
			if($webinar_schedule_id==4){
			
				if($max_attendee==0){
					$condition		=	 '';
				}else {
						$condition	=	 'having count<'.$max_attendee;
				}
					
				$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
			}else {
				$webinar_sessions =	$ob_webinar_db_interaction->get_today_unexpired_session_for_everyday_mod($webinar_id,$max_attendee,$webinar_schedule_id,$web_date);
			}
			
			if(count($webinar_sessions)==0){
				 $webinar_date = $web_date;
				if($webinar_schedule_id==4){
					
					if($max_attendee==0){
						$condition	=	 '';
					}else {
						$condition	=	 'having count<'.$max_attendee;
					}
					
					$webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
				}else {
					$webinar_sessions	=	$ob_webinar_db_interaction->get_webinar_sessions($webinar_id,$webinar_schedule_id,$web_date);
					
				}
			}else{
				$webinar_date = $web_date;
		
			}
		}
	
		$valid_sessions	=	array();
		
        
		
		
		if($webinar_timezone_id>0)
		{
		  
		  for($i=0;$i<count($webinar_sessions);$i++){
		  
		      $date = $web_date." ".$webinar_sessions[$i]->start_time;
			  $dateinsec = strtotime($date);
			  $newdate = $dateinsec+$webinar_endpoint;
			  $date_in_format = date('Y-m-d H:i:s',$newdate);
			  $webinar_time_in_gmt = $ob_webinar_db_interaction->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
			  
			if( strtotime($webinar_time_in_gmt) > strtotime($user_date_time_gmt)){
				
				$valid_sessions[]	=	$webinar_sessions[$i];
			  }	
			
			}            
		  }
		 else
		  {
		   for($i=0;$i<count($webinar_sessions);$i++)
		   {
		  
			  $date = $web_date." ".$webinar_sessions[$i]->start_time;
			  $dateinsec = strtotime($date);
			  $newdate = $dateinsec+$webinar_endpoint;
			  $date_in_format = date('Y-m-d H:i:s',$newdate);
			  
			
			 if( strtotime($date_in_format) > strtotime($timezone_date." ".$timezone_time)){
				$valid_sessions[]	=	$webinar_sessions[$i];
			}
		  }
		}
		 
		
		  
		if(count($valid_sessions)>0) {
			header('HTTP/1.1 200 OK');
			echo $_GET['callback']. '('.json_encode($valid_sessions). ')';
			exit;
		}else if(count($valid_sessions)<1){
			header('HTTP/1.1 200 OK'); 
			echo $_GET['callback']. '('.json_encode("2"). ')';
			exit;
		}else{
			header('HTTP/1.1 200 OK'); 
			echo $_GET['callback']. '('.json_encode("1"). ')';
			exit;
		}
?>