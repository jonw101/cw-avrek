<?php
	header('Content-Type: application/json');
	require_once('../../../../wp-load.php');
	require_once('webinar-db-interaction.php');
	require_once('webinar-functions.php');
	
	$ob_webinar_db_interaction	=		new webinar_db_interaction();
	$ob_webinar_functions				=		new webinar_functions();
	
	$webinar_selectes_date			=		$ob_webinar_functions->_sanitise_string($_GET['wbr_sel_dt']);
	$webinar_selected_time			=		$ob_webinar_functions->_sanitise_string($_GET['wbr_sel_tm']);
	$webinar_timezone_id 				=		(int)$_GET['wbr_sel_tz_id'];
	
	$date_time									=		$webinar_selectes_date." ".$webinar_selected_time;
	$utc_timezone								=		'GMT';
	$webinar_timezone_name_by_id=		$ob_webinar_db_interaction->get_timezone_name($webinar_timezone_id);
	$webinar_timezone_name			=		$webinar_timezone_name_by_id[0]->timezone_name;
	$utc_date_and_time					=		$ob_webinar_db_interaction->convert_time_zone($date_time,$webinar_timezone_name,$utc_timezone);
	
	header('HTTP/1.1 200 OK');
	echo $_GET['callback']. '('.json_encode($utc_date_and_time). ')';
	exit;
?>