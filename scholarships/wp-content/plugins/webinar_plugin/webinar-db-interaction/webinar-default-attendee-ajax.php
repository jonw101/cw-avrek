<?php 
	header('Content-Type: application/json');
	require_once('../../../../wp-load.php');
	require_once('webinar-db-interaction.php');
	require_once('webinar-functions.php');
	
	$ob_webinar_db_interaction=	new webinar_db_interaction();
	$ob_webinar_functions	  	=	new webinar_functions();
	
	$response_variable		=	0;	

	$webinar_id						=	$ob_webinar_functions->_sanitise_email($_GET["webinar_id"]);
	$attendee_ids					=	$ob_webinar_functions->_esc_santise_html($_GET["attendee_ids"]);
	$attendee_operation		=	(int)$_GET["attendee_operation"];
	$max_limit						=	$ob_webinar_functions->_sanitise_email($_GET["max_limit"]);
	$attendee_operation 	=	1;

	$new_attendee_name		=	array();	
	$new_attendee_ids			=	array();	
	$delete_array					=	array();	

		
	if($attendee_operation==1){
		$count 				= array_filter(explode(",",$attendee_ids));
		$attendee_ids = implode(",",$count); 
    $c 						= count($count );
		if($c<=$max_limit or $max_limit==0){
			$new_default_attendees		=	$ob_webinar_db_interaction->get_two_new_default_attedees($webinar_id,$attendee_ids);
			foreach($new_default_attendees as $attendees){
				$new_attendee_name[]		=	$attendees->attendee_name;
				$new_attendee_ids[]			=	$attendees->attendee_id;
			}
			$ids			=	implode(",",$new_attendee_ids);	
			$new_ids	=	$attendee_ids.",".$ids;
		}else {
			 $ids 		= '';
			 $new_ids = '';
		}
		header('HTTP/1.1 200 OK');
		echo $_GET['callback']. '('.json_encode(array('attendee_add'=>1,'new_attendee_name'=>$new_attendee_name,'new_attendee_ids'=>$new_attendee_ids,'new_ids'=>$new_ids,'set_attendee_operation'=>2)). ')';
	}else{
		$default_attendee_array		=	array_filter(explode(",",$attendee_ids));	
		shuffle($default_attendee_array);		
		for($i=0;$i<=1;$i++){
			$delete_array[]					=	$default_attendee_array[$i];		
			unset($default_attendee_array[$i]);
		}
		$remaining_attendees			=	implode(",",$default_attendee_array);
		header('HTTP/1.1 200 OK');
		echo $_GET['callback']. '('.json_encode(array('attendee_add'=>0,'new_ids'=>$remaining_attendees,'deleted_ids'=>$delete_array,'set_attendee_operation'=>1)). ')';
	}
?>