<?php  
	header('Content-Type: application/json');
    if(!session_id())session_start();
	require_once('../../../../wp-load.php');
	require_once('webinar-db-interaction.php');
	require_once('webinar-functions.php');
	
	$ob_webinar_db_interaction  = new webinar_db_interaction();
	$ob_webinar_functions	    = new webinar_functions();

	$webinar_id							=	(int)$_GET['webinar_id'];
	$timezone_difference		=	$ob_webinar_functions->_sanitise_string($_GET['timezone_difference']);
	$timezone_operation			=	$ob_webinar_functions->_sanitise_string($_GET['timezone_operation']);
	$timezone_date_time			=	$ob_webinar_db_interaction->get_date_from_timezone($timezone_difference,$timezone_operation);
	$timezone_date			    =	$timezone_date_time['timezone_date'];
    $timezone_time              = $timezone_date_time['timezone_time'];
	$user_gmt_date              = $ob_webinar_functions->_sanitise_string($_GET['user_date_gmt']);
	$user_gmt_time              = $ob_webinar_functions->_sanitise_string($_GET['user_time_gmt']);
	
	$dates_array 			    = $ob_webinar_db_interaction->get_webinar_slots_for_timezone($webinar_id,$timezone_date,$timezone_time,$user_gmt_date,$user_gmt_time);
	
	//On for Webinar public live page
	$is_flag                    =   $ob_webinar_functions->_sanitise_string($_GET['is_flag']);
	
	if($is_flag == 1)
	{
		$_SESSION['dates_array_live']  = json_encode($dates_array);
		$_SESSION['user_date_time_gmt']= json_encode($user_gmt_date." ".$user_gmt_time);
		$_SESSION['user_date_time']    = json_encode($timezone_date." ".$timezone_time);
	}
	
	header('HTTP/1.1 200 OK');
	echo $_GET['callback']. '('.json_encode($dates_array).')';
	exit;
?>