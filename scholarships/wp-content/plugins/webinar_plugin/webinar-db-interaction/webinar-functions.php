<?php
/**
 *	@description		This class will contains all the common functins used in the webinar
 */
class webinar_functions{
		
	function __construct(){
		date_default_timezone_set("GMT");
		
	}
			
	/**
	 *	@descripton					Following function will extract the attendees id in a array from a variable.
	 *	@param		 Array			This array contains all the ids that needs to be extracted.
	 *	@return		 id_variables	This is variable that will contains all ids seperated by commas get_ids_from_array
	 */
	
	function get_attendee_ids_from_array($logged_in_attendees){
		$return_array			=	array();
		foreach($logged_in_attendees as $attendess){
			$return_array[]	=	$attendess->registration_id_pk;
		}

		$attendee_id			=	"";
		foreach($return_array as $attendee_ids){
			$attendee_id.=$attendee_ids;
			if($attendee_ids!=end($return_array)){
				$attendee_id.=	",";
			}
		}

	return $attendee_id;
	}

	//	End of getttign all ids from array
	
	/**
	 *	@description			Following function will convert time to seconds.
	 *	@param		  time		This is time that we want to convert in seconds.
	 *	@return		  seconds	These are seconds to correspoding  time provided
	 */

	function time_to_sec($time) {
		$hours = substr($time, 0, -6);
		$minutes = substr($time, -5, 2);
		$seconds = substr($time, -2);
		return $hours * 3600 + $minutes * 60 + $seconds;
	}
	//	End of converting time to seconds
	
	
	function generate_random_series($start_number,$end_number,$time,$slots){

		 $difference_number = $start_number-$end_number;

		 $slots_per_min = $slots;
		 $pieces = floor(($time/60)*$slots_per_min);
		 $one_piece_value = ceil($difference_number/$pieces);

		 $a = 0;
		 $min = $one_piece_value;
		 $remaining = $difference_number;
		 $random_numbers = array();
		 while($a<$pieces && $min!=0){
		   $min = 0;
			if($a%2==0){

				if($remaining>=$one_piece_value+($one_piece_value/2)){

					$min 	= $one_piece_value;
					 $max = $one_piece_value+($one_piece_value/2);
				}
			}else{
				if($remaining>=$max){
					$min 	= $one_piece_value-($one_piece_value/2);
					 $max = $one_piece_value;
				}
			}
			if($min!=0)
				 $random = rand($min,$max);
			else
				$random  = $remaining;

			$start_number = $start_number - $random;
			$remaining 		= $start_number - $end_number;
			$random_numbers[] = $random;
			$a++;
		 }

		 if($remaining!=0){
			if($remaining<$pieces){
				$min_val_keys = array_keys($random_numbers, min($random_numbers));
				$key 					= $min_val_keys[0];
				$random_numbers[$key] = $random_numbers[$key]+$remaining;
			}else{
				$remainder 			 = $remaining%$pieces;
				$is_exactly_divisible = $remainder == 0 ? 1 : 0;
				$number_to_add 	 = $remaining;
				if(!$is_exactly_divisible){
					$number_to_add = $remaining - $remainder;
					$min_val_keys  = array_keys($random_numbers, min($random_numbers));
					$key 					 = $min_val_keys[0];
					$random_numbers[$key] = $random_numbers[$key]+$remainder;
				}

				$number_to_add 	= $number_to_add/$pieces;
				for($i=0;$i<count($random_numbers);$i++){
					$random_numbers[$i] +=  $number_to_add;
				}
			}
		 }
		 return $random_numbers;
	}
	
	
	/*
	*	@Description		This will calculate the start number of scarcity at parcular time in the webinar
	*/
	function calculate_scracity_start_number($time_difference,$current_time,$start_number,$end_number){
				
				$percentage_webinar_over	=		ceil(($current_time/$time_difference)*100)+20;
				$scarcity_number_over			=		ceil(($start_number-$end_number)/100)*$percentage_webinar_over;
				$scarcity_start_number		= 		($start_number-$scarcity_number_over);
				if($scarcity_start_number >= $start_number ){
					return $start_number;
				}else{
					return $scarcity_start_number;
				}
				
	}
	
	function send_email_on_registration($admin_email,$webinarname,$name,$email,$timezone,$date,$time,$login_link=''){
	
		   $subject = 'User registered';
       $message = '<p><b>A user successfully signed up for </b></p><table><tr><th>Webinar Name :</th><td>'.$webinarname.'</td></tr><tr><th>Attendee Name :</th><td>'.$name.'</td></tr><tr><th>Attendee Email :</th><td>'.$email.'</td></tr><tr><th>Selected Webinar Timezone :</th><td>'.$timezone.'</td></tr><tr><th>Selected Webinar Date :</th><td>'.$date.'</td></tr><tr><th>Selected Webinar Time :</th><td>'.$time.'</td></tr><tr><th>Webinar Link :</th><td>'.$login_link.'</td></tr></table>';
		   $headers = "MIME-Version: 1.0" . "\r\n";
       $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
       $headers .= "From: ".$name." <".$email.">";
       wp_mail($admin_email,$subject,$message,$headers);
		
	}
	
	function mail_webinar_question($admin_email,$email,$name,$question){
			
			$subject		= "Regarding webinar questions";
			$message    = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'><!--DVID=00001157-->
			<HTML>
				<head></head>
				<BODY>
					<div>
						<div><span>Name: </span><span style='margin-left:20px;'> ".$name."</span></div>	
						<div><span>Email: </span><span style='margin-left:20px;'> ".$email."</span></div>		
						<div><span>Question: </span><span> ".$question."</span></div>		
					</div>
				</BODY>
			</HTML>"; 
		
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: ".$name." <".$email.">\r\n";
		
			if(wp_mail($admin_email,$subject,$message,$headers)) {
				return 1;
			}else { 
				return 0;
			}
	
	}
	
		
	function get_template_option($template_option,$default){
		echo empty($template_option) ? $default : $template_option ;
	}
	
	function limit_words($string,$word_limit) { 
			$words = explode(' ', $this->_esc_decode_html($string)); 
			return implode(' ', array_slice($words, 0, $word_limit)); 
	}
	
	
	function is_valid_date($date){
	 	list($year,$month,$day) = explode('-',$date);
		if (is_numeric($year) && is_numeric($month) && is_numeric($day))
			return checkdate($month,$day,$year);
		return false;           
	}         
	
	
	function is_valid_time($time){
		list($hour,$minute,$second) = explode(':',$time);
		if(is_numeric($hour) && is_numeric($minute) && is_numeric($second))
			return true;
		return false;
	}
	
	function _esc_sanitise_string($str){
		return esc_sql(filter_var($str,FILTER_SANITIZE_STRING));
	}
	
	function _esc_santise_html($html){
		return esc_sql(htmlspecialchars($html,ENT_QUOTES));
	}
	
	function _sanitise_string($str){
		return filter_var($str,FILTER_SANITIZE_STRING);
	}
	
	function _sanitise_email($email){
		return filter_var($email,FILTER_SANITIZE_EMAIL);
	}
	
	function _esc_decode_string($str){
		return stripslashes($str);
	}
	
	function _esc_decode_html($html){
		return stripslashes(html_entity_decode($html,ENT_QUOTES)); 
	}
	
	function _decode_html($html){
		return stripslashes(html_entity_decode(html_entity_decode($html,ENT_QUOTES))); 
	}
	
	function _validate_email($email){
		return filter_var($email,FILTER_VALIDATE_EMAIL);
	}
	
	function get_video_type($video_url){
		
		if(preg_match('/youtube.com/', $video_url)){
			return 'youtube';
		}elseif(preg_match('/vimeo.com/', $video_url)){
			return 'vimeo';
		}elseif(pathinfo($video_url, PATHINFO_EXTENSION)=='mp4' || pathinfo($video_url, PATHINFO_EXTENSION)=='MP4'){
			return 'mp4';
		}elseif(pathinfo($video_url, PATHINFO_EXTENSION)=='flv' || pathinfo($video_url, PATHINFO_EXTENSION)=='FLV'){
			return 'flv';
		}elseif(pathinfo($video_url, PATHINFO_EXTENSION)=='mp3'){
			return 'mp3';
		}else{
			return '';
		}

	}
	
	function purge_cache(){
		header("Expires: Tue, 03 Aug 2010 01:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('X-Random-Header: ' . (rand()+time()));
	}
	
	
	function get_device_type(){
		
		$iPod 		= 	stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone 	= 	stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad 		= 	stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		
		if(stripos($_SERVER['HTTP_USER_AGENT'],"Android") && stripos($_SERVER['HTTP_USER_AGENT'],"mobile")){
		$Android 	   = true;
		}else if(stripos($_SERVER['HTTP_USER_AGENT'],"Android")){
		$Android 	   = false;
		$AndroidTablet = true;
		}else{
		$Android 	   = false;
		$AndroidTablet = false;
		}	
		
		$device_type   = array();
		
		$device_type['iPod']   				=  $iPod;
		$device_type['iPhone']   			=  $iPhone;
		$device_type['iPad']   				=  $iPad;
		$device_type['Android']  		 	=  $Android;
		$device_type['AndroidTablet'] =  $AndroidTablet;
		
		return $device_type;
	}
        
	/*
	 * Cron Function: get_webinar_notification_cron
	 * 
	 * @global type $wpdb
	 */

	public static function get_webinar_notification_cron(){
		// the message
		/* $msg = "First line of text\nSecond line of text";
		
		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);
		
		// send email
		mail("tested0312@gmail.com","My subject",$msg); */
		 //date_default_timezone_set("Asia/Kolkata");
			 $fp = fopen('data.txt', 'w');
			 fwrite($fp,'gg');
		     fclose($fp);
		/* echo 'hello';
		exit; */
		   
		   
				date_default_timezone_set("GMT");
				
				$ob_webinar_db_interaction	=	new webinar_db_interaction();
				
				$cur_date						=	date("Y-m-d");
				$cur_time						=	date("H:i");
				$timing_id_pk_array = array();

				$get_all_notification_type =	$ob_webinar_db_interaction->get_notification_detail($cur_date,$cur_time);
               
				if(count($get_all_notification_type)>0){
           
         
								$message_subject	=	"";
								$message_body			=	"";
                               //print_r($get_all_notification_type);
                             
								foreach($get_all_notification_type as $get_notification_types){
                                
								 $timing_id_pk_array[]			= $get_notification_types->timing_id_pk;
								 $attendee_email						=	$get_notification_types->attendee_email_id;
								 if(!empty($attendee_email)){	
												$message_subject		=	$get_notification_types->subject;
												$message_body				=	urldecode($get_notification_types->message);
												$webinar_id					=	$get_notification_types->webinar_id;	
												$webinar_name				=	$get_notification_types->webinar_main_topic;
												$webinar_time				=	$get_notification_types->webinar_time;
												$webinar_date				=	$get_notification_types->webinar_date;
												$webinar_real_time	=	$get_notification_types->webinar_real_time;
												$webinar_real_date	=	$get_notification_types->webinar_real_date;

												$presenter					 	= empty($get_notification_types->presenters_name) ? $get_notification_types->sender_name : $get_notification_types->presenters_name;
												$presenter_email	 	 	= empty($get_notification_types->notification_from_address) ? $get_notification_types->admin_email_value : $get_notification_types->notification_from_address;
												$presenter_name			 	= empty($get_notification_types->sender_name) ? $presenter : $get_notification_types->sender_name;
												$presenter_name_email = $presenter_name.' <'.$presenter_email.'>';
												
												$schedule_ids				=	$get_notification_types->webinar_schedule_id_fk;
								                 $affilate_param = $get_notification_types->affilate_param;
                                                 $affilate_param_val = $get_notification_types->affilate_param_val;
												    if($affilate_param_val != '' && $affilate_param != ''){
												    	$affilate_param_query = '&' . $affilate_param  . '=' . $affilate_param_val;
												    }
												    else {
												    	$affilate_param_query = '';
												    	$affilate_param_val = '';
												    	$affilate_param ='';
												    }
												$attedee_name				=	$get_notification_types->attendee_name;
												$attendee_key				=	$get_notification_types->key;

												$webinar_pages					 = 	$ob_webinar_db_interaction->get_webinar_all_pages_links($webinar_id);
												
												$login_link							 =	add_query_arg(array('key'=>$attendee_key,$affilate_param => $affilate_param_val),$webinar_pages['login_page']);
												if($affilate_param_val != '' && $affilate_param != ''){
												$replay_link						 =	add_query_arg(array('attendee'=>$attendee_key,$affilate_param => $affilate_param_val),$webinar_pages['after_webinar_page']);
												$live_link				  		 =	$webinar_pages['live_page'];
												$live_link_as_countdown  =  add_query_arg(array('countdown'=>'',$affilate_param => $affilate_param_val),$live_link);
												$live_link = 	add_query_arg(array($affilate_param => $affilate_param_val),$live_link);
												}
												else{
													$replay_link						 =	add_query_arg(array('attendee'=>$attendee_key),$webinar_pages['after_webinar_page']);
												$live_link				  		 =	$webinar_pages['live_page'];
												$live_link_as_countdown  =  add_query_arg(array('countdown'=>''),$live_link);
												}
												$replace_name_in_subject					=	str_replace("#USER_NAME#",$attedee_name,$message_subject);
												$replace_name_in_body							=	str_replace("#USER_NAME#",$attedee_name,$message_body);	
												$replace_description_in_subject		=	str_replace("#WEBINAR_TOPIC#",$webinar_name,$replace_name_in_subject);
												$replace_description_in_body			=	str_replace("#WEBINAR_TOPIC#",$webinar_name,$replace_name_in_body);
												$replace_presenter_in_subject			=	str_replace("#PRESENTER_NAME#",$presenter,$replace_description_in_subject);			
												$replace_presenter_in_body				=	str_replace("#PRESENTER_NAME#",$presenter,$replace_description_in_body);	
												$replace_login_link_in_subject		=	str_replace("#WEBINAR_LINK#",$login_link,$replace_presenter_in_subject);			
												$replace_login_link_in_body				=	str_replace("#WEBINAR_LINK#",$login_link,$replace_presenter_in_body);	
												$replace_replay_link_in_subject		=	str_replace("#WEBINAR_REPLAY#",$replay_link,$replace_login_link_in_subject);			
												$replace_replay_link_in_body			=	str_replace("#WEBINAR_REPLAY#",$replay_link,$replace_login_link_in_body);
												$replace_live_link_in_subject			=	str_replace("#WEBINAR_LIVE#",$live_link,$replace_replay_link_in_subject);			
												$replace_live_link_in_body				=	str_replace("#WEBINAR_LIVE#",$live_link,$replace_replay_link_in_body);
												$replace_live_c_link_in_subject		=	str_replace("#WEBINAR_LIVE_COUNTDOWN#",$live_link_as_countdown,$replace_live_link_in_subject);			
												$replace_video_c_link_in_body			=	str_replace("#WEBINAR_LIVE_COUNTDOWN#",$live_link_as_countdown,$replace_live_link_in_body);
												$replace_date_in_subject					=	str_replace("#WEBINAR_DATE#",$webinar_real_date,$replace_live_c_link_in_subject);	
												$replace_date_in_body							=	str_replace("#WEBINAR_DATE#",$webinar_real_date,$replace_video_c_link_in_body);
												$replace_time_in_subject					=	str_replace("#WEBINAR_TIME#",$webinar_real_time,$replace_date_in_subject);	
												$replace_time_in_body							=	str_replace("#WEBINAR_TIME#",$webinar_real_time,$replace_date_in_body);

												$row_id = $ob_webinar_db_interaction->save_scheduled_mail_to_send($attendee_email,$presenter_name_email,$replace_time_in_subject,urlencode($replace_time_in_body));	
								 }

								}
												if(count($timing_id_pk_array)>0){
												$update_notification_cron_details =   $ob_webinar_db_interaction->update_notification_cron_details($timing_id_pk_array);
												}

				}
				/* $delete_notification_cron_detail	=	$ob_webinar_db_interaction->delete_notification_cron_detail($cur_date,$cur_time); */
		
		 }
       
	 
	 /**
		* Cron Function : send_email_cron
		* 
		* @global type $wpdb
		*/

	 public static function send_email_cron(){
	 
	 	 $sendgrid_credentials = unserialize(get_option("sendGrid_credentials"));
	 $sendgrid_username 	 = $sendgrid_credentials['sendgrid']['username'];
	 	$sendgrid_password	 = $sendgrid_credentials['sendgrid']['password'];
	 	//$sendgrid_password = 'df';
	 	$response_status = '';
	 	$sendgrid_status = get_option('sendgrid_status');
	 	 
				 $delete_sent_notification_id_array	=	array();
				 $update_sent_notification_id_array	=	array();
				 $ob_webinar_db_interaction         =	new webinar_db_interaction();

				 $attendees_detail	=		$ob_webinar_db_interaction->get_attendee_to_send_email();
				
                  
				 foreach($attendees_detail as $attendee_data){
					 $update_sent_notification_id_array[]=$attendee_data->notification_sent_to_id_pk;
				 }

				 if(!empty($update_sent_notification_id_array)){
						$ob_webinar_db_interaction->update_sent_email_notifications_stat($update_sent_notification_id_array);
				 }
				 if($sendgrid_username != '' && $sendgrid_password != '' && $sendgrid_status == 0) {
                 $notification_sent_to = array();
                 $notification_sent_body = array();
                 $notification_sent_from = array();
                 $notification_sent_subject = array();
				 foreach($attendees_detail as $attendee){
				 	$body 		 = stripslashes($attendee->body);
				 	$body 		 = urldecode($body);
				 	$notification_sent_from[0] = stripslashes($attendee->email_from);
				 	$notification_sent_subject[] = $attendee->subject;
				 	$notification_sent_body[] = $body;
				 	$notification_sent_to[] = $attendee->email_to;
				 	
					 if(!empty($notification_sent_to)){
								$delete_sent_notification_id_array[]	=	$attendee->notification_sent_to_id_pk;
					 }else{
								$ob_webinar_db_interaction->update_sent_email_notifications_stat_by_id($attendee->notification_sent_to_id_pk);
					 }
				 }
				
				if(strstr($notification_sent_from[0],'<')){
				 $sending_from = chop($notification_sent_from[0],'>');
				 $notification_sent = explode('<',$sending_from);
				}
				else{
					$notification_sent[1] = $notification_sent_from[0];
				}
				if(!empty($notification_sent_to) ) {
				
					
				 $options = array("turn_off_ssl_verification" => true);
				  
				 $sendgrid = new SendGrid($sendgrid_username,$sendgrid_password,$options);
				
				 $names = array("<h1>Hey , we have f email for you</h1>","<h1>Hey , we have  second email for you</h1>","<h1>Hey , we have  third email for you</h1>","<h1>Hey , we have fourth email for you</h1>");
				 $email = new Email();
				 $email->setFrom($notification_sent[1])->
				 setTos($notification_sent_to)->
				 addSubstitution("%name%", $names)->
				 addSubstitution("%messagebody%", $notification_sent_body)->
				 addSubstitution("%messagesubject%", $notification_sent_subject)->
				 setSubject("%messagesubject%")->
				 setText("Hey %name%, we have an email for you")->
				 setHtml("%messagebody%");
				 $response = $sendgrid->send($email);
				
			     
			   	$responseDecode=json_decode($response,true);
			   	  $response_status = $responseDecode['message'];
				 
				 
				 
			     $response_notice = $responseDecode['errors'][0];
			    
				  if($response->message == 'error' && $response_notice == "Bad username / password")
				  {
				  	
				  	$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/verified_sendgrid_status.php'; 
				  	update_option('sendgrid_status',1);
				  	$response_error = $response->errors[0];
				  	$response_message_array['error_message'] = $response_error;
				  	$response_message_array['message'] = $response->message;
				  	$response_message_array['domain'] = site_url();
				  	
				  	 $ch = curl_init ($website_file_url);
		              curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
					  curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
					  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
					  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
					  curl_setopt($ch,CURLOPT_REFERER,site_url());
					  curl_setopt($ch,CURLOPT_POST,true );
					  curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
					  $sendgrid_reponse_error = curl_exec($ch);
					  curl_close($ch);
					  $domain_result = json_decode(purifyJSON($sendgrid_reponse_error));
					  $dd = $domain_result->message;
					  	update_option('sendgrid_status',1);
				  }
				  else if($response->message == 'success'){
				  	update_option('sendgrid_status',0);
				  } 
				}
				 } 
				 
				 if($response_status != 'success'){
				 	
				 	foreach($attendees_detail as $attendee){
				 	if($attendee->email_to != ''){
								$delete_sent_notification_id_array[]	=	$attendee->notification_sent_to_id_pk;
					 }else{
								$ob_webinar_db_interaction->update_sent_email_notifications_stat_by_id($attendee->notification_sent_to_id_pk);
					 }
				 		 $headers  = 'MIME-Version: 1.0' . "\r\n";
				 		 $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
				 		$headers .= "From: ".$attendee->email_from." \r\n";
				 		$body 		 = stripslashes($attendee->body);
				 		$body 		 = urldecode($body);
				 		$is_notification_mail_sent	=	wp_mail($attendee->email_to,$attendee->subject,$body,$headers);
				 		
				 	
				 	}
				 	
				 }
				 $ob_webinar_db_interaction->delete_sent_email_notifications($delete_sent_notification_id_array);
   } 
}
?>