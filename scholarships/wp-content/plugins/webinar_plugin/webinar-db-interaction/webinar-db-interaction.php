<?php
class webinar_db_interaction{
	//	Initliazed the constructor
	function __construct(){
		date_default_timezone_set('GMT');
	}

	//	End of initliazed the constructor
   /**
	*	@description	 			Following function will get the  webinar detail corresponding to the provided webinar id
	*	@param		   webinar_id	This is the id of the webinar whose information will be fetched out.
	*	@return		   Array		This array contanis the all the info regarding the webinar.
	*/

	function get_webinar_detail($webinar_id){
		global $wpdb;
		$wpdb->webinar_detail	=	"webinar";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_detail WHERE webinar_id_pk=$webinar_id");
	}
	//	End of getting a webinar detail
	/**
	 *	@description				Following function will get the all webinar created by admin
	 *	@return			Array		This will return array that contains all array information.
	 *
	 */
	 function get_all_webinar(){
	 	global $wpdb;
		$wpdb->all_webinar=	"webinar";
		return $wpdb->get_results("SELECT * FROM $wpdb->all_webinar order by created_date desc");
	 }
	//	End of get all webinar created by admin.
	/**
	 *	@description			This function get all avialable  schedule types for webinar
	 *	@return 		Array	This array will contains the all webinar schedules types entries
	 */

	function get_all_registered_attendee($webinar_id){
		global $wpdb;
		$wpdb->all_attendee = "webinar_registered_users";
		return $wpdb->get_results("SELECT registration_id_pk, attendee_name, attendee_email_id, webinar_real_time, webinar_date FROM $wpdb->all_attendee WHERE webinar_id_fk = '$webinar_id'",ARRAY_A);
	}

	function get_schedule_types(){
		global $wpdb;
		$wpdb->schedule_types	=	"webinar_schedule_type";
		return	$wpdb->get_results("SELECT * FROM $wpdb->schedule_types");
	}
	//	End of fetching schedule types.

	function get_template($page_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		return $wpdb->get_results("SELECT * FROM `webinar` LEFT JOIN $table_name ON $table_name.webinar_id=webinar.webinar_id_pk WHERE $table_name.ID='$page_id'");
	}
	
	
	/**
	 *	@description				Following function will fetch valid sesssions for particular webinars
	 *	@param		 webinar_id		This is id of the webinar.
	 *	@return 	 Array			This array will contains all valid sessions detail for particular webinars.
	 */

	 function get_today_unexpired_sessions($webinar_id,$max_attendee,$day_num,$webinar_date){
	 	global $wpdb;
		$wpdb->valid_webinar_sessions	=	"webinar_schedule";
		return $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t2.webinar_date = '$webinar_date' and  and t1.week_day='$day_num'  group by t1.webinar_schedule_id_pk having count<$max_attendee order by t1.start_time ASC");
	 }

	//	End of fetching valid sessions w.r.t  current time
   /**
	*	@description				Following function will get the unexpired session for the particular webinar
	*	@param		 webinar id		This is id of webinar.
	*				 ids			This is a variable that contains all the ids that are expired.
	*	@return		 Array			This is object type of array that will contains all the unexpired sessions.
	*/

	function get_unexpired_webinar_sessions($webinar_id,$expired_ids,$webinar_schedule_id,$web_date){
		global $wpdb;
		$wpdb->unexpired_sessions		=		"webinar_schedule";
		if(empty($expired_ids)){
			$expired_ids				=		0;
		}
		if($webinar_schedule_id==2){
			$unexpired_sessions				=		$wpdb->get_results("SELECT * FROM $wpdb->unexpired_sessions WHERE webinar_schedule_id_pk not in($expired_ids) and  webinar_id_fk=$webinar_id and webinar_date = '$web_date' ORDER BY start_time");
		}
		else{
			$unexpired_sessions				=		$wpdb->get_results("SELECT * FROM $wpdb->unexpired_sessions WHERE webinar_schedule_id_pk not in($expired_ids) and  webinar_id_fk=$webinar_id ORDER BY start_time");
		}
		return $unexpired_sessions;
	}

	//	End of getting valid rows for

	/*************Sharing **************/
	function get_sharing_detail($webinar_id){

		global $wpdb;
		$wpdb->sharing_detail		=	"webinar_sharing";
		return $wpdb->get_results("SELECT * FROM $wpdb->sharing_detail WHERE webinar_id_fk=$webinar_id");
		
	}


	function set_homepage($pageid){
		global $wpdb;
		 $table_name = $wpdb->prefix."options"; 
		 
		 $homepage = $wpdb->query("UPDATE {$table_name} SET option_value = 'page' WHERE option_name = 'show_on_front'");
		 $homepage = $wpdb->query("UPDATE {$table_name} SET option_value = '$pageid' WHERE option_name = 'page_on_front'");
		 
		 return $homepage;
	
	}

	function get_homepage($pageid){
		global $wpdb;
		
		$table_name = $wpdb->prefix . "options"; 
		return $wpdb->get_results("SELECT option_value FROM {$table_name} WHERE option_name = 'page_on_front'");
	}
	/*************************************/

   /**
    *	@description			Following function will get the path for the entire events that will be fired duriong videos.
	*	@return		  Array		This array is that will contains the
	*/

	function get_delayed_events_paths(){
		global $wpdb;
		$events_array	=	array();
		$wpdb->webinar_events	= "webinar_buttons";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_events");
	}

	//	End of fetched delayed events paths for the webinar video


  /**
   *	@description				Follwoing function will make registration page for the webinar
   *	@param		 webinar_id		This is the id of the webinar
   *				 webinat_topic	This is the topic of the webinar
   *	@return		 page_id		This is the page id for the webinar registration page
   */

    function make_webinar_registration_page($webinar_id,$registation_slug='',$storing_mode=''){
		global $userID,$wpdb;
		$page_name = trim($registation_slug);
		if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(!empty($page_name)){
			$slug	=	$page_name;
		}else{
			$slug	=	"event registration";	
		}
		$reg_post = array(
			'ID'					=>	0,
			'post_type'		=>	'page',
			'post_title'	=>	$slug,
			'post_status' =>	$storing_mode,
			'post_author' =>	1,
			'post_name'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($reg_post);
		add_post_meta($post_id,"_wp_page_template","webinar-registration.php");
		return $post_id;
   }


    function make_webinar_thankyou_page($webinar_id,$thankyou_slug='',$storing_mode=''){
		global $userID,$wpdb;
		$page_name = trim($thankyou_slug);
		if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(!empty($page_name)){
			$slug	=	$page_name;
		}else{
			$slug	=	"thank you";	
		}
		$thankyou_post = array(
			'ID'					=>	0,
			'post_type'		=>	'page',
			'post_title'	=>	$slug,
			'post_status' =>	$storing_mode,
			'post_author' =>	1,
			'post_name'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($thankyou_post);
		add_post_meta($post_id,"_wp_page_template","webinar-thankyou.php");
		return $post_id;
   }

  //	End of making webinar registration page

  /**
   *	@description				Follwoing function will make login page for the webinar
   *	@param		 webinar_id		This is the id of the webinar
   *				 webinat_topic	This is the topic of the webinar
   *	@return		 page_id		This is the page id for the webinar login page
   */

    function make_webinar_login_page($webinar_id,$login_slug='',$storing_mode=''){
		global $userID,$wpdb;
		$page_name = trim($login_slug);
       if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(!empty($page_name)){
			$slug	=	$page_name." login";
		}else{
			$slug	=	"login";	
		}
		$login_post = array(
			'ID'					=>	0,
			'post_title'	=>	$slug,
			'post_type'		=>	'page',
			'post_status' =>	$storing_mode,
			'post_author' =>	1,
			'post_name'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($login_post);
		add_post_meta($post_id,"_wp_page_template","webinar-login.php");
		return $post_id;
	}

	//	End of making webinar login page
    /**
     *	@description				Follwoing function will make login page for the webinar
     *	@param		 webinar_id		This is the id of the webinar
     *				 webinat_topic	This is the topic of the webinar
     *	@return		 page_id		This is the page id for the after webinar video page
     */

	function make_after_webinar_video_page($webinar_id,$replay_slug='',$storing_mode=''){
		global $userID,$wpdb;
		$page_name = trim($replay_slug);
	   if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(!empty($page_name)){
			$slug	=	$page_name;
		}else{
			$slug	=	"event replay";	
		}
		$replay_post = array(
			'ID'					=>	0,
			'post_title'	=>	$slug,
			'post_type'		=>	'page',
			'post_status' =>	$storing_mode,
			'post_author' =>	1,
			'post_slug'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($replay_post);
		add_post_meta($post_id,"_wp_page_template","after-webinar-video.php");
		return $post_id;
	}
	//	End of making webinar after_webinar_video_page
    /**
	 *	@description				Following funciton will make vidoe page for particular page.
	 *	@param		 webinar id		This is id of webinar.
	 *				 webinar topic	This is webinar topic for which webinar video page will be made.
	 *	@return		 page id		This is id of the webinar video page that is created by this function.
	 */

	function make_webinar_video_page($webinar_id,$event_slug='',$storing_mode=''){
		global $userID,$wpdb;
		
		$page_name = trim($event_slug);
	   if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(!empty($page_name)){
			$slug	=	$page_name;
		}else{
			$slug	=	"event";	
		}
		
		$event_post = array(
			'ID'					=>0,
			'post_title'	=>$slug,
			'post_type'		=>'page',
			'post_status' =>$storing_mode,
			'post_author' =>1,
			'post_slug'		=>$slug,
			'webinar_id'	=>$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($event_post);
		add_post_meta($post_id,"_wp_page_template","webinar-video.php");
		return $post_id;
	}
	//	End of the makeing video page for the webinar.
	
    /**
	 *	@description				Following funciton will make error page for particular page.
	 *	@param		 webinar id		This is id of webinar.
	 *				 webinar topic	This is webinar topic for which webinar error page will be made.
	 *	@return		 page id		This is id of the webinar error page that is created by this function.
	 */
	
	function make_webinar_error_page($webinar_id,$error_slug){
		global $userID,$wpdb;
		
		$page_name = trim($error_slug);
		if(!empty($page_name)){
			$slug	=	$page_name." error";
		}else{
			$slug	=	"event-error";	
		}
		
		$error_post = array(
			'ID'					=>	0,
			'post_title'	=>	$slug,
			'post_type'		=>	'page',
			'post_status' =>	'publish',
			'post_author' =>	1,
			'post_slug'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($error_post);
		add_post_meta($post_id,"_wp_page_template","webinar-error.php");
		return $post_id;
	}
	
	function make_webinar_live_page($webinar_id,$live_slug='',$storing_mode){
		global $userID,$wpdb;
	  if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		$page_name = trim($live_slug);
		if(!empty($page_name)){
			$slug	=	$page_name;
		}else{
			$slug	=	"live-event";	
		}
		
		$live_post = array(
			'ID'			=>	0,
			'post_title'	=>	$slug,
			'post_type'		=>	'page',
			'post_status' =>	$storing_mode,
			'post_author' =>	1,
			'post_slug'		=>	$slug,
			'webinar_id'	=>	$webinar_id
		);

		$post_id	=	$this->make_custom_webinar_pages($live_post);
		add_post_meta($post_id,"_wp_page_template","webinar-live.php");
		add_post_meta($post_id,'_ewp_live_page',"1");
		return $post_id;
	}
	//	End of the makeing error page for the webinar.
	
	/**
	 *	@description							Following funciton will update registration page slug and title.
	 *	@param		 ragistration_page_id		This is id of registration page.
	 *				 webinar_topic				This is registration parama link that user want to keep.
	 *	@return		 resultant_page_id			Afetcted registration page id.
	 */
	function update_webinar_registration_page($registration_page_id,$reg_slug='',$storing_mode=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($reg_slug);
	     if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(empty($slug)){
			$page_name	=	"registration";
		}else{
			$page_name	=	sanitize_title_with_dashes($slug);
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$registration_page_id,$storing_mode,'page','0'); 
		
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $registration_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$registration_page_id));
		}
		return 0;
	}
	//End of function 
	
	/**
	 *	@description							Following funciton will update login page slug and title.
	 *	@param		 ragistration_page_id		This is id of registration page.
	 *				 webinar_topic				This is slug that user need to keep for login page.
	 *	@return		 resultant_page_id			Afetcted login page id.
	 */
	function update_webinar_login_page($login_page_id,$login_slug=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($login_slug);
		if(empty($slug)){
			$page_name	=	"login";
		}else{
			$page_name	=	sanitize_title_with_dashes($slug);
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$login_page_id,'publish','page','0'); 
		
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $login_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$login_page_id));
		}
		return 0;
	}
	//End of function 
	
	
	/**
	 *	@description							Following funciton will update thankyou page slug and title.
	 *	@param		 thankyou_page_id			This is id of thank you page.
	 *				 webinar_topic				This is Slug that user need to keep for thank you page.
	 *	@return		 resultant_page_id			Afetcted thankyou page id.
	 */
	function update_webinar_thankyou_page($thankyou_page_id,$thankyou_slug='',$storing_mode=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($thankyou_slug);
	if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(empty($slug)){
			$page_name	=	"thank-you";
		}else{
			$page_name	=	sanitize_title_with_dashes($slug);	
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$thankyou_page_id,$storing_mode,'page','0'); 
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $thankyou_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$thankyou_page_id));
		}
		return 0;
	}
	//end of function
	
	/**
	 *	@description							Following funciton will update after webinar page slug and title.
	 *	@param		 after_webinar_page_id		This is id of after webinar page.
	 *				 webinar_topic				This is slug value that user wants to keep for reply page.
	 *	@return		 resultant_page_id			Afetcted replay page id.
	 */
	function update_after_webinar_page($after_webinar_page_id,$replay_slug='',$storing_mode=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($replay_slug);
	    if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(empty($slug)){
			$page_name		=	"replay";
		}else{
			$page_name		=	sanitize_title_with_dashes($slug);
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$after_webinar_page_id,$storing_mode,'page','0'); 
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $after_webinar_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$after_webinar_page_id));
		}
		return 0;
	}
	//end of function
	
	/**
	 *	@description							Following funciton will update webinar page slug and title.
	 *	@param		 webinar_page_id			This is id of webinar page.
	 *				 webinar_topic				This is Slug value that user need to keep for Event page.
	 *	@return		 resultant_page_id			Afetcted event page id.
	 */
	function update_webinar_page($webinar_page_id,$webinar_slug='',$storing_mode=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($webinar_slug);
	   if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(empty($slug)){
			$page_name	=	'event';
		}else{
			$page_name	=	sanitize_title_with_dashes($slug);	
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$webinar_page_id,$storing_mode,'page','0'); 
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $webinar_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$webinar_page_id));
		}
		return 0;
	}
	
	/**
	 *	@description							Following funciton will update webinar error page slug and title.
	 *	@param		 webinar_error_page_id		This is id of error page.
	 *				 webinar_topic				This is webinar topic.
	 *	@return		 resultant_page_id			Afetcted error page id.
	 */
	function update_webinar_error_page($webinar_error_page_id,$error_slug=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($error_slug);
		if(empty($slug)){
			$page_name		=	"error";
		}else{
			$page_name		=	sanitize_title_with_dashes($slug);
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$webinar_error_page_id,'publish','page','0'); 
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $webinar_error_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$webinar_error_page_id));
		}
		return 0;
	}
	
	
	/**
	 *	@description							Following funciton will update webinar live page slug and title.
	 *	@param		 webinar_page_id			This is id of webinar page.
	 *				 webinar_topic				This is Slug value that user need to keep for live page.
	 *	@return		 resultant_page_id			Afetcted registration page id.
	 */
	function update_webinar_live_page($live_page_id,$live_slug='',$storing_mode=''){
		global $wpdb;
		$wp_posts = $wpdb->prefix.'posts';
		$slug = trim($live_slug);
	    if($storing_mode != 'draft'){
			$storing_mode = 'publish';
		}
		if(empty($slug)){
			$page_name	=	'live';
		}else{
			$page_name	=	sanitize_title_with_dashes($slug);	
		}
		
		$unique_slug = wp_unique_post_slug($page_name,$live_page_id,$storing_mode,'page','0'); 
		$page 	 	 = $wpdb->get_row("SELECT post_name FROM $wp_posts WHERE post_type='page' AND ID = $live_page_id");
		
		if($unique_slug!=$page->post_name){
			return	$wpdb->query($wpdb->prepare("UPDATE $wp_posts set post_name=%s WHERE post_type='page' AND ID=%d",$unique_slug,$live_page_id));
		}
		return 0;
	}
	
	
	/**
	 *	@description			Following function will get all the session for particular webinars
	 *	@param		 webinar id	This is id of the webinar
	 *	@return		 Array		This array will contains all the sessions for the webinar.
	 */

	function get_webinar_sessions($webinar_id,$webinar_schedule_id,$web_date){
		global $wpdb;
		$wpdb->webinar_sessions	=	"webinar_schedule";
		if($webinar_schedule_id==2){
			$webinar_sessions		=	$wpdb->get_results("SELECT * FROM $wpdb->webinar_sessions WHERE webinar_id_fk=$webinar_id  and webinar_date = '$web_date' ORDER BY start_time ASC");
		    
		}
		else{
			$webinar_sessions		=	$wpdb->get_results("SELECT * FROM $wpdb->webinar_sessions WHERE webinar_id_fk=$webinar_id ORDER BY start_time ASC");
		}
		return $webinar_sessions;
	}
	//	End of getting all session for particular late
	/**
	 *	@description			Following function will get all unique session for particular webinars
	 *	@param		 webinar id	This is id of the webinar
	 *	@return		 Array		This array will contains all the distinct sessions for the webinar.
	 */
	 function get_distinct_webinar_sessions($webinar_id){
	     global $wpdb;
		 $wpdb->webinar_sessions	=	"webinar_schedule";
		 $webinar_sessions		=	$wpdb->get_results("SELECT DISTINCT start_time FROM $wpdb->webinar_sessions WHERE webinar_id_fk=$webinar_id ORDER BY start_time ASC");
		 return $webinar_sessions;
	 }
	 
	/**
	 *	@description				Following function will fetch the webinar session for the specific date for
	 *								specific webinar.
	 *	@param		 webinar_id		This is the webinar_id whose webinar sessions will be fetched out.
	 *				 max_attendees	This is maximum number of attendees
	 *	@return 	 Array			This is the array of the that contains all the sessions of webinar.
	 */

	function get_webinar_session_for_specific_date($webinar_id,$max_attendees){
	 	global $wpdb;
		$wpdb->specific_date_webinar_session	=	"webinar";
		$current_date = date('Y-m-d');
		if($max_attendees==0){
			$webinar_specific_date_session			=	$wpdb->get_results("SELECT count(t2.registration_id_pk) as counts , t1.webinar_schedule_id_pk,t1.start_time,t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk  WHERE t1.webinar_id_fk='".$webinar_id."' and t1.webinar_date>".$current_date." group by t1.webinar_schedule_id_pk order by t1.start_time ASC");
		}else{
			$webinar_specific_date_session			=	$wpdb->get_results("SELECT count(t2.registration_id_pk) as counts , t1.webinar_schedule_id_pk,t1.start_time,t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk  WHERE t1.webinar_id_fk='".$webinar_id."' and t1.webinar_date>".$current_date." group by t1.webinar_schedule_id_pk having counts<$max_attendees order by t1.start_time ASC");
		}
		return $webinar_specific_date_session;
	}
	//	End of getting webinar session for specific date


   /**
    *	@description					Following function will
	*	@param			webinar_id		This is the id of the webinar id
	*
	*	@return		 non-zero-Array		This array will contains the detail info regarding the webinar session and their number of  attendee
	*				 zero-Array
	*
	*/

	function get_registered_attendees_for_specific_date($webinar_id,$webinar_date) {
		global $wpdb;
		return $wpdb->get_results("SELECT count(*) AS counts,webinar_schedule_id_fk,webinar_date FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND  webinar_date = '$webinar_date'");
	}


	function get_registered_attendees_for_everyday($webinar_id,$webinar_date){
		global $wpdb;
		$wpdb->everyday_attendees	=	"webinar_registered_users";
		$current_date = date('Y-m-d');
		return $wpdb->get_results("SELECT count(*) AS counts,webinar_schedule_id_fk,webinar_date FROM webinar_registered_users WHERE webinar_id_fk =$webinar_id AND  webinar_date = '$webinar_date' and webinar_date>".$current_date." GROUP BY webinar_schedule_id_fk");
	}

	//	End of checking attendee has  registered or not  for particular webinar.
	/**
	 *	@description			Following function will unexpired session for today for everyday module.
	 *	@param		  Id		This is webinar id ,
	 *	@return 	 Array		This array will cotnains all unexpired sessions for today for everyday module.
	 */

	function get_today_unexpired_session_for_everyday_mod($webinar_id,$max_attendee,$webinar_schedule_id,$web_date){
		global $wpdb;
		if($max_attendee==0){
			if($webinar_schedule_id==2){
				//girish //syed
				$unexpired_sessions		=	$wpdb->get_results($wpdb->prepare("SELECT webinar_schedule.webinar_schedule_id_pk, webinar_schedule.start_time,webinar_schedule.end_time, webinar_schedule.gmt_start_time, webinar_schedule.webinar_date FROM webinar_schedule WHERE webinar_schedule.webinar_id_fk=%d AND webinar_schedule.webinar_date=%s GROUP BY webinar_schedule.webinar_schedule_id_pk ORDER BY webinar_schedule.start_time ASC",$webinar_id,$web_date));
				//$unexpired_sessions		=	$wpdb->get_results("SELECT COUNT(t2.registration_id_pk) AS counts, t1.webinar_schedule_id_pk, t1.start_time, t1.gmt_start_time, t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk WHERE t1.webinar_id_fk='".$webinar_id."' AND t1.webinar_date='$web_date' GROUP BY t1.webinar_schedule_id_pk HAVING counts<$max_attendee ORDER BY t1.start_time ASC");
			}else{
				$unexpired_sessions		=	$wpdb->get_results("SELECT count(t2.registration_id_pk) as counts ,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time,t1.gmt_start_time,t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk WHERE t1.webinar_id_fk='".$webinar_id."' group by t1.webinar_schedule_id_pk  order by t1.start_time ASC");
			}
		}else{
			if($webinar_schedule_id==2){
				$unexpired_sessions		=	$wpdb->get_results("SELECT count(t2.registration_id_pk) as counts ,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time,t1.gmt_start_time,t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk WHERE t1.webinar_id_fk='".$webinar_id."' and t1.webinar_date='$web_date'  group by t1.webinar_schedule_id_pk having counts<$max_attendee order by t1.start_time ASC");
			}else{
				$unexpired_sessions		=	$wpdb->get_results("SELECT count(t2.registration_id_pk) as counts ,t1.webinar_schedule_id_pk,t1.start_time,t1.end_time,t1.gmt_start_time,t1.webinar_date FROM webinar_schedule t1 LEFT JOIN webinar_registered_users t2 ON t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk WHERE t1.webinar_id_fk='".$webinar_id."' group by t1.webinar_schedule_id_pk having counts<$max_attendee order by t1.start_time ASC");
			}
		}
		return $unexpired_sessions;
    }
   //	End of getting todays unexpired sessions for today for everyday module
   /**
    *	@description
	*
	*	@param		  post_array		This is post array that contains all the posted information.
	*				  webinar_id		This is the webinar id
	*				  schedule_id		This is webiunar shcedule id that tells that for which user is going to be register.
	*
	*	@retrun		  non-zero-value	When user has succesfully registered for the particular webinar session.
	*						0			When user has not succesfully registered for the particular webinar session.
	*/
	function register_attendee_for_webinar_session($webinar_id,$schedule_id,$attendee_name,$attendee_email,$webinar_start_date,$webinar_date,$webinar_time,$webinar_real_time,$webinar_real_date,$webinar_end_time,$webinar_end_date,$selected_timezone_id,$key,$attendee_local_timezone){
		global $wpdb;
		$signup_date = date('Y-m-d');
		$wpdb->register_user	=	"webinar_registered_users";
		$wpdb->query("INSERT INTO $wpdb->register_user (webinar_id_fk,webinar_schedule_id_fk,attendee_name,attendee_email_id,attendee_local_timezone,webinar_date,webinar_start_date,webinar_time,webinar_real_time,webinar_real_date,webinar_end_time,webinar_end_date,selected_timezone_id,`key`,signup_date) VALUES($webinar_id,$schedule_id,'$attendee_name','$attendee_email','$attendee_local_timezone','$webinar_date','$webinar_start_date','$webinar_time','$webinar_real_time','$webinar_real_date','$webinar_end_time','$webinar_end_date','$selected_timezone_id','$key','$signup_date')");
		//return mysql_insert_id();
		return $wpdb->insert_id;
	}
	
	/*	@description 						Check $webinar_date is less than $webinar_end_date
	*	@return 							true if webinar is still active, otherwise false
	*	@param		$webinar_date			webinar_date to check
	*				$webinar_end_date		webinar_end_date
	*/
	function check_webinar_validity_for_end_date($webinar_date,$webinar_end_date){
		$flag = true;
		
		if((strtotime($webinar_date) > strtotime($webinar_end_date)) && ($webinar_end_date != NULL)) $flag=false;				
		return $flag;
	}
	
	/*	@description 						Check $webinar date is greater than $webinar_start_date in DB
	*	@return 							true is webinar is ready to register, otherwise false
	*	@param		$webinar_date			webinar_date to check
	*				$webinar_start_date		webinar_start_date store in DB
	*/
	function check_webinar_start_date_for_registration($webinar_start_date,$next_day){
		
		$flag = true;
		$webinar_date = date('Y-m-d',strtotime("+".$next_day." days 13 hours"));
		if(strtotime($webinar_date) <= strtotime($webinar_start_date) and $webinar_start_date != NULL) $flag = false;
		return $flag;
	}
	
	/*@description									Check $webinar date is greater than $webinar_start_date in DB
	*	@return 											true is webinar's right now feature is enable , otherwise false
	*	@param $right_now_enable			right now feature is enable or not 
	*				 $webinar_start_date		webinar_start_date
	*/
	function check_webinar_right_now_enable($right_now_enable,$webinar_start_date='NULL'){
		global $wpdb;
		
		$flag = false;
		
		$today_date = date('Y-m-d');
				
		if($right_now_enable==1 and $webinar_start_date!='NULL'){
			if(strtotime($webinar_start_date)<=strtotime($today_date)){
				$flag = true;
			}
		}else if($right_now_enable==1 and $webninar_start_date=='NULL'){
			$flag = true;
		}
			
		return $flag;
	}
	
	function check_webinar_right_now_with_fixed_timezone($right_now_enable,$webinar_start_date,$timezoneid,$webinar_schedule_type,$webinar_id,$event_type=0){
		
		global $wpdb;
		$flag = false;

		if($timezoneid==0){

			$today_date = date('Y-m-d',strtotime('+13 hours'));
			
			if($right_now_enable==1 and $webinar_start_date!=NULL and $webinar_schedule_type==1 ){
				if(strtotime($webinar_start_date)<=strtotime($today_date)){
					$flag = true;
				}
			}else if($right_now_enable==1 and $webninar_start_date==NULL and $webinar_schedule_type==1 ){
				$flag = true;
			}else if($right_now_enable==1 and ($webinar_schedule_type==2 or $webinar_schedule_type==4) ){
				
				$flag = $this->is_this_a_webinar_day_for_right_now_feature($webinar_id,$webinar_schedule_type);
			}
			
		}else{
			
			$timezone_id=$this->get_timezone_detail($timezoneid);
			
			$timezone_symbol=$timezone_id[0]->timezone_gmt_symbol;
			$timezone_hours=$timezone_id[0]->GMT;
			$timezone_hours=explode(":",$timezone_hours);
			
			$add_hour=strtotime( $date1.' '.$hours .$timezone_symbol.$timezone_hours[0]." hours".' '.$timezone_symbol.' '.$timezone_hours[1].' minutes');
			$today_date=date("Y-m-d" ,$add_hour);
	
			if($right_now_enable==1 and $webinar_start_date!=NULL and $webinar_schedule_type==1 ){
	
				if(strtotime($webinar_start_date)<=strtotime($today_date)){
					$flag = true;
				}
			}else if($right_now_enable==1 and $webninar_start_date==NULL and $webinar_schedule_type==1 ){
				$flag = true;
			}else if($right_now_enable==1 and ($webinar_schedule_type==2 or $webinar_schedule_type==4)){
				
				$flag = $this->is_this_a_webinar_day_for_right_now_feature($webinar_id,$webinar_schedule_type);
				
			}
		}
		return $flag;
	}
	
	
	function is_this_a_webinar_day_for_right_now_feature($webinar_id,$schedule_id){
		
		global $wpdb;
		$flag = false;
		
		if($schedule_id==2){
				//get total available dates for a particular webinar 
				$session_attendee_detail		=		$wpdb->get_results("SELECT * FROM webinar_schedule WHERE webinar_id_fk = $webinar_id group by webinar_date order by webinar_date ASC");
			
				// if any date is available 
				if(count($session_attendee_detail)>0){
					//check each date
					for($i=0;$i<count($session_attendee_detail);$i++){				

							$date = $session_attendee_detail[$i]->webinar_date;
													
							if(strtotime($date) >= strtotime(date('Y-m-d',strtotime("-12 hours"))) and strtotime($date) <= strtotime(date('Y-m-d',strtotime("+13 hours"))) ){
								$flag = true;
								break;
							}
					}
				}
			}

		if($schedule_id==4){
			
			$webinar_week_start_day = '';
			$get_day = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id'");
			
			if(strtotime(date('Y-m-d')) < strtotime($get_day[0]->webinar_start_date."-".$next_day." days +13 hours") and !empty($get_day[0]->webinar_start_date)){
				$webinar_registration_start_date			= 	date('Y-m-d G:i:s',strtotime($get_day[0]->webinar_start_date." +13 hours"));
			}
			else{
				$webinar_registration_start_date 			= 	date('Y-m-d G:i:s');
			}
					
			$webinar_start_date = date('Y-m-d',strtotime($webinar_registration_start_date." +13 hours"));
			$webinar_end_date = date('Y-m-d',strtotime($webinar_registration_start_date." -12 hours"));		

			if(strtotime($webinar_start_date) < strtotime($get_day[0]->webinar_start_date) and !empty($get_day[0]->webinar_start_date) ){
				return false;
			}
			
			$days = array();
			for($s=0;$s<count($get_day);$s++){
				$days[$s] = $get_day[$s]->week_day;
			}
					
			$webinar_start_day =	(int)date('N', strtotime($webinar_start_date));
			$webinar_end_day =	(int)date('N', strtotime($webinar_end_date));
			
			if(strtotime($webinar_start_date) == strtotime($get_day[0]->webinar_start_date) ){
				if( !in_array($webinar_start_day,$days) ){
					return false;
				}
			}			
						
			if( ( in_array($webinar_start_day, $days) or in_array($webinar_end_day, $days) ) ){
					$flag = true;			
			}
		}

		return $flag;
	}
	
	function get_webinar_slots($dates,$next_day,$webinar_id,$max_attendee,$schedule_id){

				global $wpdb;
				$a=0;
				$dates_array = array();

				/* ~~~~~~~~~~~For Every day Webinar~~~~~~~~  */
				if($schedule_id==1){
					
					$webinar_detail = $wpdb->get_results("SELECT * FROM webinar WHERE webinar_id_pk = $webinar_id ");
					
					/*check wheather current date is less than (webinar_starting_date - $next_days) 
					*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
					*
					*/
					if(strtotime(date('Y-m-d')) < strtotime($webinar_detail[0]->webinar_start_date."-".$next_day." days +13 hours") and $webinar_detail[0]->webinar_start_date != NULL){
						$webinar_registration_start_date			= 	date('Y-m-d',strtotime($webinar_detail[0]->webinar_start_date."-".$next_day." days +13 hours"));
					}
					else{
						$webinar_registration_start_date = date('Y-m-d');
					}
					while($a<$dates){
											
						$webinar_date				=	date('Y-m-d', strtotime($webinar_registration_start_date."+".$next_day." days +13 hours"));

						$session_attendee_detail	=		$wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = '$webinar_id' ");
						
						//check if the $webinar_date is greater than webinar_end_date
						$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
						
						if($flag_check_webinar_end_date==false ){
							break;
						}
						
						$total_session =  count($session_attendee_detail);
						$allowed_registered_attendee = $total_session*$max_attendee;
						if($allowed_registered_attendee==0){
							$dates_array[]=$webinar_date;
							$a++;
						}
						else {
							$registered_attendee	=	$wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
							$reg_att = count($registered_attendee);
							if($reg_att<$allowed_registered_attendee){
								$dates_array[] = $webinar_date;
								$a++;
							}
						}
						$next_day++;
					}
				}
				/* ~~~~~~~~For Specific Date Webinar~~~~~~~~  */
			if($schedule_id==2){
				$webinar_date				=	date('Y-m-d', strtotime("+".$next_day." days -12 hours"));
				//get totel available dates for a particular webinar 
				$session_attendee_detail		=		$wpdb->get_results("SELECT *,count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id group by webinar_date having webinar_date >= '$webinar_date' order by webinar_date ASC");
			
				// if any date is available 
				if(count($session_attendee_detail)>0){
					//check each date
					for($i=0;$i<count($session_attendee_detail);$i++){				
						
						$webinar_schedule_id = $session_attendee_detail[$i]->webinar_schedule_id_pk;
						$webinar_date = $session_attendee_detail[$i]->webinar_date;
						$total_session = $session_attendee_detail[$i]->count_session;
						//get registered attendee list for a particular date 
						$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = $webinar_id AND webinar_date = '$webinar_date' ");
					
						$reg_att = count($registered_attendee);
						$allowed_registered_attendee = $total_session*$max_attendee;
						
						//if registered attendees for a particular date is eqaual to or greater than allowed maximum attendee then leave that date
						if($reg_att>=$allowed_registered_attendee and $allowed_registered_attendee>0 ){
							continue;
						}						
						//if number of dates in array is equal to the dates needed in the webinar then break the loop
						if(count($dates_array)==$dates){
							break;
						}
						else{
							$dates_array[] = $session_attendee_detail[$i]->webinar_date;
						}
						
					}
					
				}
	
			}

		/* ~~~~~~~~~~~~~~~~Selected Days~~~~~~~~~~~~~~~~~ */
		if($schedule_id==4){

			$get_day = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id'");
			
			/*check wheather current date is less than (webinar_starting_date - $next_days) 
			*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
			*
			*/
			if(strtotime(date('Y-m-d')) < strtotime($get_day[0]->webinar_start_date."-".$next_day." days +13 hours") and $get_day[0]->webinar_start_date != NULL){
				$webinar_registration_start_date			= 	date('Y-m-d',strtotime($get_day[0]->webinar_start_date."-".$next_day." days +13 hours"));
			}
			else{
				$webinar_registration_start_date = date('Y-m-d G:i:s');
			}
			
			$days = array();
			for($s=0;$s<count($get_day);$s++)
			{
				$days[$s] = $get_day[$s]->week_day;
			}
			$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]' ");
			while($a<$dates){
										
				$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$next_day." days -12 hours"));
				
				//check if the $webinar_date is greater than webinar_end_date
				$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
				if($flag_check_webinar_end_date==false ){
						break;
				}
				
				$webinar_day =	(int)date('N', strtotime($webinar_registration_start_date."+".$next_day." days -12 hours"));
				$total_session =  count($session_attendee_detail);
				$allowed_registered_attendee = $total_session*$max_attendee;
				if(in_array($webinar_day, $days)){
					if($allowed_registered_attendee==0){
						$dates_array[] = $webinar_date;
						$a++;
					}
					else {
						$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
						$reg_att = count($registered_attendee);
						if($reg_att<$allowed_registered_attendee){
							$dates_array[] = $webinar_date;
							$a++;
						}
					}
				}
				$next_day++;
			}
		}
		return $dates_array;
	
	}

	//	End of registering attendee for particular session of webinar.
	/**
     *	@description						Following function will check the atendee has registered for particular webinar
	 *
	 *	@param		 webinar_id				This is id for the webinar.
	 *				 webinar_schedule_id	This is the schedule id for the webinar.
	 *				 attendee_email			This is the attendee_email
	 *
	 *	@return		 		0				When user has not registered for the particular session of webinar.
	 *						1				When user has registered for the particular session of webinar.
	 */

	function check_attendee_has_registered_for_specific_date($webinar_id,$webinar_schedule_id,$attendee_email){
		global $wpdb;
		$return_variable		=	0;
		$wpdb->register_user=	"webinar_registered_users";
		$user_registered		=	$wpdb->get_results("SELECT * FROM $wpdb->register_user WHERE webinar_id_fk=$webinar_id and webinar_schedule_id_fk=$webinar_schedule_id and attendee_email_id=$attendee_email");
		if(count($user_registered)>0){
			$user_registered		=		1;
		}

		return $user_registered;
	}
	//	End of checking attendee has registered or not  for particular webinar.
    /**
     *	@description			 Following function will make page it is similar to wp_insert_post but i have made custom
     *							 becuase we are having a custom field "webinar_id" in the "wp_posts" table.
     *	@param		 page_array  This is the post array that contanis all the info that needs to be inserted into the page
     *	@return 	 post_id	 This post_id is the id of the page that is currenty made by this function
     */

  	function make_custom_webinar_pages($postarr, $wp_error = false){
		global $wpdb, $wp_rewrite, $user_ID;
		$defaults = array('post_status' => 'draft', 'post_type' => 'post', 'post_author' => $user_ID,
			'ping_status' => get_option('default_ping_status'), 'post_parent' => 0,
			'menu_order' => 0, 'to_ping' =>  '', 'pinged' => '', 'post_password' => '',
			'guid' => '', 'post_content_filtered' => '', 'post_excerpt' => '', 'import_id' => 0,
			'post_content' => '', 'post_title' => '','webinar_id'=>0);

		$postarr = wp_parse_args($postarr, $defaults);
		$postarr = sanitize_post($postarr, 'db');

		// export array as variables
		extract($postarr, EXTR_SKIP);

		// Are we updating or creating?
		$update = false;
		if ( !empty($ID) ) {
			$update = true;
			$previous_status = get_post_field('post_status', $ID);
		} else {
			$previous_status = 'new';
		}

		if ( ('' == $post_content) && ('' == $post_title) && ('' == $post_excerpt) && ('attachment' != $post_type) ) {
			if ( $wp_error )
				return new WP_Error('empty_content', __('Content, title, and excerpt are empty.'));
			else
				return 0;
		}

		if ( empty($post_type) )
			$post_type = 'post';

		if ( empty($post_status) )
			$post_status = 'draft';

		if ( !empty($post_category) )
			$post_category = array_filter($post_category); // Filter out empty terms

		// Make sure we set a valid category.
		if(empty($post_category) || 0 == count($post_category)	||	!is_array($post_category)){
			// 'post' requires at least one category.
			if ( 'post' == $post_type && 'auto-draft' != $post_status )
				$post_category = array( get_option('default_category') );
			else
				$post_category = array();
		}

		if ( empty($post_author) )
			$post_author = $user_ID;

		$post_ID = 0;

		// Get the post ID and GUID
		if ( $update ) {
			$post_ID = (int) $ID;
			$guid = get_post_field( 'guid', $post_ID );
			$post_before = get_post($post_ID);
		}

		// Don't allow contributors to set to set the post slug for pending review posts
		if ( 'pending' == $post_status && !current_user_can( 'publish_posts' ) )
			$post_name = '';

		// Create a valid post name.  Drafts and pending posts are allowed to have an empty
		// post name.
		if(empty($post_name)) {
			if(!in_array( $post_status, array( 'draft', 'pending', 'auto-draft')))
				$post_name = sanitize_title($post_title);
			else
				$post_name = '';
		} else {
			$post_name = sanitize_title($post_name);
		}

		// If the post date is empty (due to having been new or a draft) and status is not 'draft' or 'pending', set date to now
		if(empty($post_date) || '0000-00-00 00:00:00' == $post_date)
			$post_date = current_time('mysql');

		if(empty($post_date_gmt) || '0000-00-00 00:00:00' == $post_date_gmt){
			if ( !in_array( $post_status, array( 'draft', 'pending', 'auto-draft' ) ) )
				$post_date_gmt = get_gmt_from_date($post_date);
			else
				$post_date_gmt = '0000-00-00 00:00:00';
		}

		if ($update || '0000-00-00 00:00:00' == $post_date){
			$post_modified     = current_time( 'mysql' );
			$post_modified_gmt = current_time( 'mysql', 1 );
		} else {
			$post_modified     = $post_date;
			$post_modified_gmt = $post_date_gmt;
		}

		if('publish' == $post_status ) {
			$now = gmdate('Y-m-d H:i:59');
			if(mysql2date('U', $post_date_gmt, false) > mysql2date('U', $now, false) )
				$post_status = 'future';
		}elseif( 'future' == $post_status ) {
			$now = gmdate('Y-m-d H:i:59');
			if(mysql2date('U', $post_date_gmt, false) <= mysql2date('U',$now,false))
				$post_status = 'publish';
		}

		if(empty($comment_status)){
			if($update)
				$comment_status = 'closed';
			else
				$comment_status = get_option('default_comment_status');
		}

		if(empty($ping_status))
			$ping_status = get_option('default_ping_status');

		if(isset($to_ping))
			$to_ping = preg_replace('|\s+|', "\n", $to_ping);
		else
			$to_ping = '';

		if(!isset($pinged))
			$pinged = '';

		if(isset($post_parent))
			$post_parent = (int) $post_parent;
		else
			$post_parent = 0;

		$post_name = wp_unique_post_slug($post_name, $post_ID, $post_status, $post_type, $post_parent);

		// expected_slashed (everything!)
		$data = compact( array( 'post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_content_filtered', 'post_title', 'post_excerpt', 'post_status', 'post_type', 'comment_status', 'ping_status', 'post_password', 'post_name', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt', 'post_parent', 'menu_order', 'guid','webinar_id' ) );
		$data = apply_filters('wp_insert_post_data', $data, $postarr);
		$data = stripslashes_deep( $data );
		$where = array( 'ID' => $post_ID );

		if($update){
			do_action( 'pre_post_update', $post_ID );
			if(false === $wpdb->update( $wpdb->posts, $data, $where)){
				if($wp_error)
					return new WP_Error('db_update_error', __('Could not update post in the database'), $wpdb->last_error);
				else
					return 0;
			}
		}else{
			if(isset($post_mime_type))
				$data['post_mime_type'] = stripslashes( $post_mime_type ); // This isn't in the update
			// If there is a suggested ID, use it if not already present
			if(!empty($import_id)){
				$import_id = (int) $import_id;
				if ( ! $wpdb->get_var( $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE ID = %d", $import_id) ) ) {
					$data['ID'] = $import_id;
				}
			}

			if(false === $wpdb->insert($wpdb->posts, $data)){
				if($wp_error)
					return new WP_Error('db_insert_error', __('Could not insert post into the database'), $wpdb->last_error);
				else
					return 0;
			}

			$data = apply_filters('wp_insert_post_data', $data, $postarr);
			$data = stripslashes_deep($data);
			$post_ID = (int) $wpdb->insert_id;

			// use the newly generated $post_ID

			$where = array( 'ID' => $post_ID );
			if(empty($data['post_name']) && !in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft'))){
				$data['post_name'] = sanitize_title($data['post_title'], $post_ID);
				$wpdb->update( $wpdb->posts, array( 'post_name' => $data['post_name'] ), $where );
			}

			if (is_object_in_taxonomy($post_type, 'category'))
				wp_set_post_categories( $post_ID, $post_category );

			if ( isset( $tags_input ) && is_object_in_taxonomy($post_type, 'post_tag') )
				wp_set_post_tags( $post_ID, $tags_input );

			// new-style support for all custom taxonomies
			if ( !empty($tax_input) ) {
				foreach ( $tax_input as $taxonomy => $tags ) {
					$taxonomy_obj = get_taxonomy($taxonomy);
					if ( is_array($tags) ) // array = hierarchical, string = non-hierarchical.
						$tags = array_filter($tags);
					if ( current_user_can($taxonomy_obj->cap->assign_terms) )
						wp_set_post_terms( $post_ID, $tags, $taxonomy );
				}
			}

			$current_guid = get_post_field( 'guid', $post_ID );

			if ( 'page' == $data['post_type'] )
				clean_page_cache($post_ID);
			else
				clean_post_cache($post_ID);

			// Set GUID
			if ( !$update && '' == $current_guid )
				$wpdb->update($wpdb->posts, array('guid' => get_permalink($post_ID)), $where);
			$post = get_post($post_ID);
			if(!empty($page_template) && 'page' == $data['post_type']){
				$post->page_template = $page_template;
				$page_templates = get_page_templates();
				if('default' != $page_template && !in_array($page_template, $page_templates)){
					if($wp_error)
						return new WP_Error('invalid_page_template', __('The page template is invalid.'));
					else
						return 0;
				}
				update_post_meta($post_ID, '_wp_page_template',  $page_template);
			}

			wp_transition_post_status($data['post_status'], $previous_status, $post);
			if($update){
				do_action('edit_post', $post_ID, $post);
				$post_after = get_post($post_ID);
				do_action( 'post_updated', $post_ID, $post_after, $post_before);
			}   
		}

		return $post_ID;
	}

	//	End of making a custom page.
	function get_unexpired_sessions_for_max_date($webinar_id,$max_attendee){
		global $wpdb;
		$current_date = date('Y-m-d');
		return $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time,t2.webinar_date from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date>".$current_date." and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk AND t2.webinar_date = (SELECT max(webinar_date) AS maximum FROM webinar_registered_users t1 WHERE webinar_id_fk =$webinar_id) where t1.webinar_id_fk=$webinar_id group by t1.webinar_schedule_id_pk having count<$max_attendee order by t1.start_time ASC");
	}

   /**
	*	@description			 	Following function will get the maximum date for particular webinar
	*	@param		  webinar_id	This is the webinar id whose session maximum date will be fetched.
	*	@return		  max_date		When there is maximum date for particular webinar
	*				  NULL			When there is no maximum date for
	*/

	function get_max_webinar_session_date($webinar_id){
		global $wpdb;
		$wpdb->webinar_max_date	= "webinar_registered_users";
		$webinar_max_date				=	$wpdb->get_results("Select max(webinar_date) as maximum from $wpdb->webinar_max_date where webinar_id_fk=$webinar_id");
		return $webinar_max_date[0]->maximum;
	}

	//	End of getting maximum session date.
	/**
	 *	@description				Following function will get the maxiumum days schedules correponding to particular webinar id
	 *	@param		  webinar_id	This is the id of the webinar whose id will be fetched.
	 *	@return 	   Array		This array contains the whose schedule records for particular date.
	 */

	 function get_maximum_date_webinar_schedules($webinar_id){
	 	global $wpdb;
		$wpdb->webinar_max_date_schedles = "webinar_registered_users";
		$current_date = date('Y-m-d');
		return $wpdb->get_results("SELECT count(*) as count,webinar_schedule_id_fk,webinar_date FROM webinar_registered_users WHERE webinar_id_fk=$webinar_id  and webinar_date>".$current_date." and webinar_date=(select max(webinar_date) from webinar_registered_users where webinar_id_fk=$webinar_id) group by webinar_schedule_id_fk order by webinar_time ASC");
	 }

	//	End of the getting date records

	/**
	 *	@description				Following function will get scheduled week day's for particular webinar of mutiple week dayy module
	 *	@param		  webinar id	This is the webinar if whose week days will be fetched.
	 *	@return		  Array			This array will contains all the week day's of particular webinar.
	 */

	 function get_week_day_of_particuler_webinar($webinar_id){
	 	global $wpdb;
	 	$wpdb->webinar_week_days	=	"webinar_schedule";
		$webinar_week_days			=	$wpdb->get_results("SELECT Distinct(week_day) as week_day  FROM $wpdb->webinar_week_days WHERE webinar_id_fk=$webinar_id");
		$week_day_array				=	array();
		foreach($webinar_week_days as $week_day){
			$week_day_array[]		=	$week_day->week_day;
		}

		return $week_day_array;
	 }

	//	End of getting week day's for particular webinar.

	/**
	 *	@description				Following function will get the session of the webinar for particular week day.
	 *	$param			week_day	This is week day of webinar
	 *					webinar_id	This is webinar id
	 *	@return
	 */

	function get_webinar_week_day_sessions($webinar_id,$week_day){
		global $wpdb;
		$wpdb->webinar_week_day_sessions	=	"webinar_schedule";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_week_day_sessions WHERE webinar_id_fk=$webinar_id and week_day=$week_day order by start_time ASC");
	}

	//	End of getting all week days
	/**
	 *	@description				Following function will get the autoresponder text for the webinar when notification type is autoresponder
	 *	@param		  Webinar_id	This is webinar id.
	 *	@return		  Text			This text is a autoresponder text for notification autotresponder.
	 */

	function get_notification_details($webinar_id){
	 	global $wpdb;
		$notification_detail_array	=	$wpdb->get_results("SELECT * FROM webinar_notification_emails WHERE webinar_id_fk=$webinar_id");
		return $notification_detail_array;
	 }

	//	End of getting autoresponder text for autoresponder notification.
	/**
	 *	@description				Following function will check that do the attendee has registered already or not for pariculer
	 *								webinar session.
	 *	@param		Webinar id		This is webinar id.
	 *				Session id		This is webinar session id.
	 *				webinar_date	This is webinar scheduled date.
	 *				attendee name	This is attedee name .
	 *				attendee_email	This is attendee email.
	 *	@return			0			When attendee has not registered before for this webinar session.
	 *					1			When attendee has registered before for this webinar session.
	 */

	function do_attendee_registered_for_webinar_session($webinar_id,$webinar_schedule_id,$attendee_email,$webinar_date){
		global $wpdb;
		$wpdb->webinar_register			=	"webinar_registered_users";

		$attendee_webinar_registration	=	$wpdb->get_results("SELECT * FROM $wpdb->webinar_register WHERE webinar_id_fk='$webinar_id' and webinar_schedule_id_fk='$webinar_schedule_id' and attendee_email_id='$attendee_email' and webinar_date='$webinar_date'");
		return 	count($attendee_webinar_registration);
	}

	//	End of checking do attendee has registered or not

	/**
	 *	@descrioption					Follwing function will check that is there any room for particular webinar session to register.
	 *	@param			Webinar id		This is webinar id.
	 *					Schedule id		This is webinar session id.
	 *					Max attendee	This is max limit for the attendees to register for particluer webinar.
	 *	@return				0			When there is no room for attendee to register for particular webinar session.
	 *						1			When attendee can register for particular webnar session.
	 */

	function can_attendee_register_for_webinar_session($webinar_id,$schedule_id,$max_attendee,$webinar_date){
		global $wpdb;
		$wpdb->webinar_registration_room		=	"webinar_registered_users";
		$webinar_registration_room				=	$wpdb->get_results("SELECT count(*) as counts FROM webinar_registered_users  WHERE webinar_id_fk = '$webinar_id' and webinar_schedule_id_fk=$schedule_id  and webinar_date='$webinar_date'");
		if($max_attendee>0){
			return $max_attendee>$webinar_registration_room[0]->counts ?0 :1;
		}else{
			return	0;
		}

//		return count($webinar_registration_room);
	}

	//	End of checking that attendee can register or not for particular webinar session.
	/**
	 *	@description 				Following function will get the all unexpired sessions for particular webinar schedule day
	 *	@param		 Webinar id		This is id of webinar
	 *				 week day		This is week day of maximum date.
	 *				 max_attendee	This is max attendee limit for the  particuelr webinar sessions.
	 *	@return		 Array			This array will contains all the rows of particluer day session.
	 */

	 function get_week_day_webinar_session($webinar_id,$max_week_day,$max_attendee){
	 	global	$wpdb;
		$current_date = date('Y-m-d');
		return	$wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time,t2.webinar_date from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date>".$current_date." and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk AND t2.webinar_date = (SELECT max(webinar_date) AS maximum FROM webinar_registered_users  WHERE webinar_id_fk =$webinar_id) where t1.webinar_id_fk=$webinar_id and t1.week_day=$max_week_day group by t1.webinar_schedule_id_pk having count<$max_attendee order by t1.start_time ASC");
	 }


	//	End of getting webinar wek_day schedules.

	/**
	 *	@description	 			Following function will get the week day of particular webinar schedule
	 *	@param		  schedule id	This is webinar schedule id.
	 *	@return		  week day		This is week day of particular webinar.
	 */

	function get_weekday_of_webinar_session($schedule_id){
		global $wpdb;
		$wpdb->webinar_schedule_week_day	=	"webinar_schedule";
	 	$webinar_schedule_week_day =	$wpdb->get_results("SELECT week_day FROM $wpdb->webinar_schedule_week_day WHERE webinar_schedule_id_pk=$schedule_id");
		return	$webinar_schedule_week_day[0]->week_day;
	}


	//	End of getting week day of particular webinar session.

   /**
    *	@description					Following function will get the webinar session detail for attendee
	*	@param			webinar id 		This is id of webinar
	*					Attendee name	This is attendee name correspoding to which we need webinar detail.
	*					Attendee email	This is attendee email correspoding to which we need webinar detail.
	*/

	function get_webinar_detail_for_attendee($webinar_id,$attendee_name,$attendee_email){
		global	$wpdb;
		$wpdb->attendee_webinar_detail	=	"webinar_registered_users";
		return	$wpdb->get_results("SELECT count(*) as count,min(webinar_date) FROM $wpdb->attendee_webinar_detail   where webinar_id_fk=1 and attendee_name='$attendee_name' and attendee_email_id='$attendee_email' and webinar_date>CURTIME()");
	 }


	//	End of get attendee webinar session detail.

    /**
	 *	@description				Follwoing function will check do attendee has registered for particular webinar or not.
	 *	@param		 webinar id		This is id of webinar.
	 *				 attendee name	This is attendee name.
	 *				 attendee email	This is attendee email.
	 *	@return		 	0			When attendee has not register for particular wehinar.
	 *					1			When attendee has register for particular webinar.
	 */

	function do_attendee_register_for_webinar($webinar_id,$attendee_name,$attendee_email){
		global $wpdb;
		$wpdb->attendee_webinar_register	=	"webinar_registered_users";
		$attendee_webinar_register			=	$wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk='$webinar_id' and attendee_name='$attendee_name' and attendee_email_id='$attendee_email'");
		return count($attendee_webinar_register);
	}


	function do_attendee_register_for_webinar_key($webinar_id,$key){
		global $wpdb;
		$wpdb->attendee_webinar_register	=	"webinar_registered_users";
		$attendee_webinar_register			=	$wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_registered_users.webinar_id_fk='$webinar_id' and webinar_registered_users.key='".mysql_real_escape_string($key)."' ");
		return count($attendee_webinar_register);
	}

    //	End of checking do attendee has register for particular webinar or not.

	
    /**
	 *	@description				Get registrant info by registrant id
	 *	@param		 attendee id	Attendee id set in session during registration
	 *	@return		 array			Array of attendee name, email and webinar_id
	 */
	function get_attendee_info_by_id($attendee_id){
		global $wpdb;
		return $wpdb->get_row($wpdb->prepare("SELECT attendee_name, attendee_email_id, webinar_id_fk FROM webinar_registered_users WHERE registration_id_pk=%d",$attendee_id));
	}
	//End of attendee_info_for_chatbox
	
	
	/**
	 *	@description				Following function will get the registratin page id for the webinar.
	 *	@param		 Webinar id 	This is webinar id whose registration page will be found.
	 *	@return		 Id				This is id of the webianr registration page.
	 */

	function get_webinar_registration_id($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		return $wpdb->get_var("SELECT ID FROM $table_name WHERE webinar_id=$webinar_id order by ID limit 0,1");
	}

	//	End of get registration id for webinar
	/**
	 *	@description				Following function will get the login page id for the webinar.
	 *	@param		 Webinar id 	This is webinar id whose login page will be found.
	 *	@return		 Id				This is id of the webianr login page.
	 */

	function get_webinar_login_id($webinar_id){
		global $wpdb;
		
		$table_name = $wpdb->prefix.'posts';
		return $wpdb->get_var("SELECT ID FROM $table_name WHERE webinar_id=$webinar_id order by ID ASC limit 1,2");
	}

	//	End of get login id for webinar

	/**
	 *	@description				Following function will get the video page id for the webinar.
	 *	@param		 Webinar id 	This is webinar id whose video page will be found.
	 *	@return		 Id				This is id of the webianr video page.
	 */

	function get_webinar_video_id($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		return $wpdb->get_var("SELECT ID FROM $table_name WHERE webinar_id=$webinar_id order by ID DESC limit 1,1");
	}

	//	End of get video id for webinar
	
	/**
	 *	@description				Following function will get the first session of attendee for particular webinar.
	 *	@param		 webinar id		This is id of webinar.
	 *				 attendee name	This is attendee name.
	 *				 attendee email	This is attendee email.
	 *	@return		 	0			When there is no webinar session left for the attendee greater than current date.
	 *					Array		When there is webinar session left for the attendee greater than current date.
	 */

	function get_latest_webinar_session_for_attendee($webinar_id,$attendee_name,$attendee_email){
		global $wpdb;
		$current_date = date('Y-m-d');
		$wpdb->latest_webinar_seesion_of_attendee	=	"webinar_registered_users";
		$attendee_webinar_seesion					=	$wpdb->get_row("select * from $wpdb->latest_webinar_seesion_of_attendee where webinar_id_fk=$webinar_id and attendee_name='$attendee_name' and attendee_email_id='$attendee_email' and webinar_date>".$current_date." order by registration_id_pk limit 0,1");
		return count($attendee_webinar_seesion)>0?$attendee_webinar_seesion:0;
	}

	function get_latest_webinar_session_for_attendee_key($webinar_id,$key){
		global $wpdb;
		$current_date = date('Y-m-d');
		$wpdb->latest_webinar_seesion_of_attendee	=	"webinar_registered_users";
		$attendee_webinar_seesion	=	$wpdb->get_row("select * from $wpdb->latest_webinar_seesion_of_attendee where webinar_id_fk=$webinar_id and `key`='$key' and webinar_date>".$current_date." order by registration_id_pk limit 1");
		return count($attendee_webinar_seesion)>0?$attendee_webinar_seesion:0;
	}

	//	End of getting latest session for the particular webinar of attendee.

	/**
	 *	@descrioption						Following funcion will get the webinar detail from registration id.
	 *	@param			Registration id		This is registration id for webinar session.
	 *	@return			Array				This is Array that cotnains the webinar detail corresponding to parti8culer webinar session.
	 */


	function get_webinar_session_from_registration_id($registration_id){
		global $wpdb;
		$wpdb->webinar_session_detail	=	"webinar_registered_users";
		return $wpdb->get_results("SELECT t1.webinar_date as webinar_date, t2.start_time as webinar_time,t1.webinar_id_fk as webinar_id,t1.attendee_name as attendee_name FROM webinar_registered_users t1  LEFT JOIN webinar_schedule t2 on t1.webinar_schedule_id_fk=t2.webinar_schedule_id_pk and t1.webinar_id_fk=t2.webinar_id_fk where t1.registration_id_pk='$registration_id'");
	}

	//	End of getting webinar detail from registration id.
    /**
	 *	@description						Following function will make db entry for attendee for a particular webinar to keep track attendees
	 *	@param			webinar id			This is webinar id for which attendee will be looged in.
	 *					registration_id		This is registration id of attendee that has loogged in.
	 *            		webinar_schedule_id	This is webinar particular schedule for which attendee has logged in.
	 *					webinar date		This is particuelr webinar date for  which attendee has logged in.
	 *	@return			Id					This id is id of currently insert record for particular webinar.
	 */

	function save_attendee_session_for_webinar_schedule($webinar_id,$schedule_id,$registartion_id,$webinar_date){
		global $wpdb;
		$wpdb->save_attendee_session	=	"webinar_logged_in_tracking";

		return	$wpdb->query("INSERT INTO $wpdb->save_attendee_session(webinar_id_fk,webinar_schedule_id_fk,registration_id_fk,date_of_webinar)  VALUES('".$webinar_id."','".$schedule_id."','".$registartion_id."','".$webinar_date."')");
	}

	//	End of keep track of attendee log in status for particular webinar schedule.
	
	/**
	 *	@description					Following function will get the current attendee email and first name for the webinar
	 *	@param			$attendeekey 		This is $attendeekey for which current attendee email and first name will be get.
	 *	@return			Array			This array will contains email and first name of current attendees  for particular webinar.
	 */
	
	function get_current_attendee_email($attendeekey)
	{
		global $wpdb;
		$wpdb->webinar_registered_users	=	"webinar_registered_users";
	    return $wpdb->get_results("SELECT attendee_email_id,attendee_name FROM $wpdb->webinar_registered_users WHERE `key`='" . $attendeekey . "'"); 
	}
	
	//	End of keep track of attendee log in status for particular webinar schedule.
	
	/**
	 *	@description					Following function will get the default attendee list for the webinar
	 *	@param			webinar id 		This is webinar id for which attendee list will be get.
	 *	@return			Array			This array will contains all attendees list for particular webinar.
	 */

	 function get_default_attendee_webinar_list($webinar_id){
	 	global $wpdb;
		$wpdb->default_attendee_list	=	"webinar_attendees";
		return $wpdb->get_results("SELECT * FROM $wpdb->default_attendee_list WHERE webinar_id_fk='".$webinar_id."'");
	 }

	//	End of getting deafult attendee list for webinar.
	/**
	 *	@description				Following function will get the looged in attendees for particular webinar schedule.
	 *	@param		  Webinar id  	This is webinar id.
	 *				  Schedule id	This is one schedule id of webinar.
	 *				  webinar date	This is webinar scheuled date.
	 *	@return		  Array			This is
	 */

	 function get_logged_in_attendees_for_webinar($webinar_id,$schedule_id,$webinar_date){
	 	global $wpdb;
		return $wpdb->get_results($wpdb->prepare("SELECT registration_id_pk FROM webinar_registered_users WHERE webinar_id_fk=%d AND webinar_schedule_id_fk=%s AND webinar_date=%s",$webinar_id,$schedule_id,$webinar_date),OBJECT);
	 }

	//	End of getting logged in attendess for particular webinar.
	/**
	 *	@description					Following function will get the all detail of webinar session correspoding to regitration id .
 	 *	@param		  registration id 	This is registration id of the attendee for particular webinar schedule.
	 *	@return		  Array				This array will contains the detail of webinar session detail for registration id.
	 */

	 function get_webinar_session_registration_id($registration_id){
	 	global $wpdb;
		$wpdb->webinar_session_detail	=	"webinar_registered_users";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_session_detail WHERE registration_id_pk='".$registration_id."'");
	 }
	//	End of get webinar session detail for registration id.
	
	/**
	 *	@description				Following function will get delayed events for particular webinar.
	 *	@param			Webinar id 	This is webinar id.
	 *	@return			Array 		This array will contains detail info of delayed events for particular webinar.
	 */
	function get_webinar_delayed_events($webinar_id){
		global $wpdb;
		$wpdb->webinar_delayed_events		=	"webinar_delayed_events";
		$wpdb->delayed_buttons				=	"webinar_buttons";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_delayed_events t1 LEFT JOIN $wpdb->delayed_buttons t2 ON t1.webinar_button_id_fk=t2.webinar_button_id_pk WHERE webinar_id_fk=$webinar_id ORDER BY t1.webinar_delayed_events_id_pk ASC");
		
	}

	 //	End of getting delayed events for particular webinar.

	 function get_webinar_scarcity($webinar_id){
		global $wpdb;
		$wpdb->webinar_scarcity		=	"webinar_scarcity";
		$wpdb->scarcity_buttons		=	"webinar_buttons";
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_scarcity t1  LEFT JOIN $wpdb->scarcity_buttons t2 on t1.button_id_fk=t2.webinar_button_id_pk where webinar_id_fk=$webinar_id");
	}

	/**
	 *	@description					Following function will get unexpired session of webinar for particular attendees.
	 *	@param		   Webinar id		This is webinar id.
	 *			   	   Attendee_name	This is attende name.
	 *				   Attendee_email	This is attendee email.
	 *	@return		   Array			This array will contains detail info of today unexpired session for particuelr attendee.
	 */
	function get_unexpired_roday_session_for_attendee($webinar_id,$attendee_name,$attendee_email){
		global $wpdb;
		$wpdb->webinar_attendee_register	=	"webinar_registered_users";
		$wpdb->webinar_schedule	=	"webinar_schedule";
		$current_time						=	date("H:i:s");
		$cuurent_date						=	date("Y-m-d");
		$attendee_unexpired_session			=	$wpdb->get_results("SELECT * FROM $wpdb->webinar_attendee_register WHERE attendee_name='$attendee_name' and attendee_email_id='$attendee_email' and webinar_id_fk='$webinar_id' and webinar_date='$cuurent_date'	 AND webinar_time>'$current_time'");
		return count($attendee_unexpired_session)>0?$attendee_unexpired_session:0;
	}

	function get_todays_unexpired_session_by_attendee_key($webinar_id,$key,$right_now=0){
		global $wpdb;
		$wpdb->webinar_attendee_register=	"webinar_registered_users";
		$wpdb->webinar_schedule				  =	"webinar_schedule";
		
		$current_time						=	date("H:i:s");
		$current_date						=	date("Y-m-d");
		
		if($right_now==0){
			$attendee_unexpired_session	=	$wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->webinar_attendee_register WHERE `key`=%s and webinar_id_fk=%d and webinar_date='$current_date' AND webinar_end_time>'$current_time'",$key,$webinar_id));
		}
		else{
			$attendee_unexpired_session	=	$wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->webinar_attendee_register WHERE `key`=%s AND webinar_id_fk=%d AND webinar_date='$current_date'",$key,$webinar_id));
		}
		return count($attendee_unexpired_session)>0?$attendee_unexpired_session:0;
	}

	//	End of getting unexpired session of webinar for particular attendee
	/**
	 *	@description				Following function will get detail info of webinar schedule session.
	 *	@param		 Schedule id	This is webinar schedule id.
	 *	@retutn		  Array 		This array will contains the detil info of webinar schedule corresponding to particular schedule id.
	 */
	function get_webinar_schedule_detail($schedule_id){
		global $wpdb;
		$wpdb->webinar_schedule	=	"webinar_schedule";
		$webinar_scheudle		=	$wpdb->get_results("SELECT * FROM $wpdb->webinar_schedule WHERE webinar_schedule_id_pk='".$schedule_id."'");
		return count($webinar_scheudle)>0?$webinar_scheudle:0;
	}
	
	/**
	 *	@description						Following function will check
	 *	@param			webinar id			This is id of webinar.
	 *					schedule id			This is one of schedule id of webinar.
	 *					registration id		This is registration id for that webinar.
	 *					webinar date		This is webinar date for which webinar is registered.
	 *	@return			Array				This is array that will contains all the attendee list.
	 */

	function is_attendee_already_logged_in($webinar_id,$schedule_id,$registartion_id,$webinar_date){
		global $wpdb;
		$wpdb->attende_already_logged_in	=	"webinar_logged_in_tracking";
		$already_logged_in	=	$wpdb->get_results("SELECT count(*) as counts FROM $wpdb->attende_already_logged_in WHERE webinar_id_fk='".$webinar_id."' AND webinar_schedule_id_fk='".$schedule_id."' AND registration_id_fk='".$registartion_id."' AND date_of_webinar='".$webinar_date."'");
		return	$already_logged_in[0]->counts;
	
	}

	//	End of check is attendee already logged in.
	
	/**
	 *	@description				Following function will check wheather first attendee is going to register for webinar.
	 *	@param		  Id	 		This is id of webinar.
	 *				  Schedule id 	This is webinar schedule id.
	 *				  Date			This is webinar scheduled date.
	 *	@return		  0				When no attendee has registered for this webinar yet.
	 *				  >0			When some attendee has already registered for this webinar.
	 */

	function do_attendees_already_registered_for_webinar($webinar_id,$schedule_id,$webinar_date){
		global $wpdb;
		$wpdb->attendees_already_registered		=	"webinar_registered_users";
		$attendees_registered					=	$wpdb->get_results("SELECT * FROM $wpdb->attendees_already_registered WHERE webinar_id_fk='".$webinar_id."' and webinar_schedule_id_fk='".$schedule_id."' and webinar_date='".$webinar_date."'");

		return count($attendees_registered);
	}
	//	End of checking wheather first attendee is going to register for webinar.
	
	/**
	 *	@description					Following function will update schedule for webinar notifications.
	 *	@param		 ID					This is id of webinar.
	 *				 Notification_date	This is date when particular notification will be sent for particular webinar.
	 *				 Notification_time	This is time when notification will be sent for particular webinar.
	 *				 Notification_type	This is notification type.
	 *				 Webinar_time		This is webinar time at which webinar is scheduled.
	 *				 Webinar_date		This is webinar date on which webinar is scheduled.
	 *	@return		  Id				This is id of row notificationd  etail inserted for particular webinar.
	 */
	
	function update_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_type,$webinar_time,$webinar_date,$status){
		global $wpdb;
		$wpdb->webinar_notification_detail		=		"notification_timing";
	
		$notification_update					=		$wpdb->query("UPDATE $wpdb->webinar_notification_detail SET notification_date='$notification_date',notification_time='$notification_time',webinar_time='$webinar_time',webinar_date='$webinar_date',notification_status=$status WHERE webinar_id=$webinar_id AND notification_type=$notification_type");
		//return mysql_insert_id();
	}
	//	End of saving schedule for webinar notification
	
	/**
	 *	@description					Following function will save schedule for webinar notifications.
	 *	@param		 ID					This is id of webinar.
	 *				 Notification_date	This is date when particular notification will be sent for particular webinar.
	 *				 Notification_time	This is time when notification will be sent for particular webinar.
	 *				 Notification_type	This is notification type.
	 *				 Webinar_time		This is webinar time at which webinar is scheduled.
	 *				 Webinar_date		This is webinar date on which webinar is scheduled.
	 *	@return		  Id				This is id of row notificationd  etail inserted for particular webinar.
	 */

	function save_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_type,$webinar_time,$webinar_date){
		global $wpdb;
		$wpdb->webinar_notification_detail		=		"notification_timing";

		$notification_insert_id					=		$wpdb->query("INSERT INTO $wpdb->webinar_notification_detail(webinar_id,notification_date,notification_time,notification_type,webinar_time,webinar_date) VALUES ('".$webinar_id."','".$notification_date."','".$notification_time."','".$notification_type."','".$webinar_time."','".$webinar_date."')");
		//return mysql_insert_id();
		return $wpdb->insert_id;
	}
	//	End of saving schedule for webinar notification

	/**
	 *	@description			Following function will get webinar page id.
	 *	@param		 Page name	This is page name for which id will be get.
	 *	@return		 Id			This is id of page corresponding to page.
	 */

	function get_webinar_template_page_id($page_name){
		global $wpdb;
		$wpdb->get_page_id		=		"webinar_page_ids";
		$webinar_page_array		=		$wpdb->get_results("SELECT * FROM $wpdb->get_page_id WHERE page_name like '%".$page_name."%'");
		
		return $webinar_page_array[0]->post_id;
	}
		
	function get_all_webinar_page_ids(){
		global $wpdb;
		$table_name  = $wpdb->prefix.'posts';
		return $wpdb->get_results("SELECT ID FROM $table_name WHERE webinar_id>0");
	}
	
	function get_webinar_register_page_id($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		$wpdb->get_page_id		=		$table_name;
		$webinar_page_array		=		$wpdb->get_results("SELECT * FROM $wpdb->get_page_id WHERE webinar_id='".$webinar_id."'");
		return $webinar_page_array[0]->ID;
	}
	
	function get_webinar_thankyou_page_id($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		$wpdb->get_page_id		=		$table_name;
		$webinar_page_array		=		$wpdb->get_results("SELECT * FROM $wpdb->get_page_id WHERE webinar_id='".$webinar_id."'");
		return $webinar_page_array[2]->ID;
	}

	function get_countdown_page_id(){
		global $wpdb;
		$wpdb->get_page_id	=	"webinar_page_ids";
		$post_id_array		  =	$wpdb->get_results("SELECT * FROM $wpdb->get_page_id WHERE page_name='countdown'");
		
		return 	$post_id_array[0]->post_id;
	}
	
	function get_error_page_id(){
		global $wpdb;
		$wpdb->get_page_id =		"webinar_page_ids";
		$post_id_array		 =		$wpdb->get_results("SELECT * FROM $wpdb->get_page_id WHERE page_name='error'");
		
		return 	$post_id_array[0]->post_id;
	}
	
	function get_webinar_notification_mails($webinar_id){
		global $wpdb;
		$wpdb->notification_emails		=		"webinar_notification_emails";
		return	$wpdb->get_results("SELECT * FROM $wpdb->notification_emails WHERE webinar_id_fk='".$webinar_id."'");
	}
	//	End of getting webinar notificaiton detail.

	/**
	 *	@description				Following function will get schedule id .
	 *	@param			Time		This is schedule time.
	 *					Id			This is webinar id.
	 *	@return			Id			This is schedule id of webinar.
	 */

	 function get_schedule_id_for_notification($webinar_time,$webinar_id){
	 	global $wpdb;
	 	$wpdb->webinar_schedule		=		"webinar_schedule";

		$schedule_id_array			=		$wpdb->get_results("SELECT * FROM $wpdb->webinar_schedule WHERE webinar_id_fk='".$webinar_id."' and start_time='".$webinar_time."'");
		return $schedule_id_array[0]->webinar_schedule_id_pk;
	 }

	//	End of getting schedule id.
	/**
	 *	@description			Following function will get notification detail for sending email
	 *	@param					No argument.
	 *	@return		 Array		This array will contains notification detail that need to send.
	 */

	function get_notification_detail($cur_date,$cur_time){
		global $wpdb;
		$current_time			=	explode(":",$cur_time);
		$previous_time		=	date("H:i:s",mktime($current_time[0],$current_time[1]-15,0,0,0,0));
		$post_time        = date("H:i:s",mktime($current_time[0],$current_time[1],0,0,0));
		$next_time        = date("H:i:s",mktime($current_time[0],$current_time[1]+15,0,0,0));
		$wpdb->notification_timing_detail	=	"notification_timing";
		/*$fp = fopen('data.txt', 'w');
		fwrite($fp, "SELECT DISTINCT notification_timing.webinar_id,webinar_notification_emails.subject,webinar_notification_emails.message,notification_timing.timing_id_pk,notification_timing.webinar_time,notification_timing.webinar_date,webinar.webinar_main_topic,webinar.presenters_name,webinar.sender_name,webinar.notification_from_address,webinar.admin_email_value,webinar_registered_users.webinar_schedule_id_fk,webinar_registered_users.webinar_real_time,webinar_registered_users.webinar_real_date,webinar_registered_users.attendee_name,webinar_registered_users.attendee_email_id,webinar_registered_users.key FROM notification_timing LEFT JOIN webinar_notification_emails ON notification_timing.notification_type = webinar_notification_emails.webinar_notification_id_pk LEFT JOIN webinar_registered_users ON ( notification_timing.webinar_id = webinar_registered_users.webinar_id_fk and notification_timing.webinar_time = webinar_registered_users.webinar_time AND notification_timing.webinar_date = webinar_registered_users.webinar_date )  LEFT JOIN webinar ON notification_timing.webinar_id = webinar.webinar_id_pk LEFT JOIN webinar_schedule ON ( notification_timing.webinar_id = webinar_schedule.webinar_id_fk AND notification_timing.webinar_date = webinar_schedule.webinar_date AND notification_timing.webinar_time = webinar_schedule.start_time ) WHERE notification_timing.notification_date='".$cur_date."' AND  notification_timing.notification_status=1 AND notification_timing.notification_time>='".$previous_time."' AND notification_timing.notification_time<='".$post_time."'");
		fclose($fp); */
		
		return	$wpdb->get_results("SELECT DISTINCT notification_timing.webinar_id,webinar_notification_emails.subject,webinar_notification_emails.message,notification_timing.timing_id_pk,notification_timing.webinar_time,notification_timing.webinar_date,webinar.webinar_main_topic,webinar.presenters_name,webinar.affilate_param,webinar.affilate_param_val,webinar.sender_name,webinar.notification_from_address,webinar.admin_email_value,webinar_registered_users.webinar_schedule_id_fk,webinar_registered_users.webinar_real_time,webinar_registered_users.webinar_real_date,webinar_registered_users.attendee_name,webinar_registered_users.attendee_email_id,webinar_registered_users.key FROM notification_timing LEFT JOIN webinar_notification_emails ON notification_timing.notification_type = webinar_notification_emails.webinar_notification_id_pk LEFT JOIN webinar_registered_users ON ( notification_timing.webinar_id = webinar_registered_users.webinar_id_fk and notification_timing.webinar_time = webinar_registered_users.webinar_time AND notification_timing.webinar_date = webinar_registered_users.webinar_date )  LEFT JOIN webinar ON notification_timing.webinar_id = webinar.webinar_id_pk LEFT JOIN webinar_schedule ON ( notification_timing.webinar_id = webinar_schedule.webinar_id_fk AND notification_timing.webinar_date = webinar_schedule.webinar_date AND notification_timing.webinar_time = webinar_schedule.start_time ) WHERE (notification_timing.notification_date='".$cur_date."' AND  notification_timing.notification_status=1 AND  notification_timing.notification_time<='".$post_time."')  OR (TIMEDIFF(notification_timing.webinar_time,notification_timing.notification_time)='00:15:00' AND notification_timing.notification_date='".$cur_date."' AND  notification_timing.notification_status=1 AND  notification_timing.notification_time <='".$next_time."')");
	}
	
	
	 function delete_notification_cron_detail($cur_date,$cur_time){
		/* global $wpdb;
		
		$current_time		=	explode(":",$cur_time);
		$previos_time		=	date("H:i:s",mktime($current_time[0],$current_time[1]-15,0,0,0,0));
		$post_time      = date("H:i:s",mktime($current_time[0],$current_time[1]+15,0,0,0,0));
		$wpdb->notification_timing_detail	=	"notification_timing";
		
		return $wpdb->get_results("DELETE FROM $wpdb->notification_timing_detail WHERE notification_date='".$cur_date."' AND notification_time>='".$previos_time."' and notification_time<='".$post_time."'"); */
		
	} 
	
	function delete_notification_cron_details($timing_id_pk_array){
	/*	global $wpdb;
		$timing_ids		=	implode(",",$timing_id_pk_array);
		return $wpdb->query("DELETE FROM notification_timing WHERE timing_id_pk IN ($timing_ids)"); */
	}
	
	function update_notification_cron_details($timing_id_pk_array){
		global $wpdb;
		$timing_ids		=	implode(",",$timing_id_pk_array);
		return $wpdb->query("UPDATE notification_timing SET notification_status=0 WHERE timing_id_pk IN ($timing_ids)");
	}
	
	//	End of getting notification detail
	/**
	 *	@description					Following funciton will get a particular webinar scheule attendees
	 *	@param			Id				This is webinar id.
	 *					Schedule Id		This is schedule id of webinar.
	 *					webinar date	This is webinar schedule date.
	 *	@return			Array			This is array that will contains all attedees of particular webinar.
	 */

	function get_webinar_attendees_for_particuler_schedule($webinar_id,$schedule_id,$webinar_date){
		global $wpdb;
		$wpdb->webinar_attendees	=		"webinar_registered_users";
		return	$wpdb->get_results("SELECT * FROM $wpdb->webinar_attendees WHERE webinar_id_fk='".$webinar_id."' and webinar_schedule_id_fk='".$schedule_id."' and webinar_date='".$webinar_date."'");
	}

	function get_webinar_attendees_for_particular_datetime($webinar_id,$webinar_time,$webinar_date){
		global $wpdb;
		$wpdb->webinar_attendees	=	"webinar_registered_users";
		return 	$wpdb->get_results("SELECT * FROM $wpdb->webinar_attendees WHERE webinar_id_fk='".$webinar_id."' and webinar_time='".$webinar_time."' and webinar_date='".$webinar_date."'");
	}


	//	End of getting attendees for particular webinar schedule.
	/**
	 *	@description				Following function will
	 *	@param		  Email to		This is email address to whom email will be sent.
	 *				  Email from	This is webinar owner email.
	 *				  Subject		This is email subject.
	 *				  Body			This is email body.
	 */

	 function save_scheduled_mail_to_send($email_to,$email_from,$subject,$body){
	 	global $wpdb;
		$wpdb->send_attendee_email		=		"notification_sent_to";
		$row_id	= $wpdb->query($wpdb->prepare("INSERT INTO $wpdb->send_attendee_email(email_to,email_from,subject,body) VALUES(%s,%s,%s,%s)",array($email_to,$email_from,$subject,$body)));
		//return mysql_insert_id();
		return $wpdb->insert_id;
	 }
	 
	 function delete_attendee_email($id){
	 	global $wpdb;
		$wpdb->send_attendee_email		=		"notification_sent_to";
		$row_id	= $wpdb->query($wpdb->prepare("INSERT INTO $wpdb->send_attendee_email(email_to,email_from,subject,body) VALUES(%s,%s,%s,%s)",array($email_to,$email_from,$subject,$body)));
		return $wpdb->insert_id;
	 }
	 
	//	End of saving attendee for sending email.
	/**
	 *	@description			Following function will get attendess record to send email
	 *	@param					No paramter is required.
	 *	@return 		Array	This is array that will contains 70 attendees record to send email.
	 */

	function get_attendee_to_send_email(){
		global $wpdb;
		$wpdb->get_attendees_detail		=		"notification_sent_to";
		/* $sent_to_result_id = mysql_query("SELECT * FROM $wpdb->get_attendees_detail WHERE stat=0 ORDER BY notification_sent_to_id_pk ASC LIMIT 100");
		return mysql_fetch_object($sent_to_result_id); */
		return $wpdb->get_results("SELECT  * FROM $wpdb->get_attendees_detail WHERE stat=0 ORDER BY notification_sent_to_id_pk ASC LIMIT 100");
	}


	function check_notification_time($notification_time,$notification_date,$webinar_id,$notification_email_id){
		global $wpdb;
		//return $wpdb->get_results("SELECT count(*) as count FROM notification_timing WHERE notification_date='$notification_date' AND notification_time='$notification_time' and webinar_id=$webinar_id and notification_type = $notification_email_id");
		return $wpdb->get_results("SELECT count(*) as count,notification_date , notification_time FROM notification_timing WHERE  notification_date='$notification_date' AND notification_time='$notification_time' AND webinar_id=$webinar_id and notification_type = $notification_email_id");
	}


	//	End of getting attendees for sending email
	/**
	 *	@description			Following fucntion will delete record from notification sent to table that has already sent with email.
	 *	@param			Id		This is is of row that has to be deleted.
	 *	@return			1		When record is successfully deleted.
	 *					0		When record is not successfully deleted.
	 */


	function delete_attendee_row_after_sending_email($id){
		global $wpdb;
		$wpdb->delete_attendee_record		=		"notification_sent_to";
		return	$wpdb->query("DELETE FROM $wpdb->delete_attendee_record WHERE notification_sent_to_id_pk='".$id."'");
	}
	//	End of deleting record that has already sent.
	/**
     *	@description			Following function will get detail of latest created webinar.
	 *	@param					No  parameter.
	 *	@return		Array		This array will contains detail of latest created webinar.
	 */

	function get_latest_webinar_info(){
		global $wpdb;
		$wpdb->latest_webinar		=		"webinar";

		return $wpdb->get_results("SELECT * FROM $wpdb->latest_webinar WHERE webinar_id_pk=(SELECT MAX(webinar_id_pk) FROM $wpdb->latest_webinar)");
	}

	//	End of  getting latest webinar info.
	/**
     *	@description				Following function will get week days for mutiple days in week.
	 *	@param			webinar id	This is id of webinar.
     *	@return			Array 		This array will contains  all days for webinar scheduled.
     */
	function get_webinar_scheduled_days ($webinar_id){
		global $wpdb;
		$wpdb->webinar_scheduled_days		=		"webinar_schedule";
		return	$wpdb->get_results("SELECT week_day FROM webinar_schedule WHERE webinar_id_fk='".$webinar_id."' group by week_day");
	}

	//	End of getting days for multiple scheduling
	/**
	 *	@description				Following function will get specific date for webinar
	 *	@param			webinar_id	This is webinar id.
	 *	@return			Array		This array contains  specific date for webinar.
	 */


	function get_specific_date($webinar_id){
		global $wpdb;
		$wpdb->specific_date		=		"webinar_schedule";
		$specific_dates				=		$wpdb->get_results("SELECT distinct webinar_date FROM webinar_schedule WHERE webinar_id_fk='".$webinar_id."'");
		
		foreach($specific_dates as $specific){
			$specific_date[]	=	$specific->webinar_date;
		}
				
		return $specific_date;
	}
	//	End of get specific date
	/**
	 *	@description				Following function will get scheduled times for webinar.
	 *	@param		  webinar id	This is webinar id .
	 *	@return		  Array			This is array that will contains scheuded times for webinar.
	 */

	function get_scheduled_times_for_webinar($webinar_id){
		global $wpdb;
		$wpdb->scheduled_times		=	"webinar_schedule";
		return	$wpdb->get_results("SELECT * FROM $wpdb->scheduled_times WHERE webinar_id_fk='".$webinar_id."' group by start_time");
	}

	//	End of getting scheduled times for webinar.
	/**
     *	@description		Following function will delete webinar corresponding to primary key supplied.
	 *	@param			Id	This is id of webinar that will be deleted.
	 *	@return			1	When  webinar has deleted successfully.
	 *					0	When webinar has not deleted successfully.
	 */

	function delete_webinar($webinar_id){
		global $wpdb;

		$wpdb->delete_webinar	=		"webinar";
		return	$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->delete_webinar WHERE webinar_id_pk=%d",$webinar_id));
	}
	//	End of deleting webinar.
	/**
	 *	@description				Following funciton will delete all default attendees.
	 *	@param		  webinar id 	This is webinar id .
	 *	@retun		    1			When all default attendees of particular webinar has deleted.
	 *					0			When all default attendees of particular webinar are not deleted.
	 */

	function delete_webinar_default_attendees($webinar_id){
		global $wpdb;
		$wpdb->delete_default_attendees		=	"webinar_attendees";
		return	$wpdb->get_results("DELETE FROM $wpdb->delete_default_attendees WHERE webinar_id_fk='".$webinar_id."'");		
	}
	//	End of deleting dafult attendees
	/**
	 *	@description				Following  function will delete webinar delayed events.
	 *	@param			webinar id	This is webinar id.
	 *	@return				1		When all delayed events are deleted from delayed event table.
 	 *						0		When all delayed events are not deleted from delayed event table.
	 */

	function delete_webinar_delayed_events($webinar_id){
		global $wpdb;
		$wpdb->delete_delayed_events		=	"webinar_delayed_events";
		return	$wpdb->get_results("DELETE FROM $wpdb->delete_delayed_events WHERE webinar_id_fk='".$webinar_id."'");
	}
	//	End of deleting webinar delayed events.
	/**
	 *	@description				Following  function will delete webinar Scarcity.
	 *	@param			webinar id	This is webinar id.
	 *	@return				1		When all delayed events are deleted from delayed event table.
 	 *						0		When all delayed events are not deleted from delayed event table.
	 */

	function delete_webinar_scarcity($webinar_id){
		global $wpdb;
		$wpdb->delete_scarcity		=	"webinar_scarcity";
		return	$wpdb->get_results("DELETE FROM $wpdb->delete_scarcity WHERE webinar_id_fk='".$webinar_id."'");
	}
	//	End of deleting webinar Scarcity.
	/**
	 *	@description				Following funciton will delete webinar notification detail.
	 *	@param			webinar id	This is webinar id.
	 *	@return				1		When notificaiton detail is deleted from notification table.
	 *						0		When notificaiton detail is not  deleted from notification table.
	 */

	function delete_notification_detail($webinar_id){
		global $wpdb;
		$wpdb->delete_webinar_notification	=	"webinar_notification_emails";
		$is_notification_detail_deleted		=	$wpdb->get_results("DELETE FROM $wpdb->delete_webinar_notification WHERE webinar_id_fk='".$webinar_id."'");
		return $is_notification_detail_deleted;
	}
	//	End of deleting webinar notification detail.
	/**
	 *	@description				Following function will delete webinar scheule rows.
	 *	@param			webinar id	This is webinar id.
	 *	@return				1		When all webinar schedule rows are deleted.
	 */

	function delete_webinar_schedules ($webinar_id){
		global $wpdb;
		$wpdb->delete_schedules		=	"webinar_schedule";
		return $wpdb->get_results("DELETE FROM $wpdb->delete_schedules	 WHERE webinar_id_fk='".$webinar_id."'");
	}
	

	
	//	End of deleting webinar scheule rows.
	/**
	 *	@description		Following function will delete registered users for webinar
	 *	@param		 	Id	This is webinar id corresponding to which users will be deleted.
	 *	@return			1	When all registered users are succesfully deleted.
	 *					0	When all registered users are not succesfully deleted.
	 */
	 
	function delete_sharing($webinar_id){
		global $wpdb;
		$wpdb->delete_sharing		=	"webinar_sharing";
		return	$wpdb->get_results("DELETE FROM $wpdb->delete_sharing	 WHERE webinar_id_fk='".$webinar_id."'");
	}
     //	End of deleting webinar sharing rows.
	
	function delete_webinar_registered_users($webinar_id){
		global $wpdb;
		$wpdb->delete_registered_users		=		"webinar_registered_users";
		return	$wpdb->query("DELETE FROM $wpdb->delete_registered_users WHERE webinar_id_fk='".$webinar_id."'");
	}
	//	End of deleting register user for webinar.
	/**
	 *	@description		Following function will delete notification details for particular webinar.
	 *	@param			Id	This is webinar id.
	 *	@return			1	When webinar notificaiton details are succesfully deleted.
	 *					0	When webinar notificaiton details are not succesfully deleted.
	 */

	function delete_wbeinar_notification_dettails($webinar_id){
		global $wpdb;
		$wpdb->delete_webinar_notification_details		=		"notification_timing";
		return	$wpdb->query("DELETE FROM $wpdb->delete_webinar_notification_details WHERE webinar_id='".$webinar_id."'");
	}
	//	End of deleting notification details for webinar
	
	/**
	 *	@description			Following function will delete webinar pages.
	 *	@param			Id		This is webinar id.
	 *	@return			1		When all webinar pages mata are deleted.
	 *					0		When all webinar pages mata are not deleted.
	 */
	function delete_webinar_pages_meta($webinar_id){
		global $wpdb;
		$meta_table_name = $wpdb->prefix.'postmeta';
		
		$all_pages_ids	 =	$this->get_webinar_all_pages_ids($webinar_id);
		
		foreach($all_pages_ids as $page_id)
		$is_webinar_meta_deleted = $wpdb->query("DELETE FROM $meta_table_name WHERE post_id=$page_id->ID");

		return $is_webinar_meta_deleted;
	}
	//	End of deleting webinar pages meta
	
	/**
	 *	@description			Following function will delete webinar pages.
	 *	@param			Id		This is webinar id.
	 *	@return			1		When all webinar pages are deleted.
	 *					0		When all webinar pages are not deleted.
	 */

	function delete_webinar_pages($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		$wpdb->delete_webinar_pages			=		$table_name;
		return	$wpdb->query("DELETE FROM $wpdb->delete_webinar_pages WHERE webinar_id='".$webinar_id."'");
	}
	//	End of deleting webinar pages
	
	function delete_webinar_questions($webinar_id){
		global $wpdb;
		$wpdb->webinar_questions	=	'webinar_questions';
		return	$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->webinar_questions WHERE webinar_id=%d",$webinar_id));
	}
	
	function delete_webinar_events_tracking($webinar_id){
		global $wpdb;
		$wpdb->webinar_events_tracking	=	'webinar_events_tracking';
		return	$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->webinar_events_tracking WHERE webinar_id=%d",$webinar_id));
	}
	
	function delete_webinar_visitor_tracking($webinar_id){
		global $wpdb;
		$wpdb->webinar_visitor_tracking	=	'webinar_visitor_tracking';
		return	$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->webinar_visitor_tracking WHERE webinar_id=%d",$webinar_id));
	}
	
	function delete_webinar_template($webinar_id){
		global $wpdb;
		$wpdb->webinar_template	=	'webinar_template';
		return	$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->webinar_template WHERE webinar_id=%d",$webinar_id));
	}
	/**
	 *	@description		Following function will get delayed button detail from button id.
	 *	@param		  Id	This is delayed button id.
	 *	@return		  Array	This array will contains detil of delayed event corresponding to button id.
     */

	function get_delayed_button_detail($button_id){
		global $wpdb;
		$wpdb->delayed_button_detail	=	"webinar_buttons";
		return	$wpdb->get_results("SELECT * FROM $wpdb->delayed_button_detail WHERE webinar_button_id_pk='".$button_id."'");
	}
	//	End of getting delayed button information.
	/**
	 *	@description				Following function will get  two new atendees from remaining half.
	 *	@param		  Id			This is webinar id.
	 *				  atendee_ids	This is default attendee ids who are already listed in video page.
	 *	@return		  Array 		This is array that will contains two new random attendees information.
	 */

	function get_two_new_default_attedees($webinar_id,$attendee_ids){
		global $wpdb;

		$wpdb->new_default_attendees		=	"webinar_attendees";
		return $wpdb->get_results("SELECT distinct(webinar_attendee_id_pk) as attendee_id,attendee_name FROM $wpdb->new_default_attendees where webinar_id_fk='".$webinar_id."' and webinar_attendee_id_pk not in(".$attendee_ids.")  ORDER BY RAND()  LIMIT 0,2");
	}
	//	End of getting 2 new attendees from remainning attendees
	/**
	 *	@description			Following function will get particular timezone detail.
	 *	@param					No parameter.
	 *	@return		 Array		This array will contains deyail of all webinar.
	 */

	function get_timezones_detail(){
		global $wpdb;
	 	$wpdb->timezones_detail		=		"webinar_timezone";
	 	return $wpdb->get_results("SELECT * FROM $wpdb->timezones_detail");
	}
	//	End of getting timezones detail
	/**
	 *	@description		Following function will get particular timezone detail.
	 *	@param			Id	This is id of timezone.
	 *	@return 	Array	This arraty will contanins detail omf particular timezone.
	 */

	function get_timezone_detail($timezone_id){
		global $wpdb;
	 	$wpdb->timezone_detail		=		"webinar_timezone";
	 	return $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->timezone_detail WHERE webinar_timezone_id_pk=%d",$timezone_id));
	}
	//	End of getting particular timezone detail.
	/**
	 *	@description			Following function will get registration detail for particular  user.
	 *	@param			Id		This is registration id.
	 *	@return			Array	This array will contains registration detail for particular user.
	 */

	function get_registration_detail($registration_id){
		global $wpdb;
		$wpdb->registration_detail		=		"webinar_registered_users";
		return	$wpdb->get_results("SELECT * FROM $wpdb->registration_detail WHERE registration_id_pk='".$registration_id."'");
	 }
    //	End of get registration detail for particular session.
	
	/**
	 *	@description			Following function will get registration details for user key.
	 *	@param			Key		User Key.
	 *	@return			Array	This array will contains registration detail for particular user.
	 */

	function get_registration_details($key){
		global $wpdb;
		$wpdb->registration_detail	=	"webinar_registered_users";
		return	$wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->registration_detail WHERE $wpdb->registration_detail.key=%s",$key));
	 }
    //	End of get registration detail for particular session.
	
	/**
	 *	@description			This function will get all schedules for particular webinar.
	 *	@param			Id		This is id of webinar.
	 *	#return			Array	This is array that will contains detail of all schedules for particular webinar.
	 */

	function get_all_schedules_for_particuler_webinar($webianr_id){
		global $wpdb;
		$wpdb->schedules_detail	=	"webinar_schedule";
		return $wpdb->get_results("SELECT * FROM $wpdb->schedules_detail WHERE webinar_id_fk='".$webianr_id."'");
	}
	
	/**
	 *	@description				Following function will get all pages id for webinar.
	 *	@param			webinar id	This is webinar id.
	 *	@return			Array		This is array that will contains all webinar pages created.
	 */
	
	function get_webinar_all_pages_ids($webinar_id){
		global $wpdb;
		$table_name = $wpdb->prefix.'posts';
		return $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE webinar_id=%d ORDER BY ID ASC",$webinar_id));
	}	
	//	End of getting all pages of webinar
	
	/**
	 *	@description			This function results out the all the pages link in an array.
	 *	@param			ID		This is id of webinar.
	 *	#return			Array	This array contain all pages link.
	 */
	function get_webinar_all_pages_links($webinar_id){
		$links_detail							 =	$this->get_webinar_all_pages_ids($webinar_id);
		
		$link['registration_page'] =	get_permalink($links_detail[0]->ID);
		$link['login_page'] 			 =	get_permalink($links_detail[1]->ID);
		$link['thankyou_page']		 =	get_permalink($links_detail[2]->ID);
		$link['video_page']				 =	get_permalink($links_detail[4]->ID);
		$link['after_webinar_page']=	get_permalink($links_detail[3]->ID);
		$link['live_page']				 =	get_permalink($links_detail[5]->ID);
		return $link;		
		
	}
	/*
	*	@description			This function get value from notification emails table for before emails
	*/ 
	function get_before_webinar_notification_emails($webinar_id){
		global $wpdb;
		
		$wpdb->webinar_notification_emails = 'webinar_notification_emails';
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_notification_emails WHERE webinar_id_fk=$webinar_id and notification_type_id='2' ");
	}
	
	/*
	 *	@description			This function get value from notification timing table for notification timimg details
	*/
	/* function get_notification_timing_details($webinar_id){
		global $wpdb;
		$wpdb->notification_timing = 'notification_timing';
		return $wpdb->get_results("SELECT * FROM $wpdb->notification_timing WHERE webinar_id=$webinar_id");
	} 
	*/
	
	/*
	*	@description			This function get value from notification emails table for welcome email
	*/ 
	function get_welcome_webinar_notification_emails($webinar_id){
		global $wpdb;
		$wpdb->webinar_notification_emails = 'webinar_notification_emails';
		
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_notification_emails WHERE webinar_id_fk=$webinar_id and notification_type_id='1' ");
	}
	
	/*
	*	@description			This function get value from notification emails table for after webinar email
	*/ 
	function get_after_webinar_notification_emails($webinar_id){
		global $wpdb;
		
		$wpdb->webinar_notification_emails = 'webinar_notification_emails';
		return $wpdb->get_results("SELECT * FROM $wpdb->webinar_notification_emails WHERE webinar_id_fk=$webinar_id and notification_type_id='3' ");
	}
		
	/*
	*	@description		This function delete all the notifications emails entry which was send in cron job.
	*/
	function delete_sent_email_notifications($notification_id){
				global $wpdb;
				
				$notification_ids							=		"'".implode("','",$notification_id)."'";
				$wpdb->delete_attendee_record	=		"notification_sent_to";
				
				return $wpdb->get_results("DELETE FROM $wpdb->delete_attendee_record WHERE notification_sent_to_id_pk in ($notification_ids) ");
				
	}
	
	
	
	function update_sent_email_notifications_stat($notification_id){
				global $wpdb;
				
				$notification_ids					 = "'".implode("','",$notification_id)."'";
				$wpdb->notification_sent_to= "notification_sent_to";
				return 	$wpdb->query("UPDATE $wpdb->notification_sent_to SET stat=1 WHERE notification_sent_to_id_pk IN ($notification_ids)");
	}
	
	function update_sent_email_notifications_stat_by_id($notification_id){
				global $wpdb;
				
				$wpdb->notification_sent_to	=	"notification_sent_to";
				if($notification_id)
				return 	$wpdb->query("UPDATE $wpdb->notification_sent_to SET stat=0 WHERE notification_sent_to_id_pk=$notification_id");
	}
	
	
	/*
	*	@Description		This will fetch atendee data for a particular webinar.
	*/
	function get_webinar_attendee_list($webinar_id){
			global $wpdb;
			return	$wpdb->get_results("select * from webinar_registered_users where webinar_id_fk = $webinar_id  order by webinar_start_date,webinar_time");
	}
    
    /** //syed
	 *@Description						        This function gives current user timezone
	*		@param			param1				This is the timezone value.
	*						param2				This is the timezone operation either + or  - .
	*		@return								this will return the timezone name
	 */
	function get_user_timezone_name($timezone_operation,$timezone_difference){
	       global $wpdb;
		   return	$wpdb->get_results("SELECT timezone_name FROM webinar_timezone WHERE GMT = $timezone_difference AND timezone_gmt_symbol = $timezone_operation");
	}		
	/*
	*		@Description						This function calculate the date for a particular timezone
	*		@param			param1				This is the timezone value.
	*						param2				This is the timezone operation either + or  - .
	*		@return								this will return the date for timezone
	*/	
	function get_date_from_timezone($timezone_difference,$timezone_operation){
		
		//split timezone value to hours, minutes and seconds
		$timezone_difference_array	=	explode(":",$timezone_difference);
		$hours_difference						=	$timezone_difference_array[0];
		$minutes_difference					=	$timezone_difference_array[1];
		$seconds_difference					=	$timezone_difference_array[2];
		
		//calculate date for timezone
		$date_for_timezone					=	strtotime(" ".$timezone_operation.$hours_difference." hours ".$timezone_operation.$minutes_difference." minutes ".$timezone_operation.$seconds_difference." seconds");
		
		$timezone_date				=	date('Y-m-d',$date_for_timezone);
		$timezone_hour				=	date("G:i:s",$date_for_timezone);
		return 	array('timezone_date'=>$timezone_date, 'timezone_time'=>$timezone_hour);
	}
	
	/*
	*		@Description						This function calculate webinar available dates for particular timezone
	*		@param			param1				This is number of days to calculate
	*						param2				This is the days difference that admin wants to keep between webinar registration date and webinar date to show as first  registration
	*						param3				This is the webinar id
	*						param4				This is the maximum number of attendees that can register for webinar
	*						param5				This is the schedule id of webinar
	*						param6				This is date according to selected timezone
	*		@return								this will return the date for timezone
	*/	
	function get_webinar_slots_for_timezone($webinar_id,$today_timezone_date,$today_timezone_time,$user_gmt_date,$user_gmt_time){
                                   
		global $wpdb;
		$a = 0;
		$dates_array = array();

				 $webinar_details			=	$this->get_webinar_detail($webinar_id);
				 $days_to_show 				= $webinar_details[0]->webinar_days_to_show;
				 $block_days 				= $webinar_details[0]->webinar_block_days;
				 $max_attendee				= $webinar_details[0]->max_number_of_attendees;
				 $schedule_id				= $webinar_details[0]->webinar_schedule_type_id_fk;
				 $webinar_timezone_id    	= (int)$webinar_details[0]->webinar_timezone_id_fk;
				 $webinar_timezone_detail	= $this->get_timezone_detail($webinar_timezone_id);
				 $webinar_timezone_name 	= stripslashes($webinar_timezone_detail[0]->timezone_name);
				 $webinar_video_length   	= $webinar_details[0]->webinar_video_length;
				 $user_todays_gmt_date_time = $user_gmt_date." ".$user_gmt_time;
				 $is_allowed_end_sessions   = (int)$webinar_details[0]->allow_unexpired_end_sessions;
				 $webinar_endpoint 			= ($is_allowed_end_sessions==1) ? $webinar_video_length : 0;
				 
				/* ~~~~~~~~~~~For Every day Webinar~~~~~~~~  */
				if($schedule_id==1){
					     
						if($webinar_timezone_id>0)  //this is for admin timezone 
						{
						  
						   $webinar_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = $webinar_id ORDER BY webinar_schedule.start_time DESC");
						      $web_start_date = $webinar_detail[0]->webinar_start_date;
						      if(count($webinar_detail) > 0)
								{ 
								   
								    do{
										for($i=0;$i<count($webinar_detail);$i++)
										{
											$date = $web_start_date." ".$webinar_detail[$i]->start_time;
											$dateinsec = strtotime($date);
											$newdate = $dateinsec+$webinar_endpoint;
											$date_in_format = date('Y-m-d H:i:s',$newdate);
											$webinar_gmt_date = $this->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
											
											if(strtotime($webinar_gmt_date) > strtotime($user_todays_gmt_date_time))
											{ $flag = 1; 
											  $webinar_reg_start_date = $web_start_date;
											  break; 
											 }
										}
										if($flag ==1){break;}
										$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										
									}while(1);
									 
								}
						          
								 
								  $webinar_registration_start_date = $webinar_reg_start_date;
								  
						          while($a<$days_to_show){
									  
									  
												
							$webinar_date				=	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
	
							$session_attendee_detail	=	$wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = '$webinar_id' ");
							
							//check if the $webinar_date is greater than webinar_end_date
							
							$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
							
							if($flag_check_webinar_end_date==false ){
								break;
							}
							
							$total_session =  count($session_attendee_detail);
							$allowed_registered_attendee = $total_session*$max_attendee;
							if($allowed_registered_attendee==0){
								$dates_array[]=$webinar_date;
								$a++;
							}
							else {
								$registered_attendee	=	$wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
								$reg_att = count($registered_attendee);
								if($reg_att<$allowed_registered_attendee){
									$dates_array[] = $webinar_date;
									$a++;
								}
							}
							$block_days++;
							
							
						 }
							
					   
						}
						else{
						
						$webinar_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = $webinar_id ORDER BY webinar_schedule.start_time DESC ");
						
						/*check wheather current date is less than (webinar_starting_date - $next_days) 
						*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
						*
						*/
						$web_start_date = $webinar_detail[0]->webinar_start_date;
						      if(count($webinar_detail) > 0)
								{ 
								   
								    do{
										for($i=0;$i<count($webinar_detail);$i++)
										{
											$date = $web_start_date." ".$webinar_detail[$i]->start_time;
											$dateinsec = strtotime($date);
											$newdate = $dateinsec+$webinar_endpoint;
											$date_in_format = date('Y-m-d H:i:s',$newdate);
											
											
											if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time))
											{ $flag = 1; 
											  $webinar_reg_start_date = $web_start_date;
											  break; 
											 }
										}
										if($flag ==1){break;}
										$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										
									}while(1);
									 
								}
						          
								 
								  $webinar_registration_start_date = $webinar_reg_start_date;
						
						
						while($a<$days_to_show){
												
							$webinar_date				=	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
	
							$session_attendee_detail	=		$wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = '$webinar_id' ");
							
							//check if the $webinar_date is greater than webinar_end_date
							$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
							
							if($flag_check_webinar_end_date==false ){
								break;
							}
							
							$total_session =  count($session_attendee_detail);
							$allowed_registered_attendee = $total_session*$max_attendee;
							if($allowed_registered_attendee==0){
								$dates_array[]=$webinar_date;
								$a++;
							}
							else {
								$registered_attendee	=	$wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
								$reg_att = count($registered_attendee);
								if($reg_att<$allowed_registered_attendee){
									$dates_array[] = $webinar_date;
									$a++;
								}
							}
							$block_days++;
						 }
						
						}//end-else
						
					}
				
				
				/* ~~~~~~~~For Specific Date Webinar~~~~~~~~*/
				if($schedule_id==2){  //schedule_id == 2 i.e for Specific Dates        
					
					  if($webinar_timezone_id >0) //check for admin timezone
						{
						
								
					         $particular_webinar_dates = $wpdb->get_results("SELECT *  FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = $webinar_id ORDER BY webinar_date ASC");
								if(count($particular_webinar_dates) > 0)
								{
									
									for($j=0; $j<count($particular_webinar_dates);$j++)
									{    
									$w_date = $particular_webinar_dates[$j]->webinar_date;
							$web_sessions = $wpdb->get_results("SELECT count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id AND webinar_date = '$w_date' GROUP BY webinar_date ORDER BY webinar_date ASC");
										 
										 
									  	 $date = $particular_webinar_dates[$j]->webinar_date." ".$particular_webinar_dates[$j]->start_time;
							             $dateinsec = strtotime($date." -".$block_days." days");
							             $newdate = $dateinsec+$webinar_endpoint;
							             $date_in_format = date('Y-m-d H:i:s',$newdate);
										
									     $adminwebinar_date_gmt = $this->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
									   if(strtotime($adminwebinar_date_gmt) > strtotime($user_todays_gmt_date_time)){
							
							$webinar_schedule_id = $particular_webinar_dates[$j]->webinar_schedule_id_pk;
							$webinar_date = $particular_webinar_dates[$j]->webinar_date;
							$total_session = $web_sessions[0]->count_session;
							//get registered attendee list for a particular date 
							$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = $webinar_id AND webinar_date = '$webinar_date' ");
						    
							//check if the $webinar_date is greater than webinar_end_date						
											$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$particular_webinar_dates[0]->webinar_end_date);
											
											if($flag_check_webinar_end_date==false ){
												break;
											}
							
							$reg_att = count($registered_attendee); 
							$allowed_registered_attendee = $total_session*$max_attendee;
							
							//if registered attendees for a particular date is eqaual to or greater than allowed maximum attendee then leave that date
							if($reg_att>=$allowed_registered_attendee and $allowed_registered_attendee>0 ){
								continue;
							}						
							//if number of dates in array is equal to the dates needed in the webinar then break the loop
							if(count($dates_array)==$days_to_show){
								break;
							}
							else{
								
								if(!in_array($particular_webinar_dates[$j]->webinar_date,$dates_array)){
									$dates_array[] =$particular_webinar_dates[$j]->webinar_date;
								}
							}
						 				   
										   }// end-if
									}//end-for
								
								}//end-if
					          
						
						}
					  else{
					
					      //$webinar_date				=	date('Y-m-d', strtotime($today_timezone_date." +".$block_days." days ")); 
					$particular_webinar_dates = $wpdb->get_results("SELECT *  FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = $webinar_id ORDER BY webinar_date ASC");
					        if(count($particular_webinar_dates) > 0)
								{
								     for($j=0; $j<count($particular_webinar_dates);$j++)
									{
									   $webinar_date = $particular_webinar_dates[$j]->webinar_date;
									//get totel available dates for a particular webinar 
									     $session_attendee_detail		=		$wpdb->get_results("SELECT count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id AND '$webinar_date' = webinar_date group by webinar_date order by webinar_date ASC");
					
					                     $date = $particular_webinar_dates[$j]->webinar_date." ".$particular_webinar_dates[$j]->start_time;
							             $dateinsec = strtotime($date." -".$block_days." days");
							          
							             $newdate = $dateinsec+$webinar_endpoint;
							             $date_in_format = date('Y-m-d H:i:s',$newdate); 
										 if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time))
										 {
									// if any date is available 
									
										//check each date
											
											$webinar_schedule_id = $particular_webinar_dates[$j]->webinar_schedule_id_pk;
											$webinar_date = $particular_webinar_dates[$j]->webinar_date;
											$total_session = $session_attendee_detail[0]->count_session;
											//get registered attendee list for a particular date 
											$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = $webinar_id AND webinar_date = '$webinar_date' ");
										    //check if the $webinar_date is greater than webinar_end_date
							
											$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$particular_webinar_dates[0]->webinar_end_date);
											
											if($flag_check_webinar_end_date==false ){
												break;
											}
											
											$reg_att = count($registered_attendee); 
											$allowed_registered_attendee = $total_session*$max_attendee; 
											
											//if registered attendees for a particular date is eqaual to or greater than allowed maximum attendee then leave that date
											if($reg_att>=$allowed_registered_attendee and $allowed_registered_attendee>0 ){
												continue;
											}						
											//if number of dates in array is equal to the dates needed in the webinar then break the loop
											if(count($dates_array)==$days_to_show){
												break;
											}
											else{
											
											   if(!in_array($particular_webinar_dates[$j]->webinar_date,$dates_array)){
									           $dates_array[] =$particular_webinar_dates[$j]->webinar_date;
								         }                 
												
											}
										}//end-if
									  
									} //end-for
								}//end-if
					
					
					
					}//end-else
				}


				/* ~~~~~~~~~~~~~~~~Selected Days~~~~~~~~~~~~~~~~~ */
				if($schedule_id==4){
		         
		      if($webinar_timezone_id >0){ 
						/* Admin timezone */  
						
					$get_day = $wpdb->get_results($wpdb->prepare("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = %d ORDER BY webinar_schedule.start_time DESC",$webinar_id));
					
					          $web_start_date = $get_day[0]->webinar_start_date;
						      if(count($get_day) > 0)
								{ 
								   
								    do{
										for($i=0;$i<count($get_day);$i++)
										{
											$date = $web_start_date." ".$get_day[$i]->start_time;
											$dateinsec = strtotime($date);
											$newdate = $dateinsec+$webinar_endpoint;
											$date_in_format = date('Y-m-d H:i:s',$newdate);
											$webinar_gmt_date = $this->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
											
											if(strtotime($webinar_gmt_date) > strtotime($user_todays_gmt_date_time))
											{ $flag = 1; 
											  $webinar_reg_start_date = $web_start_date;
											  break; 
											 }
										}
										if($flag ==1){break;}
										$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										
									}while(1);
									 
								}
						          
								 
								  $webinar_registration_start_date = $webinar_reg_start_date;
					
					$days 	= array();
					foreach($get_day as $g_d){
						$days[] = $g_d->week_day;
					}
					
					//return count($get_day);
					$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]'");
					//return $session_attendee_detail[0]->webinar_end_date;

					while($a<$days_to_show){
												
						$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
						
						//check if the $webinar_date is greater than webinar_end_date
						$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$get_day[0]->webinar_end_date);
						if($flag_check_webinar_end_date==false ){
								break;
						}
						
						$webinar_day 	 =	(int)date('N',strtotime($webinar_registration_start_date."+".$block_days." days "));
						$total_session =  count($session_attendee_detail);
						$allowed_registered_attendee = $total_session*$max_attendee;
						if(in_array($webinar_day, $days)){
							if($allowed_registered_attendee==0){
								$dates_array[] = $webinar_date;
								$a++;
							}
							else {
								$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
								$reg_att = count($registered_attendee);
								if($reg_att<$allowed_registered_attendee){
									$dates_array[] = $webinar_date;
									$a++;
								}
							}
						}
						$block_days++;
					}
						   
						   
				}else{ 
						/* Attendee timezone */
						
						$get_day = $wpdb->get_results($wpdb->prepare("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = %d ORDER BY webinar_schedule.start_time DESC",$webinar_id));
						/*check wheather current date is less than (webinar_starting_date - $next_days) 
						*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
						*
						*/
						
						 $web_start_date = $get_day[0]->webinar_start_date;
						      if(count($get_day) > 0)
								{ 
								   
								    do{
										for($i=0;$i<count($get_day);$i++)
										{
											$date = $web_start_date." ".$get_day[$i]->start_time;
											$dateinsec = strtotime($date);
											$newdate = $dateinsec+$webinar_endpoint;
											$date_in_format = date('Y-m-d H:i:s',$newdate);
											
											if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time))
											{ $flag = 1; 
											  $webinar_reg_start_date = $web_start_date;
											  break; 
											 }
										}
										if($flag ==1){break;}
										$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										
									}while(1);
									 
								}
						          
								 
								  $webinar_registration_start_date = $webinar_reg_start_date;
						
						$days = array();
						for($s=0;$s<count($get_day);$s++){
							$days[$s] = $get_day[$s]->week_day;
						}
						$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]'");
						while($a<$days_to_show){
													
							$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
							
							//check if the $webinar_date is greater than webinar_end_date
							$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
							if($flag_check_webinar_end_date==false ){
									break;
							}
							
							$webinar_day =	(int)date('N', strtotime($webinar_registration_start_date."+".$block_days." days "));
							$total_session =  count($session_attendee_detail);
							$allowed_registered_attendee = $total_session*$max_attendee;
							if(in_array($webinar_day, $days)){
								if($allowed_registered_attendee==0){
									$dates_array[] = $webinar_date;
									$a++;
								}
								else {
									$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = '$webinar_id' AND webinar_date = '$webinar_date'");
									$reg_att = count($registered_attendee);
									if($reg_att<$allowed_registered_attendee){
			
										$dates_array[] = $webinar_date;
										$a++;
									}
								}
							}
							$block_days++;
						}
						
					}  //end-else
					
				}
			
	 return $dates_array;
	        
}
	
	function get_webinar_key(){
		global $wpdb;
		$key_info = $wpdb->get_results("select * from webinar_license");
		return $key_info[0];		
	}
		
	function get_timezones_detail_by_id($id){
		global $wpdb;
	 	$wpdb->timezones_detail		=		"webinar_timezone";
	 	return	$wpdb->get_results("SELECT * FROM $wpdb->timezones_detail where webinar_timezone_id_pk = '$id'");
	}
		
	/**
	 *	@description			            This function will get timezone name for a given time zone id.
	 *	@param			$timezone_id		Time Zone ID
	 *	#return			Array				Contains Time Zone Name.
	 */
	function get_timezone_name($timezone_id){
		global $wpdb;
		return	$wpdb->get_results("SELECT timezone_name FROM webinar_timezone WHERE webinar_timezone_id_pk = $timezone_id LIMIT 1");
	}
	
	/**
	 *	@description			                This function will convert selected date and time by attenddee from admin's preferred time zone to UTC time zone.
	 *	@param			$date_time			    Date and Time selected by attendee
	 *	@param			$current_timezone	Admin's preffered timezone name
	 *	@param			$to_timezone		  Required timezone
	 *	#return			date and time			UTC converted date and time.
	 */	
	function convert_time_zone($date_time,$current_timezone,$to_timezone){
		/*if($current_timezone == 'UTC'){
		$current_timezone = 'Europe/London';
		} */
		$time_object = new DateTime($date_time, new DateTimeZone($current_timezone));
		$time_object->setTimezone(new DateTimeZone($to_timezone));
		return $time_object->format('Y-m-d H:i:s');
	
	}
		
	function check_initiator($token){
		$ch = curl_init(EASYWEBINAR_WEBSITE_LINK."/wp-initiator.php?token=".$token);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	
	function get_all_webinar_info(){
		global $wpdb;
		$wpdb->all_webinar		=	"webinar";
		return	$wpdb->get_results("SELECT webinar_id_pk,webinar_event_name,webinar_main_topic,created_date,modified_date FROM $wpdb->all_webinar ORDER BY modified_date DESC , created_date DESC");
	}
		
	/**
   *	@description				Follwoing function will fetch the slug for the postmeta table for the given webinar id
   *	@param		 webinar_id		This is the id of the webinar
   *	
   *	@return		 slug 		    Contains the slug for all webinar pages
   */
	
	function get_webinar_slug($webinar_id){
		global $wpdb;
		$tabel_name = $wpdb->prefix."posts";
		return $wpdb->get_results("SELECT post_name FROM $tabel_name WHERE post_type='page' AND post_status='publish' AND webinar_id ='".$webinar_id."' ORDER BY ID ASC");
	}
	
	
	function get_webinar_template($webinar_id){
		global $wpdb;
		return $wpdb->get_row($wpdb->prepare("SELECT template_options FROM webinar_template WHERE webinar_id=%d",$webinar_id));
	}
		
		
	function get_registration_template_data($webinar_id){
		global $wpdb;
		$registration_template_data = $wpdb->get_row($wpdb->prepare("SELECT template_options FROM webinar_template WHERE webinar_id=%d AND template_part=%d",$webinar_id,1));
		return @unserialize($registration_template_data->template_options);
	}
	
	function get_thankyou_template_data($webinar_id){
		global $wpdb;
		$thankyou_template_data = $wpdb->get_row($wpdb->prepare("SELECT template_options FROM webinar_template WHERE webinar_id=%d AND template_part=%d",$webinar_id,2));
		return @unserialize($thankyou_template_data->template_options);
	}
	
	function get_event_template_data($webinar_id){
		global $wpdb;
		$event_template_data = $wpdb->get_row($wpdb->prepare("SELECT template_options FROM webinar_template WHERE webinar_id=%d AND template_part=%d",$webinar_id,3));
		return @unserialize($event_template_data->template_options);
	}
	
	function get_replay_template_data($webinar_id){
		global $wpdb;
		$replay_template_data = $wpdb->get_row($wpdb->prepare("SELECT template_options FROM webinar_template WHERE webinar_id=%d AND template_part=%d",$webinar_id,4));
		return @unserialize($replay_template_data->template_options);
	}
	
	function set_attendee_question($webinar_id,$attendee_id,$question,$live,$name,$email){
		global $wpdb;
		return $wpdb->insert( 
						'webinar_questions', 
						array( 
							'webinar_id' => $webinar_id,
							'user_id' => $attendee_id,
							'quest'=> $question,
							'is_live'=>$live,
							'user_name'=>$name,
							'user_email'=>$email
						), 
						array( 
							'%d', 
							'%d',
							'%s',
							'%d',
							'%s',
							'%s'
						) 
					);
	}
	
	function set_events_tracking($webinar_id,$attendee_id,$event_id,$button_id,$is_event,$live=0){
		global $wpdb;
		
		$query_to_execute = "INSERT INTO `webinar_events_tracking` (`webinar_id`,`user_id`,`event_id`,`is_event`,`button_id`,`is_live`) VALUES ($webinar_id,$attendee_id,$event_id,$is_event,$button_id,$live)";
				
		if($wpdb->last_query==$query_to_execute){return 0;}
		
		return $wpdb->insert( 
						'webinar_events_tracking', 
						array( 
							'webinar_id' => $webinar_id,
							'user_id' => $attendee_id,
							'event_id'=> $event_id,
							'is_event' => $is_event,
							'button_id'=>$button_id,
							'is_live'=>$live
						), 
						array( 
							'%d', 
							'%d',
							'%d',
							'%d',
							'%d',
							'%d'
						) 
					);
	}
	
	function get_webinar_registrant_insights($webinar_id,$offset,$limit,$action=''){
		global $wpdb;
		
		if(empty($action)){
			$search_by = "";
		}elseif($action==4){
			$search_by = $wpdb->prepare("AND webinar_events_tracking.webinar_id =%d",$webinar_id);
		}elseif($action==3){
			$search_by = "AND webinar_registered_users.replay_activity_status > 0";
		}else{
			$action 	+= 1;
			$search_by = $wpdb->prepare("AND webinar_registered_users.activity_status>=%d",$action);
		}
	
		return $wpdb->get_results($wpdb->prepare("SELECT webinar_registered_users.registration_id_pk as attendee_id,
		webinar_registered_users.attendee_name,
		webinar_registered_users.attendee_email_id,
		webinar_registered_users.webinar_date,
		webinar_registered_users.webinar_time,
		webinar_registered_users.attendee_local_timezone,
		webinar_registered_users.webinar_start_date,
		webinar_registered_users.webinar_real_date,
		webinar_registered_users.webinar_real_time,
		webinar_registered_users.activity_status,
		webinar_registered_users.replay_activity_status,
		webinar_registered_users.watched_percentage,
		webinar_registered_users.selected_timezone_id,
		webinar_registered_users.webinar_id_fk,
		webinar_registered_users.signup_date,
		webinar_events_tracking.action_id as user_action
		FROM webinar_registered_users
		LEFT JOIN webinar_events_tracking ON webinar_events_tracking.user_id = webinar_registered_users.registration_id_pk
		WHERE webinar_registered_users.webinar_id_fk = %d $search_by
		GROUP BY webinar_registered_users.registration_id_pk
		ORDER BY webinar_registered_users.registration_id_pk DESC,
		webinar_registered_users.webinar_date DESC LIMIT %d, %d",$webinar_id,$offset,$limit));
	}
	
	function get_registrant_stats($registrant_id){
		global $wpdb;
		
		$clicked_buttons = $wpdb->get_row($wpdb->prepare("SELECT GROUP_CONCAT( webinar_events_tracking.button_id ) AS button_ids FROM webinar_events_tracking WHERE webinar_events_tracking.user_id = %d",$registrant_id));
		
		if(!empty($clicked_buttons->button_ids)){
		 	$button_urls = $wpdb->get_row("SELECT GROUP_CONCAT( button_source_path ) AS button_paths FROM webinar_buttons WHERE webinar_buttons.webinar_button_id_pk IN ($clicked_buttons->button_ids)");
		}
		
		$asked_questions = $wpdb->get_row($wpdb->prepare("SELECT GROUP_CONCAT( webinar_questions.quest SEPARATOR '###') AS questions FROM webinar_questions WHERE webinar_questions.user_id = %d",$registrant_id));
		
		return array('clicked_events'=>$button_urls->button_paths,'questions'=>$asked_questions->questions);
		
	}
	
	function set_visitor_info($webinar_id,$ip,$time){
		global $wpdb;
		
		return $wpdb->insert( 
						'webinar_visitor_tracking', 
						array( 
							'webinar_id' => $webinar_id,
							'visitor_ip' => $ip,
							'visit_time'=> $time,
						), 
						array( 
							'%d', 
							'%s',
							'%s',
						) 
					);
		
	}
	
	function set_attendee_event_activity_status($attendee_id,$status){
		global $wpdb;
		return $wpdb->update( 
					'webinar_registered_users', 
					array( 
						'activity_status' => $status,
					), 
					array('registration_id_pk'=> $attendee_id ), 
					array( 
						'%s'	
					), 
					array('%d') 
			   );
	}
	
	function set_attendee_replay_activity_status($attendee_id,$status){
		global $wpdb;
		
		return $wpdb->update( 
					'webinar_registered_users', 
					array( 
						'replay_activity_status' => $status
					), 
					array('registration_id_pk'=> $attendee_id ), 
					array( 
						'%s'	
					), 
					array('%d') 
			   );
	}
	
	function get_webinar_activity_stats($webinar_id){
		global $wpdb;
		return $wpdb->get_row($wpdb->prepare("SELECT SUM(CASE WHEN (activity_status>=0) THEN 1 ELSE 0 END) total, 
		SUM(CASE WHEN (activity_status>0) THEN 1 ELSE 0 END) attended, 
		SUM(CASE WHEN (activity_status>1) THEN 1 ELSE 0 END) watched, 
		SUM(CASE WHEN (activity_status>2) THEN 1 ELSE 0 END) watched_till_end, 
		SUM(CASE WHEN (replay_activity_status >0) THEN 1 ELSE 0 END) attended_replay, 
		SUM(CASE WHEN (replay_activity_status >1) THEN 1 ELSE 0 END) watched_replay, 
		SUM(CASE WHEN (replay_activity_status >2) THEN 1 ELSE 0 END) watched_replay_till_end 
		FROM webinar_registered_users WHERE webinar_registered_users.webinar_id_fk=%d",$webinar_id));
	}
	
	function get_webinar_total_visitor($webinar_id){
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM webinar_visitor_tracking WHERE webinar_visitor_tracking.webinar_id = %d",$webinar_id));
	}
	
	function get_webinar_total_actions_taken($webinar_id){
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare("SELECT COUNT(DISTINCT webinar_events_tracking.user_id) FROM webinar_events_tracking WHERE webinar_events_tracking.webinar_id = %d AND webinar_events_tracking.user_id>0",$webinar_id));
	}
	
	function get_all_webinars_with_insights($offset,$limit,$search='',$storing_mode=''){
		global $wpdb;
		if($storing_mode != 'draft'){
			$storing_mode = "storing_mode = 'Save' OR storing_mode=''";
		}
		else{
			$storing_mode = "storing_mode = 'draft'";
		}
		if(empty($search)){
			return $wpdb->get_results($wpdb->prepare("SELECT webinar_id_pk, webinar_event_name, webinar_main_topic,	created_date,	modified_date FROM webinar WHERE ($storing_mode) ORDER BY created_date DESC LIMIT %d, %d",$offset,$limit));
		}else{
			return $wpdb->get_results($wpdb->prepare("SELECT webinar_id_pk, webinar_event_name, webinar_main_topic, created_date, modified_date FROM webinar WHERE (webinar.webinar_id_pk LIKE %s OR webinar.webinar_event_name LIKE %s OR webinar.webinar_main_topic LIKE %s) ORDER BY created_date DESC LIMIT %d, %d",'%'.$search.'%','%'.$search.'%','%'.$search.'%',$offset,$limit));
		}
		
	}
	
	function get_single_webinar_with_insight($webinar_id){
		global $wpdb;
		
		return $wpdb->get_results($wpdb->prepare("SELECT webinar_id_pk,	webinar_event_name, webinar_main_topic,	created_date,	modified_date
	  FROM webinar WHERE webinar.webinar_id_pk = %d GROUP BY webinar.webinar_id_pk",$webinar_id));
	}
	
	
	function set_attendee_watched_stat($attendee_id,$watched_status){
		global $wpdb;
		
		return $wpdb->update( 
					'webinar_registered_users', 
					array( 
						'watched_percentage' => $watched_status
					), 
					array('registration_id_pk'=> $attendee_id ), 
					array( 
						'%d'	
					), 
					array('%d') 
			   );
	}
	
	function get_attendee_activity_stats($attendee_id){
		global $wpdb;
		return $wpdb->get_row($wpdb->prepare("SELECT SUM(CASE WHEN is_live=1 THEN 1 ELSE 0 END) user_live_actions, 
		SUM(CASE WHEN is_live=0 THEN 1 ELSE 0 END) user_replay_actions, 
		COUNT(DISTINCT webinar_events_tracking.event_id) as user_unique_actions 
		FROM webinar_events_tracking WHERE webinar_events_tracking.user_id=%d",$attendee_id));
	}
	
	function get_filtered_webinar_registrant_insights($webinar_id,$event_actions,$replay_actions,$took_action,$attended,$attended_last,$registerd,$registerd_last,$watched,$watching_opt,$query_opt,$namemail,$offset,$limit){
		global $wpdb;
		
		$query = $wpdb->prepare("SELECT webinar_registered_users.registration_id_pk as attendee_id,
		webinar_registered_users.attendee_name,
		webinar_registered_users.attendee_email_id,
		webinar_registered_users.webinar_date,
		webinar_registered_users.webinar_time,
		webinar_registered_users.attendee_local_timezone,
		webinar_registered_users.webinar_start_date,
		webinar_registered_users.webinar_real_date,
		webinar_registered_users.webinar_real_time,
		webinar_registered_users.activity_status,
		webinar_registered_users.replay_activity_status,
		webinar_registered_users.watched_percentage,
		webinar_registered_users.selected_timezone_id,
		webinar_registered_users.webinar_id_fk,
		(SELECT COUNT(DISTINCT webinar_events_tracking.event_id) FROM webinar_events_tracking WHERE webinar_events_tracking.user_id = webinar_registered_users.registration_id_pk) AS user_action 
		FROM webinar_registered_users LEFT JOIN webinar_events_tracking ON webinar_events_tracking.webinar_id=webinar_registered_users.webinar_id_fk WHERE webinar_registered_users.webinar_id_fk = %d",$webinar_id);
        
		$clauses = " AND (";
		$sclause = '';
		if($query_opt == 1) { 
		$condition_opt = ' AND ';
		}
		else{
		$condition_opt = ' OR ';
		}
		  if(!empty($watched) and ($watched>0)){
		  	      if($watching_opt == 1){
				  $sclause .=$wpdb->prepare("webinar_registered_users.watched_percentage >= %d",$watched);
		  	      } else{
		  	      $sclause .=$wpdb->prepare("webinar_registered_users.watched_percentage <= %d",$watched);
		  	      }
		  }
		  
		  if(!empty($attended) and !empty($attended_last)){
				  $sclause .=$wpdb->prepare(" $condition_opt (webinar_registered_users.webinar_date BETWEEN %s AND %s)",$attended,$attended_last);
		  }
		  
		  if(!empty($registerd) and !empty($registerd_last)){
				  $sclause .=$wpdb->prepare(" $condition_opt (webinar_registered_users.webinar_start_date BETWEEN %s AND %s)",$registerd,$registerd_last);
		  }
		  
		  if(strlen($event_actions)>0){
				  $sclause.= " $condition_opt webinar_registered_users.activity_status IN ($event_actions)";
		  }
		  
			if(strlen($took_action)>0){
					$sclause.= " $condition_opt webinar_events_tracking.user_id = webinar_registered_users.registration_id_pk";
			}
			
		  if(strlen($replay_actions)>0){
				  $sclause.= " $condition_opt webinar_registered_users.replay_activity_status IN ($replay_actions)";
		  }
		  
		  if(!empty($namemail)){
			  	$namemail = "%".$namemail."%";
				  $sclause .=$wpdb->prepare(" $condition_opt (webinar_registered_users.attendee_name LIKE %s OR webinar_registered_users.attendee_email_id LIKE %s)",$namemail,$namemail);
		  }
		  
		 $clauses .= ltrim(trim($sclause), "$condition_opt");
		 
		 $clauses .= " )";

     if(strlen($clauses)>8) $query.=$clauses;
		  		  
		 $count_query = $query .= " GROUP BY webinar_registered_users.registration_id_pk ORDER BY webinar_registered_users.registration_id_pk DESC, webinar_registered_users.webinar_date DESC";
		 
		 $query .= $wpdb->prepare(" LIMIT %d,%d",$offset,$limit);
		  
		 $result=  array();
		
		 $result['count'] 		= count($wpdb->get_results($count_query));
		 $result['attendees']	= $wpdb->get_results($query);
		 
		 return $result;
	}
	
	function get_webinar_details_for_slots($limit){
		global $wpdb;
		return $wpdb->get_results($wpdb->prepare("SELECT webinar.webinar_id_pk,webinar.webinar_main_topic,webinar.topic,webinar.webinar_days_to_show,webinar.webinar_block_days,webinar.max_number_of_attendees,webinar.webinar_schedule_type_id_fk FROM webinar LIMIT %d",$limit));
	}
	
	function get_webinars_for_a_date($requested_date,$limit=10){
				global $wpdb;
				$webinar_events = array();
				$webinars_detail = $wpdb->get_results($wpdb->prepare("SELECT webinar.webinar_id_pk,webinar.webinar_main_topic,webinar.topic,webinar.webinar_days_to_show,webinar.webinar_block_days,webinar.webinar_schedule_type_id_fk,webinar.webinar_start_date,webinar.webinar_end_date FROM webinar ORDER BY webinar_id_pk DESC LIMIT %d",$limit));
				
				foreach ($webinars_detail as $webinar_detail){
					$webinar_days_to_show		= $webinar_detail->webinar_days_to_show;
					$webinar_block_days 		= $webinar_detail->webinar_block_days;
					$webinar_id							= $webinar_detail->webinar_id_pk;
					
					/* ~~~~~~~~~~~For Every day Webinar~~~~~~~~  */
					if($webinar_detail->webinar_schedule_type_id_fk==1){
								
								$dates_array						= array();
								$dates_count						=	0;
								if(strtotime($requested_date) < strtotime($webinar_detail->webinar_start_date."-".$webinar_block_days." days +13 hours") and ($webinar_detail->webinar_start_date != NULL)){
									$webinar_registration_start_date = date('Y-m-d',strtotime($webinar_detail->webinar_start_date."-".$webinar_block_days." days +13 hours"));
								}else{
									$webinar_registration_start_date = $requested_date;
								}
								
								while($dates_count<$webinar_days_to_show){
														
									$webinar_date	=	date('Y-m-d',strtotime($webinar_registration_start_date."+".$webinar_block_days." days +13 hours"));
									$session_attendee_detail=	$wpdb->get_results($wpdb->prepare("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk=%d",$webinar_id));
									$webinar_date_is_less_than_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
									
									if($webinar_date_is_less_than_end_date==false ){
										break;
									}
									
									//Remove the condition if multiple dates from requested date is rquired
									if($webinar_date==$requested_date){
										$dates_array[]	=	$webinar_date;
										break;
									}
									
									$dates_count++;
									$webinar_block_days++;
								}
								
								foreach($dates_array as $dt){
											$title	= empty($webinar_detail->webinar_event_name) ? $webinar_detail->webinar_main_topic : $webinar_detail->webinar_event_name;
											$title  = empty($title) ? 'No title' : stripslashes($title);
											$webinar_events[] = array(
													'webinar_id' => $webinar_id,
													'title'			 => $title,
													'date'			 => $dt
											);
								}
					}
					
					/* ~~~~~~~~For Specific Date Webinar~~~~~~~~  */
					elseif($webinar_detail->webinar_schedule_type_id_fk==2){
						
						$dates_array		= array();
						$webinar_date		=	date('Y-m-d', strtotime($requested_date."+".$webinar_block_days." days -12 hours"));
					
						$session_attendee_detail		=		$wpdb->get_results("SELECT *,count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id group by webinar_date having webinar_date >= '$webinar_date' order by webinar_date ASC");
				
						if(count($session_attendee_detail)>0){
							for($i=0;$i<count($session_attendee_detail);$i++){				
								$webinar_schedule_id = $session_attendee_detail[$i]->webinar_schedule_id_pk;
								$webinar_date = $session_attendee_detail[$i]->webinar_date;
								$total_session = $session_attendee_detail[$i]->count_session;
								if(count($dates_array)==$webinar_days_to_show){
									break;
								}else{
										//Remove the condition if multiple dates from requested date is rquired
										if($requested_date==$session_attendee_detail[$i]->webinar_date){
											$dates_array[] = $session_attendee_detail[$i]->webinar_date;
											break;
										}
								}
							}
							
							foreach($dates_array as $dt){
											$title	= empty($webinar_detail->webinar_event_name) ? $webinar_detail->webinar_main_topic : $webinar_detail->webinar_event_name;
											$title  = empty($title) ? 'No title' : stripslashes($title);
											$webinar_events[] = array(
													'webinar_id' => $webinar_id,
													'title'			 => $title,
													'date'			 => $dt
											);
							}
							
						}
		
					}
					
					/* ~~~~~~~~~~~~~~~~Selected Days~~~~~~~~~~~~~~~~~ */
					elseif($webinar_detail->webinar_schedule_type_id_fk==4){
				
							$dates_count = 0;
							$dates_array = array();
							$get_day 		 = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id'");
							
							if(strtotime($requested_date) < strtotime($get_day[0]->webinar_start_date."-".$webinar_block_days." days +13 hours") and $get_day[0]->webinar_start_date != NULL){
								$webinar_registration_start_date =	date('Y-m-d',strtotime($get_day[0]->webinar_start_date."-".$webinar_block_days." days +13 hours"));
							}else{
								$webinar_registration_start_date = date('Y-m-d G:i:s',strtotime($requested_date));
							}
							
							$days = array();
							for($s=0;$s<count($get_day);$s++){
								$days[$s] = $get_day[$s]->week_day;
							}
							$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]' ");
							
							while($dates_count<$webinar_days_to_show){
														
								$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$webinar_block_days." days -12 hours"));
								
								//check if the $webinar_date is greater than webinar_end_date
								$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
								if($flag_check_webinar_end_date==false){
										break;
								}
								
								$webinar_day =	(int)date('N', strtotime($webinar_registration_start_date."+".$webinar_block_days." days -12 hours"));
								
								if(in_array($webinar_day, $days)){
									 	if($webinar_date==$requested_date){
											$dates_array[] = $webinar_date;
											break;
										}
										$dates_count++;
								}
								$webinar_block_days++;
							}
							
							foreach($dates_array as $dt){
											$title	= empty($webinar_detail->webinar_event_name) ? $webinar_detail->webinar_main_topic : $webinar_detail->webinar_event_name;
											$title  = empty($title) ? 'No title' : stripslashes($title);
											$webinar_events[] = array(
													'webinar_id' => $webinar_id,
													'title'			 => $title,
													'date'			 => $dt
											);
							}
					}
				
				}
				return $webinar_events;
				
	}
	
	function get_webinars_count($mode=''){
		global $wpdb;
		if($mode == 'draft'){
		$saving_mode = " storing_mode='$mode' ";
		} else{
		$saving_mode = " storing_mode='' || storing_mode='Save' ";
		}
		return $wpdb->get_row("SELECT COUNT(*) as total FROM webinar WHERE  $saving_mode");
	}
	
	function delete_specific_attendees($ids_array){
		global $wpdb;
		if(count($ids_array)<=0) return false;
		
		foreach($ids_array as $index => $id){
			$ids_array[$index] = absint($id);
		}
		$attendee_ids = implode(',',$ids_array);
		$wpdb->query("DELETE FROM webinar_registered_users WHERE registration_id_pk IN ($attendee_ids)");
		$wpdb->query("DELETE FROM webinar_questions WHERE user_id IN ($attendee_ids)");
		$wpdb->query("DELETE FROM webinar_events_tracking WHERE user_id IN ($attendee_ids)");
		$wpdb->query("DELETE FROM webinar_logged_in_tracking WHERE registration_id_fk IN ($attendee_ids)");
		return true;
		
	}
	
	function get_webinar_insights_for_specific_registrants($webinar_id,$ids_array,$offset,$limit='100'){
		global $wpdb;
		
		if(count($ids_array)<=0) return array();
		foreach($ids_array as $index => $id){
			$ids_array[$index] = absint($id);
		}
		
		$attendee_ids = implode(',',$ids_array);
		
		return $wpdb->get_results($wpdb->prepare("SELECT webinar_registered_users.registration_id_pk as attendee_id,
		webinar_registered_users.attendee_name,
		webinar_registered_users.attendee_email_id,
		webinar_registered_users.webinar_date,
		webinar_registered_users.webinar_time,
		webinar_registered_users.attendee_local_timezone,
		webinar_registered_users.webinar_start_date,
		webinar_registered_users.webinar_real_date,
		webinar_registered_users.webinar_real_time,
		webinar_registered_users.activity_status,
		webinar_registered_users.replay_activity_status,
		webinar_registered_users.watched_percentage,
		webinar_registered_users.selected_timezone_id,
		webinar_registered_users.webinar_id_fk,
		(SELECT COUNT(DISTINCT webinar_events_tracking.event_id) FROM webinar_events_tracking WHERE webinar_events_tracking.user_id = webinar_registered_users.registration_id_pk) AS user_action
		FROM webinar_registered_users
		LEFT JOIN webinar_events_tracking ON webinar_events_tracking.user_id = webinar_registered_users.registration_id_pk
		WHERE webinar_registered_users.webinar_id_fk = %d AND webinar_registered_users.registration_id_pk IN ($attendee_ids) 
		GROUP BY webinar_registered_users.registration_id_pk
		ORDER BY webinar_registered_users.registration_id_pk DESC,
		webinar_registered_users.webinar_date DESC LIMIT %d, %d",$webinar_id,$offset,$limit));
	}
	
	function get_webinar_slots_for_livepage($webinar_id,$today_timezone_date,$today_timezone_time,$user_gmt_date,$user_gmt_time){
                                   
		global $wpdb;
					 $a =0;
		$dates_array = array();

				 $webinar_details						=	$this->get_webinar_detail($webinar_id);
				 $days_to_show 							= $webinar_details[0]->webinar_days_to_show;
				 $block_days 								= $webinar_details[0]->webinar_block_days;
				 $max_attendee							= $webinar_details[0]->max_number_of_attendees;
				 $schedule_id								= $webinar_details[0]->webinar_schedule_type_id_fk;
				 $webinar_timezone_id    		= (int)$webinar_details[0]->webinar_timezone_id_fk;
				 $webinar_timezone_detail		= $this->get_timezone_detail($webinar_timezone_id);
				 $webinar_timezone_name 		= stripslashes($webinar_timezone_detail[0]->timezone_name);
				 $webinar_endpoint   				= $webinar_details[0]->webinar_video_length;
				 $user_todays_gmt_date_time = $user_gmt_date." ".$user_gmt_time;
				 $is_allowed_end_sessions 	= (int)$webinar_details[0]->allow_unexpired_end_sessions;
				 
				 	/* ~~~~~~~~~~~For Admin Timezone~~~~~~~~  */
				 if($webinar_timezone_id>0){
						
						
						if($schedule_id==1){
						   /* ~~~~~~~~~~~For Every day Webinar~~~~~~~~  */
						   $webinar_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = $webinar_id ORDER BY webinar_schedule.start_time DESC");
						      $web_start_date = $webinar_detail[0]->webinar_start_date;
						      if(count($webinar_detail) > 0){ 
								   
									 do{
											for($i=0;$i<count($webinar_detail);$i++){
												
												$webinar_gmt_date = $this->convert_time_zone(date('Y-m-d H:i:s',strtotime($web_start_date." ".$webinar_detail[$i]->start_time)+$webinar_endpoint),$webinar_timezone_name,"GMT");
												
												if(strtotime($webinar_gmt_date) > strtotime($user_todays_gmt_date_time)){ $flag = 1; 
													$webinar_reg_start_date = $web_start_date;
													break; 
												 }
											}
											if($flag ==1){break;}
											$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										}while(1);
									 
									}
						          
								 
								  $webinar_registration_start_date = $webinar_reg_start_date;
								  
									while($a<$days_to_show){
																		
													$webinar_date	=	date('Y-m-d',strtotime($webinar_registration_start_date."+".$block_days." days "));
							
													$session_attendee_detail	=	$wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = '$webinar_id' ");
													
													$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
													
													if($flag_check_webinar_end_date==false ){
														break;
													}
													
													$total_session =  count($session_attendee_detail);
													$allowed_registered_attendee = $total_session*$max_attendee;
													$dates_array[]=$webinar_date;
													$a++;
													$block_days++;
									}
							
					   
						
						}elseif($schedule_id==2){
							
							/* ~~~~~~~~For Specific Dates Webinar~~~~~~~~*/
							$particular_webinar_dates = $wpdb->get_results("SELECT *  FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = $webinar_id ORDER BY webinar_date ASC");
								if(count($particular_webinar_dates) > 0){
									
									for($j=0; $j<count($particular_webinar_dates);$j++){    
										$w_date	  		= $particular_webinar_dates[$j]->webinar_date;
										$web_sessions = $wpdb->get_results("SELECT count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id AND webinar_date = '$w_date' GROUP BY webinar_date ORDER BY webinar_date ASC");
										     
							     	$date_in_format = date('Y-m-d H:i:s',strtotime($particular_webinar_dates[$j]->webinar_date." ".$particular_webinar_dates[$j]->start_time." -".$block_days." days")+$webinar_endpoint);
										
									  $adminwebinar_date_gmt = $this->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
									  
										if(strtotime($adminwebinar_date_gmt) > strtotime($user_todays_gmt_date_time)){
							
											$webinar_schedule_id = $particular_webinar_dates[$j]->webinar_schedule_id_pk;
											$webinar_date 			 = $particular_webinar_dates[$j]->webinar_date;
											$total_session 			 = $web_sessions[0]->count_session;
											
											$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$particular_webinar_dates[0]->webinar_end_date);
											
											if($flag_check_webinar_end_date==false ){
												break;
											}
											
											if(count($dates_array)==$days_to_show){
												break;
											}else{
												if(!in_array($particular_webinar_dates[$j]->webinar_date,$dates_array)){
													$dates_array[] =$particular_webinar_dates[$j]->webinar_date;
												}
											}
						 				}
									}
								}
					  
						}elseif($schedule_id==4){
							
							/* ~~~~~~~~~~~~~~~~Selected Days in a Week~~~~~~~~~~~~~~~~~ */
							 
								$get_day = $wpdb->get_results($wpdb->prepare("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = %d ORDER BY webinar_schedule.start_time DESC",$webinar_id));
								$web_start_date = $get_day[0]->webinar_start_date;
								if(count($get_day) > 0){ 
									 do{
										for($i=0;$i<count($get_day);$i++){
											
											$webinar_gmt_date = $this->convert_time_zone(date('Y-m-d H:i:s',strtotime($web_start_date." ".$get_day[$i]->start_time)+$webinar_endpoint),$webinar_timezone_name,"GMT");
											if(strtotime($webinar_gmt_date) > strtotime($user_todays_gmt_date_time)){ $flag = 1; 
												$webinar_reg_start_date = $web_start_date;
												break; 
											}
										}
										if($flag ==1){break;}
										$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										
									}while(1);
							  }
													
								$webinar_registration_start_date = $webinar_reg_start_date;
								
								$days 	= array();
								foreach($get_day as $g_d){
									$days[] = $g_d->week_day;
								}
							
								$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]'");
							
								while($a<$days_to_show){
															
									$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
									
									$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$get_day[0]->webinar_end_date);
									if($flag_check_webinar_end_date==false ){
											break;
									}
									
									$webinar_day 	 =	(int)date('N',strtotime($webinar_registration_start_date."+".$block_days." days "));
									
									if(in_array($webinar_day,$days)){
										$dates_array[] = $webinar_date;
										$a++;
									}
									$block_days++;
								}
						}
						
				 }else{
					/* ~~~~~~~~~~~For Attendee Timezone~~~~~~~~  */
						
						
						if($schedule_id==1){
								/* ~~~~~~~~~~~For Every day Webinar~~~~~~~~  */
								$webinar_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = $webinar_id ORDER BY webinar_schedule.start_time DESC ");
									
								/*check wheather current date is less than (webinar_starting_date - $next_days) 
								*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
								*/
									
								$web_start_date = $webinar_detail[0]->webinar_start_date;
								if(count($webinar_detail) > 0){ 
											do{
											for($i=0;$i<count($webinar_detail);$i++){
												
												$date_in_format = date('Y-m-d H:i:s',strtotime($web_start_date." ".$webinar_detail[$i]->start_time)+$webinar_endpoint);
												
												if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time)){
													$flag = 1; 
													$webinar_reg_start_date = $web_start_date;
													break; 
												 }
											}
											if($flag ==1){break;}
											$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
											
										}while(1);
												 
							 }
											 
							 $webinar_registration_start_date = $webinar_reg_start_date;
									
							 while($a<$days_to_show){
															
										$webinar_date				=	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
				
										$session_attendee_detail	=		$wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = '$webinar_id' ");
										
										//check if the $webinar_date is greater than webinar_end_date
										$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
										
										if($flag_check_webinar_end_date==false ){
											break;
										}
										
										$dates_array[]=$webinar_date;
										$a++;
										$block_days++;
							 }
					 }elseif($schedule_id==2){
						 		
								/* ~~~~~~~~For Specific Dates Webinar~~~~~~~~*/
								$particular_webinar_dates = $wpdb->get_results("SELECT *  FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = $webinar_id ORDER BY webinar_date ASC");
					      if(count($particular_webinar_dates)>0){
								     for($j=0; $j<count($particular_webinar_dates);$j++){
									   	$webinar_date = $particular_webinar_dates[$j]->webinar_date;
											$session_attendee_detail		=		$wpdb->get_results("SELECT count(start_time) as count_session FROM webinar_schedule WHERE webinar_id_fk = $webinar_id AND '$webinar_date' = webinar_date group by webinar_date order by webinar_date ASC");
					     
								      $date_in_format = date('Y-m-d H:i:s',strtotime($particular_webinar_dates[$j]->webinar_date." ".$particular_webinar_dates[$j]->start_time." -".$block_days." days")+$webinar_endpoint); 
										 
										 if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time)){
											//check each date
											$webinar_schedule_id = $particular_webinar_dates[$j]->webinar_schedule_id_pk;
											$webinar_date 			 = $particular_webinar_dates[$j]->webinar_date;
											$total_session 			 = $session_attendee_detail[0]->count_session;
											//get registered attendee list for a particular date 
											$registered_attendee = $wpdb->get_results("SELECT * FROM webinar_registered_users WHERE webinar_id_fk = $webinar_id AND webinar_date = '$webinar_date' ");
										  //check if the $webinar_date is greater than webinar_end_date
							
											$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$particular_webinar_dates[0]->webinar_end_date);
											
											if($flag_check_webinar_end_date==false ){
												break;
											}
											
											$reg_att = count($registered_attendee); 
											$allowed_registered_attendee = $total_session*$max_attendee; 
											
											//if number of dates in array is equal to the dates needed in the webinar then break the loop
											if(count($dates_array)==$days_to_show){
												break;
											}else{
											  if(!in_array($particular_webinar_dates[$j]->webinar_date,$dates_array)){
									        $dates_array[] =$particular_webinar_dates[$j]->webinar_date;
								      	}                 
											}
										}
									}
								}
					  
						}elseif($schedule_id==4){
										
								/* ~~~~~~~~~~~~~~~~Selected Days in a week~~~~~~~~~~~~~~~~~ */
										 
								$get_day = $wpdb->get_results($wpdb->prepare("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_schedule.webinar_id_fk = %d ORDER BY webinar_schedule.start_time DESC",$webinar_id));
								/*check wheather current date is less than (webinar_starting_date - $next_days) 
								*	If so, then webinar_registration is webinar_start_date in DB, otherwise  its the current date
								*
								*/
									
							  $web_start_date = $get_day[0]->webinar_start_date;
								if(count($get_day) > 0){ 
										do{
											for($i=0;$i<count($get_day);$i++){
													
												$date_in_format = date('Y-m-d H:i:s',strtotime($web_start_date." ".$get_day[$i]->start_time)+$webinar_endpoint);
												if(strtotime($date_in_format) > strtotime($today_timezone_date." ".$today_timezone_time)){ 
														$flag = 1; 
														$webinar_reg_start_date = $web_start_date;
														break; 
												}
											 }
											 if($flag ==1){break;}
												$web_start_date = date("Y-m-d",strtotime($web_start_date."+1 days"));
										}while(1);
							 }
														
							$webinar_registration_start_date = $webinar_reg_start_date;
									
							$days = array();
							for($s=0;$s<count($get_day);$s++){
							 $days[$s] = $get_day[$s]->week_day;
							}
							
							$session_attendee_detail = $wpdb->get_results("SELECT * FROM webinar_schedule left join webinar on webinar_schedule.webinar_id_fk = webinar.webinar_id_pk WHERE webinar_id_fk = '$webinar_id' AND week_day = '$days[0]'");
							while($a<$days_to_show){
																
								$webinar_date =	date('Y-m-d', strtotime($webinar_registration_start_date."+".$block_days." days "));
										
								//check if the $webinar_date is greater than webinar_end_date
								$flag_check_webinar_end_date = $this->check_webinar_validity_for_end_date($webinar_date,$session_attendee_detail[0]->webinar_end_date);
								if($flag_check_webinar_end_date==false ){
									break;
								}
										
								$webinar_day 	 =	(int)date('N', strtotime($webinar_registration_start_date."+".$block_days." days "));
							
								if(in_array($webinar_day, $days)){
									$dates_array[] = $webinar_date;
									$a++;
								}
								$block_days++;
						  }
									
								
					}
				 
				 
				 }
		
	 return $dates_array;
	        
	}
	
}
//	Intiliazile	class object
$ob_webinar_db_interaction	=	new webinar_db_interaction();
//	End of Intiliazile	class object
?>