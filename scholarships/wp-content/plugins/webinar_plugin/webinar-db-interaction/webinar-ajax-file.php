<?php
	header('Content-Type: application/json');
	if(!session_id())session_start();
	require_once('../../../../wp-load.php');
	require_once('webinar-db-interaction.php');
	require_once('webinar-functions.php');
	require_once('../lib/integration.infusionsoft.php');
	date_default_timezone_set("GMT");
	
	if(!session_id())session_start();
	
	$ob_webinar_db_interaction	=	new webinar_db_interaction();	
	$ob_webinar_functions		=	new webinar_functions();
	$flag_event = 0;
	$response_variable		=	0;	
	$insert_id 					 	=	0;	
	$webinar_id						=	(int)$_GET["webinar_id"];
	$schedule_id					=	(int)$_GET["schedule_id"];	
	$attendee_name				=	$ob_webinar_functions->_esc_sanitise_string($_GET["aten_name"]);	
	$attendee_email				=	$ob_webinar_functions->_sanitise_email(base64_decode($_GET["aten_email"]));	
	$attendee_local_timezone    = 	$ob_webinar_functions->_esc_sanitise_string(base64_decode($_GET['attendee_local_timezone']));
	$attendee_date_time_in_gmt  =   $ob_webinar_functions->_esc_sanitise_string(base64_decode($_GET['attendee_date_time_in_gmt']));
	$webinar_real_time		=	$ob_webinar_functions->_esc_sanitise_string(base64_decode($_GET["webinar_real_time"]));
	$webinar_real_date		=	$ob_webinar_functions->_esc_sanitise_string(base64_decode($_GET["webinar_real_date"]));
	$webinar_time					=	$ob_webinar_functions->_esc_sanitise_string($_GET["webinar_time"]);
	$webinar_check_time					=	$ob_webinar_functions->_esc_sanitise_string($_GET["webinar_check_time"]);
	$local_time					=	$ob_webinar_functions->_esc_sanitise_string($_GET["localtime"]);
	$webinar_date					=	$ob_webinar_functions->_esc_sanitise_string($_GET["webinar_date"]);	
	$webinar_start_date		=	$ob_webinar_functions->_esc_sanitise_string($_GET["webinar_start_date"]);	
	$max_attendee					=	(int)$_GET["max_attendee"];	
	$after_webinar_enabled=	$ob_webinar_functions->_sanitise_string($_GET["after_webinar_enabled"]);
	$after_webinar_hours	=	$ob_webinar_functions->_sanitise_string($_GET["after_webinar_hours"]);
	$video_length					=	(int)$_GET["video_length"];
	$webinar_duration = gmdate("H:i:s", $video_length);
	$webinar_details			=	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
	$is_allowed_end_sessions    =  (int)$webinar_details[0]->allow_unexpired_end_sessions;
	$is_allowed_event_page      =   (int)$webinar_details[0]->allow_event_page;   
	$webinar_endpoint 			=  ($is_allowed_end_sessions==1) ? $video_length : 0;
	
	$selected_timezone_id	=	(int)$_GET["selected_timezone_id"];	
	$webinar_timezone_detail= $ob_webinar_db_interaction->get_timezone_detail($selected_timezone_id);
	$webinar_timezone_name  = stripslashes($webinar_timezone_detail[0]->timezone_name);
	$string								=	$attendee_email.$webinar_date.$webinar_real_time;
	$key 									=	md5($string);
	$webinar_pages				=	$ob_webinar_db_interaction->get_webinar_all_pages_links($webinar_id);
	$login_link	 					=	strtotime($webinar_real_date)===false ? add_query_arg(array('key'=>$key,'right_now'=>1),$webinar_pages['login_page']) :add_query_arg(array('key'=>$key),$webinar_pages['login_page']);
	
	$scheduled_time				=	$webinar_date." ".$webinar_time;
	$webinar_end_time			=	strtotime($scheduled_time)+$video_length;

	$webinar_end_date_array	=	date("Y-m-d H:i:s",$webinar_end_time);	

	$end_date_array				=	explode(" ",$webinar_end_date_array);
	$end_date							=	$end_date_array[0];	
	$end_time							=	$end_date_array[1];		
	$rows									=	$ob_webinar_db_interaction->do_attendees_already_registered_for_webinar($webinar_id,$schedule_id,$webinar_date);

	$schedule_detail			=	$ob_webinar_db_interaction->get_webinar_schedule_detail($schedule_id);		
	$webinar_orignal_date	=	date('Y-m-d', strtotime($webinar_real_date)); 
	$webinar_date_array		=	explode("-",$webinar_date);	
	$webinar_time_array		=	explode(":",$webinar_time);	
   	
	$can_attende_register	=	$ob_webinar_db_interaction->can_attendee_register_for_webinar_session($webinar_id,$schedule_id,$max_attendee,$webinar_date);
	
	if($can_attende_register!=1){
			
			$do_attende_register  	=	$ob_webinar_db_interaction->do_attendee_registered_for_webinar_session($webinar_id,$schedule_id,$attendee_email,$webinar_date);
			
		if($do_attende_register!=0){
			$response_variable			=	2;		
		}
	}else{
		$response_variable				=	1;	
	}	
    
		$webinar_detail						=	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
		$emarketing_settings			= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar_id));
		$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
		$infusionsoft_enabled			= (int)$emarketing_settings['infusionsoft']['enabled'];
		
		if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		}
		
		
	    if($response_variable==0){
			$insert_id							=	$ob_webinar_db_interaction->register_attendee_for_webinar_session($webinar_id,$schedule_id,$attendee_name,$attendee_email,$webinar_start_date,$webinar_date,$webinar_time,$webinar_real_time,$webinar_real_date,$end_time,$end_date,$selected_timezone_id,$key,$attendee_local_timezone);
			
			if($infusionsoft_enabled && $insert_id){
				$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				if($authorized_credentials){
				$iui		 =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendee_name,'Email'=>$attendee_email));
				$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['registered']);
				$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['didnt_came']);
				}
				else{
					        $emarket_status = get_option('ewp_emarketing_status');
					        if($emarket_status != 1){
				            update_option('ewp_emarketing_status',1);
							$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/change_infusionsoft_status.php'; 
				  	        $response_message_array['error_message'] = 0;
				  	        $response_message_array['message'] = 0;
				  	        $response_message_array['domain'] = site_url();
				  	        $ch = curl_init ($website_file_url);
			               curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
						   curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
						   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch,CURLOPT_REFERER,site_url());
						   curl_setopt($ch,CURLOPT_POST,true );
						   curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
						   $infusionsoft_reponse_error = curl_exec($ch);
						   curl_close($ch);
				}
				}
			}	
		
		 $admin_email_enable       =  $webinar_detail[0]->admin_email;
		 $admin_email_value        =  $webinar_detail[0]->admin_email_value;
		 $webinar_name             =  $webinar_detail[0]->webinar_event_name;
		 if($admin_email_enable == 1)
		 $ob_webinar_functions->send_email_on_registration($admin_email_value,$webinar_name,$attendee_name,$attendee_email,$attendee_local_timezone,$webinar_real_date,$webinar_real_time,$login_link);
		if((strtotime($webinar_date." ".$webinar_time)+$webinar_endpoint) >= strtotime($attendee_date_time_in_gmt)){		 

		$welcome_email_detail			=	$ob_webinar_db_interaction->get_welcome_webinar_notification_emails($webinar_id);

		$message_subject					=	$welcome_email_detail[0]->subject;		
		$message_body							=	urldecode($welcome_email_detail[0]->message);	
		$notification_type				=	$webinar_detail[0]->notification_type;
		$res=0;
		if($notification_type==3 || $notification_type==2){
		
			global $wpdb;
			$result = $wpdb->get_results("SELECT * FROM `webinar_registered_users` left join webinar on webinar_registered_users.`webinar_id_fk` = webinar.webinar_id_pk WHERE webinar_registered_users.`attendee_email_id` = '$attendee_email' AND (webinar.notification_type='3' OR webinar.notification_type='2')");
			if(count($result) == 0){
				$res = 0;
			}else{ 
				$res = 5;
			}
		}
		
		$_SESSION['user_id'] 			=	$insert_id;
		$affilate_param = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param);
    $affilate_param_val = $ob_webinar_functions->_esc_decode_string($webinar_detail[0]->affilate_param_val);
    if($affilate_param_val != '' && $affilate_param != ''){
    	$affilate_param_query = '&' . $affilate_param  . '=' . $affilate_param_val;
    }
    else {
    	$affilate_param_query = '';
    	$affilate_param_val = '';
    	$affilate_param ='';
    }		
		$webinar_name							=	$webinar_detail[0]->webinar_main_topic;
		$webinar_description			=	$webinar_detail[0]->description;	
		$presenter								=	$webinar_detail[0]->presenters_name;
		$mail_sender							= empty($webinar_detail[0]->sender_name) ? $presenter : $webinar_detail[0]->sender_name;
		$live_link								=	$webinar_pages['live_page'];
		if($affilate_param_val != '' && $affilate_param != ''){
			$replay_link						 	=	add_query_arg(array('attendee'=>$key,$affilate_param => $affilate_param_val),$webinar_pages['after_webinar_page']);
		$live_link_as_countdown  	= add_query_arg(array('countdown'=>'',$affilate_param => $affilate_param_val),$live_link);
		}
		else{
			$live_link_as_countdown  	= add_query_arg(array('countdown'=>''),$live_link);
			$replay_link						 	=	add_query_arg(array('attendee'=>$key),$webinar_pages['after_webinar_page']);
		}
		if($affilate_param_val != '' && $affilate_param != ''){
		$live_link = 	add_query_arg(array($affilate_param => $affilate_param_val),$live_link);
		}					     
		$webinar_owner_email			=	$webinar_detail[0]->notification_from_address;
		$replace_name_in_subject	=	str_replace("#USER_NAME#",$attendee_name,$message_subject);		
		$replace_name_in_body			=	str_replace("#USER_NAME#",$attendee_name,$message_body);	
		$replace_webinar_name_in_subject=	str_replace("#WEBINAR_TOPIC#",$webinar_name,$replace_name_in_subject);	
		$replace_webinar_name_in_body		=	str_replace("#WEBINAR_TOPIC#",$webinar_name,$replace_name_in_body);	
		$replace_presenter_in_subject		=	str_replace("#PRESENTER_NAME#",$presenter,$replace_webinar_name_in_subject);			
		$replace_presenter_in_body			=	str_replace("#PRESENTER_NAME#",$presenter,$replace_webinar_name_in_body);	
		$replace_login_link_in_subject	=	str_replace("#WEBINAR_LINK#",$login_link,$replace_presenter_in_subject);	
		$replace_login_link_in_body			=	str_replace("#WEBINAR_LINK#",$login_link,$replace_presenter_in_body);	
		$replace_replay_link_in_subject	=	str_replace("#WEBINAR_REPLAY#",$replay_link,$replace_login_link_in_subject);			
		$replace_replay_link_in_body		=	str_replace("#WEBINAR_REPLAY#",$replay_link,$replace_login_link_in_body);
		$replace_live_link_in_subject		=	str_replace("#WEBINAR_LIVE#",$live_link,$replace_replay_link_in_subject);			
		$replace_live_link_in_body			=	str_replace("#WEBINAR_LIVE#",$live_link,$replace_replay_link_in_body);
		$replace_live_c_link_in_subject	=	str_replace("#WEBINAR_LIVE_COUNTDOWN#",$live_link_as_countdown,$replace_live_link_in_subject);			
		$replace_video_c_link_in_body		=	str_replace("#WEBINAR_LIVE_COUNTDOWN#",$live_link_as_countdown,$replace_live_link_in_body);
		$replace_date_in_subject				=	str_replace("#WEBINAR_DATE#",$webinar_real_date,$replace_live_c_link_in_subject);	
		$replace_date_in_body						=	str_replace("#WEBINAR_DATE#",$webinar_real_date,$replace_video_c_link_in_body);
		$replace_time_in_subject				=	str_replace("#WEBINAR_TIME#",$webinar_real_time,$replace_date_in_subject);	
		$replace_time_in_body						=	str_replace("#WEBINAR_TIME#",$webinar_real_time,$replace_date_in_body);
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= "From: $mail_sender <$webinar_owner_email> \r\n";				
				
		if(($notification_type==1 || $notification_type==3) and count($welcome_email_detail)!=0 ){
			if(wp_mail($attendee_email,$replace_time_in_subject,$replace_time_in_body,$headers)){
				if($insert_id!=0){
					$response_variable		=	3;						
				}
			}
		}
		
        if($selected_timezone_id == 0) { 
        	
        	$local_time_str = strtotime($local_time);
            $webinar_check_time =  strtotime($webinar_check_time);
            $time_difference = $webinar_check_time - $local_time_str;
            
            if($time_difference < 0){
             
            }
        }
        
		$before_webinar_email_detail		=	$ob_webinar_db_interaction->get_before_webinar_notification_emails($webinar_id);
		$after_webinar_email_detail			=	$ob_webinar_db_interaction->get_after_webinar_notification_emails($webinar_id);
		$webinar_duation_array = explode(':',$webinar_duration);
		for($i=0;$i<count($before_webinar_email_detail);$i++){
			
			$before_days							=	$before_webinar_email_detail[$i]->notification_days;
			$before_hours							=	$before_webinar_email_detail[$i]->notification_hours;
			$before_minutes						=	$before_webinar_email_detail[$i]->notification_minutes;
			$notification_email_id		=	       $before_webinar_email_detail[$i]->webinar_notification_id_pk;
			
			$notification_time				=	date("Y-m-d H:i:s",mktime($webinar_time_array[0]-$before_hours,$webinar_time_array[1]- $before_minutes,$webinar_time_array[2],$webinar_date_array[1],$webinar_date_array[2]-$before_days,$webinar_date_array[0]));
			$gmt_date_time = gmdate("Y-m-d H:i:s");
			$notification_unix_time = strtotime($notification_time);
			$gmt_unix_time = strtotime($gmt_date_time);
			$diff_notification_gmt_time = $notification_unix_time - $gmt_unix_time;
			$notification_time_array	=	explode(" ",$notification_time);	
			$notification_date				=	$notification_time_array[0];	
			$notification_time				=	$notification_time_array[1];
			$count = $ob_webinar_db_interaction->check_notification_time($notification_time,$notification_date,$webinar_id,$notification_email_id);
			
			if($count[0]->count==0){
				if($diff_notification_gmt_time > 0) {
				$ob_webinar_db_interaction->save_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_email_id,$webinar_time,$webinar_date);
				}
				}
				else{
					$old_notification_date = $count[0]->notification_date;
					$old_notification_time = $count[0]->notification_time;
					if($diff_notification_gmt_time > 0) {
						$status = 1;
					}
					else { 
						$status  = 0;
					}
					if($notification_date != $old_notification_date || $old_notification_time != $notification_time){
						$ob_webinar_db_interaction->update_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_email_id,$webinar_time,$webinar_date,$status);
					}
						
				}
		}
		
		for($i=0;$i<count($after_webinar_email_detail);$i++){
			
			$after_days								=	$after_webinar_email_detail[$i]->notification_days;
			$after_hours							=	$after_webinar_email_detail[$i]->notification_hours;
			$after_minutes						=	$after_webinar_email_detail[$i]->notification_minutes;
			$notification_email_id		=	$after_webinar_email_detail[$i]->webinar_notification_id_pk;
			$duation_hours = $webinar_duation_array[0];
			$duation_minutes = $webinar_duation_array[1];
			$duation_seconds = $webinar_duation_array[2];
			$notification_time				=	date("Y-m-d H:i:s",mktime($webinar_time_array[0]+$after_hours+$duation_hours,$webinar_time_array[1]+ $after_minutes+$duation_minutes,$webinar_time_array[2]+$duation_seconds,$webinar_date_array[1],$webinar_date_array[2]+$after_days,$webinar_date_array[0]));
			$gmt_date_time = gmdate("Y-m-d H:i:s");
			$notification_unix_time = strtotime($notification_time);
			$gmt_unix_time = strtotime($gmt_date_time);
			$diff_notification_gmt_time = $notification_unix_time - $gmt_unix_time;
			$notification_time_array	=	explode(" ",$notification_time);	
			$notification_date				=	$notification_time_array[0];	
			$notification_time				=	$notification_time_array[1];
			$count = $ob_webinar_db_interaction->check_notification_time($notification_time,$notification_date,$webinar_id,$notification_email_id);
			
			if($count[0]->count==0){
				if($diff_notification_gmt_time > 0) {
				$ob_webinar_db_interaction->save_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_email_id,$webinar_time,$webinar_date);
				}
				}
			else{
				$old_notification_date = $count[0]->notification_date;
				$old_notification_time = $count[0]->notification_time;
				if($diff_notification_gmt_time > 0) {
					$status = 1;
				}
				else {
					$status  = 0;
				}
				if($notification_date != $old_notification_date || $old_notification_time != $notification_time){
					$ob_webinar_db_interaction->update_webinar_notification_detail($webinar_id,$notification_date,$notification_time,$notification_email_id,$webinar_time,$webinar_date,$status);
				}
			
			}
		}
		
		/*******
		** For ongoing event if user register for the same It directly goest to event page not on thankyou page
		***/
			if($is_allowed_event_page == 1 ) {
				   if( ((strtotime($webinar_date." ".$webinar_time)+$webinar_endpoint) > strtotime($attendee_date_time_in_gmt)) && (strtotime($attendee_date_time_in_gmt) > strtotime($webinar_date." ".$webinar_time) ))
				   {
					   $flag_event = 1;
				   }
			 }
		}
	}
	$response = array("register_id"=>$insert_id,"response"=>$response_variable, "redirect"=>$res, "key"=>$key,"redirect_to_event"=>$flag_event);
	header('HTTP/1.1 200 OK');
	echo $_GET['callback']. '('.json_encode($response).')';
	exit;
?>
