<?php
/**
 * Template Name: After Webinar Video
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
error_reporting(0);
define('DONOTCACHEPAGE',true);
if(!session_id())session_start();
include('webinar-config.php');
global $current_user;
global $post;

	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions				=	new webinar_functions();
	
	$purge_cache								=	$ob_webinar_functions->purge_cache();
	$webinar_detail							=	$ob_webinar_db_interaction->get_webinar_detail($post->webinar_id);
	$webinar_pages		        	=	$ob_webinar_db_interaction->get_webinar_all_pages_ids($post->webinar_id);
	$check_webinar_end_date     =	$ob_webinar_db_interaction->check_webinar_validity_for_end_date(date('Y-m-d'),$webinar_detail[0]->webinar_end_date);
	$error_page_link            =	get_permalink($ob_webinar_db_interaction->get_error_page_id());
	
	if($check_webinar_end_date==false){
		$_SESSION['not_found'] = 'Webinar has expired on '.date('d M, Y',strtotime($webinar_detail[0]->webinar_end_date));
		header("Location: ".$error_page_link);
		exit;
	}
	
	$attendee_detail =	$ob_webinar_db_interaction->get_registration_details($_GET['attendee']);
	
	if(!empty($attendee_detail)){
		$attendee_id	 	 =  $attendee_detail[0]->registration_id_pk;
		$attendee_name	 =  $attendee_detail[0]->attendee_name;
		$attendee_email  =  $attendee_detail[0]->attendee_email_id;
		
		$ob_webinar_db_interaction->set_attendee_replay_activity_status($attendee_id,1); 
		
		$emarketing_settings	= maybe_unserialize(get_option('ewp_emarketing_settings_'.$post->webinar_id));
	    $infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
		$infusionsoft_enabled			= (int)$emarketing_settings['infusionsoft']['enabled'];
		if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		}
		 
		if($infusionsoft_enabled){
				$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				
				$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
						if($authorized_credentials)
						{
						update_option('ewp_emarketing_status',0);	
				$iui =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendee_name,'Email'=>$attendee_email));
				$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['watched_replay']);
		}
		else{
			$emarket_status = get_option('ewp_emarketing_status');
					        if($emarket_status != 1){
							update_option('ewp_emarketing_status',1);
							$website_file_url =  EASYWEBINAR_WEBSITE_LINK . '/wp-content/themes/easywebinar' .'/change_infusionsoft_status.php'; 
				  	        $response_message_array['error_message'] = 0;
				  	        $response_message_array['message'] = 0;
				  	        $response_message_array['domain'] = site_url();
				  	        $ch = curl_init ($website_file_url);
			               curl_setopt($ch,CURLOPT_HTTPHEADER, array('Expect:'));
						   curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)");
						   curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
						   curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
						   curl_setopt($ch,CURLOPT_REFERER,site_url());
						   curl_setopt($ch,CURLOPT_POST,true );
						   curl_setopt($ch,CURLOPT_POSTFIELDS,$response_message_array);
						   $infusionsoft_reponse_error = curl_exec($ch);
						   curl_close($ch);
		}
		}
	}
	}
	$video_url	       =	trim($webinar_detail[0]->webinar_video_url);		
	$video_length			 =	$webinar_detail[0]->webinar_video_length;	
	$service_type			 =	$webinar_detail[0]->video_type;	

	$video_format 		 = 	pathinfo($video_url, PATHINFO_EXTENSION);
	$video_stream			 =	"";	
	
	$alternate_video_url		= trim($webinar_detail[0]->webinar_alternate_video_url);		
	$alternate_video_format	=	$ob_webinar_functions->get_video_type($alternate_video_url);

	$current_time						=	date("H:i:s");		
	$current_time_seconds		=	$ob_webinar_functions->time_to_sec($current_time);
	$seeking_point					=	0;
		
	if($service_type=="stream"){
		$rtmp_url						=	"rtmp:";
		$rtmp_url_array			=	explode("/",$video_url);
		$video_stream_node	=	count($rtmp_url_array)-1;
		$video_stream				=	$rtmp_url_array[$video_stream_node];
		for($i=1;$i<$video_stream_node;$i++){
			$rtmp_url.="/".$rtmp_url_array[$i];	
		}
		$video_url					=	$rtmp_url;		
	}
	
	$device_type 			= 	$ob_webinar_functions->get_device_type();
	$iPod 						= 	$device_type['iPod'];
	$iPhone 					= 	$device_type['iPhone'];
	$iPad 						= 	$device_type['iPad'];
	$Android 					=   $device_type['Android'];
	$AndroidTablet  	=		$device_type['AndroidTablet'];
	$webinar_event_template_options  =  $ob_webinar_db_interaction->get_event_template_data($post->webinar_id);
	include_once(WEBINAR_PLUGIN_PATH.'templates/'.$selected_template_dir.'/'.$current_template);
?>