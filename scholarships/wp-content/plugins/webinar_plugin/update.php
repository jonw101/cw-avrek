<?php 
header('Content-Type: application/json');
require_once('../../../wp-load.php');
require_once('webinar-config.php');
require_once('webinar-db-interaction/webinar-db-interaction.php');
require_once('license.php');
require_once('access-validation.php');

$fname 			= EASYWEBINAR_UPDATE_FILE;
$destFile 	= 'webinar_plugin.zip';

function full_copy( $source, $target ) {
	if ( is_dir( $source ) ) {
		@mkdir( $target );
		$d = dir( $source );

		while ( FALSE !== ( $entry = $d->read() ) ) {
			if ( $entry == '.' || $entry == '..' ) {
				continue;
			}
			
			$Entry = $source . '/' . $entry; 
			if ( is_dir( $Entry ) ) {
				full_copy( $Entry, $target . '/' . $entry );
				continue;
			}
			copy( $Entry, $target . '/' . $entry );
		}
		$d->close();
	}else {
		copy( $source, $target );
	}
}

function delete_directory($dirname) {
   if (is_dir($dirname))
	  $dir_handle = opendir($dirname);
   if (!$dir_handle)
	  return false;
   while($file = readdir($dir_handle)) {
	  if ($file != "." && $file != "..") {
		 if (!is_dir($dirname."/".$file))
			unlink($dirname."/".$file);
		 else
			delete_directory($dirname.'/'.$file);    
	  }
   }
   closedir($dir_handle);
   rmdir($dirname);
   return true;
}

function get_update_version_queries($data){
	$query = json_decode(base64_decode($data));
	return $query;
} 

if(!copy($fname,$destFile)){
	  $fp = fopen($destFile,'w');
    $crl = curl_init($fname);
    curl_setopt($crl, CURLOPT_HEADER,false);
		curl_setopt($crl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($crl, CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($crl, CURLOPT_AUTOREFERER,true);
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($crl, CURLOPT_FILE,$fp);
    $data = curl_exec($crl);
    curl_close($crl);
    fclose($fp);
}

try{
	
	$zip = new ZipArchive;
	$res = $zip->open($destFile);
	if ($res === TRUE) {
		 $update_successful = $zip->extractTo('updates');
		 $zip->close();
		 full_copy('updates','../webinar_plugin/');
		 unlink($destFile);
		 $update_successful = delete_directory('updates'); 
		
		 if($update_successful){
			 
			 $mysql_error = 0 ; 
			 $data 				= $_POST['data'];
			 $query 			= get_update_version_queries($data);
			 foreach($query as $value){
				 if(!empty($value)){
					 $qry_result = $wpdb->query($value);
						if(mysql_error()){
							$mysql_error = 1 ; 
							break;
						}
				 }
			 }
			 if($mysql_error==1){ 
					 echo json_encode(array('success'=>'0')); 
					  exit;
			  }
			 else{ 
			 		$ob_webinar_plugin =  new webinar_plugin();
					$ob_webinar_plugin->implement_live_page();
					echo json_encode(array('success'=>'1')); 
					exit;
			  } 
		}
	}
}catch(Exception $e){
   echo json_encode(array('success'=>'0')); 
   exit;
}
?>