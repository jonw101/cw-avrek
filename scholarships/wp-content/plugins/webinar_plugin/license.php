<?php 
$key = filter_var($_POST['user_key'],FILTER_SANITIZE_STRING);

if(isset($_POST['user_key'])){
	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$key_info = $ob_webinar_db_interaction->get_webinar_key();
	if(strtolower($key_info->key)!=strtolower($key)){
		$response	=	array('auth'=>'0');
		echo json_encode($response);
		exit;
	}
}else{
	$response	=	array('auth'=>'0');
	echo json_encode($response);
	exit;
}
?>