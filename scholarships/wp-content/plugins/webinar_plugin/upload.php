<?php
error_reporting(0);
header('Content-Type: application/json');
require_once	'../../../wp-load.php';
require_once    'webinar-db-interaction/webinar-db-interaction.php';
$ob_webinar_db_interaction = new webinar_db_interaction();
global $wpdb;

require_once('license.php');
require_once('access-validation.php');

$uploaddir = 'webinar-attachment/';
if(isset($_POST['name'])){
	$names = json_decode(stripslashes($_POST['name']));
	foreach($names as $key=>$value){
		$temp_file 				= $_FILES[$key]['tmp_name'];
		$real_file_name 	= $value;
		$destination_path = $uploaddir . basename($real_file_name);
		move_uploaded_file($temp_file,$destination_path);
	}
}
echo json_encode(array("success"=>true));
exit;
?>