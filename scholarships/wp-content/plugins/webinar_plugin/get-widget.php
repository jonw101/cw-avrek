<?php
error_reporting(0);
if(!session_id())session_start();
$width = isset($_GET['w']) && (int)$_GET['w']!='' ? (int) $_GET['w'] : 240;
$height = isset($_GET['h']) && (int)$_GET['h']!='' ? (int) $_GET['h'] : 240;
$webinar_id = isset($_GET['wid']) && (int)$_GET['wid']!='' ? (int)$_GET['wid'] : 0;
$widget_version= isset($_GET['ver']) && (int)$_GET['ver']!='' ? (int)$_GET['ver'] : 0;
$width = $width<240 ? 240 : (int)$width;
$theme_color = in_array($_GET['color'],array('blue','green','red','orange'))? filter_var($_GET['color'],FILTER_SANITIZE_STRING):'blue';
require_once( '../../../wp-load.php' );
error_reporting(0);
if (!class_exists ('Webinar_Widget')) {
?>
<style>
.inactive_widget {
  padding:10px;
  margin:0 auto;
  text-align:center;
  font-size:20px;
  border:1px solid #999;
  background:#eaeaea;
}
</style>
<div class="inactive_widget">Widget is Inactive</div>
<?php
}else{
	
	$ob_webinar_db_interaction	=	new webinar_db_interaction();
	$ob_webinar_functions		=	new webinar_functions();
	$ob_webinar_widget			=	new Webinar_Widget();
	if($widget_version===0){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow:hidden !important;">
<head>
<title>Event Registration Dates</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<script>
		$(function(){
			$('#opt_in_form').submit(function(event){
				event.preventDefault();
				
				var form_action = $(this).attr('action');
				var w_date = $('input[name=webinar_date]:radio:checked').val();
				window.parent.location = form_action+'&webinar_date='+w_date+'&submit=1';
			})
			
		})
		</script>
</head>
<body style="margin:0">
<?php 
		$ob_webinar_widget->get_widget_html(array('title'=>$title,'webinar_width'=>$width,'webinar_id'=>$webinar_id,'color'=>$theme_color));
	?>
</body>
</html><?php }else if($widget_version===1 && $webinar_id>=1){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow:hidden !important;">
<head>
<title>Event Registration Widget</title>
</head>
<body style="margin:0;">
<?php 
		$ob_webinar_widget->get_registration_widget_html(array('title'=>$title,'webinar_width'=>$width,'webinar_id'=>$webinar_id,'color'=>$theme_color));
	?>
</body>
</html>
<?php }else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow:hidden !important;">
<head>
<title>Event Registration Widget</title>
</head>
<body style="margin:0;">
<p>Didn't find the page you wanted.</p>
<!--Invalid Version-->
</body>
</html>
<?php
	  }
}
?>