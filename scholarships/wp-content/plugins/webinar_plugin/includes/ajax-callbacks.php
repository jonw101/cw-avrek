<?php
	
	function ewp_delete_webinar() {
		$ob_webinar_db_interaction = new webinar_db_interaction();	
		$webinar_id = (int)$_POST['webinar_to_delete'];
		$is_deleted = 0;	
		  if($webinar_id){
			  $is_webinar_deleted								=	$ob_webinar_db_interaction->delete_webinar($webinar_id);
			  if($is_webinar_deleted==1){
				  $is_attendees_deleted						=	$ob_webinar_db_interaction->delete_webinar_default_attendees($webinar_id);	
				  $is_delayed_event_deleted				=	$ob_webinar_db_interaction->delete_webinar_delayed_events($webinar_id);	
				  $is_notification_detail_deleted	=	$ob_webinar_db_interaction->delete_notification_detail($webinar_id);	
				  $is_schedules_deleted						=	$ob_webinar_db_interaction->delete_webinar_schedules($webinar_id);	
				  $is_registered_users_deleted		=	$ob_webinar_db_interaction->delete_webinar_registered_users($webinar_id);
				  $is_webinar_notification_details_deleted	=	$ob_webinar_db_interaction->delete_wbeinar_notification_dettails($webinar_id);
				  $is_webinar_meta_deleted				=	$ob_webinar_db_interaction->delete_webinar_pages_meta($webinar_id);				
				  $is_webinar_pages_deleted				=	$ob_webinar_db_interaction->delete_webinar_pages($webinar_id);	
				  $is_scarcity_deleted						=	$ob_webinar_db_interaction->delete_webinar_scarcity($webinar_id);
				  $is_sharing_deleted							=	$ob_webinar_db_interaction->delete_sharing($webinar_id);
					$is_question_deleted						=	$ob_webinar_db_interaction->delete_webinar_questions($webinar_id);
					$is_event_trackin_deleted				=	$ob_webinar_db_interaction->delete_webinar_events_tracking($webinar_id);
					$is_visitor_deleted							=	$ob_webinar_db_interaction->delete_webinar_visitor_tracking($webinar_id);
					$is_template_deleted						=	$ob_webinar_db_interaction->delete_webinar_template($webinar_id);
					$is_options_deleted							= delete_option('ewp_emarketing_settings_'.$webinar_id); 
				  $is_deleted 										=	1;
			  }
			  
		  }
		  echo $is_deleted;
		  exit;
  }
  add_action('wp_ajax_delete_webinar', 'ewp_delete_webinar');	
	  
  function ewp_set_webinar_as_homepage(){
		  $ob_webinar_db_interaction = new webinar_db_interaction();	
		  $webinar_id  = (int)$_POST['webinar_to_set_as_homepage'];
		  $reg_page_id = $ob_webinar_db_interaction->get_webinar_registration_id($webinar_id);
		  $is_set      = 0;
		  if($reg_page_id){
			  $is_set = $ob_webinar_db_interaction->set_homepage($reg_page_id);
			  flush_rewrite_rules();
		  }
		  echo $is_set;
		  exit;
  }
  add_action('wp_ajax_webinar_as_homepage', 'ewp_set_webinar_as_homepage');
	  
  function ewp_add_attendee_questions(){
		  $ob_webinar_db_interaction = new webinar_db_interaction();
			$ob_webinar_functions		 	 = new webinar_functions();
			 
		  $webinar  = absint($_POST['webinar']);
		  $attendee = absint($_POST['attendee']);
		  $question = esc_html($_POST['question']);
		  $is_live  =	absint($_POST['live']);
		  $name	  	= sanitize_text_field($_POST['name']);
		  $email	  =	sanitize_email($_POST['email']);
			$admin_email =	sanitize_email($_POST['admin']);
		  			
			$response = $ob_webinar_functions->mail_webinar_question($admin_email,$email,$name,$question);
			
			if($attendee==0){
		   $response = $ob_webinar_db_interaction->set_attendee_question($webinar,$attendee,$question,$is_live,$name,$email);
		  }else{
			 
		   $response = $ob_webinar_db_interaction->set_attendee_question($webinar,$attendee,$question,$is_live);
		  }
			
			$emarketing_settings	= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar));
			$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
			$infusionsoft_enabled	= (int)$emarketing_settings['infusionsoft']['enabled'];
			if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		}
			if($infusionsoft_enabled){
					if($attendee>0){
						$attendee_info 	= $ob_webinar_db_interaction->get_attendee_info_by_id($attendee);
						$name						= $attendee_info->attendee_name;
						$email 					= $attendee_info->attendee_email_id;
					}
					
					$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
					$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				if($authorized_credentials){
					$iui		 =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$name,'Email'=>$email));
					$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['asked_question']);
					}
					
			}
			
		  echo $response; exit;
  }
  add_action('wp_ajax_add_attendees_questions','ewp_add_attendee_questions');
  add_action( 'wp_ajax_nopriv_add_attendees_questions', 'ewp_add_attendee_questions');  
  
  
  function ewp_add_events_tracking(){
		  $ob_webinar_db_interaction = new webinar_db_interaction();
		  $webinar  = absint($_POST['webinar']);
		  $attendee = absint($_POST['attendee']);
		  $event_id = absint($_POST['event_id']);
		  $is_event = absint($_POST['is_event']);
		  $btn_id   = absint($_POST['btn_id']);
		  $is_live  = absint($_POST['live']);
			
			if($attendee){	
				$emarketing_settings	= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar));
				$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
				$infusionsoft_enabled	= (int)$emarketing_settings['infusionsoft']['enabled'];
				if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		        $emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	            $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		}
				if($infusionsoft_enabled){
					$attendee_info 	= $ob_webinar_db_interaction->get_attendee_info_by_id($attendee);
					$attendee_name	= $attendee_info->attendee_name;
					$attendee_email = $attendee_info->attendee_email_id;
					$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
					$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				if($authorized_credentials){
					$iui		 =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendee_name,'Email'=>$attendee_email));
					$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['clicked_actions']);
					}
				}
			
		  	echo $ob_webinar_db_interaction->set_events_tracking($webinar,$attendee,$event_id,$btn_id,$is_event,$is_live);
			}else{
				echo 0;
			}
		  exit;
  }
  
  add_action('wp_ajax_add_call_to_action','ewp_add_events_tracking');
  add_action( 'wp_ajax_nopriv_add_call_to_action', 'ewp_add_events_tracking'); 
  
  
  function get_available_webinar_registration_dates(){
	  
		global $wpdb;
		
	  $ob_webinar_db_interaction = new webinar_db_interaction();
	  $ob_webinar_functions		   =	new webinar_functions();
  
	  $webinar_id 					=	(int)$_REQUEST['webinar_id'];
	  $timezone_operation		=	$ob_webinar_functions->_sanitise_string($_REQUEST['timezone_operation']);
	  $timezone_difference	=	$ob_webinar_functions->_sanitise_string($_REQUEST['timezone_difference']);
	  
	  $user_gmt_date      	= $ob_webinar_functions->_sanitise_string($_REQUEST['attendee_date_in_gmt']);
	  $user_gmt_time      	= $ob_webinar_functions->_sanitise_string($_REQUEST['attendee_time_in_gmt']);
	  $webinar_detail				=	$ob_webinar_db_interaction->get_webinar_detail($webinar_id);
	
	  $max_attendee 				=	$webinar_detail[0]->max_number_of_attendees;
	  $webinar_schedule_id 	=	$webinar_detail[0]->webinar_schedule_type_id_fk;
	  $webinar_days_to_show	=	$webinar_detail[0]->webinar_days_to_show;	
	  $attendee_timezone 	  = $webinar_detail[0]->webinar_timezone_id_fk;
	  $timezone_date_time		=	$ob_webinar_db_interaction->get_date_from_timezone($timezone_difference,$timezone_operation);	
	  $timezone_time				=	$timezone_date_time['timezone_time'];
	  $timezone_date				=	$timezone_date_time['timezone_date'];
	  
	  $webinar_video_length     =  (int)$webinar_detail[0]->webinar_video_length; 
	  $is_allowed_end_sessions  =  (int)$webinar_detail[0]->allow_unexpired_end_sessions;
	  $webinar_endpoint 				=  ($is_allowed_end_sessions==1) ? $webinar_video_length : 0;
	  
	  $valid_date				=	'';
	  $valid_times			=	array();
	  
		$dates_array 			= $ob_webinar_db_interaction->get_webinar_slots_for_livepage($webinar_id,$timezone_date,$timezone_time,$user_gmt_date,$user_gmt_time);
			  
		foreach($dates_array as $date_array){
		  $web_date 	  	=	$date_array;
		  $day_name	    	=	gmdate("N", strtotime($web_date));
		  
		  $everyday_session_detail=	$ob_webinar_db_interaction->get_registered_attendees_for_everyday($webinar_id,$web_date);
		  if(count($everyday_session_detail)>0){
			  $invalid_sessions	=	"";	
			  $total_rows				=	count($everyday_session_detail);
			  $i=1;
			  foreach($everyday_session_detail as $session_detail){
				  if($session_detail->counts==$max_attendee){
					  $invalid_sessions.=	$session_detail->webinar_schedule_id_fk;
					  if($i!=$total_rows){
						  $invalid_sessions.=	",";	
					  }
					  $i++;
						  
				  }	
				  if($session_detail->counts!=0){
					  $scheduled_date	= $web_date;
				  }
			  }
			  $last_char = $invalid_sessions[strlen($invalid_sessions)-1];
			  if($last_char==","){
				  $invalid_sessions	=	substr($invalid_sessions,0,strlen($invalid_sessions)-1);	
			  }
			  if($webinar_schedule_id==4){
				  
				  if($max_attendee==0){
					  $condition	=	 '';
				  }else {
						  $condition	=	 'having count<'.$max_attendee;
				  }
				  
				  $webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
			  }else {
				  $webinar_sessions	=	$ob_webinar_db_interaction->get_unexpired_webinar_sessions($webinar_id,$invalid_sessions,$webinar_schedule_id,$web_date);
			  }
			  
			  $webinar_date	=	$scheduled_date;			
		  }else{
			  if($webinar_schedule_id==4){
			  
				  if($max_attendee==0){
					  $condition	=	 '';
				  }else {
						  $condition	=	 'having count<'.$max_attendee;
				  }
					  
				  $webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
			  }else {
				  $webinar_sessions =	$ob_webinar_db_interaction->get_today_unexpired_session_for_everyday_mod($webinar_id,$max_attendee,$webinar_schedule_id,$web_date);
			  }
			  
			  if(count($webinar_sessions)==0){
				   $webinar_date = $web_date;
				  if($webinar_schedule_id==4){
					  
					  if($max_attendee==0){
						  $condition	=	 '';
					  }else {
						  $condition	=	 'having count<'.$max_attendee;
					  }
					  
					  $webinar_sessions = $wpdb->get_results("select count(t2.registration_id_pk) as count,t1.webinar_schedule_id_pk,t1.start_time from webinar_schedule t1 LEFT JOIN webinar_registered_users t2 on t1.webinar_id_fk=t2.webinar_id_fk and t2.webinar_date = '$web_date'  and t1.webinar_schedule_id_pk=t2.webinar_schedule_id_fk where t1.webinar_id_fk=$webinar_id and t1.week_day='$day_name'  group by t1.webinar_schedule_id_pk ".$condition." ORDER BY t1.start_time");
				  }else {
					  $webinar_sessions	=	$ob_webinar_db_interaction->get_webinar_sessions($webinar_id,$webinar_schedule_id,$web_date);
					  
				  }
			  }else{
				  $webinar_date = $web_date;
			  }
		  }
		  
		  if($attendee_timezone>0){
		  $webinar_timezone	      	  	=	$ob_webinar_db_interaction->get_timezone_name($attendee_timezone);
		  $webinar_timezone_name	      = $webinar_timezone[0]->timezone_name;
				for($i=0;$i<count($webinar_sessions);$i++){
						
					$date_in_format = date('Y-m-d H:i:s',strtotime($web_date." ".$webinar_sessions[$i]->start_time)+$webinar_endpoint);
					$webinar_time_in_gmt = $ob_webinar_db_interaction->convert_time_zone($date_in_format,$webinar_timezone_name,"GMT");
				 
					if(strtotime($webinar_time_in_gmt) > strtotime($user_gmt_date." ".$user_gmt_time)){
					$valid_times[]	=	$webinar_sessions[$i];
					}	
				
				}          
		  
			}else{
				  for($i=0;$i<count($webinar_sessions);$i++){
		  
					 $date_in_format = date('Y-m-d H:i:s',strtotime($web_date." ".$webinar_sessions[$i]->start_time)+$webinar_endpoint);
					  if( strtotime($date_in_format) > strtotime($timezone_date." ".$timezone_time)){
						 $valid_times[]	=	$webinar_sessions[$i];
						}
				  }
		  }
		  
		  if(!empty($valid_times)){
			  $valid_date	= $date_array;
			  break; 
		  }
	  }
	  
		if(!empty($valid_times)) {
	  if($attendee_timezone=='0'){
		  sscanf($timezone_difference, "%d:%d:%d", $hours, $minutes, $seconds);
		  $timezonde_difference_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		  if($timezone_operation=='-'){$tz_opt = '+';}else{$tz_opt = '-';}
		  
		  $gmt_date_time = gmdate("Y-m-d H:i:s",strtotime($valid_date." ".$valid_times[0]->start_time." ". $tz_opt.$timezonde_difference_seconds." Seconds"));
		  $gmt_date_time_array = explode(" ",$gmt_date_time);
		  
		  $current_timestamp			  =	time();
		  $remaining_time				  =	1000*((strtotime($gmt_date_time))-$current_timestamp);
		  
		  echo json_encode(array('date'=>$gmt_date_time_array[0],'time'=>$gmt_date_time_array[1],'time_stamp'=>strtotime($gmt_date_time),'remaining_time'=>$remaining_time));	
		  exit;
	  }else{
			
			$webinar_timezone	      	  	=	$ob_webinar_db_interaction->get_timezone_name($attendee_timezone);
			$webinar_timezone_name	      = $webinar_timezone[0]->timezone_name;
		  $webinar_utc_start_date_time  =	$ob_webinar_db_interaction->convert_time_zone($valid_date.' '.$valid_times[0]->start_time,$webinar_timezone_name,'GMT');
		  $webinar_utc_end_date_time	  = date('Y-m-d H:i:s', strtotime($webinar_utc_start_date_time)+$webinar_detail[0]->webinar_video_length); 
						  
		  $webinar_start_date_time_array	= explode(' ',$webinar_utc_start_date_time);
		  $webinar_utc_end_date_time_array= explode(' ',$webinar_utc_end_date_time);
		  
		  $current_timestamp			=	time();
		  $remaining_time				  =	1000*(strtotime($webinar_utc_start_date_time)-$current_timestamp);
	      
		  echo json_encode(array('date'=>$webinar_start_date_time_array[0],'time'=>$webinar_start_date_time_array[1],'time_stamp'=>strtotime($webinar_utc_start_date_time),'remaining_time'=>$remaining_time));
	  }
	  }// end if !empty($valid_times)
	   else{
		   echo json_encode(array());
		   }
	  exit;
  }
  add_action('wp_ajax_available_webinar_registration_dates','get_available_webinar_registration_dates');
  add_action('wp_ajax_nopriv_available_webinar_registration_dates', 'get_available_webinar_registration_dates'); 
  
  
  function set_attendee_status(){
	 
	  $attendee_id = (int) $_POST['attendee'];
	  $status 	   = (int) $_POST['status'];
	  $is_live	   = (int) $_POST['live'];
	  
	  $ob_webinar_db_interaction  = new webinar_db_interaction();
	  $attendee_info = $ob_webinar_db_interaction->get_attendee_info_by_id($attendee_id);
		
	  if($is_live==1){
			if($status===3){	
				$emarketing_settings	= maybe_unserialize(get_option('ewp_emarketing_settings_'.$attendee_info->webinar_id_fk));
				$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
				$infusionsoft_enabled	= (int)$emarketing_settings['infusionsoft']['enabled'];
				if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		           $emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	               $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		        }
				if($infusionsoft_enabled){
					$attendee_name	= $attendee_info->attendee_name;
					$attendee_email = $attendee_info->attendee_email_id;
					$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
					$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
				if($authorized_credentials){
					$iui		 =  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendee_name,'Email'=>$attendee_email));
					$obj_eII->applyTagToUser($iui,$emarketing_settings['infusionsoft']['tags']['watched_full']);
					$obj_eII->grpRemoveFromUser($iui,$emarketing_settings['infusionsoft']['tags']['left_early']);
					}
				}
			}
		 echo $ob_webinar_db_interaction->set_attendee_event_activity_status($attendee_id,$status); 
	  }else{
		 echo $ob_webinar_db_interaction->set_attendee_replay_activity_status($attendee_id,$status); 
	  }
	  
	  exit;
  }
  add_action('wp_ajax_track_attendee_status','set_attendee_status');
  add_action( 'wp_ajax_nopriv_track_attendee_status','set_attendee_status'); 
  
  function track_event_infusionsoft(){
  global $wpdb;
  $wpdb->webinar_delayed_events = "webinar_delayed_events";
    $attendeeName =  $_POST['attendeeName'];
	$attendeeEmail =  $_POST['attendeeEmail'];
	$webinar_idd = (int) $_POST['webinarId'];
	$delayed_event = (int) $_POST['delayedEvent'];
	$del_infusiontag = (int) $_POST['deltag'];
	/*$added_tag = $wpdb->get_results("SELECT infusionsoft_delayed_tag FROM $wpdb->webinar_delayed_events WHERE webinar_delayed_events_id_pk='" . $delayed_event . "'");
	$infusion_tag_id = $added_tag[0]->infusionsoft_delayed_tag; */
	$emarketing_settings			= maybe_unserialize(get_option('ewp_emarketing_settings_'.$webinar_idd));
	$infusionsoft_credentials		= maybe_unserialize(get_option('infusionsoft_credentials'));
	if(!empty($infusionsoft_credentials) && is_array($infusionsoft_credentials) ) {
		$emarketing_settings['infusionsoft']['credentials']['account_id'] = $infusionsoft_credentials['infusionsoft']['account_id'];
	    $emarketing_settings['infusionsoft']['credentials']['account_key'] = $infusionsoft_credentials['infusionsoft']['account_key'];
		}
	$obj_eII =	new ewpInfusionsoftIntegration($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
	$authorized_credentials = $obj_eII->authenticateCredentials($emarketing_settings['infusionsoft']['credentials']['account_id'],$emarketing_settings['infusionsoft']['credentials']['account_key']);
	if($authorized_credentials){
	$iui	=  $obj_eII->createContactIDbyEmail(array('FirstName'=>$attendeeName,'Email'=>$attendeeEmail));
	$obj_eII->grpRemoveFromUser($iui,$del_infusiontag);
	$obj_eII->applyTagToUser($iui,$delayed_event);
	}
    exit;	
  }
  
  add_action('wp_ajax_store_event_in_infusionsoft','track_event_infusionsoft');
  add_action('wp_ajax_nopriv_store_event_in_infusionsoft','track_event_infusionsoft');
  function set_attendee_watched_status(){
	  
	  $attendee_id 		= (int) $_POST['attendee'];
	  $watched_status = (int) $_POST['watched'];
	  
	  $ob_webinar_db_interaction  = new webinar_db_interaction();
	  echo $ob_webinar_db_interaction->set_attendee_watched_stat($attendee_id,$watched_status);
	  
	  exit;
  }
  
  add_action('wp_ajax_track_attendee_watched_status','set_attendee_watched_status');
  add_action( 'wp_ajax_nopriv_track_attendee_watched_status','set_attendee_watched_status'); 
?>