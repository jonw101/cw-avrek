<?php
add_theme_support( 'post-thumbnails' );
add_image_size('inner_photo', 289, 9999); // featured image for inner page content areas, fixed width, variable height
add_image_size('attorney_photo', 300, 9999); // featured image for attorney bio pages, fixed width, variable height
add_image_size('attorney_thumb', 119, 126, true); // attorney thumbnail for sidebars, fixed height and width, hard crop


add_theme_support( 'menus' );
register_nav_menus( array(
	'main_nav' => 'Main Navigation', 
	'mobile_drop' => 'Mobile Drop Menu'
));


/* remove [...] from default excerpt */
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/* Custom excerpt lengths */
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}


/* post view count */
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


/* Enqueue scripts and styles */

function avrek_theme_scripts() {
	
	wp_enqueue_style( 'avrek-theme-style', get_stylesheet_uri() );
	
	if(is_page_template('page-attorney.php')) {
		wp_enqueue_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array('jquery'), '1.10.3', true );
	}
		
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'avrek_theme_scripts' );


add_action( 'wp_enqueue_scripts', 'de_script', 100 );

function de_script() {
/*
	if ( is_plugin_active('optimizePressPlugin/optimizepress.php') ) {

    wp_dequeue_script( 'fancybox' );
    wp_deregister_script( 'fancybox' );
    wp_dequeue_style( 'fancybox' );
	
	} 
*/
}

/* Remove url field from comments form */
function alter_comment_form_fields($fields){
    $fields['url'] = '';  //removes website field
    return $fields;
}
add_filter('comment_form_default_fields','alter_comment_form_fields');


/* H1 title */
function h1_title($id=false, $echo=true){
	global $post;
	$title = "";
	if ($id)
		$title = get_field('h1_title', $id)? get_field('h1_title', $id) : get_the_title($id);
	else 
		$title = get_field('h1_title', $post->ID)? get_field('h1_title', $post->ID) : get_the_title($post->ID);
	if ($title != ""){
		if ($echo)
			echo $title;
		else 
			return $title;	
	}else{
		return false;
	}
}


/* Fix for WP SEO Local Plugin */
function locations_cpt($args){
	if (count($args)){
		$args['rewrite'] = array( 'slug' => 'locations' , 'with_front' => false);
	}
	return $args;
}
add_filter('wpseo_local_cpt_args', 'locations_cpt' );


 /**
  * Ajax Load Posts
  */
 function pbd_alp_init() {
 	global $wp_query;
	
	//if( isset( $_REQUEST['debug'] )  ) {
		$page_id = get_the_ID();
		if( in_array( $page_id, array(264, 262) ) ) {
			// Queue JS and CSS
			
			$s_args = array(
				264 => array(
								  'post_type'  =>  'testimonials',
								  'orderby'    =>  'menu_order',
								  'order'      =>  'ASC'
							),
				262 => array(
								  'post_type'  =>  'case-results',
								  'orderby'    =>  'menu_order',
								  'order'      =>  'ASC'
							)
			);
			
			wp_enqueue_script(
				'pbd-alp-load-posts',
				get_template_directory_uri() . '/js/ajax-load-posts.js',
				array('jquery'),
				'1.0',
				true
			);
			$query_args = $s_args[$page_id];
			$the_query = new WP_Query ( $query_args );
			$max = $the_query->max_num_pages; 
			//}
			$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
			
			// Add some parameters for the JS.
			wp_localize_script(
				'pbd-alp-load-posts',
				'pbd_alp',
				array(
					'startPage' => $paged,
					'maxPages' => $max,
					'nextLink' => next_posts($max, false)
				)
			);
		}
		
	//} else {
	/*
 	// Add code to testimonial page.
 	if( is_page(264) ) {
 		// Queue JS and CSS
 		wp_enqueue_script(
 			'pbd-alp-load-posts',
 			get_template_directory_uri() . '/js/ajax-load-posts.js',
 			array('jquery'),
 			'1.0',
 			true
 		);		
 		
 		// What page are we on? And what is the pages limit?
 		//$max = $wp_query->max_num_pages;
		//if( isset( $_REQUEST['debug'] ) ) {
		$query_args = array(
			  'post_type'  =>  'testimonials',
			  'orderby'    =>  'menu_order',
			  'order'      =>  'ASC'
			);  
		$the_query = new WP_Query ( $query_args );
		$max = $the_query->max_num_pages; 
		//}
 		$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
 		
 		// Add some parameters for the JS.
 		wp_localize_script(
 			'pbd-alp-load-posts',
 			'pbd_alp',
 			array(
 				'startPage' => $paged,
 				'maxPages' => $max,
 				'nextLink' => next_posts($max, false)
 			)
 		);
 	}
	
	
	}*/
 }
 add_action('template_redirect', 'pbd_alp_init');

function is_parent($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if(is_page()&&($post->post_parent==$pid||is_page($pid))) 
               return true;   // we're at the page or at a sub page
	else 
               return false;  // we're elsewhere
};

?>