<?php if( !is_front_page() ){ ?>

<div class="wrapper">
		<div class="spanfull award-logos">
		<img alt="The National Trial Lawyers" src="http://www.thenationaltriallawyers.org/images/NTL-top-40-40-member.png" width="75" height="75">
<?php
			for($i = 1; $i <= 6; $i++){
				echo '<img src="/wp-content/uploads/2014/08/logo'.$i.'.jpg" alt="logo '.$i.'" />';
			}
			?>
<!--			
			<img src="/wp-content/themes/avrek/images/logo-mmdaf.jpg" alt="Multi-Million Dollar Advocates Forum" />
			<img src="/wp-content/themes/avrek/images/logo-caala.jpg" alt="Consumer Attorneys Association of Los Angeles" />
			<img src="/wp-content/themes/avrek/images/consumer-attorneys.png" alt="Consumer Attorneys Association of Los Angeles" />
			<img src="/wp-content/themes/avrek/images/campaign-justice-logo.jpg" alt="Campaign Justice logo" />
			<img src="/wp-content/themes/avrek/images/state-bar-califrnia-logo.jpg" alt="State bar of California" />
-->
		</div>
</div>

<?php } ?>

<footer>
	<section id="top-footer">
		<div class="wrapper">
			<div class="right">
				<?php get_template_part('includes/call-chat'); ?>
				<div class="social-icons">
					<h4>Síganos en:</h4>
					<ul>
						<!--<li><a href="#"><img src="/wp-content/themes/avrek/images/ico-googleplus.jpg" alt="Google+" /></a></li>
						<li><a href="#"><img src="/wp-content/themes/avrek/images/ico-linkedin.jpg" alt="LinkedIn" /></a></li>-->
						<li><a href="https://twitter.com/avreklawfirm" target="_blank"><img src="/wp-content/themes/avrek/images/ico-twitter.jpg" alt="Twitter" /></a></li>
						<li><a href="http://www.facebook.com/avreklawfirm" target="_blank"><img src="/wp-content/themes/avrek/images/ico-facebook.jpg" alt="Facebook" /></a></li>
						<li><a href="https://www.youtube.com/user/AvrekLaw" target="_blank"><img src="/wp-content/themes/avrek/images/ico-youtube.jpg" alt="YouTube" /></a></li>
					</ul>
				</div>
			</div>		
			<div class="left">
				<div class="column ctm-no-swap">
					<h3>Localización</h3>
					<?php  
			          if( function_exists( 'wpseo_local_show_address' ) ) {
			               $params = array(
			                    'echo' => true,
			                    'id' => 213,
			                    'show_state' => true,
			                    'show_country' => false,
			                    'show_phone' => true,
			                    'show_fax' => false,
			                    'show_phone_2' => false,
			                    'oneline' => false,
			                    'show_opening_hours' => false
			               );
			               wpseo_local_show_address( $params );
			          }
		  			?>	
		  			<a href="https://goo.gl/maps/ZYQer" target="_blank" class="btn-small-wt">Indicaciones Hasta la Oficina</a>				
				</div>
				<div class="column">
					<h3>Comunidades a las que Servimos </h3>
					<ul>
						<li>Irvine</li>
						<li>Riverside</li>
						<li>Orange County</li>
						<li>Riverside County</li>
						<li>Santa Ana</li>
						<li>Sirviendo a todo California</li>
					</ul>
				</div>
			</div>
			<?php get_template_part('includes/tap-buttons-footer'); ?>
		</div>
	</section>
				
	<section id="copyright">
		<div class="wrapper">
			<div class="left">
				<p class="foot-links"><a href="/mapa-del-sitio/">Mapa del Sitio</a> | <a href="/politica-de-privacidad/">Política de Privacidad</a> | <a href="/descargo-de-responsabilidad/">Descargo de Responsabilidad</a></p>
				<p>&copy; <?php echo date('Y'); ?> Avrek Law Firm - Todos Los Derechos Reservados.</p>
			</div>
			<div class="right">
				<!-- <p>Site by <a href="http://www.consultwebs.com" target="_blank" rel="nofollow">Consultwebs.com</a>: Law Firm Website Designers / Personal Injury Lawyer Marketing</p> -->
			</div>
		</div>
	</section>
	
</footer>
<!--
<div id="fixedDiv">
    <a href="/contact-us/"><img src="http://www.avrek.com/wp-content/uploads/2014/03/request-call-back.jpg" /></a>
</div>
-->

<?php wp_footer(); ?>

<script type="text/javascript" src="http://www.cw-apps.com/wp-content/js-repository/a472a92dfbb958e8dc650ab7f3acc2cb.js" > </script>

<!-- avrek.com-spanish --> 
<!-- Start Of Ngage --> 
<script type="text/javascript" src="https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=9-146-52-216-28-50-153-115"></script> 
<!-- End Of NGage -->

</body>
</html>