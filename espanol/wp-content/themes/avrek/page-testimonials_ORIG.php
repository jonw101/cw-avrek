<?php 
/*
* Template Name: Testimonials
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="intro">
			<strong>Our success is measured not only by our successful litigation but also by the overall positive responses from our clients. </strong>
		</div>
		
		<div class="grid">
			
			<?php
			$the_query = new WP_Query( 'post_type=testimonials&posts_per_page=2' );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<div class="box">
				<p>"<?php echo excerpt(80); ?>"</p>
				<p>- <strong><?php the_title(); ?></strong></p>
				<a href="#testimonial<?php the_ID(); ?>" class="btn-plus popup">+</a>
				<div style="display:none;">
					<div id="testimonial<?php the_ID(); ?>" class="box-popup">
						<?php the_content(); ?>
						<p>- <strong><?php the_title(); ?></strong></p>
					</div>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
			
			
			<div class="box review">
				<div class="head">
					<h2>Leave a Review</h2>
					<img src="/wp-content/themes/avrek/images/stars-small.png" alt="stars" />
				</div>
				<div class="logos">
					<!--<a href="#"><img src="/wp-content/themes/avrek/images/logo-google.jpg" alt="Google" /></a>-->
					<a href="http://www.yelp.com/biz/avrek-law-firm-irvine" target="_blank"> <img src="/wp-content/themes/avrek/images/logo-yelp.jpg" alt="Yelp" /></a>
				</div>
				<div class="review-body">
					<?php
					$the_query = new WP_Query( 'post_type=testimonials&offset=2&posts_per_page=1' );
					while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					<p>"<?php echo excerpt(80); ?>"</p>
					<p>- <strong><?php the_title(); ?></strong></p>
					<a href="#testimonial<?php the_ID(); ?>" class="btn-plus popup">+</a>
					<div style="display:none;">
						<div id="testimonial<?php the_ID(); ?>" class="box-popup">
							<?php the_content(); ?>
							<p>- <strong><?php the_title(); ?></strong></p>
						</div>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
				</div>
			</div>
			
			<?php
			$the_query = new WP_Query( 'post_type=testimonials&offset=3&posts_per_page=9999' );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<div class="box">
				<p>"<?php echo excerpt(80); ?>"</p>
				<p>- <strong><?php the_title(); ?></strong></p>
				<a href="#testimonial<?php the_ID(); ?>" class="btn-plus popup">+</a>
				<div style="display:none;">
					<div id="testimonial<?php the_ID(); ?>" class="box-popup">
						<?php the_content(); ?>
						<p>- <strong><?php the_title(); ?></strong></p>
					</div>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
														
		</div>
		
		<!--<a href="/testimonials/" class="load-more">Load More Testimonials</a>-->
		
	</div>
</section>

<?php get_footer(); ?>