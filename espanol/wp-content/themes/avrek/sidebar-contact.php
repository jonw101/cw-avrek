		<aside class="sidebar right">
		
			<div class="padding-box">
			
				<div>
					<h3>¿Quién va a recibir esta información?</h3>
					<?php if(is_page(256)) { ?>
					<img src="/wp-content/themes/avrek/images/who-receives-info-calculator.jpg" alt="Who receives this information?" />
					<? } else { ?>
					<img src="/wp-content/themes/avrek/images/who-receives-info-contact.jpg" alt="Who receives this information?" />
					<?php } ?>
					<p>Su información sera revisada por el equipo legal de Avrek que han representado por más de 30 años a víctimas de accidentes lesionados. </p>
				</div>
				
				<div>
					<h3>¿Qué Esperar?</h3>
					<p>Una llamada telefónica de nuestro personal dentro de 1 hora para llevar a cabo su consulta inicial donde vamos a hablar sobre cómo podemos ayudarle.</p>
				</div>
				
				<div>
					<h3>Su Privacidad es Importante</h3>
					<p>Toda la información que nos ha facilitado se mantendrá estrictamente confidencial.</p>
				</div>	
			
			</div>
						
			<!-- <div class="side-award">
				<a href="http://www.avvo.com/" target="_blank"><img src="/wp-content/themes/avrek/images/logo-avvo.jpg" alt="AVVO" /></a>
			</div>-->
		
		</aside>