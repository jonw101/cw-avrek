<?php 
/*
* Template Name: Testimonials
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div id="grid-holder" class="wrapper">
	
		<div class="intro">
			<strong>Our success is measured not only by our successful litigation but also by the overall positive responses from our clients. </strong>
		</div>
		
		<div class="grid">

			<?php
			$display_count = 12;
			$page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			$offset = ( $page - 1 ) * $display_count;
			$query_args = array(
			  'post_type'  =>  'testimonials',
			  'orderby'    =>  'menu_order',
			  'order'      =>  'ASC',
			  'posts_per_page'     =>  $display_count,
			  'page'       =>  $page,
			  'offset'     =>  $offset
			);
			$the_query = new WP_Query ( $query_args );
			$counter = 1;
			if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$third_box = $counter == 3 ? TRUE : FALSE;
			?>
			<?php if ( !$third_box ) { ?>
			<div class="box post">
				<p>"<?php echo excerpt(80); ?>"</p>
				<p>- <strong><?php the_title(); ?></strong></p>
				<a href="#testimonial<?php the_ID(); ?>" class="btn-plus popup">+</a>
				<div style="display:none;">
					<div id="testimonial<?php the_ID(); ?>" class="box-popup">
						<?php the_content(); ?>
						<p>- <strong><?php the_title(); ?></strong></p>
					</div>
				</div>
			</div>
			<?php } else { ?>
			<div class="box review">
				<div class="head">
					<h2>Leave a Review</h2>
					<img src="/wp-content/themes/avrek/images/stars-dark-small.png" alt="stars" />
				</div>
				<div class="logos">
					<a href="#"><img src="/wp-content/themes/avrek/images/logo-google.jpg" alt="Google" /></a>
					<a href="#"><img src="/wp-content/themes/avrek/images/logo-yelp.jpg" alt="Yelp" /></a>
				</div>
				<div class="review-body">					
					<p>"<?php echo excerpt(80); ?>"</p>
					<p>- <strong><?php the_title(); ?></strong></p>
				</div>
			</div>
			<?php } ?>
			<?php $counter++; ?>
			<?php endwhile; ?>
		
		</div>
		
		<div class="navigation load-more">
			<?php next_posts_link( 'Next Posts >>', $the_query->max_num_pages ); ?>
		</div>
		
		<?php endif; wp_reset_postdata(); ?>		
		
	</div>
</section>

<?php get_footer(); ?>