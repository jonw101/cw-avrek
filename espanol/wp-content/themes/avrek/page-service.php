<?php 
/*
* Template Name: Legal Services / Service Area
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
		<?php
		$the_query = new WP_Query( 'post_type=page&orderby=menu_order&order=ASC&post_parent='.$post->ID );
		while ( $the_query->have_posts() ) : $the_query->the_post();
		?>
		  <div class="services-box clearfix">			
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>     
			<?php the_post_thumbnail('thumbnail'); ?> 			
			<p><?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">Seguir Leyendo &raquo;</a></p>
		  </div>
		<?php endwhile; wp_reset_postdata(); ?>
		
		</div>
		
		<?php get_sidebar(); ?>
		
	</div>
</section>

<?php get_footer(); ?>