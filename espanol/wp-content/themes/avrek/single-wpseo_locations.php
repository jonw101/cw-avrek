<?php 
/*
* Template Name: Community/Location
*/
get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<?php if( get_field('banner_image') ) { ?>
<section id="banner" class="location">
	<div class="wrapper">
		<div class="slide" style="background-image:url(<?php the_field('banner_image'); ?>);">
			<?php if(get_field('banner_photo_credit')) { ?>
			<div class="attribution">
				Photo Credit: <?php the_field('banner_photo_credit'); ?>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php } ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="body">
	<div class="wrapper">
	
		<div class="content left location">
		
			<?php the_field('intro'); ?>
		
			<?php get_template_part('includes/contact-form'); ?>
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>		
			<?php endwhile; endif; ?>
		
		</div>
		
		<?php get_sidebar('locations'); ?>
		
	</div>
</section>

<?php get_footer(); ?>