			<div class="contact-form calculator clearB">
				<span class="req">* Requerido</span>
				<h3>Averigüe si  Usted tiene un caso y el valor de este.</h3>
				<form action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
					<input type="text" value="Nombre Completo *" name="name" class="required" />
					<input type="text" value="Correo electrónico *" name="email" class="required" />
					<input type="text" value="Teléfono *" name="phone" class="required PhoneField" />
					<input type="text" value="Gastos médicos *" name="medical_costs" class="required" />
					<input type="text" value="Gastos de Reparación Mecánica *" name="auto_repair_costs" class="required" />
					<input type="text" value="Gastos por alquiler de coche *" name="auto_rental_costs" class="required" />
					<input type="text" value="Pérdidas de ingresos *" name="income_lost" class="required" />
					<div style="position: absolute; bottom: 27px;" class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" checked value="He leído y acepto los Términos y Condiciones de uso">
						<label for="disclaimer" style="color:#fff;">He leído y acepto los Términos <br /> y Condiciones de uso</label>
					</div>
					<input type="button" id="calc-estimate" value="Calcular Mi Indemizacion" />
					<div id="calc-results">
						<p>Su pago de lesiones personales se valora entre:</p>
						<input type="text" readonly="readonly" id="calc-value" />
						<p>Debido a varios otros factores que pueden afectar a cada caso individual y la pericia del abogado, su pago podrá ser mayor o menor. Le aconsejamos que consulte a unos cuantos profesionales del derecho para determinar la mejor representación para su caso. Para una evaluación completa con Avrek llame al 1.888.333.5009 o clique a la derecha para una consulta GRATUITA.</p>
					</div>
					
					<input type="submit" id="calc-submit" value="Enviar Para Una Consulta Gratis" />
				</form>
			</div>			