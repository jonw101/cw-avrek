<?php
if( is_parent(18) && !is_page(18) ){
	$form_id = 'sb-cmform';
}

?>


			<div id="<?php echo $form_id; ?>" class="contact-form clearB">
				<span class="req">* Requerido</span>
				<?php if(!is_page(9)) { ?>
				<h3>Revisión Gratuita de Casos <strong>¿Tiene un caso?</strong></h3>
				<?php } ?>
				<form action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
					<input type="text" value="Nombre completo *" name="name" class="required" />
					<input type="text" value="Tel&eacute;fono o correo electronic *" name="phone_or_email" class="required PhoneEmailField" />
					<textarea name="comments">Preguntas o Comentarios</textarea>
					<div style="margin-bottom: 10px;" class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" checked value="He leído y acepto las condiciones de uso.">
						<label for="disclaimer">He leído y acepto las condiciones de uso.</label>
					</div> 
					<input type="submit" value="Enviar para una consulta gratis" />
				</form>
			</div>			