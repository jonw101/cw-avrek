				<div class="faq">
					<h3>Preguntas Frecuentes</h3>
					<ul>
                	<?php
					global $post;
	                $slug = get_post($post)->post_name;
	                $term = term_exists($slug, 'faq_category');
	                if ($term !== 0 && $term !== null) {
                		$the_query = new WP_Query( array( 'post_type' => 'qa_faqs', 'posts_per_page' => 3, 'orderby' => 'rand', 'tax_query' => array(
                    					array(
                                             'taxonomy' => 'faq_category',
                                             'field' => 'slug',
                                             'terms' => $slug
                                        )
                    ) ) );
                    } else {
	                    $the_query = new WP_Query( array( 'post_type' => 'qa_faqs', 'posts_per_page' => 3, 'orderby' => 'rand' ) );
                    }
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>		
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; wp_reset_postdata(); ?>
					</ul>
					<a href="/faq/" class="btn-small-wt right">Ver todas las FAQ</a>
				</div>
