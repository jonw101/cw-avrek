				<div class="meet-attorneys">
					<h3>Meet Our Attorneys</h3>
					<div class="attorneys-list">
						<?php
						$the_query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 27, 'post__not_in' => array(380,960), 'orderby' => 'menu_order', 'order' => 'ASC' ) );
						while ( $the_query->have_posts() ) : $the_query->the_post();
						?>	
						<div class="atty-thumb">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('attorney_thumb'); ?></a>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>	
					</div>			
				</div>