		<div class="case-results">
			<h3>Resultados del Caso</h3>
			<div class="results-holder">
                <?php
				global $post;
	            $slug = get_post($post)->post_name;
	            $term = term_exists($slug, 'case-results-category');
	            if ($term !== 0 && $term !== null) {
                	$the_query = new WP_Query( array( 'post_type' => 'case-results', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC', 'tax_query' => array(
                    					array(
                                             'taxonomy' => 'case-results-category',
                                             'field' => 'slug',
                                             'terms' => $slug
                                        )
                ) ) );
                } else {
	                        	$the_query = new WP_Query( array( 'post_type' => 'case-results', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC', 'tax_query' => array(
                    					array(
                                             'taxonomy' => 'case-results-category',
                                             'field' => 'slug',
                                             'terms' => 'featured'
                                        )
                ) ) );
                }
                while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>	
				<div class="result">
					<h4><?php the_title(); ?></h4>
					<?php the_content(); ?>
					<strong>Cantidad de  
					Indemnización: <span><?php the_field('settlement_amount'); ?></span>
					<?php if(get_field('confidential_settlement')) { ?> -- Confidential Settlement.<?php } ?></strong>
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<div class="results-nav"></div> 
		</div>