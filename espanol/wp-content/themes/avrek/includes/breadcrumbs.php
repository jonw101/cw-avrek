<section id="breadcrumbs" class="visible-desktop">
	<div class="wrapper">
		<?php 
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p>','</p>');
		} 
		?>
	</div>
</section>