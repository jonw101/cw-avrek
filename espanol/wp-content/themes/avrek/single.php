<?php get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>


<div itemscope itemtype="http://schema.org/Article">

<section id="inner-headline" class="blog">
	<div class="wrapper">
		<h1 itemprop="name"><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left blog">
		
		<div class="search-archives visible-phone">
			<?php get_search_form(); ?>
			<div class="archives">
				<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					<option value=""><?php echo esc_attr( __( 'Archives' ) ); ?></option> 
					<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 0 ) ); ?>
				</select>
			</div>
		</div>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			wpb_set_post_views(get_the_ID());
		?>
		
			<div class="post-header">
				<div class="date left" itemprop="datePublished">
					<span><?php the_time('M'); ?></span><span class="blue"><?php the_time('j'); ?></span>
				</div>
				<span class="metadata">by <span itemprop="author"><?php the_author(); ?></span></span>
			</div>		
			
			<?php
			$post_object = get_field('video');
			if( $post_object ) {
				// override $post
				$post = $post_object;
				setup_postdata( $post );
				$vid = $post->ID;
				$video_id = get_post_meta($vid, 'cw_video_id', true);
				$video_type = get_post_meta($vid, 'cw_video_type', true);
				$video_image = get_post_meta($vid, 'cw_video_thumbnail', true);
			?>
				<div class="inner-video">
					<a class="cw_video_open_popup_customized" href="javascript:void(0);" video_id="<?php esc_attr_e($video_id); ?>" video_type="<?php echo $video_type; ?>" video_rel="<?php esc_attr_e($rel); ?>" video_showinfo="<?php esc_attr_e($showinfo); ?>" video_autoplay="1">
						<img src="/wp-content/themes/avrek/images/btn-play-video.png" alt="Play Video" class="cw_video_video_btn" />
						<img src="<?php echo $video_image; ?>" alt="Play Video" class="cw_video_img">
					</a>
				</div>
			<?php wp_reset_postdata();
			} elseif ( '' != get_the_post_thumbnail() ) { ?>
				<span itemprop="image"><?php the_post_thumbnail('inner_photo'); ?></span>
			<?php } ?> 			
			      
			<?php the_content(); ?>	
			
			<?php get_template_part('includes/btns-social-share'); ?>	
		
		<?php endwhile; endif; ?>
		
			<?php comments_template(); ?>
		
		</div>
		
		<?php get_sidebar('blog'); ?>
		
	</div>
</section>

</div> <!-- /end schema container -->

<?php get_footer(); ?>