jQuery(document).ready(function($){

	/* mobile menu dropdown toggle */
	jQuery('.menu-drop a').bind('click touch',function(){
		jQuery('.menu-second').slideToggle('slow'); 
		return false;
	});
	
	/* sticky nav */
	jQuery('nav').sticky({topSpacing:0});

	/* clear form fields */
	var el = jQuery('input[type=text], textarea');
	    el.focus(function(e) {
	        if (e.target.value == e.target.defaultValue)
	            e.target.value = '';
	    });
	    el.blur(function(e) {
	        if (e.target.value == '')
	            e.target.value = e.target.defaultValue;
	    });	
	
	jQuery('#commentform').addClass('noauto');
	jQuery('#searchform').addClass('noauto');
	
	/* home banner cycle */
	jQuery('.slide-holder').cycle({
		fx:			'scrollLeft',
		timeout:	14000,
		pager:		'.banner-nav'
	});
	jQuery('.banner-nav a:first-child').text('Integridad');
	jQuery('.banner-nav a:nth-child(2)').text('Pasión');
	jQuery('.banner-nav a:nth-child(3)').text('Responsabilidad');
	jQuery('.banner-nav a:last-child').text('Resultados');

	/* case results cycle */
	/*
	jQuery('.results-holder').cycle({
		fx:			'scrollHorz',
		speed:		500,
		timeout:	0,
		pager:		'.results-nav'
	});*/

	jQuery('.results-nav').append( '<a href="/resultados-del-caso/" class="btn-small">Ver Todo</a>' );
	
	/* step form cycle */
	if ( jQuery('.step-form-cycle').length > 0 ) {
		jQuery('.step-form-cycle').cycle({
			fx:			'scrollHorz',
			speed:		500,
			timeout:	0,
			pager:		'.step-nav', 
			prev:   	'.prev-btn', 
			next:		'.next-btn'
		});
	}
	
	/* dynamic sidebar height */
	if (jQuery(window).width() > 767) {
		jQuery('.sidebar').height(Math.max(jQuery('#body .content').height(),jQuery('.sidebar').height()));
	}

	/* attorney form & testimonial/results popup */
	if ( jQuery('a.popup').length > 0 ) {  // don't run this on pages that don't have a link with the class "popup"
		jQuery("a.popup").fancybox({
			'padding' : 20
		});	  
	}	
	
	/* masonry grid for case results & testimonials */
	if ( jQuery('.grid').length > 0 ) {
		jQuery('.grid').masonry({
		  itemSelector: '.box'
		});
	}
	
	/* spanish text for local sidebar */
	jQuery('.postid-230 .wpseo-phone').html('Teléfono: <a itemprop="telephone" class="tel" href="tel:9092354858">909-235-4858</a>');
	jQuery('.postid-213 .wpseo-phone').html('Teléfono: <a itemprop="telephone" class="tel" href="tel:9493133577">949-313-3577</a>');
	jQuery('.page-id-8 .wpseo-phone').html('Teléfono: <a itemprop="telephone" class="tel" href="tel:9493133577">949-313-3577</a>');
	jQuery('#top-footer .wpseo-phone').html('Teléfono: <a itemprop="telephone" class="tel" href="tel:9493133577">949-313-3577</a>');
	jQuery('.postid-230 #top-footer .wpseo-phone').html('Teléfono: <a itemprop="telephone" class="tel" href="tel:9092354858">909-235-4858</a>');
	
	jQuery('.office-hours h3').html('Horario de Operaciones');
	jQuery('.wpseo-opening-hours tr:first-child td.day').html('Lunes');
	jQuery('.wpseo-opening-hours tr:nth-child(2) td.day').html('Martes');
	jQuery('.wpseo-opening-hours tr:nth-child(3) td.day').html('Miércoles');
	jQuery('.wpseo-opening-hours tr:nth-child(4) td.day').html('Jueves');
	jQuery('.wpseo-opening-hours tr:nth-child(5) td.day').html('Viernes');

});