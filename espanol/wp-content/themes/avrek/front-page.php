<?php get_header(); ?>

<section id="banner">
	<div class="wrapper">
		<div class="slide-holder visible-desktop">
			<div class="slide slide1">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Integridad</strong> <span>UN COMPROMISO CON<br> EL PROFESIONALISMO <br>Y SERVICIO AL CLIENTE</span></h2>
						<p>Ayudamos a nuestros clientes en sus momentos difíciles, por medio de la elaboración de un caso que ofrecerá una solución  justa y plena.</p>
					</div>
				</div>
			</div>
			<div class="slide slide2">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Pasión</strong> <span>NOS PREOCUPAMOS <br>POR NUESTROS<br> CLIENTES Y SUS CASOS</span></h2>
						<p>Nuestros profesionales en legales  son impulsados por el deseo de tener éxito para nuestros clientes de lesiones. Lucharemos agresivamente y defender sus derechos.</p>
					</div>
				</div>
			</div>
			<div class="slide slide3">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Responsabilidad</strong> <span>DEDICAMOS ATENCION PERSONAL A CADA PERSONA QUE AYUDAMOS</span></h2>
						<p>Le mantendremos informado a cada paso del camino durante los tiempos difíciles. Usted será tratado con el respeto y la compasión que se merece.</p>
					</div>
				</div>
			</div>

	<!-- Additional slide -->
			<div class="slide slide4">
				<div class="slide-wrapper">
					<div class="slide-text">
						<h2><strong>Resultados</strong> <span>UN RECORD DE <br>EXITO PARA<br> NUESTROS CLIENTES</span></h2>
						<p>Desde 1998 el Despacho Jurídico Avrek ha obtenido más de $ 100 millones de dólares en indemnizaciones por  accidentes  y clientes lesionados.</p>
					</div>
				</div>
			</div>

		</div>
		<div class="banner-nav visible-desktop"></div>
		<div class="visible-phone">
			<img src="/wp-content/themes/avrek/images/bg-banner-solutions-mobile.jpg" alt="Modern Solutions" />
			<div class="slide-text">
				<h2><span>Utilizing</span> <strong>Modern Solutions</strong> <span>to get you results</span></h2>
				<?php get_template_part('includes/testimonial'); ?>
			</div>
		</div>		
	</div>
</section>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="pa-cols">
	<div class="wrapper">
		<h2>¿Cómo se lesionó ?</h2>
		<!-- additional -->
		<div class="column">
			<a href="/servicios-legales/accidentes/accidentes-automovilisticos/"><img src="http://www.avrek.com/wp-content/uploads/2014/03/ico-car-accidents.jpg" alt="Car Accidents" />
			Accidentes Automovilísticos</a>
		</div>
		<!-- end -->
		<div class="column">
			<a href="/servicios-legales/danos-personales/lesiones-por-quemaduras/"><img src="/wp-content/themes/avrek/images/ico-burn-injuries.jpg" alt="Burn Injuries" />
			Lesiones por Quemaduras</a>
		</div>
		<div class="column">
			<a href="/servicios-legales/accidentes/accidentes-de-bicicleta/"><img src="/wp-content/themes/avrek/images/ico-bike-injuries.jpg" alt="Bike Injuries" />
			Accidentes de  Bicicleta</a>	
		</div>	
		<div class="column">
			<a href="/servicios-legales/danos-personales/leciones-por-mordidas-de-perro/"><img src="/wp-content/themes/avrek/images/ico-animal-attacks.jpg" alt="Animal Attacks" />
			Lesiones por Mordidas de Perro</a>	
		</div>
		<div class="column">
			<a href="/servicios-legales/negligencia-medica/defectos-de-nacimiento/"><img src="/wp-content/themes/avrek/images/ico-birth-injuries.jpg" alt="Birth Injuries" />
			Defectos de Nacimiento</a>	
		</div>
		<div class="column">
			<a href="/servicios-legales/negligencia-medica/paralisis-cerebral/"><img src="/wp-content/themes/avrek/images/ico-brain-injuries.jpg" alt="Brain Injuries" />
			Lesiones Cerebrales</a>	
		</div>
		<div class="column col-last">
			<a href="/servicios-legales/danos-personales/lesiones-de-espalda-espina-dorsal/"><img src="/wp-content/themes/avrek/images/ico-back-spine-injuries.jpg" alt="Back/Spine Injuries" />
			Lesiones de Cordón Espinal</a>	
		</div>
		<a href="/servicios-legales/" class="btn-services">Vea Nuestros Servicios Legales</a>	
	</div>			
</section>

<section id="case-form-mobile" class="visible-phone">
	<div class="wrapper">
		<h3><span>Calculadora</span> De Lesiones</h3>
		<p>Solamente unos pequeños pasos para  determinar su Usted tiene un caso y el valor de este</p>
		<a href="/servicios-legales/danos-personales/calculadora-de-lesiones/">¿ Cuanto es el total de sus gastos medicos ? $ </a>
	</div>
</section>

<section id="attorneys">
	<div class="wrapper">
		<div class="column-span2">
			<h1><?php h1_title(); ?></h1>

			<?php the_content()?>

			<a href="/quienes-somos/" class="btn-small right">Quiénes Somos</a>
		</div>
		
		<div class="column">
			<?php get_template_part('includes/contact-form'); ?>
		</div>


		<div class="spanfull award-logos">
			<img alt="The National Trial Lawyers" src="http://www.thenationaltriallawyers.org/images/NTL-top-40-40-member.png" width="75" height="75">
			<?php
			for($i = 1; $i <= 6; $i++){
				echo '<img src="/wp-content/uploads/2014/08/logo'.$i.'.jpg" alt="logo '.$i.'" />';
			}
			?>
			
		</div>
	</div>
</section>

<section id="case-form" class="visible-desktop">
	<div class="wrapper">
		<div class="step-form">
			
			<h3>Solamente unos pequeños pasos para  determinar su Usted tiene un caso y el valor de este</h3>

			<span class="req">* Cantidad</span>
			
			<div class="step-nav"></div>
			
			<form class="step-form-cycle" action="http://www.cw-apps.com/form-processor-noscript.php" method="post">
			
				<div id="form-slide-1" class="step-slide">
					<div class="left">
						<h4>¿ Cuanto es el total de sus gastos medicos ? </h4>
						<p>Los gastos médicos son el coste de su tratamiento médico y en cuánto costos más se incurre para el siguiente tratamiento.</p>
					</div>
					<h4 class="dollar">$</h4>
					<input type="text" value="Cantidad *" name="medical_costs" class="required" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Siguiente Paso" class="next-btn" />
				</div>
				
				<div id="form-slide-2" class="step-slide">
					<h4>¿Cuanto es el total de sus gastos de reparación / alquiler de auto ?</h4>
					<div class="clearfix">
						<div class="left">
							<h4 class="dollar">$</h4>
							<input type="text" value="Gastos de Reparación Mecánica *" name="auto_repair_costs" class="required" />
						</div>
						<div class="right">
							<h4 class="dollar">$</h4>
							<input type="text" value="Gastos por Alquiler de Coche *" name="auto_rental_costs" class="required" />
						</div>
					</div>
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Siguiente Paso" class="next-btn" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Atrás" class="prev-btn" />
				</div>
								
				<div id="form-slide-3" class="step-slide">
					<h4>¿Cuanto es el total de perdida de ingresos? </h4>
					<h4 class="dollar">$</h4>
					<input type="text" value="Pérdidas de Ingresos *" name="income_lost" class="required" />
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'next step');" type="button" value="Siguiente Paso" class="next-btn" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Atrás" class="prev-btn" />
					
				</div>
				
				<div id="form-slide-4" class="step-slide">
					<div id="calc-info">
						<h4>Por favor, ingrese su nombre e información de contacto.</h4>
						<input type="text" value="Nombre completo *" name="name" class="name required" />
						<input type="text" value="Correo Electrónico" name="user_email" />
						<input type="text" value="Teléfono *" name="phone" class="required PhoneField" />
						<div class="disclaimer-container checkbox-container">
						<input type="checkbox" name="disclaimer" id="disclaimer" class="checkbox" value="He leído el aviso legal" checked />
						<label for="disclaimer">He leído el aviso legal</label>
						</div> 
					</div>
					
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'estimate');" type="button" value="Calcular mi indemnización " id="calc-estimate" />
					<input onclick="ga('send', 'event', 'settlement form', 'click', 'back');" type="button" value="Atrás" class="estimate-btn prev-btn" />
					<div id="calc-results">
						<div class="clearfix">
							<h4 class="left">Su pago de lesiones personales se valora entre:</h4>
							<input class="right" type="text" readonly="readonly" id="calc-value" />
						</div>
						<div class="clearfix">
							<p class="left">Debido a varios otros factores que pueden afectar a cada caso individual y la pericia del abogado, su pago podrá ser mayor o menor. Le aconsejamos que consulte a unos cuantos profesionales del derecho para determinar la mejor representación para su caso. Para una evaluación completa con Avrek llame al 1.888.333.5009 o clique a la derecha para una consulta GRATUITA.</p>
							<input onclick="ga('send', 'event', 'settlement form', 'click', 'free consult');" class="right" type="submit" value="Enviar Para Una Consulta Gratis" />
						</div>
					</div>					
				</div>
					
			</form>
			
			<script type="text/javascript">
			
				function getHashValue(key) {
					var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
					return ( matches !== null && matches.length >= 2 ? jQuery.trim(unescape(matches[1])).replace(/\"|\<|\>|\/|\'/g, '') : '' );
				}
				
				jQuery(document).ready(function() {
					var name = getHashValue('name'),
					    phone = getHashValue('phone'),
					    email = getHashValue('email');
				    
					setTimeout(function() {
						if( name != '' ) jQuery('.step-form-cycle input[name=name]').val(name).valid();
						if( phone != '' ) jQuery('.step-form-cycle input[name=phone]').val(phone).valid();
						if( email != '' ) jQuery('.step-form-cycle input[name=email]').val(email).valid();
						jQuery('.step-form-cycle input[name=phone]').focus();
					}, 500);
				});
			
				jQuery('#calc-estimate').click(function() {
					var damages = ( parseFloat(jQuery('.step-form-cycle input[name=income_lost]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('.step-form-cycle input[name=auto_repair_costs]').val().replace(/\$|\s|\,/g, '')) + parseFloat(jQuery('.step-form-cycle input[name=auto_rental_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
					var special_damages = ( parseFloat(jQuery('.step-form-cycle input[name=medical_costs]').val().replace(/\$|\s|\,/g, '')) ) || 0;
					var low_settlement = (2.5 * special_damages) + damages;
					var high_settlement = (3.5 * special_damages) + damages;
						
					jQuery('#calc-value').val('$' + Math.round(low_settlement) + ' - $' + Math.round(high_settlement));
						
					jQuery('#calc-info,#calc-estimate,#form-slide-4 .prev-btn').hide();
					jQuery('#calc-results').fadeIn();
				});
			
			</script>			
	
		</div>
	</div>
</section>

<section id="results-blog">
	<div class="wrapper">
			<div class="spanfull award-logos" style="margin-top: -30px;">
						<?php
			for($i = 1; $i <= 5; $i++){
				echo '<img src="/wp-content/uploads/2014/08/avvo'.$i.'.png" alt="logo '.$i.'" />';
			}
			?>

			</div>
	
		<?php get_template_part('includes/case-results'); ?>
		
		<div class="blog-post">
			<h3>De Nuestro Blog</h3>
			<?php
			$the_query = new WP_Query( 'post_type=post&posts_per_page=1' );
			while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<span class="metadata">Publicar en <?php the_time('F j, Y'); ?> Por <?php the_author(); ?></span>
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="btn-small">Leer Más</a>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
		
	</div>
</section>

<?php get_footer(); ?>