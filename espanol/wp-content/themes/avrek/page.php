<?php get_header(); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<h1><?php h1_title(); ?></h1>
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
			<?php
			$post_object = get_field('video');
			if( $post_object ) {
				// override $post
				$post = $post_object;
				setup_postdata( $post );
				$vid = $post->ID;
				$video_id = get_post_meta($vid, 'cw_video_id', true);
				$video_type = get_post_meta($vid, 'cw_video_type', true);
				$video_image = get_post_meta($vid, 'cw_video_thumbnail', true);
			?>
				<div class="inner-video">
					<a class="cw_video_open_popup_customized" href="javascript:void(0);" video_id="<?php esc_attr_e($video_id); ?>" video_type="<?php echo $video_type; ?>" video_rel="<?php esc_attr_e($rel); ?>" video_showinfo="<?php esc_attr_e($showinfo); ?>" video_autoplay="1">
						<img src="/wp-content/themes/avrek/images/btn-play-video.png" alt="Play Video" class="cw_video_video_btn" />
						<img src="<?php echo $video_image; ?>" alt="Play Video" class="cw_video_img">
					</a>
				</div>
			<?php wp_reset_postdata();
			} elseif ( '' != get_the_post_thumbnail() ) {
				the_post_thumbnail('inner_photo');
			} 
			?> 			
			      
			<?php the_content(); ?>		
		
		<?php endwhile; endif; ?>
		
		<?php if(is_page(23)) { ?>
	    	<ul>
	    		<?php wp_list_pages('title_li=');  // list pages for sitemap ?>
			</ul>
		<?php } ?>
		
		</div>
		
		<?php get_sidebar(); ?>
		
	</div>
</section>

<?php get_footer(); ?>