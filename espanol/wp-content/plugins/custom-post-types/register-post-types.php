<?php

/*
Plugin Name: Custom Post Types
Plugin URI: http://www.consultwebs.com
Description: This plugin registers CPTs and runs flush_rewrite_rules() on activation, which in some extraordinary cases makes automatic permalinking work.
Author: Consultwebs.com
Version: 1.1
Author URI: http://www.consultwebs.com
*/


add_action( 'init', 'create_my_post_types' );

function create_my_post_types() {
	register_post_type( 'testimonials',
		array(
			'labels' => array(
				'name' => __( 'Testimonials' ),
				'singular_name' => __( 'Testimonial' )
			),
			'public' => false,
			'show_ui' => true,
			'has_archive' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'page-attributes' ),
			'rewrite' => false
		)
	);
	register_post_type( 'case-results',
		array(
			'labels' => array(
				'name' => __( 'Case Results' ),
				'singular_name' => __( 'Case Result' )
			),
			'public' => false,
			'show_ui' => true,
			'has_archive' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'page-attributes' ),
			'rewrite' => false
		)
	);					
}


// custom taxonomy for testimonials post type
function custom_taxonomies_testimonials() {
     $labels = array(
          'name'              => _x( 'Categories', 'taxonomy general name' ),
          'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
          'search_items'      => __( 'Search Categories' ),
          'all_items'         => __( 'All Categories' ),
          'parent_item'       => __( 'Parent Category' ),
          'parent_item_colon' => __( 'Parent Category:' ),
          'edit_item'         => __( 'Edit Category' ),
          'update_item'       => __( 'Update Category' ),
          'add_new_item'      => __( 'Add New Category' ),
          'new_item_name'     => __( 'New Category' ),
          'menu_name'         => __( 'Categories' )
     );
     $args = array(
          'labels' => $labels,
          'hierarchical' => true,
          'rewrite' => array('with_front' => false), 
          'show_admin_column' => true
     );
     register_taxonomy( 'testimonials-category', 'testimonials', $args );
}
add_action( 'init', 'custom_taxonomies_testimonials', 0 );


// custom taxonomy for case results post type
function custom_taxonomies_case_results() {
     $labels = array(
          'name'              => _x( 'Categories', 'taxonomy general name' ),
          'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
          'search_items'      => __( 'Search Categories' ),
          'all_items'         => __( 'All Categories' ),
          'parent_item'       => __( 'Parent Category' ),
          'parent_item_colon' => __( 'Parent Category:' ),
          'edit_item'         => __( 'Edit Category' ),
          'update_item'       => __( 'Update Category' ),
          'add_new_item'      => __( 'Add New Category' ),
          'new_item_name'     => __( 'New Category' ),
          'menu_name'         => __( 'Categories' )
     );
     $args = array(
          'labels' => $labels,
          'hierarchical' => true,
          'rewrite' => array('with_front' => false), 
          'show_admin_column' => true
     );
     register_taxonomy( 'case-results-category', 'case-results', $args );
}
add_action( 'init', 'custom_taxonomies_case_results', 0 );


function my_rewrite_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry, 
    // when you add a post of this CPT.
    create_my_post_types();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );

?>