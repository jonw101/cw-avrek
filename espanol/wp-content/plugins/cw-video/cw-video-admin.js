	
	var CWVideoPreview = null;
	var CWVideoURL = 'http://';
	var $CWRemoveButton = null;
	
	jQuery(document).ready(function($) {
		if( jQuery('#CWVideoURLText').length > 0 ) {
			jQuery('#CWVideoURLText')
				.change(cwVideoHandleURLChange)
				.keyup(cwVideoHandleURLChange);
			jQuery("form#post").bind('keypress', function(evt) {
				if( evt.keyCode == 13 ) {
	                return false;
				}
			});
		}
		if( jQuery('#CWVideoAutoUpdate').length > 0 ) {
			jQuery('#CWVideoAutoUpdate').click(cwVideoAutoUpdate);
			cwVideoAutoUpdate();
		}
		if( jQuery('.cw_video_remove_auto_pull').length > 0 ) {
			jQuery('.cw_video_remove_auto_pull').click(function() {
				if( confirm('Are you sure you want to remove this feed?') ) {
					$CWRemoveButton = jQuery(this);
					jQuery.ajax(
						{
							url: $CWRemoveButton.attr('href')
						})
						.always(function() {
							$CWRemoveButton.parent().fadeOut(1000, function() {
								$CWRemoveButton.parent().remove();
								$CWRemoveButton = null;
							});
						});
				}
				return false;
			});
		}
		/** Added JS Script by Rey for Video Selector Option **/
		jQuery("#video_type").change(function() {
			var type = jQuery(this).val();
			if(type) {
				jQuery(".videotype.type_show").removeClass("type_show");
				jQuery(".type-" + type).addClass("type_show");
				jQuery("#CWVideoPreview").html('');
				jQuery('#CWVideoExamples').show();
			}
		});
		jQuery(".refresh_sitemap").live('click', csRefreshSiteMap);
		jQuery("#extract-vimeo").live('click', csVideoExtractVimeo);	
		jQuery("#extract-wistia").live('click', csVideoExtractWistia);			
	});
	
	function csRefreshSiteMap() {
		CWVideoPreview = true;
		jQuery('#show_loader').show();
		jQuery("#show_status").html('');
		jQuery.ajax(
			{
				url: '?cw_video_refresh_sitemap=1',
			})
			.done(function(data) {
				jQuery("#show_status").html('Completed').css("color", "blue");
			})
			.fail(function() {
				jQuery("#show_status").html('Failed. Please try again.').css("color","red");
			})
			.always(function() {
				jQuery('#show_loader').hide();
			});
	}
	
	function editPermalink(post_id) {
		var i, c = 0, e = jQuery('#editable-post-name'), revert_e = e.html(), real_slug = jQuery('#post_name'), revert_slug = real_slug.val(), b = jQuery('#edit-slug-buttons'), revert_b = b.html(), full = jQuery('#editable-post-name-full').html();
		console.log(full);
		jQuery('#view-post-btn').hide();
		b.html('<a href="#" class="save button button-small">'+postL10n.ok+'</a> <a class="cancel" href="#">'+postL10n.cancel+'</a>');
		b.children('.save').click(function() {
			var new_slug = e.children('input').val();
			if ( new_slug == jQuery('#editable-post-name-full').text() ) {
				return jQuery('.cancel', '#edit-slug-buttons').click();
			}
			
			jQuery.ajax(
			{
				url: '?cw_video_update_slug=1&post_id=' + post_id + '&new_slug=' + new_slug + '&new_title=' + escape(jQuery("#title").val())+ '&samplepermalinknonce=' + escape(jQuery('#samplepermalinknonce').val()) + '&original=' + escape(full),
			})
			.done(function(data) {
				if(data) {
					
				}
				jQuery.post(ajaxurl, {
					action: 'sample-permalink',
					post_id: post_id,
					new_slug: new_slug,
					new_title: jQuery('#title').val(),
					samplepermalinknonce: jQuery('#samplepermalinknonce').val()
				}, function(response) {
					jQuery('#edit-slug-box_custom').html(response);
					b.html(revert_b);
					real_slug.val(new_slug);
					makeSlugeditClickable();
					jQuery('#view-post-btn').show();
				});
			});
			
			/*  */
			return false;
		});

		jQuery('.cancel', '#edit-slug-buttons').click(function() {
			jQuery('#view-post-btn').show();
			e.html(revert_e);
			b.html(revert_b);
			real_slug.val(revert_slug);
			return false;
		});

		for ( i = 0; i < full.length; ++i ) {
			if ( '%' == full.charAt(i) )
				c++;
		}

		slug_value = ( c > full.length / 4 ) ? '' : full;
		e.html('<input type="text" id="new-post-slug" value="'+slug_value+'" />').children('input').keypress(function(e){
			var key = e.keyCode || 0;
			// on enter, just save the new slug, don't save the post
			if ( 13 == key ) {
				b.children('.save').click();
				return false;
			}
			if ( 27 == key ) {
				b.children('.cancel').click();
				return false;
			}
			real_slug.val(this.value);
		}).focus();
	}
	
	var makeSlugeditClickable = function() {
		jQuery('#editable-post-name').click(function() {
			jQuery('#edit-slug-buttons').children('.edit-slug').click();
		});
	}
	
	function cwVideoHandleURLChange(evt) {
		if( CWVideoPreview !== null ) {
			if( CWVideoPreview === true ) {
				return;
			}
			clearTimeout(CWVideoPreview);
			CWVideoPreview = null;
		}
		var url = jQuery.trim(jQuery('#CWVideoURLText').val());
		if( url != '' && ( url != CWVideoURL || evt.keyCode == 13 ) ) {
			jQuery('#CWVideoInvalid,#CWVideoPreview').hide();
			jQuery('#CWVideoExamples').show();
			CWVideoPreview = setTimeout(cwVideoUpdatePreview, 500);
		} else if( url == '' ) {
			jQuery('#CWVideoInvalid,#CWVideoPreview').hide();
			jQuery('#CWVideoExamples').show();
		}
	}
	
	function csVideoExtractVimeo() {
		CWVideoPreview = true;
		jQuery('#CWVideoProgress').show();
		jQuery.ajax(
			{
				url: '?cw_video_vimeo=1',
			})
			.done(function(data) {
				jQuery('#CWVideoPreview').show().html(data);
				jQuery('#CWVideoExamples').hide();
				jQuery('.cw_video_open_popup').live('click', cwVideoOpenVideo);
				jQuery('#CWVideoSelectAll').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').attr('checked', 'checked');
				});
				jQuery('#CWVideoSelectNone').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').removeAttr('checked');
				});
			})
			.fail(function() {
				jQuery('#CWVideoInvalid').fadeIn();
			})
			.always(function() {
				CWVideoPreview = null;
				CWVideoURL = jQuery('#CWVideoURLText').val();
				jQuery('#CWVideoProgress').hide();
			});
	}
	
	function csVideoExtractWistia() {
		CWVideoPreview = true;
		jQuery('#CWVideoProgress').show();
		jQuery.ajax(
			{
				url: '?cw_video_wistia=1',
			})
			.done(function(data) {
				jQuery('#CWVideoPreview').show().html(data);
				jQuery('#CWVideoExamples').hide();
				jQuery('.cw_video_open_popup').live('click', cwVideoOpenVideo);
				jQuery('#CWVideoSelectAll').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').attr('checked', 'checked');
				});
				jQuery('#CWVideoSelectNone').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').removeAttr('checked');
				});
			})
			.fail(function() {
				jQuery('#CWVideoInvalid').fadeIn();
			})
			.always(function() {
				CWVideoPreview = null;
				CWVideoURL = jQuery('#CWVideoURLText').val();
				jQuery('#CWVideoProgress').hide();
			});
	}
	
	function cwVideoUpdatePreview() {
		CWVideoPreview = true;
		jQuery('#CWVideoProgress').show();
		jQuery.ajax(
			{
				url: '?cw_video_preview=' + escape(jQuery('#CWVideoURLText').val()),
			})
			.done(function(data) {
				jQuery('#CWVideoPreview').show().html(data);
				jQuery('#CWVideoExamples').hide();
				jQuery('.cw_video_open_popup').live('click', cwVideoOpenVideo);
				jQuery('#CWVideoSelectAll').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').attr('checked', 'checked');
				});
				jQuery('#CWVideoSelectNone').click(function() {
					jQuery('#CWVideoPreview .cw_video_list input[type=checkbox]').removeAttr('checked');
				});
			})
			.fail(function() {
				jQuery('#CWVideoInvalid').fadeIn();
			})
			.always(function() {
				CWVideoPreview = null;
				jQuery('#CWVideoProgress').hide();
			});
	}
	
	function cwVideoAutoUpdate() {
		if( jQuery('#CWVideoAutoUpdate').is(':checked') ) {
			jQuery('#CWVideoTitle,#CWVideoDescription').attr('disabled', 'disabled');
		} else {
			jQuery('#CWVideoTitle,#CWVideoDescription').removeAttr('disabled');
		}
	}
	
	function cwVideoOpenVideoCustom() {
		var video_html = jQuery("#video_play").html();
		jQuery.fancybox(
			{
				'padding':			 0,
				'autoScale':		false,
				'transitionIn':		'elastic',
				'transitionOut':	'elastic',
				'titleShow':		false,
				'overlayOpacity':	0.7,
				'overlayColor':		'#000',
				'scrolling'     : 'no',
				'content': video_html
			}
		);
		return false;
	}
	
	function cwVideoOpenVideo() {
		var tmp = document.createElement('a');
		tmp.href = this.href;
		var domain = tmp.hostname;
		var video_id = jQuery(this).attr("video_id");
		if(domain == "youtube.com") {
		var video_html = '<iframe class="cw_video_single" src="http://www.youtube.com/embed/' + video_id + '?rel=' + video_rel + '&showinfo=' + video_showinfo + '&autoplay=' + video_autoplay + '" frameborder="0" allowfullscreen="allowfullscreen" width="' + video_width + '" height="' + video_height +'"></iframe>';
		} else if(domain == "vimeo.com") {
			var video_html = '<iframe src="http://player.vimeo.com/video/' + video_id + '?title=' + video_showinfo + '&byline=' + video_showinfo + '&autoplay=' + video_autoplay + '&portrait=0&color=333" width="' + video_width + '" height="' + video_height +'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		} else {
			var video_html = '<iframe src="http://fast.wistia.net/embed/iframe/' + video_id + '" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="' + video_width + '" height="' + video_height + '"></iframe>';
		}
		
		
		jQuery.fancybox(
			{
				'padding':			 0,
				'autoScale':		false,
				'transitionIn':		'elastic',
				'transitionOut':	'elastic',
				'titleShow':		false,
				'overlayOpacity':	0.7,
				'overlayColor':		'#000',
				'scrolling'     : 'no',
				'content': video_html
				/* 'width':			640,
				'height':			480,
				'href':				this.href = this.href.replace(new RegExp("watch\\?v=", "i"), 'v/') + '?autoplay=1&rel=0&showinfo=0&modestbranding=1&autohide=1',
				'type':				'swf',
				'swf':				{
										'allowfullscreen':	'true',
										'wmode': 			'transparent'
									} */
			}
		);
		return false;
	}