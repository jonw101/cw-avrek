<?php
/*
Plugin Name: CW Video Plugin
Plugin URI: http://www.consultwebs.com
Description: Provides management of video posts
Version: 2013.8.30
Author: Consultwebs
Author URI: http://www.consultwebs.com
*/
require_once('vimeo.php');
require_once('WistiaApi.class.php');
 	// Make sure class isn't already loaded
	if( !class_exists('CWVideoLibrary') ) {
		
		// Define plugin class
		class CWVideoLibrary {
			private $version = '2013.8.30';
			private $schema = '20120628';
			private $url;
			private $url_parsed;
			private $notices = array();
			private $warnings = array();
			private $vimeo_options = array();
			private $video_options = array();
/* 			public $vimeo_title = 0;
			public $vimeo_author = 0; */
			public $video_rel = 1;
			public $video_autoplay = 0;
			public $video_showinfo = 1;
			public $global_width = 640;
			public $global_height = 480;
			public $global_alignment = "alignright";
            
            // Constructor
			function __construct() {
                
				// Register actions, filters, hooks
                add_action('init',                      			array(&$this, 'init'));
				add_action('admin_menu', 							array(&$this, 'admin_menu'));
				add_action('manage_edit-cw_video_columns', 			array(&$this, 'custom_columns'), 10, 2);
				add_action('manage_cw_video_posts_custom_column',	array(&$this, 'custom_column'), 10, 2);
				add_action('add_meta_boxes', 						array(&$this, 'add_meta_boxes'));
				add_action('save_post', 							array(&$this, 'save_post'));
				add_action('sample-permalink', 							array(&$this, 'update_permalink'));
                add_action('cw_video_hourly',   					array(&$this, 'refresh'));
                add_action('widgets_init', 							array(&$this, 'widgets_init'));
				add_action('admin_notices',							array(&$this, 'admin_notices'));
				add_filter('the_content', 							array(&$this, 'the_content'));
				register_activation_hook(__FILE__,      			array(&$this, 'register_activation_hook'));
                register_deactivation_hook(__FILE__,    			array(&$this, 'register_deactivation_hook'));				
				// Get current URL
				$this->url = ( !empty($_SERVER['HTTPS']) ? 'https://' : 'http://' ) . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				$this->url_parsed = parse_url($this->url);
            }
			
			function custom_get_sample_permalink_html( $id, $new_title = null, $new_slug = null ) {
				global $wpdb;
				$post = get_post($id);

				list($permalink, $post_name) = get_sample_permalink($post->ID, $new_title, $new_slug);

				if ( 'publish' == get_post_status( $post ) ) {
					$ptype = get_post_type_object($post->post_type);
					$view_post = $ptype->labels->view_item;
					$title = __('Click to edit this part of the permalink');
				} else {
					$title = __('Temporary permalink. Click to edit this part.');
				}

				if ( false === strpos($permalink, '%postname%') && false === strpos($permalink, '%pagename%') ) {
					$return = '<strong>' . __('Permalink:') . "</strong>\n" . '<span id="sample-permalink" tabindex="-1">' . $permalink . "</span>\n";
					if ( '' == get_option( 'permalink_structure' ) && current_user_can( 'manage_options' ) && !( 'page' == get_option('show_on_front') && $id == get_option('page_on_front') ) )
						$return .= '<span id="change-permalinks"><a href="options-permalink.php" class="button button-small" target="_blank">' . __('Change Permalinks') . "</a></span>\n";
					if ( isset( $view_post ) )
						$return .= "<span id='view-post-btn'><a href='$permalink' class='button button-small'>$view_post</a></span>\n";

					$return = apply_filters('get_sample_permalink_html', $return, $id, $new_title, $new_slug);

					return $return;
				}

				if ( function_exists('mb_strlen') ) {
					if ( mb_strlen($post_name) > 30 ) {
						$post_name_abridged = mb_substr($post_name, 0, 14). '&hellip;' . mb_substr($post_name, -14);
					} else {
						$post_name_abridged = $post_name;
					}
				} else {
					if ( strlen($post_name) > 30 ) {
						$post_name_abridged = substr($post_name, 0, 14). '&hellip;' . substr($post_name, -14);
					} else {
						$post_name_abridged = $post_name;
					}
				}

				$post_name_html = '<span id="editable-post-name" title="' . $title . '">' . $post_name_abridged . '</span>';
				$display_link = str_replace(array('%pagename%','%postname%'), $post_name_html, $permalink);
				$view_link = str_replace(array('%pagename%','%postname%'), $post_name, $permalink);
				$return =  '<strong>' . __('Permalink:') . "</strong>\n";
				$return .= '<span id="sample-permalink" tabindex="-1">' . $display_link . "</span>\n";
				$return .= '&lrm;'; // Fix bi-directional text display defect in RTL languages.
				$return .= '<span id="edit-slug-buttons"><a href="#post_name" class="edit-slug button button-small hide-if-no-js" onclick="editPermalink(' . $id . '); return false;">' . __('Edit') . "</a></span>\n";
				$return .= '<span id="editable-post-name-full">' . $post_name . "</span>\n";
				if ( isset($view_post) )
					$return .= "<span id='view-post-btn'><a href='$view_link' class='button button-small'>$view_post</a></span>\n";

				$return = apply_filters('get_sample_permalink_html', $return, $id, $new_title, $new_slug);

				return $return;
				
			}
			
			function update_permalink() {
				echo 1;
				die();
			}
			
			// Handles the 'init' action       
            function init() {
				
				// Check version
				$version = get_option('cw_video_version', false);
				if( ( $version !== false && $version != $this->version ) || ( $version === false && (int) get_option('cw_video_pinged', 0) > 0 ) ) {
					
					// Send upgraded version ping to CW
					$ping_opt = array(
						'pingname' => 'cw_video_upgraded',
						'sitename' => get_bloginfo('name'),
						'siteurl' => get_bloginfo('url'),
						'wp_version' => get_bloginfo('version'),
						'plugin_version' => $this->version
					);
					if( $this->cw_ping($ping_opt) ) {
						update_option('cw_video_version', $this->version);
					}
				} elseif( $version === false ) {
				
					// Set version
					add_option('cw_video_version', $this->version);
				}
				
				// Check schema
				$schema = get_option('cw_video_schema', false);
				if( $schema !== false && (int) $schema > 0 && (int) $schema < (int) $this->schema ) {
					
					// Upgrade schema
					$this->upgrade_schema($schema);
				} elseif( $schema === false ) {
				
					// Set schema
					add_option('cw_video_schema', $this->schema);
				}
				
				// Send ping to CW
				if( (int) get_option('cw_video_pinged', 0) < 1 ) {
					$ping_opt = array(
						'pingname' => 'cw_video_installed',
						'sitename' => get_bloginfo('name'),
						'siteurl' => get_bloginfo('url'),
						'wp_version' => get_bloginfo('version'),
						'plugin_version' => $this->version
					);
					if( $this->cw_ping($ping_opt) ) {
						update_option('cw_video_pinged', gmdate('Ymd'));
					}
				}
             
				// Register video post type
                register_post_type('cw_video',
                    array(
                        'labels'				=> array(
							'name' 					=> _x('Videos', 'post type general name'),
							'singular_name' 		=> _x('Video', 'post type singular name'),
							'add_new' 				=> _x('Add New', 'Video'),
							'add_new_item' 			=> __('Add New Videos'),
							'edit_item' 			=> __('Edit Video'),
							'new_item' 				=> __('New Videos'),
							'view_item' 			=> __('View Video'),
							'search_items' 			=> __('Search Videos'),
							'not_found' 			=> __('No Videos found'),
							'not_found_in_trash' 	=> __('No Videos found in Trash'), 
							'parent_item_colon' 	=> ''
						),
                        'public'  				=> true,
                        'show_ui' 				=> true,
						'hierarchical'			=> false,
						'publicly_queryable'	=> true,
						'rewrite'				=> array('slug' => 'videos', 'with_front' => false),
                        'supports' 				=> 'none',
						'taxonomies' => array('post_tag')
                    )
                );
				
				// Register taxonomy for video post type
				register_taxonomy(
					'cw_video_category',  
					'cw_video',  
					array(
						'hierarchical' 	=> true,
						'labels'		=> array(
							'name' 							=> __('Categories'),
							'singular_name' 				=> __('Category'),
							'add_new' 						=> __('Add New'),
							'add_new_item' 					=> __('Add New Category'),
							'edit_item' 					=> __('Edit Category'),
							'new_item' 						=> __('New Category'),
							'view_item' 					=> __('View Category'),
							'search_items' 					=> __('Search Categories'),
							'popular_items'					=> __('Popular Categories'),
							'all_items' 					=> __('All Categories'),
							'not_found' 					=> __('No Categories found'),
							'not_found_in_trash' 			=> __('No Categories found in Trash'), 
							'separate_items_with_commas'	=> __('Separate categories with commas'),
							'add_or_remove_items'			=> __('Add or remove categories'),
							'choose_from_most_used'			=> __('Choose from the most used categories'),
							'parent_item_colon' 			=> ''
						),
						'query_var' 	=> true,
						'rewrite' 		=> array('slug' => 'videos', 'with_front' => false, 'hierarchical' => true),
					)
				);
				register_taxonomy_for_object_type('cw_video_category', 'cw_video');
				
				// Flush rewrite rules
				//flush_rewrite_rules();
				
				// Register shortcodes
				add_shortcode('videogallery', array(&$this, 'shortcode_videogallery'));
				add_shortcode('videosingle', array(&$this, 'shortcode_videosingle'));
				if( get_option('cw_video_shortcode_20120628_flag', false) == true ) {
					add_shortcode('video-gallery', array(&$this, 'shortcode_video_gallery_20120628'));
					add_shortcode('video-single', array(&$this, 'shortcode_video_single_20120628'));
				}
				
				$this->global_width = get_option('cw_video_width');
				$this->global_height = get_option('cw_video_height');
				$this->global_alignment = get_option('cw_video_alignment');
				$this->video_showinfo = get_option('cw_video_showinfo');
				$this->video_autoplay = get_option('cw_video_autoplay');
				$this->video_autoplay_popup = get_option('cw_video_autoplay_popup');
				$this->video_rel = get_option('cw_video_rel');
				
				$js_file = plugins_url( 'global-js.php?video_height='. $this->global_height.'&video_width='. $this->global_width.'&video_alignment='.$this->global_alignment.'&video_showinfo='. $this->video_showinfo .'&video_autoplay='.$this->video_autoplay.'&video_rel='.$this->video_rel, __FILE__ );
				wp_enqueue_script('global_var', $js_file, array('jquery'));
				
				// Site is being viewed
				if( !is_admin() ) {
					
					// Queue scripts
					wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
					//wp_enqueue_script('wistia', '//fast.wistia.net/static/iframe-api-v1.js');
					wp_enqueue_script('fancybox', plugins_url('/fancybox/jquery.fancybox-1.3.4.pack.js', __FILE__), 'jquery');
					wp_enqueue_script('cw_video', plugins_url('/cw-video.js', __FILE__), 'fancybox');
					wp_enqueue_style('fancybox', plugins_url('/fancybox/jquery.fancybox-1.3.4.css', __FILE__));
					wp_enqueue_style('cw_video', plugins_url('/cw-video.css', __FILE__));
					
				// Admin panel being shwon
				} else {
					
					// Queue admin scripts
					wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
					wp_enqueue_script('fancybox', plugins_url('/fancybox/jquery.fancybox-1.3.4.pack.js', __FILE__), 'jquery');
					wp_enqueue_script('cw_video_admin', plugins_url('/cw-video-admin.js', __FILE__), 'jquery');
					wp_enqueue_style('fancybox', plugins_url('/fancybox/jquery.fancybox-1.3.4.css', __FILE__));
					wp_enqueue_style('cw_video_admin', plugins_url('/cw-video-admin.css', __FILE__));
					//wp_enqueue_style('global_var', plugins_url('/global-js.php', __FILE__));
					
					
					
					// Determine if previewing URL
					if( isset($_REQUEST['cw_video_preview']) && $_REQUEST['cw_video_preview'] != '' ) {
						$this->preview_url($_REQUEST['cw_video_preview']);
						exit();
					}
					
					if(isset($_REQUEST['cw_video_refresh_sitemap']) && $_REQUEST['cw_video_refresh_sitemap'] != "") {
						$this->video_sitemap();
						exit();
					}
					
					if( isset($_REQUEST['cw_video_vimeo']) && $_REQUEST['cw_video_vimeo'] != "") {
						$this->extract_vimeo();
						exit();
					}
					
					if( isset($_REQUEST['cw_video_wistia']) && $_REQUEST['cw_video_wistia'] != "") {
						$this->extract_wistia();
						exit();
					}
					
					if( isset($_REQUEST['cw_video_update_slug'])) {
						$this->update_slug($_REQUEST);
						exit();
					}
					
					// Determine if removing an Auto-Pull URL
					if( isset($_REQUEST['cw_video_remove_auto_pull']) && $_REQUEST['cw_video_remove_auto_pull'] != '' ) {
						$this->remove_auto_pull($_REQUEST['cw_video_remove_auto_pull']);
						exit();
					}
					
					// Determine if old shortcodes are being used
					if( get_option('cw_video_shortcode_20120628_flag', false) == true ) {
						$pull = @unserialize(get_option('cw_video_shortcode_20120628_urls', ''));
						$pull = ( is_array($pull) ? $pull : array() );
						foreach( $pull as $url ) {
							$this->warnings[] = "Older 'video-gallery' or 'video-single' shortcode detected and needs to be updated in <strong>$url</strong>";
						}
					}
				}
				
				// Determine if outputing video sitemap
				if( isset($this->url_parsed['path']) && strlen($this->url_parsed['path']) >= strlen('/video-sitemap.xml') && substr($this->url_parsed['path'], -strlen('/video-sitemap.xml')) == '/video-sitemap.xml' ) {
					$this->video_sitemap();
					exit();
				}
				
				// Determine if outputing sitemap index
				if( isset($this->url_parsed['path']) && strlen($this->url_parsed['path']) >= strlen('/sitemap-index.xml') && substr($this->url_parsed['path'], -strlen('/sitemap-index.xml')) == '/sitemap-index.xml' ) {
					$this->sitemap_index();
					exit();
				}
				
				// Determine if manually refreshing videos
				if( isset($this->url_parsed['path']) && strlen($this->url_parsed['path']) >= strlen('/cw-video-manual-refresh') && substr($this->url_parsed['path'], -strlen('/cw-video-manual-refresh')) == '/cw-video-manual-refresh' ) {
					$this->refresh();
					exit();
				}
				
				//Call Vimeo Options to add data to wp_options
				$this->video_options();
				
				
            }
			//Initialize Vimeo Settings Data
			function video_options() {
				$this->vimeo_options = array('consumer_key', 'consumer_secret', 'access_token', 'access_token_secret', 'vimeo_user_id', 'vimeo_channel_id');
				foreach($this->vimeo_options as $option):
					if(get_option($option) === false):
						add_option($option, '', '', 'yes');
					endif;
				endforeach;
				$this->video_options = array('cw_video_width', 'cw_video_height', 'cw_video_alignment','cw_video_rel', 'cw_video_autoplay', 'cw_video_autoplay_popup', 'cw_video_showinfo', 'wistia_secret_key', 'wistia_video_url');
				foreach($this->video_options as $option):
					if(get_option($option) === false):
						add_option($option, '', '', 'yes');
					endif;
				endforeach;
			}

			// Handles the 'admin_notices' hook
			function admin_notices() {
				$this->show_messages();
			}
			
			// Handles the 'widgets_init' hook
			function widgets_init() {
				register_widget('CWVideoWidgetGalleryShortcode');
			}
			
			// Handles the 'activation' hook
			function register_activation_hook() {
                wp_schedule_event(current_time('timestamp'), 'hourly', 'cw_video_hourly');
            }
            
			// Handles the 'deactivation' hook
            function register_deactivation_hook() {
				$ping_opt = array(
					'pingname' => 'cw_video_deactivate',
					'sitename' => get_bloginfo('name'),
					'siteurl' => get_bloginfo('url'),
					'wp_version' => get_bloginfo('version'),
					'plugin_version' => $this->version
				);
				$this->cw_ping($ping_opt);
				delete_option('cw_video_version');
				delete_option('cw_video_schema');
				delete_option('cw_video_pinged');
                wp_clear_scheduled_hook('cw_video_hourly');
            }
            
			// Handles the 'cw_video_hourly' hook
            function refresh() {
                
				// Pull new videos from Auto-Pull URLs
				$this->auto_pull();
				
				// Update all videos needing updating
				$this->update_videos();
            }
			
			// Handles the 'admin_menu' action
			function admin_menu() {
				add_submenu_page('edit.php?post_type=cw_video', 'Settings', 'General Settings', 'edit_posts', 'cw_video_settings', array(&$this, 'settings_page'));
				add_submenu_page('edit.php?post_type=cw_video', '3rd Party Integration', 'Vimeo Settings', 'edit_posts', 'cw_vimeo_settings', array(&$this, 'vimeo_integration'));
				add_submenu_page('edit.php?post_type=cw_video', '3rd Party Integration', 'Wistia Settings', 'edit_posts', 'cw_wistia_settings', array(&$this, 'wistia_integration'));
			}
			
			function wistia_integration() {
				$has_error = false;
				$err_message = '';
				$is_submit = false;
				
				if(is_array($_POST) && isset($_POST) && count($_POST)) {
					if($_POST['submit'] == "Submit"):
						$is_submit = true;
						foreach($_POST as $key => $value):
							update_option($key, $value, '', 'yes');
						endforeach;
					endif;
				}
				if(get_option('wistia_secret_key')):
					$wistia = new WistiaApi(get_option('wistia_secret_key'));
					$accountInfo = $wistia->accountRead();
					
					if(!$accountInfo) {
						$has_error = true;
						$err_message = "<p>Encountered an API error.</p><p>Please make sure you have supplied the correct API details.</p>";
					} else {
						update_option('wistia_video_url', $accountInfo->url, '', 'yes');
					}
				endif;
				?>
				<div class="wrap columns-2 cw_video_settings_page">
					<div id="icon-edit" class="icon32 icon32-posts-cw_video"><br /></div>
					<h2>Wistia Settings</h2>
					<div id="poststuff" class="metabox-holder has-right-sidebar">
						<div class="postbox">
							<?php if($is_submit): ?>
							<div class="updated">Wistia setting has been updated.</div>
							<script type="text/javascript">
								jQuery(".updated").fadeOut(5000);
							</script>
							<?php endif; ?>
							<?php if($has_error): ?>
							<div class="error"><?php echo $err_message; ?></div>
							<?php endif; ?>
							<h3 class="hndle"><span>API Details</span></h3>
							<div class="inside">
								<p>To get your API key please login to your <a href="https://secure.wistia.com/login" target="_blank">Wistia Account</a> and enable access API .</p>
								<form method="post">
									<table>
										<tr>
											<td>Secret API Password</td>
											<td><input class="widefat" id="wistia_secret_key" name="wistia_secret_key" type="text" value="<?php echo get_option('wistia_secret_key'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td></td>
											<td>
												<input type="submit" name="submit" value="Submit" class="button button-primary button-large"/>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			
			function vimeo_integration() {
				$is_submit = false;
				$has_error = false;
				$err_message = '';
				if(is_array($_POST) && isset($_POST) && count($_POST)) {
					
					if($_POST['submit'] == "Submit"):
						$is_submit = true;
						foreach($_POST as $key => $value):
							update_option($key, $value, '', 'yes');
						endforeach;
					endif;
				}
				
				?>
				<div class="wrap columns-2 cw_video_settings_page">
					<div id="icon-edit" class="icon32 icon32-posts-cw_video"><br /></div>
					<h2>Vimeo Settings</h2>
					<div id="poststuff" class="metabox-holder has-right-sidebar">
						<div class="postbox">
							<?php if($is_submit): ?>
							<div class="updated">Vimeo setting has been updated.</div>
							<script type="text/javascript">
								jQuery(".updated").fadeOut(5000);
							</script>
							<?php endif; ?>
							<?php if($has_error): ?>
							<div class="error"><?php echo $err_message; ?></div>
							<?php endif; ?>
							<h3 class="hndle"><span>API Details</span></h3>
							<div class="inside">
								<p>Get your API <a href="https://developer.vimeo.com/apps" target="_blank">here</a>. Create a new app if you don't have one.</p>
								<form method="post">
									<table>
										<tr>
											<td>Client ID (Also known as Consumer Key or API Key)</td>
											<td><input class="widefat" id="consumer_key" name="consumer_key" type="text" value="<?php echo get_option('consumer_key'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td>Client Secret (Also known as Consumer Secret or API Secret)</td>
											<td><input class="widefat" id="consumer_secret" name="consumer_secret" type="text" value="<?php echo get_option('consumer_secret'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td>Access token</td>
											<td><input class="widefat" id="access_token" name="access_token" type="text" value="<?php echo get_option('access_token'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td>Access token secret</td>
											<td><input class="widefat" id="access_token_secret" name="access_token_secret" type="text" value="<?php echo get_option('access_token_secret'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td>User ID</td>
											<td><input class="widefat" id="vimeo_user_id" name="vimeo_user_id" type="text" value="<?php echo get_option('vimeo_user_id'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td>Channel ID (If Empty, API will retrieve All Videos)</td>
											<td><input class="widefat" id="vimeo_channel_id" name="vimeo_channel_id" type="text" value="<?php echo get_option('vimeo_channel_id'); ?>" size="80"/></td>
										</tr>
										<tr>
											<td></td>
											<td>
												<input type="submit" name="submit" value="Submit" class="button button-primary button-large"/>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			
			// Outputs the Settings page
			function settings_page() {
				
				// Get Auto-Pull URLs
				$video_auto_pull_urls = array();
				$auto_pull = @unserialize(get_option('cw_video_auto_pull', ''));
				$video_auto_pull_urls = ( is_array($auto_pull) ? $auto_pull : array() );
					
				// Output Settings content
				?>
				<div class="wrap columns-2 cw_video_settings_page">
					<div id="icon-edit" class="icon32 icon32-posts-cw_video"><br /></div>
					<h2>Video Settings</h2>
					<div id="poststuff" class="metabox-holder has-right-sidebar">
						<div class="postbox">
							<h3 class="hndle"><span>Automatically Add Videos</span></h3>
							<div class="inside">
								<p>
									The following URLs will be checked periodically for new videos:
								</p>
								<?php
								
								// Loop for each Auto-Pull URL
								foreach( $video_auto_pull_urls as $video_auto_pull_url ) {
									?>
									<div class="cw_video_add_feed">
										<span><a href="<?php esc_attr_e($video_auto_pull_url['source_url']); ?>" target="_blank"><?php esc_html_e($video_auto_pull_url['source_url']); ?></a><?php esc_html_e(( count($video_auto_pull_url['video_categories']) > 0 ? ' --> Categories: ' . implode(', ', $video_auto_pull_url['video_categories']) : '' )); ?></span><a href="?cw_video_remove_auto_pull=<?php esc_attr_e(urlencode($video_auto_pull_url['source_url'])); ?>" class="button cw_video_remove_auto_pull">Remove</a>
									</div>
									<?php
								}
								
								// No Auto-Pull URLs found
								if( count($video_auto_pull_urls) < 1 ) {
									?>
									<p class="cw_video_settings_no_auto_pull">
										<span>No video URLs found.</span> <a href="post-new.php?post_type=cw_video">Add New</a>
									</p>
									<?php
								}
								?>
							</div>
						</div>
						<div class="postbox">
							<h3 class="hndle"><span>Sitemap</span></h3>
							<div class="inside">
								<table>
									<tr>
										<td>Sitemap Link:</td>
										<td width="300px"><a href="<?php echo get_site_url(); ?>/video-sitemap.xml" target="_blank"><?php echo get_site_url(); ?>/video-sitemap.xml</a></td>
										<td><a href="javascript:void(0);" class="button refresh_sitemap">Refresh</a>
										
										</td>
										<td id="loader_cw">
										<span id="show_status"></span>
										<span id="show_loader" style="display:none;"><img src="<?php esc_attr_e(plugins_url('/images/ajax-loader.gif', __FILE__)); ?>" alt="..." /></span>
										</div>
									</tr>
								</table>
							</div>
						</div>
						<?php 
						if(is_array($_POST) && isset($_POST) && count($_POST)) {
					
							if($_POST['update'] == "update"):
								$is_submit = true;
								unset($_POST['update']);
								foreach($_POST as $key => $value):
									if($key != "cw_video_alignment")
										$value = (int) $value;
									update_option($key, $value, '', 'yes');
								endforeach;
							endif;
							
						}
						?>
						<div class="postbox">
							<h3 class="hndle"><span>General Settings</span></h3>
							<div class="inside">
								
								<form method="post">
									<p>Global settings for the following attributes</p>
									<table>
										<tr>
											<th>Attribute</th>
											<th>Value</th>
											<th>Options</th>
										</tr>
										<tr>
											<td>Width:</td>
											<td><input type="text" name="cw_video_width" value="<?php echo get_option('cw_video_width'); ?>" style="width:50px;"/></td>
											<td>Default value is set to 640px. Write only numeric values (i.e 640).</td>
										</tr>
										<tr>
											<td>Height:</td>
											<td><input type="text" name="cw_video_height" value="<?php echo get_option('cw_video_height'); ?>" style="width:50px;"/></td>
											<td>Default value is set to 480px. Write only numeric values (i.e 480).</td>
										</tr>
										<tr>
											<td>Alignment:</td>
											<td>
												<select name="cw_video_alignment">
													<option value="">--Select--</option>
													<option value="alignright" <?php echo (get_option('cw_video_alignment') == "alignright" ? 'selected="selected"': ""); ?>>Right</option>
													<option value="alignleft" <?php echo (get_option('cw_video_alignment') == "alignleft" ? 'selected="selected"': ""); ?>>Left</option>
													<option value="aligncenter" <?php echo (get_option('cw_video_alignment') == "aligncenter" ? 'selected="selected"': ""); ?>>Center</option>
												</select>
											</td>
											<td>Default value is set to Right.</td>
										</tr>
										<tr>
											<td>Show Info:</td>
											<td>
												<select name="cw_video_showinfo">
													<option value="1" <?php echo (get_option('cw_video_showinfo') == 1 ? 'selected="selected"': ""); ?>>True</option>
													<option value="0" <?php echo (get_option('cw_video_showinfo') == 0 ? 'selected="selected"': ""); ?>>False</option>
												</select>
											</td>
											<td>Values: False or True. The parameter's default value is True. If you set the parameter value to False, then the player will not display information like the video title and uploader before the video starts playing.</td>
										</tr>
										<tr>
											<td>Autoplay [Embed]:</td>
											<td>
												<select name="cw_video_autoplay">
													<option value="0" <?php echo (get_option('cw_video_autoplay') == 0 ? 'selected="selected"': ""); ?>>False</option>
													<option value="1" <?php echo (get_option('cw_video_autoplay') == 1 ? 'selected="selected"': ""); ?>>True</option>
												</select>
											</td>
											<td>Values: False or True. Default is False. Sets whether or not the initial video will autoplay when the player loads.</td>
										</tr>
										<tr>
											<td>Autoplay [Popup]:</td>
											<td>
												<select name="cw_video_autoplay_popup">
													<option value="0" <?php echo (get_option('cw_video_autoplay_popup') == 0 ? 'selected="selected"': ""); ?>>False</option>
													<option value="1" <?php echo (get_option('cw_video_autoplay_popup') == 1 ? 'selected="selected"': ""); ?>>True</option>
												</select>
											</td>
											<td>Values: False or True. Default is False. Sets whether or not the initial video will autoplay when the player loads.</td>
										</tr>
										<tr>
											<td>Show related Videos</td>
											<td>
												<select name="cw_video_rel">
													<option value="0" <?php echo (get_option('cw_video_rel') == 0 ? 'selected="selected"': ""); ?>>False</option>
													<option value="1" <?php echo (get_option('cw_video_rel') == 1 ? 'selected="selected"': ""); ?>>True</option>
												</select>
											</td>
											<td>Values: False or True. Default is True. This parameter indicates whether the player should show related videos when playback of the initial video ends.</td>
										</tr>
										<tr>
											<td></td>
											<td><input type="submit" name="update" value="update" class="button button-primary button-large" style="padding:0 !important;width:80px;"/></td>
											<td></td>
										</tr>
									</table>
								</form>
								
								
							</div>
						</div>
						<div class="postbox">
							<h3 class="hndle"><span>Gallery Shortcode</span></h3>
							<div class="inside">
								<div class="cw_video_shortcode">
									<code>[videogallery /]</code>
								</div>
								<table>
									<tr>
										<th>Parameter</th>
										<th>Description</th>
										<th>Options</th>
										<th>Example</th>
									</tr>
									<tr>
										<td class="cw_video_parameter">type</td>
										<td>Type of gallery to display</td>
										<td>matrix, list</td>
										<td><code>[videogallery type="matrix" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">category</td>
										<td>Category of videos to display</td>
										<td>(category title or slug)</td>
										<td><code>[videogallery category="mesothelioma" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">format</td>
										<td>HTML format of each video item</td>
										<td>li, div</td>
										<td><code>[videogallery format="li" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">size</td>
										<td>Size of video thumbnails</td>
										<td>small, medium, large</td>
										<td><code>[videogallery size="medium" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">shade</td>
										<td>Shade and opacity of the background for each video</td>
										<td>(white-##, black-##)</td>
										<td><code>[videogallery shade="white-60" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">cols</td>
										<td>Number of columns to place videos before wrapping to a new line</td>
										<td>(number)</td>
										<td><code>[videogallery cols="3" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">limit</td>
										<td>Limits the number of videos to display</td>
										<td>(number)</td>
										<td><code>[videogallery limit="6" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">sort</td>
										<td>Sort order</td>
										<td>newest, oldest, random</td>
										<td><code>[videogallery sort="newest" /]</code></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="postbox">
							<h3 class="hndle"><span>Single Shortcode</span></h3>
							<div class="inside">
								<div class="cw_video_shortcode">
									<code>[videosingle /]</code>
								</div>
								<table>
									<tr>
										<th>Parameter</th>
										<th>Description</th>
										<th>Options</th>
										<th>Example</th>
									</tr>
									<tr>
										<td class="cw_video_parameter">id</td>
										<td>ID of the video to display</td>
										<td></td>
										<td><code>[videosingle id="" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">category</td>
										<td>Category from which to pull video</td>
										<td>(category title or slug)</td>
										<td><code>[videosingle category="mesothelioma" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">width</td>
										<td>CSS width of video frame</td>
										<td>(width in pixels), auto</td>
										<td><code>[videosingle width="400" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">height</td>
										<td>CSS height of video frame</td>
										<td>(height in pixels), auto</td>
										<td><code>[videosingle height="300" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">sort</td>
										<td>Sort order</td>
										<td>newest, oldest, random</td>
										<td><code>[videosingle sort="newest" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">popup</td>
										<td>Show video as Popup</td>
										<td>true, false. Default value: refer to General Settings.</td>
										<td><code>[videosingle popup="true" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">autoplay</td>
										<td>Autoplay video</td>
										<td>true, false. Default value: refer to General Settings.</td>
										<td><code>[videosingle autoplay="true" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">showinfo</td>
										<td>Hide or Show Video Info</td>
										<td>true or false. Default value: refer to General Settings.</td>
										<td><code>[videogallery showinfo="false" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">rel</td>
										<td>Show related videos. Only works on youtube.</td>
										<td>true or  false.Default value: refer to General Settings.</td>
										<td><code>[videogallery rel="true" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">align</td>
										<td>Video Alignment</td>
										<td>right, left or center. Default value: refer to General Settings.</td>
										<td><code>[videogallery align="right" /]</code></td>
									</tr>
									<tr>
										<td class="cw_video_parameter">format</td>
										<td>Show Video Description</td>
										<td>none. If not set then it will show the video description.</td>
										<td><code>[videogallery format="none" /]</code></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			
			// Handles the 'manage_edit-cw_video_columns' action
			function custom_columns($columns) {
				unset($columns['author']);
				
				$columns = array('thumbnail' => '') + $columns + array('cw_video_category' => __('Categories')) + array('shortcode' => __('Shortcode'));
				return $columns;
			}
			
			// Handles the 'manage_cw_video_posts_custom_column' action
			function custom_column($column, $post_id) {
				global $wpdb;
				
				switch( $column ) {
					case 'thumbnail':
						$thumbnail = get_post_meta($post_id, 'cw_video_thumbnail', true);
						echo '<a href="' . get_edit_post_link($post_id, 'display') . '"><img class="cw_video_column" src="' . esc_attr($thumbnail) . '" /></a>';
						break;
					case 'cw_video_category':
						echo get_the_term_list($post_id, 'cw_video_category', '', ', ', '');
						break;
					case 'shortcode':
						echo '<code style="font-weight:bold;">[videosingle id="' . $post_id . '" /]</code>';
						break;
				}
			}
			
			// Handles the 'the_content' filter
			function the_content($content) {
				global $post;
				
				// Single video post
				if( is_single() && $post->post_type == 'cw_video' ) {
					
					// Get video ID
					$video_id = get_post_meta($post->ID, 'cw_video_id', true);
					
					// Get video description
					$description = get_post_meta($post->ID, 'cw_video_description', true);
					
					// Get Video Transcript
					$transcript = get_post_meta($post->ID, 'cw_video_transcript', true);
					
					//Get Video Thumbnail
					$thumbnail = get_post_meta($post->ID, 'cw_video_thumbnail', true);
					
					// Get Video Title
					$title = $post->post_title;
					
					//Height / Width
					$width = $this->global_width;
					$height = $this->global_height;
					
					// Capture content
					ob_start();
					$video_type = get_post_meta($post->ID, 'cw_video_type', true);	?>
					
					<div itemtype="http://schema.org/VideoObject" itemscope="" >
						<meta itemprop="name" content="<?php esc_attr_e($title); ?>" />
						<meta itemprop="thumbnailURL" content="<?php esc_attr_e($thumbnail); ?>" />
						
						<?php
						if($video_type == "youtube") {
						?>
							<meta itemprop="embedURL"     content="http://www.youtube.com/watch?v=<?php esc_attr_e($video_id); ?>" />									
							<iframe class="cw_video_single" src="http://www.youtube.com/embed/<?php esc_attr_e($video_id); ?>?rel=0" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						<?php } else if($video_type == "vimeo") { ?>
							<meta itemprop="embedURL"     content="http://player.vimeo.com/video/<?php esc_attr_e($video_id); ?>" />					
							<iframe src="http://player.vimeo.com/video/<?php esc_attr_e($video_id); ?>?portrait=0&color=333" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
						<?php } else { ?>
							<meta itemprop="embedURL"     content="http://fast.wistia.net/embed/iframe/<?php esc_attr_e($video_id); ?>" />						
							<iframe src="http://fast.wistia.net/embed/iframe/<?php esc_attr_e($video_id); ?>" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>"></iframe>
						<?php } ?>
						<div itemprop="description" class="cw_video_description">
							<?php echo wpautop($description); ?>
						</div>
						
						<?php if ($transcript != ""){ ?>
						<h2>Transcript</h2>
						<div itemprop="text" class="cw_video_transcript">
							<?php echo wpautop($transcript); ?>
						</div>
						<?php } ?>
					</div>
					<?php
					$content = ob_get_clean();
				}
				
				// Return content
				return $content;
			}
			
			// Handles the 'add_meta_boxes' action
			function add_meta_boxes() {
				add_meta_box(
					'cw_video_edit_meta_box',
					__('Videos'),
					array(&$this, 'edit_meta_box'),
					'cw_video',
					'normal',
					'high'
				);
			}
			
			// Outputs a New or Edit meta box
			function edit_meta_box($post) {
				wp_nonce_field(plugin_basename( __FILE__ ), 'cw_video_nounce');
				
				// New post
				if( $post->post_name == '' ) {
					
					?>
					<div id="CWVideoPanel">
						<input type="hidden" name="cw_video_new" value="1" />
						<div id="video-selector">
							<table>
								<tr>
									<td><label>Select Video Type</label></td>
									<td>
										<select name="video_type" id="video_type">
										<option value="0">Select</option>
										<option value="youtube">Youtube</option>
										<option value="vimeo">Vimeo</option>
										<option value="wistia">Wistia</option>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div id="CWVideoURL">
							<div class="videotype type-youtube">
								<table>
									<tr>
										<td>
											<label for="CWVideoURLText">Add New Video(s) by URL:</label>
										</td>
										<td class="cw_video_url_text_wrapper">
											<input id="CWVideoURLText" type="text" name="video_url" />
										</td>
									</tr>
								</table>
								<div id="CWVideoExamples">
									Examples:
									<ul>
										<li>http://www.youtube.com/<strong>playlist</strong>?list=PL97BC74A8254CD5E0</li>
										<li>http://www.youtube.com/<strong>watch</strong>?v=NPTX7QXyHeM</li>
										<li>http://www.youtube.com/<strong>user</strong>/GinarteLaw</li>
										<li>http://<strong>youtu.be</strong>/lpfqtHj7H0E</li>
									</ul>
								</div>
							</div>
							<div class="videotype type-vimeo">
								<p>This will extract all videos from Vimeo. Please make sure that the <a href="/wp-admin/edit.php?post_type=cw_video&page=cw_vimeo_settings" target="_blank">API details</a> are correct.</p>
								<input type="hidden" name="page_num" id="page_num" value="1" />
								<input type="button" name="submit" id="extract-vimeo" value="Extract Now" class="button button-primary button-large" style="width:100px !important;padding:0 !important;"/>
							</div>
							<div class="videotype type-wistia">
								<p>This will extract all videos from Wistia. Please make sure that the <a href="/wp-admin/edit.php?post_type=cw_video&page=cw_wistia_settings" target="_blank">API details</a> are correct.</p>
								<input type="hidden" name="page_num" id="page_num" value="1" />
								<input type="button" name="submit" id="extract-wistia" value="Extract Now" class="button button-primary button-large" style="width:100px !important;padding:0 !important;"/>
							</div>
						</div>
						<div id="CWVideoProgress">
							<img src="<?php esc_attr_e(plugins_url('/images/ajax-loader.gif', __FILE__)); ?>" alt="..." />
						</div>
						<div id="CWVideoInvalid">
							No videos found.  Valid inputs are individual videos, playlists, and user YouTube URLs.
						</div>
						<div id="CWVideoPreview"></div>
					</div>
					<?php
				
				// Editing post
				} else {
				
					$video_id = get_post_meta($post->ID, 'cw_video_id', true);
					$title = $post->post_title;
					$slug = $post->guid;
					$description = get_post_meta($post->ID, 'cw_video_description', true);
					$transcript = get_post_meta($post->ID, 'cw_video_transcript', true);
					$auto_update = get_post_meta($post->ID, 'cw_video_auto_update', true);
					$video_type = get_post_meta($post->ID, 'cw_video_type', true);
					?>
					<div id="CWVideoPanel">
						<div>
							<label for="CWVideoTitle">Title:</label>
							<input type="text" name="cw_video_title_new" id="CWVideoTitle" value="<?php esc_attr_e($title); ?>" />
						</div>

						<input type="hidden" id="title" name="post_title" value="<?php esc_attr_e($title); ?>" />
						<?php $sample_permalink_html = $this->custom_get_sample_permalink_html($post->ID);?>
						<div id="edit-slug-box_custom" class="hide-if-no-js">
						<?php
							if ( $sample_permalink_html && 'auto-draft' != $post->post_status )
								echo $sample_permalink_html;
						?>
						</div>
						<?php
						wp_nonce_field( 'samplepermalink', 'samplepermalinknonce', false );
						?>
						<?php if($video_type == "youtube") { ?>
						<iframe width="640" height="480" src="http://www.youtube.com/embed/<?php esc_attr_e($video_id); ?>?rel=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						<div class="cw_video_preview_url">
							<label for="CWVideoPreviewURL">URL:</label>
							<a href="http://www.youtube.com/watch?v=<?php esc_attr_e($video_id); ?>" target="_blank"><span id="CWVideoPreviewURL">http://www.youtube.com/watch?v=<?php esc_attr_e($video_id); ?></span></a>
						</div>
						<?php } else if ($video_type == "vimeo") { ?>
						<iframe src="http://player.vimeo.com/video/<?php esc_attr_e($video_id); ?>?portrait=0&color=333&title=0&byline=0" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
						<div class="cw_video_preview_url">
							<label for="CWVideoPreviewURL">URL:</label>
							<a href="http://vimeo.com/<?php esc_attr_e($video_id); ?>" target="_blank"><span id="CWVideoPreviewURL">http://vimeo.com/<?php esc_attr_e($video_id); ?></span></a>
						</div>
						<?php } else { ?>
						<iframe src="http://fast.wistia.net/embed/iframe/<?php esc_attr_e($video_id); ?>" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="480" height="324"></iframe>
						<div class="cw_video_preview_url">
							<label for="CWVideoPreviewURL">URL:</label>
							<a href="<?php echo get_option('wistia_video_url'); ?>/medias/<?php esc_attr_e($video_id); ?>" target="_blank"><span id="CWVideoPreviewURL"><?php echo get_option('wistia_video_url'); ?>/medias/<?php esc_attr_e($video_id); ?></span></a>
						</div>
						<?php } ?>
						<div>
							<input type="checkbox" id="CWVideoAutoUpdate" name="cw_video_auto_update_new" value="1"<?php if( $auto_update > 0 ) { ?> checked="checked"<?php } ?> /><label for="CWVideoAutoUpdate">Automatically keep video title and description up-to-date using feed</label>
						</div>
						<div>
							<label for="CWVideoDescription" style="font-size: 14px; margin: 4px;">Description:</label>
							<textarea name="cw_video_description_new" id="CWVideoDescription" style="height: 100px; width: 100%; display: block;"><?php esc_html_e($description); ?></textarea>
						</div>
						<div>
							<label for="CWVideoTranscript" style="font-size: 14px; margin: 4px;">Transcript:</label>
							<textarea name="cw_video_transcript_new" id="CWVideoTranscript" style="height: 70px; width: 100%; display: block;"><?php esc_html_e($transcript); ?></textarea>
						</div>
					</div>
					<?php
				}
			}
			
			// Handles the 'save_post' action
			function save_post($post_id) {
			  
				// Verify submission
				if( !current_user_can('edit_post', $post_id) || !isset($_REQUEST['cw_video_nounce']) || !wp_verify_nonce($_REQUEST['cw_video_nounce'], plugin_basename( __FILE__ )) ) {
					return;
				}
				
				// Skip autosave
				if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
					return;
				}
				
				// Unhook this action to prevent infinite loop
				remove_action('save_post', array(&$this, 'save_post'));
				
				// New post
				if( isset($_REQUEST['cw_video_new']) ) {

					// Get categories
					$video_categories = wp_get_post_terms($post_id, 'cw_video_category');
					$video_categories_list = array();
					$video_category_ids = array();
					foreach( $video_categories as $video_category ) {
						$video_categories_list[] = $video_category->name;
						$video_category_ids[] = $video_category->term_id;
					}

					// Loop through videos to add
					foreach( $this->get_request('cw_videos', array()) as $video ) {
						
						// Add video
						$video_post = array(
							'post_content' => '',
							'post_date_gmt' => gmdate('Y-m-d H:i:s'),
							'post_excerpt' => '',
							/*'post_name' => $video,*/
							'post_parent' => '',
							'post_status' => get_post_status($post_id),
							'post_title' => $this->get_request('cw_video_' . $video . '_title', $video),
							'post_type' => 'cw_video',
							'tax_input' => array('cw_video_category' => $video_category_ids)
						);
						$video_post_id = wp_insert_post($video_post);
						
						// Add meta
						if( $video_post_id <> 0 ) {
							add_post_meta($video_post_id, 'cw_video_id', $video);
							add_post_meta($video_post_id, 'cw_video_type', urldecode($this->get_request('cw_video_' . $video . '_type', '')));
							add_post_meta($video_post_id, 'cw_video_description', urldecode($this->get_request('cw_video_' . $video . '_description', '')));
							add_post_meta($video_post_id, 'cw_video_transcript', '', '');
							add_post_meta($video_post_id, 'cw_video_thumbnail', $this->get_request('cw_video_' . $video . '_thumbnail', ''));
							add_post_meta($video_post_id, 'cw_video_duration', $this->get_request('cw_video_' . $video . '_duration', ''));
							add_post_meta($video_post_id, 'cw_video_auto_update', $this->get_request('cw_video_auto_update', '0'));
						}
					}
					
					// Add Auto-Pull
					if( $this->get_request('cw_video_auto_pull', '0') > 0 ) {
						$video_source_url = $this->get_request('cw_video_source_url', '');
						$auto_pull = @unserialize(get_option('cw_video_auto_pull', ''));
						$auto_pull = ( !is_array($auto_pull) ? array() : $auto_pull );
						foreach( $auto_pull as $auto_pull_source ) {
							if(
								isset($auto_pull_source['source_url'])
								&& $auto_pull_source['source_url'] == $video_source_url
								&& isset($auto_pull_source['video_categories'])
								&& count(array_diff($auto_pull_source['video_categories'], $video_categories_list)) < 1
							) {
								$video_source_url = '';
								break;
							}
						}
						if( $video_source_url != '' ) {
							$auto_pull[] = array(
								'source_url' => $video_source_url,
								'video_categories' => $video_categories_list
							);
							update_option('cw_video_auto_pull', serialize($auto_pull));
						}
					}

					// Delete current post
					wp_delete_post($post_id, true);
					
					// Redirect to video list
					header('Location: edit.php?post_type=cw_video');
					exit;
						
				// Existing post
				} else {
					
					// Update Auto-Update
					delete_post_meta($post_id, 'cw_video_auto_update');
					add_post_meta($post_id, 'cw_video_auto_update', $this->get_request('cw_video_auto_update_new', '0'));
					
					// Manually set title and description
					if( $this->get_request('cw_video_auto_update_new', '0') == 0 ) {
					
						// Update Title
						wp_update_post(array(
							'ID' => $post_id,
							'post_title' => $this->get_request('cw_video_title_new', get_the_title($post_id)),
						));
						
						// Update Description
						delete_post_meta($post_id, 'cw_video_description');
						add_post_meta($post_id, 'cw_video_description', $this->get_request('cw_video_description_new', ''));
					
					// Automatically set title and description
					} else {
						
						// Update from feed
						$this->update_video($post_id);
					}
					
					// Update Transcript
					delete_post_meta($post_id, 'cw_video_transcript');
					add_post_meta($post_id, 'cw_video_transcript', $this->get_request('cw_video_transcript_new', ''));
				}
				
				// Re-hook this action
				add_action('save_post', array(&$this, 'save_post'));
			}
			
			// Handles the 'videogallery' shortcode
			function shortcode_videogallery($atts) {
				
				// Extract parameters
				extract(shortcode_atts(array(
					'type' 							=> 'matrix',
					'category' 						=> '',
					'format' 						=> 'div',
					'size' 							=> 'small',
					'cols' 							=> '0',
					'shade' 						=> '',
					'limit' 						=> '0',
					'sort' 							=> '',
					'cw_attr_shortcode_20120628'	=> false
				), $atts));
				
				// Remove current URL from list of URLs needing shortcode update
				if( get_option('cw_video_shortcode_20120628_flag', false) == true && $cw_attr_shortcode_20120628 == false ) {
					$pull = @unserialize(get_option('cw_video_shortcode_20120628_urls', ''));
					$pull = ( is_array($pull) ? $pull : array() );
					$url_key = array_search($this->url, $pull);
					if( $url_key !== false ) {
						unset($pull[$url_key]);
						update_option('cw_video_shortcode_20120628_urls', serialize($pull));
					}
				}
				
				// Get video size class
				$size_class = 'cw_video_size_' . $size;
				
				// Get columns class
				$cols_class = ( $cols > 0 ? 'cw_video_cols' : '' );
				
				// Get type class
				$type_class = 'cw_video_type_' . $type;
				
				// Get shade style
				$shade_style = '';
				if( $shade != '' && strpos($shade, '-') !== false ) {
					$shade_parts = explode('-', $shade);
					$shade_style = ' style="background-image: url(\'' . esc_attr(plugins_url('/shader.php?shade=' . urlencode($shade_parts[0]) . '&amp;percent=' . urlencode($shade_parts[1]), __FILE__)) . '\');"';
				}
				
				// Get limit
				$limit = (int) $limit;
				
				// Get sort order
				$sort_order = array();
				if( $sort == 'newest' || $sort == 'oldest' ) {
					$sort_order = array(
						'orderby' => 'post_date',
						'order' => ( $sort == 'newest' ? 'DESC' : 'ASC' )
					);
				}
				
				// Get matching videos
				$videos = get_posts(array(
					'post_type' => 'cw_video',
					'numberposts' => -1,
					'post_status' => 'publish'
				) + $sort_order);
				
				// Filter by category
				if( $category != '' ) {
					$videos_filtered = array();
					$categories_list = explode(',', strtolower($category));
					foreach( $videos as $video ) {
						$video_categories = wp_get_post_terms($video->ID, 'cw_video_category');
						foreach( $video_categories as $video_category ) {
							foreach( $categories_list as $category ) {
								if( trim($category) == strtolower($video_category->name) || trim($category) == strtolower($video_category->slug) ) {
									$videos_filtered[] = $video;
								}
							}
						}
					}
					$videos = $videos_filtered;
				}
				
				// Randomize videos
				if( $sort == 'random' ) {
					shuffle($videos);
				}
				
				// Limit videos
				if( $limit > 0 ) {
					$videos = array_slice($videos, 0, $limit);
				}

				// Capture output of matching videos
				ob_start();
				if( count($videos) > 0 ) {
					$video_index = 0;
					$cols = ( $cols > 0 ? $cols : count($videos) );
					
					// Output gallery wrapper opening tag
					?>
					<div class="cw_video_gallery <?php esc_attr_e($type_class); ?>">
					<?php
					
					// Loop through videos
					while( $video_index < count($videos) ) {
						
						// Output row wrapper opening tag
						if( strtolower($format) == 'li' ) {
							
							// 'List' format
							?>
							<ul class="<?php esc_attr_e($cols_class); ?>">
							<?php
						} else {
							
							// 'Div' format
							?>
							<div class="<?php esc_attr_e($cols_class); ?>">
							<?php
						}
						
						// Loop for each column
						for( $i = $video_index; $i < ($video_index + $cols); $i++ ) {
							if( !isset($videos[$i]) ) {
								break;
							}
							
							// Get video information
							$video_id = get_post_meta($videos[$i]->ID, 'cw_video_id', true);
							$title = $videos[$i]->post_title;
							$description = get_post_meta($videos[$i]->ID, 'cw_video_description', true);
							$thumbnail = get_post_meta($videos[$i]->ID, 'cw_video_thumbnail', true);
							$video_type = get_post_meta($videos[$i]->ID, 'cw_video_type', true);
							// Output video item
							if( strtolower($format) == 'li' ) {
							
								// 'List' format
								?>
								<li class="cw_video_item <?php esc_attr_e($size_class); ?>"<?php echo $shade_style; ?>><a href="javascript:void(0);" video_width="640" video_height="480" video_type="<?php esc_attr_e($video_type); ?>" video_id="<?php echo $video_id; ?>" video_autoplay="<?php esc_attr_e($this->video_autoplay_popup); ?>" video_rel="<?php esc_attr_e($this->video_rel); ?>" video_showinfo="<?php esc_attr_e($this->video_showinfo); ?>" class="cw_video_open_popup" title="<?php esc_attr_e($title); ?>"> <img src="<?php esc_attr_e($thumbnail); ?>" alt="<?php esc_attr_e($title); ?>" /> <div class="cw_video_item_title"> <?php esc_html_e($title); ?> </div></a></li>
								<?php
							} else {
								
								// 'Div' format
								?>
								<div class="cw_video_item <?php esc_attr_e($size_class); ?>"<?php echo $shade_style; ?>><a href="javascript:void(0);" video_width="640" video_height="480" video_type="<?php esc_attr_e($video_type); ?>" video_id="<?php echo $video_id; ?>"  video_autoplay="<?php esc_attr_e($this->video_autoplay_popup); ?>" video_rel="<?php esc_attr_e($this->video_rel); ?>" video_showinfo="<?php esc_attr_e($this->video_showinfo); ?>" class="cw_video_open_popup" title="<?php esc_attr_e($title); ?>"> <img src="<?php esc_attr_e($thumbnail); ?>" alt="<?php esc_attr_e($title); ?>" /> <div class="cw_video_item_title"> <?php esc_html_e($title); ?> </div></a></div>
								<?php
							}
						}
						
						// Output row wrapper closing tag
						if( strtolower($format) == 'li' ) {
							
							// 'List' format
							?>
							</ul>
							<?php
						} else {
							
							// 'Div' format
							?>
							</div>
							<?php
						}
						
						$video_index += $cols;
					}
					
					// Output gallery wrapper closing tag
					?>
						<div class="cw_video_gallery_clear"></div>
					</div>
					<?php
				}
				
				// Return captured output
				return ob_get_clean();
			}
			
			// Handles the older 'video-gallery' shortcode
			function shortcode_video_gallery_20120628($atts) {
				
				// Determine if URL is not being previewed
				if( !isset($_REQUEST['preview']) ) {
				
					// Add current URL to list of URLs needing updating
					$current_url = $this->url;
					$pull = @unserialize(get_option('cw_video_shortcode_20120628_urls', ''));
					$pull = ( is_array($pull) ? $pull : array() );
					if( !in_array($current_url, $pull) ) {
						$pull[] = $current_url;
						update_option('cw_video_shortcode_20120628_urls', serialize($pull));
					}
					
					// Add special flag parameter
					$atts['cw_attr_shortcode_20120628'] = true;
				}
				
				// Process and return shortcode
				return $this->shortcode_videogallery($atts);
			}
			
			// Handles the 'videosingle' shortcode
			function shortcode_videosingle($atts) {
				/*
				$this->global_width = get_option('cw_video_width');
				$this->global_height = get_option('cw_video_height');
				$this->global_alignment = get_option('cw_video_alignment');
				$this->video_showinfo = get_option('cw_video_showinfo');
				$this->video_autoplay = get_option('cw_video_autoplay');
				$this->video_rel = get_option('cw_video_rel');
				*/
				// Extract parameters
				extract(shortcode_atts(array(
					'id'								=> '',
					'format'						=> '',
					'category' 					=> '',
					'width' 						=> $this->global_width,
					'height'						=> $this->global_height,
					'sort' 							=> '',
					'cw_attr_shortcode_20120628'	=> false,
					'popup'						=> false,
					'autoplay'					=> false,
					'showinfo'					=> false,
					'rel'								=> false,
					'align'							=> $this->global_alignment
				), $atts));
				
				// Remove current URL from list of URLs needing shortcode update
				if( get_option('cw_video_shortcode_20120628_flag', false) == true && $cw_attr_shortcode_20120628 == false ) {
					$pull = @unserialize(get_option('cw_video_shortcode_20120628_urls', ''));
					$pull = ( is_array($pull) ? $pull : array() );
					$url_key = array_search($this->url, $pull);
					if( $url_key !== false ) {
						unset($pull[$url_key]);
						update_option('cw_video_shortcode_20120628_urls', serialize($pull));
					}
				}
				
				// Get sort order
				$sort_order = array();
				if( $sort == 'newest' || $sort == 'oldest' ) {
					$sort_order = array(
						'orderby' => 'post_date',
						'order' => ( $sort == 'newest' ? 'DESC' : 'ASC' )
					);
				}
				
				// Check if ID wasn't provided
				if( $id == '' ) {
					
					// Get matching videos
					$videos = get_posts(array(
						'post_type' => 'cw_video',
						'numberposts' => -1,
						'post_status' => 'publish'
					) + $sort_order);

					// Filter by category
					if( $category != '' ) {
						$videos_filtered = array();
						$categories_list = explode(',', strtolower($category));
						foreach( $videos as $video ) {
							$video_categories = wp_get_post_terms($video->ID, 'cw_video_category');
							foreach( $video_categories as $video_category ) {
								foreach( $categories_list as $category ) {
									if( trim($category) == strtolower($video_category->name) || trim($category) == strtolower($video_category->slug) ) {
										$videos_filtered[] = $video;
									}
								}
							}
						}
						$videos = $videos_filtered;
					}
					
					// Randomize videos
					if( $sort == 'random' ) {
						shuffle($videos);
					}
					
					// Limit to first video
					$videos = array_slice($videos, 0, 1);
					
					// Get video ID
					$video = reset($videos);
					$id = $video->ID;
					
					
					$video_id = get_post_meta($id, 'cw_video_id', true);
				} else {
					
					// Get video ID -- to-do: change to searching for 'cw_video_id'
					$video_id = get_post_meta($id, 'cw_video_id', true);
				}

				// Get Video Title
				$title = get_the_title($id);
							
				// Get video description
				$description = get_post_meta($id, 'cw_video_description', true);

				//Get Video Thumbnail
				$thumbnail = get_post_meta($id, 'cw_video_thumbnail', true);
				
				// Get video frame size
				$width = $width ? $width : $this->global_width;
				$height = $height ? $height : $this->global_height;
				$width = ( ctype_digit($width) ? $width . 'px' : $width );
				$height = ( ctype_digit($height) ? $height . 'px' : $height );
				$showinfo = $showinfo == "true" ? 1 :  ($showinfo == "false" ? 0 : $this->video_showinfo);
				$rel = $rel == "true" ? 1 : ($rel == "false" ? 0 : $this->video_rel);
				// Capture content
				ob_start();
				if( $video_id != '' ) {
					$video_type = get_post_meta($id, 'cw_video_type', true);
					if($popup == "true") {
						$autoplay = $autoplay == "true"  ? 1 :  ($autoplay == "false"  ? 0 : $this->video_autoplay_popup);
				?>
				
				<a class="cw_video_open_popup_customized" href="javascript:void(0);" video_width="<?php esc_attr_e($width); ?>" video_height="<?php esc_attr_e($height); ?>" video_id="<?php esc_attr_e($video_id); ?>" video_type="<?php echo $video_type; ?>" video_rel="<?php esc_attr_e($rel); ?>" video_showinfo="<?php esc_attr_e($showinfo); ?>" video_autoplay="<?php esc_attr_e($autoplay); ?>">Show Video (PopUp)</a>
				<?php					
					} else {
					$autoplay = $autoplay == "true"  ? 1 :  ($autoplay == "false"  ? 0 : $this->video_autoplay);
				?>
				<div itemtype="http://schema.org/VideoObject" itemscope="" id="video_play" class="<?php echo $this->global_alignment; ?>" style="display:<?php echo $popup == "true" ? "none" : "block"; ?>">
					<meta itemprop="name" content="<?php esc_attr_e($title); ?>" />
					<meta itemprop="thumbnailURL" content="<?php esc_attr_e($thumbnail); ?>" />
					<?php
					
					if($video_type == "youtube") {
					?>
						<meta itemprop="embedURL"     content="http://www.youtube.com/watch?v=<?php esc_attr_e($video_id); ?>" />											
						<iframe class="cw_video_single" src="http://www.youtube.com/embed/<?php esc_attr_e($video_id); ?>?rel=<?php esc_attr_e($rel); ?>&showinfo=<?php esc_attr_e($showinfo); ?>&autoplay=<?php esc_attr_e($autoplay); ?>" frameborder="0" allowfullscreen="allowfullscreen" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>" ></iframe>
					<?php } else if ($video_type == "vimeo") { ?>
						<meta itemprop="embedURL"     content="http://player.vimeo.com/video/<?php esc_attr_e($video_id); ?>" />												
						<iframe src="http://player.vimeo.com/video/<?php esc_attr_e($video_id); ?>?title=<?php echo $showinfo; ?>&byline=<?php esc_attr_e($showinfo); ?>&autoplay=<?php esc_attr_e($autoplay); ?>&portrait=0&color=333" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
					<?php } else { ?>					
						<meta itemprop="embedURL"     content="http://fast.wistia.net/embed/iframe/<?php esc_attr_e($video_id); ?>" />							
						<iframe src="http://fast.wistia.net/embed/iframe/<?php esc_attr_e($video_id); ?>" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="<?php esc_attr_e($width); ?>" height="<?php esc_attr_e($height); ?>"></iframe>
					<?php } ?>

				<?php } ?>
					<?php if( $format == '' or $format != "none") { ?>
					<p itemprop="description" class="cw_video_description">
						<?php echo esc_attr_e($description); ?>
					</p>
					<?php
					}else{ ?>
						<meta itemprop="description"     content="<?php esc_attr_e($description); ?>" />							
				
				<?php } ?>
				</div>
				<?php
				}
				
				// Return captured output
				return ob_get_clean();
			}
			
			// Handles the older 'video-single' shortcode
			function shortcode_video_single_20120628($atts) {
				
				// Determine if URL is not being previewed
				if( !isset($_REQUEST['preview']) ) {
				
					// Add current URL to list of URLs needing updating
					$current_url = $this->url;
					$pull = @unserialize(get_option('cw_video_shortcode_20120628_urls', ''));
					$pull = ( is_array($pull) ? $pull : array() );
					if( !in_array($current_url, $pull) ) {
						$pull[] = $current_url;
						update_option('cw_video_shortcode_20120628_urls', serialize($pull));
					}
					
					// Add special flag parameter
					$atts['cw_attr_shortcode_20120628'] = true;
				}
				
				// Process and return shortcode
				return $this->shortcode_videosingle($atts);
			}
			
			// Pulls videos from a URL
			function get_videos_from_url($url) {
			
				// Get feed
				$feed_url = $this->get_feed_from_url($url);
				if( $feed_url === false ) {
					return array();
				}
				// Return video array
				return $this->get_videos_from_feed($feed_url);
			}
			
			// Retrieves a feed URL from a Youtube URL
			function get_feed_from_url($url) {
				
				// Parse URL
				$url = trim($url);
				$url_request = array();
				$url_parts = parse_url($url);
				if( isset($url_parts['query']) ) {
					parse_str($url_parts['query'], $url_request);
				}
				
				// Check for 'list'
				if( isset($url_request['list']) && strlen($url_request['list']) > 2 && strtoupper(substr($url_request['list'], 0, 2)) == 'PL' ) {
					return 'https://gdata.youtube.com/feeds/api/playlists/' . substr($url_request['list'], 2);
					
				// Check for 'watch'
				} elseif( isset($url_parts['path']) && strtolower(substr($url_parts['path'], 0 - strlen('/watch'))) == '/watch' && isset($url_request['v']) ) {
					return 'https://gdata.youtube.com/feeds/api/videos/' . $url_request['v'] . '?v=2';
				
				// Check for 'user'
				} elseif( isset($url_parts['path']) && stripos($url_parts['path'], '/user/') !== false && strlen($url_parts['path']) >= strlen('/user/1') ) {
					$path_parts = explode('/', $url_parts['path']);
					$user_index = array_search('user', array_map('strtolower', $path_parts));
					return 'https://gdata.youtube.com/feeds/api/users/' . $path_parts[$user_index + 1] . '/uploads';
					
				// Check for 'v'
				} elseif( isset($url_parts['path']) && stripos($url_parts['path'], '/v/') !== false && strlen($url_parts['path']) >= strlen('/v/1') ) {
					$path_parts = explode('/', $url_parts['path']);
					$v_index = array_search('v', array_map('strtolower', $path_parts));
					return 'https://gdata.youtube.com/feeds/api/videos/' . $path_parts[$v_index + 1] . '?v=2';
					
				// Check for 'youtu.be'
				} elseif( isset($url_parts['host']) && stripos($url_parts['host'], 'youtu.be') !== false && isset($url_parts['path']) && strlen($url_parts['path']) > 2 && $url_parts['path']{0} == '/' ) {
					$path_parts = explode('/', $url_parts['path']);
					return 'https://gdata.youtube.com/feeds/api/videos/' . $path_parts[count($path_parts) - 1] . '?v=2';
					
				// Check for video ID
				} elseif( !isset($url_parts['host']) && isset($url_parts['path']) ) {
					return 'https://gdata.youtube.com/feeds/api/videos/' . $url_parts['path'] . '?v=2';
				
				// Not a valid URL
				} else {
					return false;
				}
			}
			
			// Pulls a video array from a feed URL
			function get_videos_from_feed($feed_url) {
				$videos = array();

				// Load feed
				$feed = @file_get_contents($feed_url);
				
				// Load XML
				if( $feed !== false ) {
					$sxml = @simplexml_load_string($feed);
					if( $sxml === false ) {
						return array();
					}
				} else {
					return array();
				}
				
				// Multiple entries
				if( isset($sxml->entry) ) {

					// Watch for XML errors
					try {
						
						// Loop through entries
						foreach( $sxml->entry as $entry ) {
							$video = array();
							
							// Set namespace for media
							$media = $entry->children('http://search.yahoo.com/mrss/');
					
							// Get video thumbnail
							$attrs = $media->group->thumbnail[0]->attributes();
							$video['thumbnail'] = (string) $attrs['url'];
							
							// Get video description
							$video['description'] = (string) $media->group->description;
	
							// Get video player URL
							$watcharray = explode('/', $video['thumbnail']);
								
							// Get the video URL
							$video['ID'] = (string) $watcharray[count($watcharray) - 2];
							$video['URL'] = 'http://www.youtube.com/v/' . $video['ID'];
							
							// Set namespace for yt
							$yt = $media->group->children('http://gdata.youtube.com/schemas/2007');
	
							// Get video duration
							$attrs = $yt->duration->attributes();
							$video['duration'] = (string) $attrs['seconds'];
							
							// Get the title        
							$video['title'] = (string) $entry->title;
							
							// Add video to array
							$videos[] = $video;
						}
						
					// Invalid XML
					} catch( Exception $e ) {
						return array();
					}
				
				// Single entry
				} else {

					// Watch for XML errors
					try {
						
						// Set namespace for media
						$media = $sxml->children('http://search.yahoo.com/mrss/');
				
						// Get video thumbnail
						$attrs = $media->group->thumbnail[0]->attributes();
						$video['thumbnail'] = (string) $attrs['url'];
						
						// Get video description
						$video['description'] = (string) $media->group->description;

						// Get video player URL
						$watcharray = explode('/', $video['thumbnail']);
							
						// Get the video URL
						$video['ID'] = (string) $watcharray[count($watcharray) - 2];
						$video['URL'] = 'http://www.youtube.com/v/' . $video['ID'];
						
						// Set namespace for yt
						$yt = $media->group->children('http://gdata.youtube.com/schemas/2007');

						// Get video duration
						$attrs = $yt->duration->attributes();
						$video['duration'] = (string) $attrs['seconds'];
							
						// Get the title        
						$video['title'] = (string) $sxml->title;
						
						// Add video to array
						$videos[] = $video;
					
					// Invalid XML
					} catch( Exception $e ) {
						return array();
					}
				}

				// Return video array
				return $videos;
			}
			//custom function to update slug name
			function update_slug($_values) {
				global $wpdb;
				//update post slug 
				$post_id = $_values['post_id'];
				$permalink = get_permalink( $_values['post_id'] );
				$update_link = str_replace($_values['original'], $_values['new_slug'], $permalink);
				wp_update_post(array(
					'ID' => $_values['post_id'],
					'guid' => (string) $update_link
				));
				$sql = "UPDATE $wpdb->posts SET guid = '$update_link' WHERE ID=$post_id";
				$result = $wpdb->query($sql);
				echo $result;
			}
			//pulls and previews videos from wistia
			function extract_wistia() {
				$wistia = new WistiaApi(get_option('wistia_secret_key'));			
				$videos = $wistia->mediaList(null, 1, 100, true);			
				if(!$videos) {
					echo  "<div class=\"error\"><p>Encountered an API error.</p><p>Please make sure you have supplied the correct API details.</p></div>";
					exit();
				}
				?>
				<div id="results-video">
					<p class="cw_video_select">
						Select: <a href="#" id="CWVideoSelectAll">All</a> | <a href="#" id="CWVideoSelectNone">None</a>
					</p>
					<?php if(count($videos)): ?>
					<div class="cw_video_list">
						<?php foreach($videos as $video) { ?>
						<div class="cw_video_small">
							<input type="checkbox" id="CWVideoCheckbox_<?php esc_attr_e($video->hashed_id); ?>" name="cw_videos[]" value="<?php esc_attr_e($video->hashed_id); ?>" checked="checked" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->hashed_id); ?>_title" value="<?php esc_attr_e($video->name); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->hashed_id); ?>_thumbnail" value="<?php esc_attr_e($video->thumbnail->url); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->hashed_id); ?>_duration" value="<?php esc_attr_e($video->duration); ?>" />
							<input type="hidden" value="wistia" name="cw_video_<?php esc_attr_e($video->hashed_id); ?>_type" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->hashed_id); ?>_description" value="<?php esc_attr_e(urlencode($video->description)); ?>" />
							<a href="<?php echo get_option('wistia_video_url'); ?>/medias/<?php esc_attr_e($video->hashed_id); ?>" video_id="<?php echo $video->hashed_id; ?>" class="cw_video_open_popup iframe" title="<?php esc_attr_e($video->name); ?>">
								<img src="<?php esc_attr_e($video->thumbnail->url);  ?>" alt="<?php esc_attr_e($video->name); ?>">
								<div class="cw_video_small_title"> <?php esc_html_e($video->name); ?> </div>
							</a>
						</div>
						<?php } ?>
					</div>
					<?php endif; ?>
					</div>
					<?php
			}
			
			// Pulls and previews videos from Vimeo
			function extract_vimeo() {
				$vimeo = new phpVimeo(get_option('consumer_key'), get_option('consumer_secret'));
				//$vimeo->enableCache(phpVimeo::CACHE_FILE, './cache', 300);
				$vimeo->setToken(get_option('access_token'), get_option('access_token_secret'));
				
				try {
					$params = array(
						'user_id' => get_option('vimeo_user_id'),
						'sort' => 'newest',
						'full_response' => 1
					);
					$call_method = 'vimeo.videos.getAll';
					$channel_id = get_option('vimeo_channel_id');
					
					if( !empty($channel_id) ){
						$params['channel_id'] = $channel_id;
						$call_method = 'vimeo.channels.getVideos';
					}
					//print_r($params);
					//echo $channel_id . ' -- ' . $call_method;
					$videos = $vimeo->call($call_method, $params);
					
				} catch (VimeoAPIException $e) {
					$has_error = true;
					$err_message = "<p>Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}</p><p>Please make sure you have supplied the correct API details.</p>";
					echo $videos;
					exit();
				}
				?>
				<div id="results-video">
					<p class="cw_video_select">
						Select: <a href="#" id="CWVideoSelectAll">All</a> | <a href="#" id="CWVideoSelectNone">None</a>
					</p>
					<?php if(count($videos)): ?>
					<div class="cw_video_list">
						<?php foreach($videos->videos->video as $video) { ?>
						<div class="cw_video_small">
							<input type="checkbox" id="CWVideoCheckbox_<?php esc_attr_e($video->id); ?>" name="cw_videos[]" value="<?php esc_attr_e($video->id); ?>" checked="checked" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->id); ?>_title" value="<?php esc_attr_e($video->title); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->id); ?>_thumbnail" value="<?php esc_attr_e($video->thumbnails->thumbnail[0]->_content); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->id); ?>_duration" value="<?php esc_attr_e($video->duration); ?>" />
							<input type="hidden" value="vimeo" name="cw_video_<?php esc_attr_e($video->id); ?>_type" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video->id); ?>_description" value="<?php esc_attr_e(urlencode($video->description)); ?>" />
							<a href="http://vimeo.com/<?php esc_attr_e($video->id); ?>" video_id="<?php echo $video->id; ?>" class="cw_video_open_popup iframe" title="<?php esc_attr_e($video->title); ?>">
								<img src="<?php esc_attr_e($video->thumbnails->thumbnail[0]->_content);  ?>" alt="<?php esc_attr_e($video->title); ?>">
								<div class="cw_video_small_title"> <?php esc_html_e($video->title); ?> </div>
							</a>
						</div>
						<?php } ?>
					</div>
					<?php else: ?>
					<div>No Videos Found.</div>
					<?php endif; ?>
					</div>
					<?php
			}
			
			// Pulls and previews videos from a URL
			function preview_url($url) {
				
				// Get videos from URL
				$videos = $this->get_videos_from_url($url);
				if( count($videos) <= 0 ) {
					
					// No videos found
					header("Status: 404 Not Found");
				} else {
					
					// Output videos in preview
					?>
					<input type="hidden" name="cw_video_source_url" value="<?php esc_attr_e($url); ?>" />
					<p class="cw_video_select">
						Select: <a href="#" id="CWVideoSelectAll">All</a> | <a href="#" id="CWVideoSelectNone">None</a>
					</p>
					<div class="cw_video_list">
					<?php
					
					// Loop for each video
					foreach( $videos as $video ) {
						?>
						<div class="cw_video_small">
							<input type="checkbox" id="CWVideoCheckbox_<?php esc_attr_e($video['ID']); ?>" name="cw_videos[]" value="<?php esc_attr_e($video['ID']); ?>" checked="checked" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video['ID']); ?>_title" value="<?php esc_attr_e($video['title']); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video['ID']); ?>_thumbnail" value="<?php esc_attr_e($video['thumbnail']); ?>" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video['ID']); ?>_duration" value="<?php esc_attr_e($video['duration']); ?>" />
							<input type="hidden" value="youtube" name="cw_video_<?php esc_attr_e($video['ID']); ?>_type" />
							<input type="hidden" name="cw_video_<?php esc_attr_e($video['ID']); ?>_description" value="<?php esc_attr_e(urlencode($video['description'])); ?>" />
							<a href="http://www.youtube.com/v/<?php esc_attr_e($video['ID']); ?>" video_id="<?php echo $video['ID']; ?>" class="cw_video_open_popup iframe" title="<?php esc_attr_e($video['title']); ?>">
								<img src="<?php esc_attr_e($video['thumbnail']); ?>" alt="<?php esc_attr_e($video['title']); ?>">
								<div class="cw_video_small_title"> <?php esc_html_e($video['title']); ?> </div>
							</a>
						</div>
						<?php
					}
					?>
					</div>
					<p class="cw_video_results"><?php esc_html_e(count($videos) . ' video' . ( count($videos) > 1 ? 's' : '' ) . ' found.'); ?></p>
					<?php if( count($videos) > 1 ) { ?>
					<p class="cw_video_settings">
						<input type="checkbox" id="CWVideoAutoPull" name="cw_video_auto_pull" value="1" checked="checked" /><label for="CWVideoAutoPull">Automatically add new videos from this feed</label>
					</p>
					<?php } ?>
					<p class="cw_video_settings">
						<input type="checkbox" id="CWVideoAutoUpdate" name="cw_video_auto_update" value="1" checked="checked" /><label for="CWVideoAutoUpdate">Automatically keep video title<?php esc_html_e(( count($videos) > 1 ? 's' : '' )); ?> and description<?php esc_html_e(( count($videos) > 1 ? 's' : '' )); ?> up-to-date using this feed</label>
					</p>
					<?php
				}
			}
			
			// Updates the title and description of a video post
			function update_video($post_id) {
		
				// Get video ID
				$video_id = get_post_meta($post_id, 'cw_video_id', true);
				
				// Pull latest video data from feed
				$videos = $this->get_videos_from_url('http://www.youtube.com/watch?v=' . $video_id);
				if( count($videos) > 0 && isset($videos[0]['title']) && isset($videos[0]['description']) ) {
				
					// Update Title
					wp_update_post(array(
						'ID' => $post_id,
						'post_title' => (string) $videos[0]['title'],
					));

					// Update Description
					delete_post_meta($post_id, 'cw_video_description');
					add_post_meta($post_id, 'cw_video_description', (string) $videos[0]['description']);
				}
			}
			
			// Updates the title and description of all videos needing updating
			function update_videos() {
				
				// Unhook the 'save_post' action
				remove_action('save_post', array(&$this, 'save_post'));

				// Loop through all videos
				$videos = get_posts(array(
					'post_type' => 'cw_video',
					'numberposts' => -1,
					'post_status' => array('publish', 'draft', 'pending', 'future')
				));
				foreach( $videos as $video_post ) {
					
					// Determine if video needs updating
					if( (int) get_post_meta($video_post->ID, 'cw_video_auto_update', true) > 0 ) {
						// Update video
						$this->update_video($video_post->ID);
					}
					
					// Determine if video has been removed from YouTube
					$video_type = get_post_meta($video_post->ID, 'cw_video_type', true);
					if($video_type == "youtube") {
						if( count($this->get_videos_from_url('http://www.youtube.com/watch?v=' . urlencode(get_post_meta($video_post->ID, 'cw_video_id', true)))) < 1 ) {
							
							// Change video status to 'Pending'
							wp_update_post(array(
								'ID' => $video_post->ID,
								'post_status' => 'pending',
								'post_name' => $video_post->post_name
							));
						}
					}
					// Determine if video has been marked as 'Pending' and is available on YouTube
					if( $video_post->post_status == 'pending' && count($this->get_videos_from_url('http://www.youtube.com/watch?v=' . urlencode(get_post_meta($video_post->ID, 'cw_video_id', true)))) > 0 ) {
						
						// Change video status to 'Published'
						wp_update_post(array(
							'ID' => $video_post->ID,
							'post_status' => 'publish',
							'post_name' => $video_post->post_name
						));
					}
				}
				
				// Re-hook the 'save_post' action
				add_action('save_post', array(&$this, 'save_post'));
			}
			
			// Pulls and adds new videos from Auto-Pull URLs
			function auto_pull() {
				global $wpdb;
				
				// Unhook the 'save_post' action
				remove_action('save_post', array(&$this, 'save_post'));
				
				// Get Auto-Pull URLs
				$video_auto_pull_urls = @unserialize(get_option('cw_video_auto_pull', ''));
				$video_auto_pull_urls = ( !is_array($video_auto_pull_urls) ? array() : $video_auto_pull_urls );
				
				// Get list of all categories
				$all_video_categories = get_terms('cw_video_category', array('fields' => 'ids'));
				
				// Loop for each URL
				foreach( $video_auto_pull_urls as $video_source ) {
					$url = $video_source['source_url'];
					$video_categories = $video_source['video_categories'];
					
					// Get videos from URL
					$videos = $this->get_videos_from_url($url);
					
					// Get categories
					$video_category_ids = array();
					foreach( $video_categories as $video_category ) {
						$video_category_obj = get_term_by('name', $video_category, 'cw_video_category');
						$video_category_ids[] = $video_category_obj->term_id;
					}
					
					// Loop for each video
					foreach( $videos as $video ) {
						
						// Set video search parameters
						$video_search = array(
							'post_type' => 'cw_video',
							'meta_key' => 'cw_video_id',
							'meta_value' => $video['ID'],
							'numberposts' => -1,
							'post_status' => array('publish', 'draft', 'pending', 'future')
						);
						
						// Determine if including any categories
						if( count($video_category_ids) > 0 ) {
							$video_search['tax_query'] = array(
								'taxonomy' => 'cw_video_category',
								'field' => 'id',
								'terms' => implode(',', $video_category_ids)
							);
						} else {
							$video_search['tax_query'] = array(
								'taxonomy' => 'cw_video_category',
								'field' => 'id',
								'terms' => implode(',', $all_video_categories),
								'operator' => 'NOT IN'
							);
						}
						
						// Check to see if video is new
						$existing_videos = get_posts($video_search);
						if( count($existing_videos) < 1 ) {

							// Add video
							$video_post = array(
								'post_content' => '',
								'post_date_gmt' => gmdate('Y-m-d H:i:s'),
								'post_excerpt' => '',
								/*'post_name' => $video['ID'],*/
								'post_parent' => '',
								'post_status' => 'publish',
								'post_title' => $video['title'],
								'post_type' => 'cw_video',
								'tax_input' => array('cw_video_category' => $video_category_ids)
							);
							$video_post_id = wp_insert_post($video_post);
							
							// Add meta
							if( $video_post_id <> 0 ) {
								add_post_meta($video_post_id, 'cw_video_id', $video['ID']);
								add_post_meta($video_post_id, 'cw_video_type', 'youtube');
								add_post_meta($video_post_id, 'cw_video_description', $video['description']);
								add_post_meta($video_post_id, 'cw_video_transcript', '', '');
								add_post_meta($video_post_id, 'cw_video_thumbnail', $video['thumbnail']);
								add_post_meta($video_post_id, 'cw_video_auto_update', '1');
							}
						}
					}
				}
				
				// Re-hook the 'save_post' action
				add_action('save_post', array(&$this, 'save_post'));
			}
			
			// Removes an Auto-Pull URL
			function remove_auto_pull($url) {
				$new_auto_pull_urls = array();
				$auto_pull = @unserialize(get_option('cw_video_auto_pull', ''));
				$auto_pull = ( is_array($auto_pull) ? $auto_pull : array() );
				foreach( $auto_pull as $video_auto_pull_url ) {
					if( $video_auto_pull_url['source_url'] != $url ) {
						$new_auto_pull_urls[] = $video_auto_pull_url;
					}
				}
				update_option('cw_video_auto_pull', serialize($new_auto_pull_urls));
			}
			
			// Outputs a video sitemap in XML format
			function video_sitemap() {
				global $wpdb;
				// Get matching videos
				$videos = get_posts(array(
					'post_type' => 'cw_video',
					'numberposts' => -1,
					'post_status' => 'publish'
				));
				
				/* Added Query by Reykats to get videos embedded in the Post */
				$sql = "SELECT * FROM $wpdb->posts WHERE post_status = 'publish' AND post_content LIKE '%[videosingle%'";
				$query_result = $wpdb->get_results($sql);
				
				if(count($query_result)) {
					$videos = array_merge( (array) $query_result, (array) $videos);
				}
				header('Content-Type: text/xml');
				// Output XML
				echo '<' . '?xml version="1.0" encoding="UTF-8"' . '?' . '>' . "\r\n";
				?>
				<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
					<?php
					$exclude_videos = array();
					// Loop for each video
					foreach( $videos as $video ) {
						$post_video_id = 0;
						if($video->post_type != "cw_video") {
							$post_content = $video->post_content;
							if (preg_match('/\[videosingle (.*?)]/', $post_content, $matches)) {
								$match = $matches[0];
								//get video ID from shortcode string
								if(preg_match('/\id="*([0-9]+)"/', $match, $id)) {
									//clean up output
									$post_video_id = str_replace(array('id="', '"'), "", $id[0]);
								}
							}
							if(!$post_video_id) continue;							
						}
						// Get video information
						$video_id = $post_video_id ?  get_post_meta($post_video_id, 'cw_video_id', true) : get_post_meta($video->ID, 'cw_video_id', true);
						if(in_array($video_id, $exclude_videos))
							continue;
						$exclude_videos[] = $video_id;
						$permalink = get_permalink($video->ID);
						$video_type = $post_video_id ? get_post_meta($post_video_id, 'cw_video_type', true) : get_post_meta($video->ID, 'cw_video_type', true);
						$title = $post_video_id ? get_the_title($post_video_id) : $video->post_title;
						$description = $post_video_id ? get_post_meta($post_video_id, 'cw_video_description', true) : get_post_meta($video->ID, 'cw_video_description', true);
						$thumbnail = $post_video_id ? get_post_meta($post_video_id, 'cw_video_thumbnail', true) :get_post_meta($video->ID, 'cw_video_thumbnail', true);
						$duration = $post_video_id ? get_post_meta($post_video_id, 'cw_video_duration', true) :get_post_meta($video->ID, 'cw_video_duration', true);
						$video_url = 'http://www.youtube.com/v/';
						if($video_type == "vimeo") {
							$video_url = 'http://vimeo.com/';
						} else if($video_type == "wistia") {
							$video_url = get_option('wistia_video_url') . '/medias/';
						}
						$tags =  $post_video_id ? wp_get_post_tags( $post_video_id ) : wp_get_post_tags($video->ID);
						if($video->post_type == "cw_video") {
							$categories = wp_get_post_terms($video->ID, 'cw_video_category', array("fields" => "all"));
						} else if($video->post_type == "post") {
							$categories = get_the_category($video->ID);
						} else {
							$categories = wp_get_post_terms($post_video_id, 'cw_video_category', array("fields" => "all"));
						}
					/* 	if($video->post_type == "post") {
							$categories = get_the_category($video->ID);
						} else if($video->post_type == "page") {
							$categories = wp_get_post_terms($post_video_id, 'cw_video_category', array("fields" => "all"));
						} else {
							//$categories = wp_get_post_categories($video->ID);
							$categories = wp_get_post_terms($video->ID, 'cw_video_category', array("fields" => "all"));
						} */
						?>
						<url> 
							<loc><?php esc_html_e($permalink); ?></loc>
							<video:video>
								<video:thumbnail_loc><?php esc_html_e($thumbnail); ?></video:thumbnail_loc> 
								<video:title><?php esc_html_e($title); ?></video:title>
								<video:description><?php esc_html_e($description); ?></video:description>
								<video:player_loc allow_embed="yes" autoplay="ap=1"><?php echo $video_url; ?><?php esc_html_e(urlencode($video_id)); ?></video:player_loc>
								<?php if( $duration != '' ) { ?><video:duration><?php esc_html_e($duration); ?></video:duration><?php } ?>
								<?php foreach($tags as $tag): ?>
								<video:tag><?php echo $tag->name; ?></video:tag>
								<?php endforeach; ?>
								<?php foreach($categories as $category): ?>
								<?php// $cat = get_category( $category ); ?>
								<video:category><?php esc_html_e($category->name); ?></video:category>
								<video:family_friendly>Yes</video:family_friendly>
								<?php endforeach; ?>
							</video:video> 
					   </url>
					   <?php
					}
					?>
				</urlset>
				<?php
			}
			
			// Outputs a sitemap index
			function sitemap_index() {
				header('Content-Type: text/xml');
				// Output XML
				echo '<' . '?xml version="1.0" encoding="UTF-8"' . '?' . '>' . "\r\n";
				?>
				<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
					<sitemap>
						<loc><?php bloginfo('url'); ?>/sitemap.xml</loc>
						<lastmod><?php esc_html_e(date('Y-m-d')); ?></lastmod>
					</sitemap>
					<sitemap>
						<loc><?php bloginfo('url'); ?>/video-sitemap.xml</loc>
						<lastmod><?php esc_html_e(date('Y-m-d')); ?></lastmod>
					</sitemap>
				</sitemapindex>
				<?php
			}
			
			// Retrieves a request variable or returns a default value if it doesn't exist
			function get_request($name, $default) {
				return ( isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default );
			}
			
			// Displays messages to the user
			function show_messages() {
				$warnings = 0;
				$notices = 0;
				
				// Determine if user has admin rights
				if( !current_user_can('edit_plugins') ) {
					return;
				}
				
				// Output any warning messages
				if( count($this->warnings) > 0 ) {
				?>
				<div id="CWVideoWarnings" class="error"><?php
					foreach( $this->warnings as $message ) {
						$warnings++;
						?>
						<p><?php
						
						// Output raw message (can contain HTML)
						echo $message;
						?></p>
						<?php
						if( $warnings >= 5 ) {
							?>
							<p>...</p>
							<?php
							break;
						}
					}
				?></div>
				<?php
				}
				
				// Output any notification messages
				if( count($this->notices) > 0 ) {
				?>
				<div id="CWVideoNotices" class="updated"><?php
					foreach( $this->notices as $message ) {
						$notices++;
						?>
						<p><?php
						
						// Output raw message (can contain HTML)
						echo $message;
						?></p>
						<?php
						if( $notices >= 5 ) {
							?>
							<p>...</p>
							<?php
							break;
						}
					}
				?></div>
				<?php
				}
			}
			
			// Upgrades the existing schema to the current version
			function upgrade_schema($existing_schema) {
				
				// Upgrade to 20120611 schema
				if( (int) $existing_schema < 20120611 ) {
					$auto_pull_new = array();
					$auto_pull_old = trim(get_option('cw_video_auto_pull', ''));
					$video_auto_pull_urls = ( $auto_pull_old != '' ? explode('|', $auto_pull_old) : array() );
					foreach( $video_auto_pull_urls as $video_auto_pull_url ) {
						$auto_pull_new[] = array(
							'source_url' => $video_auto_pull_url,
							'video_categories' => array()
						);
					}
					update_option('cw_video_auto_pull', serialize($auto_pull_new));
					update_option('cw_video_schema', $this->schema);
					$this->cw_ping(array(
						'pingname' => 'cw_video_schema_upgrade',
						'sitename' => get_bloginfo('name'),
						'siteurl' => get_bloginfo('url'),
						'schema_version' => $this->schema,
						'plugin_version' => $this->version
					));
				}
				
				// Upgrade to 20120628 schema
				if( (int) $existing_schema < 20120628 ) {
					
					// Add flag to notify of shortcode changes
					add_option('cw_video_shortcode_20120628_flag', true);
					
					// Finalize schema upgrade
					update_option('cw_video_schema', $this->schema);
					$this->cw_ping(array(
						'pingname' => 'cw_video_schema_upgrade',
						'sitename' => get_bloginfo('name'),
						'siteurl' => get_bloginfo('url'),
						'schema_version' => $this->schema,
						'plugin_version' => $this->version
					));
				}
			}
			
			// Sends data to CW
			function cw_ping($data) {
				if( file_get_contents('http://cw-apps.com/cw-ping/?ping=' . urlencode(serialize($data))) === false ) {
					return false;
				}
				return true;
			}
		}
		
		// Define video widget gallery shortcode class
		class CWVideoWidgetGalleryShortcode extends WP_Widget {
			public $title = "Video Gallery Shortcode";
			public $desc = "Outputs a video gallery based on the videogallery shortcode";
			
			// Constructs the object
			function CWVideoWidgetGalleryShortcode() {
				$this->WP_Widget(get_class($this), __($this->title), array('description' => __($this->desc)));
			}
		
			// Outputs the widget's content
			function widget($args, $instance) {
				
				// Pull widget arguments
				extract($args);
				
				// Output 'before_widget' markup
				echo $before_widget;
				
				// Output widget title
				$title = apply_filters('widget_title', ( empty($instance['title']) ? __($this->title) : $instance['title'] ));
				if( $title != '' ) {
					echo $before_title . $title . $after_title;
				}

				// Output widget content
				echo do_shortcode('[videogallery ' . $instance['shortcode_params'] . ' /]');
				
				// Output 'after_widget' markup
				echo $after_widget;
				
				// Flag widget output
				$GLOBALS['cw_video_widget_gallery_shortcode'] = true;
			}
			
			// Updates the widget's settings
			function update($new_instance, $old_instance) {
				$instance = $old_instance;
				$instance['title'] = strip_tags($new_instance['title']);
				$instance['shortcode_params'] = strip_tags($new_instance['shortcode_params']);
				return $instance;
			}
			
			// Outputs the widget's settings
			function form($instance) {
				$instance = wp_parse_args((array) $instance, array( 
					'title' => 'Our Videos',
					'shortcode_params' => 'category="" type="list" format="li"'
				));	
				?>  
				<p><?php esc_html_e($this->desc); ?></p>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php esc_attr_e($instance['title']); ?>" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('shortcode_params'); ?>"><?php _e('Shortcode Parameters:'); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id('shortcode_params'); ?>" name="<?php echo $this->get_field_name('shortcode_params'); ?>" type="text" value="<?php esc_attr_e($instance['shortcode_params']); ?>" />
				</p>
				<?php
			}
		}
	}
	
	// Initialize plugin
	new CWVideoLibrary();
