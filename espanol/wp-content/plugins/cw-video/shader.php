<?php

    /*
        --- Outputs a shaded background image ---
    */
    
	// Get parameters
    $shade = ( isset($_REQUEST['shade']) && $_REQUEST['shade'] == 'black' ? 'black' : 'white' );
    $percent = ( isset($_REQUEST['percent']) ? intval($_REQUEST['percent']) : 50 );
    $transparency = 127 - floor($percent * 127 / 100);
    
	// Create alpha-based image
    $im = imagecreatetruecolor(20, 20);
    imagesavealpha($im, true);
    
	// Fill image with color
    $color = ( $shade == 'white' ? imagecolorallocatealpha($im, 255, 255, 255, $transparency) : imagecolorallocatealpha($im, 0, 0, 0, $transparency) );
    imagefill($im, 0, 0, $color);
	
	// Ouput PNG
    header('Content-Type: image/png');
	imagepng($im);
	imagedestroy($im);